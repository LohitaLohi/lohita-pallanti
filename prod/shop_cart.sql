-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 28, 2018 at 07:44 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shop_cart`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_no` int(10) NOT NULL,
  `admin_id` varchar(30) NOT NULL,
  `admin_name` varchar(30) NOT NULL,
  `emp_designation` varchar(50) NOT NULL,
  `team_name` varchar(30) DEFAULT NULL,
  `admin_type` varchar(20) NOT NULL,
  `admin_phone` varchar(14) DEFAULT NULL,
  `admin_city` varchar(40) DEFAULT NULL,
  `date_of_joining` date NOT NULL,
  `admin_img` text,
  `admin_password` varchar(50) DEFAULT NULL,
  `father_name` varchar(50) NOT NULL,
  `blood_group` varchar(50) NOT NULL,
  `gender` varchar(6) NOT NULL,
  `dob` date NOT NULL,
  `personal_phone` varchar(50) NOT NULL,
  `address` text,
  `admin_created_dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` varchar(80) NOT NULL,
  `update_date` datetime NOT NULL,
  `admin_status` varchar(20) NOT NULL,
  `admin_type_id` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_no`, `admin_id`, `admin_name`, `emp_designation`, `team_name`, `admin_type`, `admin_phone`, `admin_city`, `date_of_joining`, `admin_img`, `admin_password`, `father_name`, `blood_group`, `gender`, `dob`, `personal_phone`, `address`, `admin_created_dt`, `updated_by`, `update_date`, `admin_status`, `admin_type_id`) VALUES
(89, 'admin', 'lohita', '', NULL, 'admin', '92221827', '', '2018-10-28', 'emp_img.png', 'admin@123', '', '', 'female', '1996-09-30', '', '', '2018-10-26 09:55:26', 'admin', '0000-00-00 00:00:00', 'active', 1);

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE `brand` (
  `brand_sno` int(2) NOT NULL,
  `brand_name` varchar(100) NOT NULL,
  `brand_img` text NOT NULL,
  `brand_created_date` datetime NOT NULL,
  `brand_updated_date` datetime NOT NULL,
  `brand_created_by` varchar(100) NOT NULL,
  `brand_updated_by` varchar(100) NOT NULL,
  `brand_status` int(1) NOT NULL COMMENT '1=>active,0=>inactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`brand_sno`, `brand_name`, `brand_img`, `brand_created_date`, `brand_updated_date`, `brand_created_by`, `brand_updated_by`, `brand_status`) VALUES
(21, 'Zara', '1540655865_zara_zara logo.jpg', '2018-10-27 21:27:45', '0000-00-00 00:00:00', '89', '', 1),
(22, 'kalamandir', '1540656055_kalamandir_kalamandir.jpeg', '2018-10-27 21:30:55', '0000-00-00 00:00:00', '89', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `sno` int(10) NOT NULL,
  `product_id` int(10) NOT NULL,
  `quanity` int(5) NOT NULL,
  `date` datetime NOT NULL,
  `order_id` int(10) NOT NULL,
  `size_id` int(5) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`sno`, `product_id`, `quanity`, `date`, `order_id`, `size_id`) VALUES
(466, 92, 2, '2018-10-26 14:52:15', 1540492200, NULL),
(457, 95, 3, '2018-10-25 21:12:03', 15, NULL),
(465, 93, 1, '2018-10-26 00:00:00', 1540492200, NULL),
(463, 102, 1, '2018-10-26 00:00:00', 20, NULL),
(464, 100, 7, '2018-10-26 14:46:57', 20, NULL),
(459, 92, 4, '2018-10-26 07:28:51', 15, NULL),
(443, 91, 1, '2018-10-22 00:00:00', 1540146600, NULL),
(467, 92, 1, '2018-10-27 00:00:00', 1540578600, NULL),
(468, 9, 4, '2018-10-28 00:00:00', 20, NULL),
(469, 23, 1, '2018-10-28 20:59:19', 20, NULL),
(470, 23, 1, '2018-10-28 00:00:00', 35, NULL),
(471, 9, 7, '2018-10-28 17:41:18', 35, NULL),
(472, 18, 1, '2018-10-28 18:14:00', 35, NULL),
(473, 15, 3, '2018-10-28 18:50:03', 35, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `cat_sno` int(10) NOT NULL,
  `cat_name` varchar(100) NOT NULL,
  `menu_id` int(2) NOT NULL,
  `cat_created_date` datetime NOT NULL,
  `cat_updated_date` datetime NOT NULL,
  `cat_created_by` varchar(100) NOT NULL,
  `cat_updated_by` varchar(100) NOT NULL,
  `cat_position_no` int(2) NOT NULL,
  `cat_status` int(2) NOT NULL COMMENT '1=>active,0=>inactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`cat_sno`, `cat_name`, `menu_id`, `cat_created_date`, `cat_updated_date`, `cat_created_by`, `cat_updated_by`, `cat_position_no`, `cat_status`) VALUES
(1, 'women-clothes', 48, '2018-10-27 21:20:13', '0000-00-00 00:00:00', '89', '', 1, 1),
(2, 'western-clothes', 48, '2018-10-27 21:21:06', '0000-00-00 00:00:00', '89', '', 2, 1),
(3, 'men', 48, '2018-10-27 21:22:26', '0000-00-00 00:00:00', '89', '', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `checkout_address`
--

CREATE TABLE `checkout_address` (
  `con_no` int(20) NOT NULL,
  `confirm_pincode_name` varchar(120) NOT NULL,
  `confirm_name` varchar(120) NOT NULL,
  `confirm_address` varchar(120) NOT NULL,
  `confirm_landmark` varchar(120) NOT NULL,
  `confirm_city` varchar(120) NOT NULL,
  `confirm_state` varchar(120) NOT NULL,
  `confirm_mobile_number` varchar(120) NOT NULL,
  `confirm_address_type` varchar(120) NOT NULL,
  `address` int(120) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `checkout_address`
--

INSERT INTO `checkout_address` (`con_no`, `confirm_pincode_name`, `confirm_name`, `confirm_address`, `confirm_landmark`, `confirm_city`, `confirm_state`, `confirm_mobile_number`, `confirm_address_type`, `address`, `user_id`) VALUES
(34, '4327', 'Lohita', '    hanavein 15          ', 'rogaland', 'sandnes', 'rogaland', '92221827', 'home', 0, 36);

-- --------------------------------------------------------

--
-- Table structure for table `forgot_pass`
--

CREATE TABLE `forgot_pass` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `random` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `menu_sno` int(2) NOT NULL,
  `menu_name` varchar(100) NOT NULL,
  `menu_created_date` datetime NOT NULL,
  `menu_updated_date` datetime NOT NULL,
  `menu_created_by` varchar(100) NOT NULL,
  `menu_updated_by` varchar(100) NOT NULL,
  `menu_position_no` int(2) NOT NULL,
  `menu_status` int(2) NOT NULL COMMENT '1=>active,0=>inactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`menu_sno`, `menu_name`, `menu_created_date`, `menu_updated_date`, `menu_created_by`, `menu_updated_by`, `menu_position_no`, `menu_status`) VALUES
(48, 'clothes', '2018-10-05 22:40:18', '2018-10-27 21:19:35', '10', '89', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_no` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `shipping_address_sno` int(11) NOT NULL,
  `grand_total` float NOT NULL,
  `shipping_status` int(2) NOT NULL COMMENT '0=>Empty,1=>Dispatched,2=>Delivered',
  `shipping_cost` int(11) NOT NULL,
  `delivery_boy_id` int(11) NOT NULL,
  `order_status` int(2) NOT NULL COMMENT '0=>Order Placed,1=>Processing,2=>Completed,3=>Cancelled',
  `payment_type` int(2) NOT NULL COMMENT '1=>Pending,2=>Completed',
  `order_date_time` datetime NOT NULL,
  `order_updated` datetime NOT NULL,
  `shipping_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_no`, `user_id`, `shipping_address_sno`, `grand_total`, `shipping_status`, `shipping_cost`, `delivery_boy_id`, `order_status`, `payment_type`, `order_date_time`, `order_updated`, `shipping_date`) VALUES
(1540749143, 36, 0, 1000, 0, 0, 0, 0, 1, '2018-10-28 18:52:23', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1540749285, 36, 0, 1000, 0, 0, 0, 0, 1, '2018-10-28 18:54:45', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1540749452, 36, 0, 1000, 0, 0, 0, 0, 1, '2018-10-28 18:57:32', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1540749720, 36, 0, 1000, 0, 0, 0, 0, 1, '2018-10-28 19:02:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1540749883, 36, 0, 1500, 0, 0, 0, 0, 1, '2018-10-28 19:04:43', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1540750377, 36, 0, 2000, 0, 0, 0, 0, 1, '2018-10-28 19:12:57', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `order_products`
--

CREATE TABLE `order_products` (
  `sno` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `order_no` varchar(50) NOT NULL,
  `product_cost` float NOT NULL,
  `product_final_cost` float NOT NULL,
  `product_quanity` int(11) NOT NULL,
  `date_time` datetime NOT NULL,
  `prod_size` varchar(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `prod_type` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_products`
--

INSERT INTO `order_products` (`sno`, `product_id`, `order_no`, `product_cost`, `product_final_cost`, `product_quanity`, `date_time`, `prod_size`, `user_id`, `prod_type`) VALUES
(1502703037, 14, '1540749883', 500, 0, 2, '2018-10-28 19:04:43', NULL, 36, ''),
(1502703038, 15, '1540749883', 500, 0, 1, '2018-10-28 19:04:43', NULL, 36, ''),
(1502703039, 10, '1540750377', 500, 0, 4, '2018-10-28 19:12:57', NULL, 36, '');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `prod_sno` int(11) NOT NULL,
  `prod_name` varchar(100) NOT NULL,
  `prod_tags` text NOT NULL,
  `prod_sub_cat_sno` int(11) NOT NULL,
  `prod_price` float NOT NULL,
  `prod_discount` float NOT NULL,
  `prod_dis_price` float NOT NULL,
  `prod_stock` text NOT NULL,
  `prod_stock_rem` int(11) NOT NULL,
  `today_deals_status` int(1) NOT NULL COMMENT '1=>active,0=>inactive',
  `prod_des` text NOT NULL,
  `prod_status` int(2) NOT NULL COMMENT '1=>active,0=>inactive',
  `type` varchar(2) NOT NULL,
  `prod_created_date` datetime NOT NULL,
  `prod_updated_date` datetime NOT NULL,
  `prod_created_by` varchar(100) NOT NULL,
  `prod_updated_by` varchar(100) NOT NULL,
  `origin_of_country` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`prod_sno`, `prod_name`, `prod_tags`, `prod_sub_cat_sno`, `prod_price`, `prod_discount`, `prod_dis_price`, `prod_stock`, `prod_stock_rem`, `today_deals_status`, `prod_des`, `prod_status`, `type`, `prod_created_date`, `prod_updated_date`, `prod_created_by`, `prod_updated_by`, `origin_of_country`) VALUES
(9, 'skirt', 'skirts', 57, 100, 100, 100, '100', 100, 0, '<h1 class=\"_9E25nV\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; color: rgb(33, 33, 33); font-size: 18px; font-family: Roboto, Arial, sans-serif; letter-spacing: normal;\"><span class=\"_35KyD6\" style=\"margin: 0px; padding: 0px; line-height: 1.4; font-size: inherit; font-weight: inherit; display: inline-block;\">Fabcartz Embroidered Women Flared Dark Blue Skirt</span></h1>', 1, '', '2018-10-28 09:37:45', '0000-00-00 00:00:00', '89', '', 'Europe'),
(10, 'Saree', 'Saree', 60, 500, 500, 500, '200', 200, 0, '<h1 class=\"pdp-name\" style=\"box-sizing: inherit; font-size: 20px; margin-top: 0px; margin-bottom: 0px; color: rgb(83, 86, 101); padding: 5px 20px 14px 0px; opacity: 0.8; border-bottom: 1px solid rgb(212, 213, 217); font-family: Whitney; letter-spacing: normal;\">Burgundy Solid Ruffle Saree</h1> ', 1, '', '2018-10-28 12:58:12', '0000-00-00 00:00:00', '89', '', 'Europe'),
(11, 'Sleeves Stylish Casual Shirt', ' Sleeves Stylish Casual Shirt', 61, 500, 500, 500, '500', 500, 0, '<div id=\"featurebullets_feature_div\" class=\"feature\" data-feature-name=\"featurebullets\" data-cel-widget=\"featurebullets_feature_div\" style=\"color: rgb(17, 17, 17); font-family: \"Amazon Ember\", Arial, sans-serif;\"><div id=\"feature-bullets\" class=\"a-section a-spacing-medium a-spacing-top-small\" style=\"margin-bottom: 0px; margin-top: 10px !important;\"><ul class=\"a-unordered-list a-vertical a-spacing-none\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; color: rgb(148, 148, 148); padding: 0px;\"><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Style & Fit: Blue -Cotton Printed-Double Pocket-Long Sleeve-Slim Fit-Casual Shirt</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Material: Cotton with Casual Rich Look</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Best for: Casual wear, regular wear, evening wear, party wear & smart casual wear for any occasion</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Material Care: Regular wash with cold water</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">DISCLAIMER: 1) Product color may slightly vary due to photographic lighting sources or your monitor settings. 2) As this is a slim fit shirt, it is highly recommended that you take ONE size BIGGER than your regular size. Ex: If you usually wear M size, then buy L size shirt here.</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><div><br></div></li></ul></div></div><div id=\"andonCord_feature_div\" class=\"feature\" data-feature-name=\"andonCord\" data-cel-widget=\"andonCord_feature_div\" style=\"color: rgb(17, 17, 17); font-family: \"Amazon Ember\", Arial, sans-serif;\"></div><div id=\"edpIngress_feature_div\" class=\"feature\" data-feature-name=\"edpIngress\" data-cel-widget=\"edpIngress_feature_div\" style=\"color: rgb(17, 17, 17); font-family: \"Amazon Ember\", Arial, sans-serif;\"></div><div id=\"heroQuickPromo_feature_div\" class=\"feature\" data-feature-name=\"heroQuickPromo\" data-cel-widget=\"heroQuickPromo_feature_div\" style=\"color: rgb(17, 17, 17); font-family: \"Amazon Ember\", Arial, sans-serif;\"><div id=\"hero-quick-promo-grid_feature_div\"><div id=\"hero-quick-promo\" class=\"a-row a-spacing-medium\" style=\"width: 487.219px; margin-bottom: 0px !important;\"></div></div></div> ', 1, '', '2018-10-28 13:00:46', '0000-00-00 00:00:00', '89', '', 'Europe'),
(12, 'Harpa Womens A-Line Dress', 'Harpa Womens A-Line Dress ', 60, 500, 500, 500, '600', 600, 0, '<div id=\"featurebullets_feature_div\" class=\"feature\" data-feature-name=\"featurebullets\" data-cel-widget=\"featurebullets_feature_div\" style=\"color: rgb(17, 17, 17); font-family: \"Amazon Ember\", Arial, sans-serif;\"><div id=\"feature-bullets\" class=\"a-section a-spacing-medium a-spacing-top-small\" style=\"margin-bottom: 0px; margin-top: 10px !important;\"><ul class=\"a-unordered-list a-vertical a-spacing-none\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; color: rgb(148, 148, 148); padding: 0px;\"><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">100% Polyester</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Hand wash in cold water, dry in shade for lasting color</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">A-Line</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Sleeveless</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Maxi</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><div><br></div></li></ul></div></div><div id=\"andonCord_feature_div\" class=\"feature\" data-feature-name=\"andonCord\" data-cel-widget=\"andonCord_feature_div\" style=\"color: rgb(17, 17, 17); font-family: \"Amazon Ember\", Arial, sans-serif;\"></div><div id=\"edpIngress_feature_div\" class=\"feature\" data-feature-name=\"edpIngress\" data-cel-widget=\"edpIngress_feature_div\" style=\"color: rgb(17, 17, 17); font-family: \"Amazon Ember\", Arial, sans-serif;\"></div><div id=\"heroQuickPromo_feature_div\" class=\"feature\" data-feature-name=\"heroQuickPromo\" data-cel-widget=\"heroQuickPromo_feature_div\" style=\"color: rgb(17, 17, 17); font-family: \"Amazon Ember\", Arial, sans-serif;\"><div id=\"hero-quick-promo-grid_feature_div\"><div id=\"hero-quick-promo\" class=\"a-row a-spacing-medium\" style=\"width: 679.104px; margin-bottom: 0px !important;\"></div></div></div> ', 1, '', '2018-10-28 13:02:56', '0000-00-00 00:00:00', '89', '', 'Europe'),
(13, 'Cherokee by Unlimited Girl Regular Fit Cotton Skirt', 'Cherokee by Unlimited Girls Regular Fit Cotton Skirt', 57, 500, 500, 500, '200', 200, 0, '<h1 id=\"title\" class=\"a-size-large a-spacing-none\" style=\"padding: 0px; margin-top: 0px; text-rendering: optimizeLegibility; color: rgb(17, 17, 17); font-family: \"Amazon Ember\", Arial, sans-serif; letter-spacing: normal; margin-bottom: 0px !important; font-size: 21px !important; line-height: 1.3 !important;\"><span id=\"productTitle\" class=\"a-size-large\" style=\"text-rendering: optimizeLegibility; font-size: 19px !important; line-height: 1.3 !important;\">Cherokee by Unlimited Girls\' Regular Fit Cotton Skirt</span></h1> ', 1, '', '2018-10-28 13:04:32', '0000-00-00 00:00:00', '89', '', 'Europe'),
(14, 'Traditional Anarkali Salwar Suit Dress Materials', 'Traditional Anarkali Salwar Suit Dress Materials', 60, 500, 500, 500, '100', 100, 0, '<h1 id=\"title\" class=\"a-size-large a-spacing-none\" style=\"padding: 0px; margin-top: 0px; text-rendering: optimizeLegibility; color: rgb(17, 17, 17); font-family: \"Amazon Ember\", Arial, sans-serif; letter-spacing: normal; margin-bottom: 0px !important; font-size: 21px !important; line-height: 1.3 !important;\"><span id=\"productTitle\" class=\"a-size-large\" style=\"text-rendering: optimizeLegibility; font-size: 19px !important; line-height: 1.3 !important;\">Traditional Anarkali Salwar Suit Dress Materials</span></h1> ', 1, '', '2018-10-28 13:06:04', '0000-00-00 00:00:00', '89', '', 'Europe'),
(15, 'Fashion Freak Mens Cotton Half Sleeve Polo T-Shirt', 'Fashion Freak Mens Cotton Half Sleeve Polo T-Shirt', 58, 500, 500, 500, '600', 600, 0, '<h1 id=\"title\" class=\"a-size-large a-spacing-none\" style=\"padding: 0px; margin-top: 0px; text-rendering: optimizeLegibility; color: rgb(17, 17, 17); font-family: \"Amazon Ember\", Arial, sans-serif; letter-spacing: normal; margin-bottom: 0px !important; font-size: 21px !important; line-height: 1.3 !important;\"><span id=\"productTitle\" class=\"a-size-large\" style=\"text-rendering: optimizeLegibility; font-size: 19px !important; line-height: 1.3 !important;\">Fashion Freak Men\'s Cotton Half Sleeve Polo T-Shirt</span></h1> ', 1, '', '2018-10-28 13:08:09', '0000-00-00 00:00:00', '89', '', '300'),
(16, 'Mens Skinny Fit Jeans', 'Mens Skinny Fit Jeans', 59, 600, 600, 600, '200', 200, 0, '<h1 id=\"title\" class=\"a-size-large a-spacing-none\" style=\"padding: 0px; margin-top: 0px; text-rendering: optimizeLegibility; color: rgb(17, 17, 17); font-family: \"Amazon Ember\", Arial, sans-serif; letter-spacing: normal; margin-bottom: 0px !important; font-size: 21px !important; line-height: 1.3 !important;\"><span id=\"productTitle\" class=\"a-size-large\" style=\"text-rendering: optimizeLegibility; font-size: 19px !important; line-height: 1.3 !important;\">Men\'s Skinny Fit Jeans</span></h1> ', 1, '', '2018-10-28 13:09:51', '0000-00-00 00:00:00', '89', '', 'Europe'),
(17, 'Red Tape Mens Skinny Fit Jeans', 'Red Tape Mens Skinny Fit Jeans', 59, 1000, 1000, 1000, '100', 100, 0, '<h1 id=\"title\" class=\"a-size-large a-spacing-none\" style=\"padding: 0px; margin-top: 0px; text-rendering: optimizeLegibility; color: rgb(17, 17, 17); font-family: \"Amazon Ember\", Arial, sans-serif; letter-spacing: normal; margin-bottom: 0px !important; font-size: 21px !important; line-height: 1.3 !important;\"><span id=\"productTitle\" class=\"a-size-large\" style=\"text-rendering: optimizeLegibility; font-size: 19px !important; line-height: 1.3 !important;\">Red Tape Men\'s Skinny Fit Jeans</span></h1> ', 1, '', '2018-10-28 13:11:45', '0000-00-00 00:00:00', '89', '', 'Europe'),
(18, 'Diverse Mens Printed Slim Fit Casual Shirt', 'Diverse Men\'s Printed Slim Fit Casual Shirt', 61, 500, 500, 500, '100', 100, 0, '<h1 id=\"title\" class=\"a-size-large a-spacing-none\" style=\"padding: 0px; margin-top: 0px; text-rendering: optimizeLegibility; color: rgb(17, 17, 17); font-family: \"Amazon Ember\", Arial, sans-serif; letter-spacing: normal; margin-bottom: 0px !important; font-size: 21px !important; line-height: 1.3 !important;\"><span id=\"productTitle\" class=\"a-size-large\" style=\"text-rendering: optimizeLegibility; font-size: 19px !important; line-height: 1.3 !important;\">Diverse Men\'s Printed Slim Fit Casual Shirt</span></h1> ', 1, '', '2018-10-28 13:13:25', '0000-00-00 00:00:00', '89', '', 'Europe'),
(19, 'Maniac Men\'s Cotton T-Shirt', 'Maniac Men\'s Cotton T-Shirt ', 58, 500, 500, 500, '100', 100, 0, '<h1 id=\"title\" class=\"a-size-large a-spacing-none\" style=\"padding: 0px; margin-top: 0px; text-rendering: optimizeLegibility; color: rgb(17, 17, 17); font-family: \"Amazon Ember\", Arial, sans-serif; letter-spacing: normal; margin-bottom: 0px !important; font-size: 21px !important; line-height: 1.3 !important;\"><span id=\"productTitle\" class=\"a-size-large\" style=\"text-rendering: optimizeLegibility; font-size: 19px !important; line-height: 1.3 !important;\">Maniac Men\'s Cotton T-Shirt </span></h1> ', 1, '', '2018-10-28 13:14:51', '0000-00-00 00:00:00', '89', '', 'Europe'),
(20, 'Newport by Unlimited Women\'s Skinny Fit shirts', 'Newport by Unlimited Women\'s Skinny Fit shirts', 61, 0, 0, 500, '100', 100, 0, '<h1 id=\"title\" class=\"a-size-large a-spacing-none\" style=\"padding: 0px; margin-top: 0px; text-rendering: optimizeLegibility; color: rgb(17, 17, 17); font-family: \" amazon=\"\" ember\",=\"\" arial,=\"\" sans-serif;=\"\" letter-spacing:=\"\" normal;=\"\" margin-bottom:=\"\" 0px=\"\" !important;=\"\" font-size:=\"\" 21px=\"\" line-height:=\"\" 1.3=\"\" !important;\"=\"\"><span id=\"productTitle\" class=\"a-size-large\" style=\"text-rendering: optimizeLegibility; font-size: 19px !important; line-height: 1.3 !important;\">Newport by Unlimited Women\'s Skinny Fit Jeans</span></h1> ', 1, '', '2018-10-28 13:16:08', '2018-10-28 14:15:37', '89', '89', 'Europe'),
(21, 'Lee Mens Skinny Fit shirts', 'Lee Mens Skinny Fit shirts', 61, 0, 0, 400, '100', 100, 0, '<h1 id=\"title\" class=\"a-size-large a-spacing-none\" style=\"padding: 0px; margin-top: 0px; text-rendering: optimizeLegibility; color: rgb(17, 17, 17); font-family: \" amazon=\"\" ember\",=\"\" arial,=\"\" sans-serif;=\"\" letter-spacing:=\"\" normal;=\"\" margin-bottom:=\"\" 0px=\"\" !important;=\"\" font-size:=\"\" 21px=\"\" line-height:=\"\" 1.3=\"\" !important;\"=\"\"><span id=\"productTitle\" class=\"a-size-large\" style=\"text-rendering: optimizeLegibility; font-size: 19px !important; line-height: 1.3 !important;\">Lee Men\'s Skinny Fit shirts</span></h1> ', 1, '', '2018-10-28 13:18:40', '2018-10-28 18:24:29', '89', '89', 'Europe'),
(22, 'Generic Womens Silk Dress Material', 'Generic Womens Silk Dress Materia', 60, 100, 100, 100, '100', 100, 0, '<h1 id=\"title\" class=\"a-size-large a-spacing-none\" style=\"padding: 0px; margin-top: 0px; text-rendering: optimizeLegibility; color: rgb(17, 17, 17); font-family: \"Amazon Ember\", Arial, sans-serif; letter-spacing: normal; margin-bottom: 0px !important; font-size: 21px !important; line-height: 1.3 !important;\"><span id=\"productTitle\" class=\"a-size-large\" style=\"text-rendering: optimizeLegibility; font-size: 19px !important; line-height: 1.3 !important;\">Generic Women\'s Silk Dress Materia</span></h1> ', 1, '', '2018-10-28 13:20:29', '0000-00-00 00:00:00', '89', '', 'Europe'),
(23, 'WEXFORD Mens Cotton Polo', 'WEXFORD Mens Cotton Polo (Wex-Wfs034C)', 58, 0, 0, 300, '100', 100, 0, '<h1 id=\"title\" class=\"a-size-large a-spacing-none\" style=\"padding: 0px; margin-top: 0px; text-rendering: optimizeLegibility; color: rgb(17, 17, 17); font-family: \" amazon=\"\" ember\",=\"\" arial,=\"\" sans-serif;=\"\" letter-spacing:=\"\" normal;=\"\" margin-bottom:=\"\" 0px=\"\" !important;=\"\" font-size:=\"\" 21px=\"\" line-height:=\"\" 1.3=\"\" !important;\"=\"\"><span id=\"productTitle\" class=\"a-size-large\" style=\"text-rendering: optimizeLegibility; font-size: 19px !important; line-height: 1.3 !important;\">WEXFORD Men\'s Cotton Polo (Wex-Wfs034C)</span></h1> ', 1, '', '2018-10-28 13:27:56', '2018-10-28 16:20:08', '89', '89', 'Europe'),
(24, 'Louis Philippe Mens Printed Milano Fit Cotton Formal Shirt', ' Louis Philippe Mens Printed Milano Fit Cotton Formal Shirt\r\n', 61, 100, 100, 100, '200', 200, 0, '<h1 id=\"title\" class=\"a-size-large a-spacing-none\" style=\"padding: 0px; margin-top: 0px; text-rendering: optimizeLegibility; color: rgb(17, 17, 17); font-family: \"Amazon Ember\", Arial, sans-serif; letter-spacing: normal; margin-bottom: 0px !important; font-size: 21px !important; line-height: 1.3 !important;\"><span id=\"productTitle\" class=\"a-size-large\" style=\"text-rendering: optimizeLegibility; font-size: 19px !important; line-height: 1.3 !important;\">Louis Philippe Men\'s Printed Milano Fit Cotton Formal Shirt</span></h1> ', 1, '', '2018-10-28 13:30:45', '0000-00-00 00:00:00', '89', '', 'Europe'),
(29, 'Decot Paradise Womens Cotton Print Skirt', 'Decot Paradise Womens Cotton Print Skirt', 57, 100, 100, 100, '100', 100, 0, '<h1 id=\"title\" class=\"a-size-large a-spacing-none\" style=\"padding: 0px; margin-top: 0px; text-rendering: optimizeLegibility; color: rgb(17, 17, 17); font-family: \"Amazon Ember\", Arial, sans-serif; letter-spacing: normal; margin-bottom: 0px !important; font-size: 21px !important; line-height: 1.3 !important;\"><span id=\"productTitle\" class=\"a-size-large\" style=\"text-rendering: optimizeLegibility; font-size: 19px !important; line-height: 1.3 !important;\">Decot Paradise Women\'s Cotton Print Skirt</span></h1> ', 1, '', '2018-10-28 13:41:59', '0000-00-00 00:00:00', '89', '', 'Europe'),
(30, 'FASHIONS OUTLET Womens Satin Dress', 'FASHIONS OUTLET Womens Satin Dress', 60, 100, 100, 100, '100', 100, 0, '<h1 id=\"title\" class=\"a-size-large a-spacing-none\" style=\"padding: 0px; margin-top: 0px; text-rendering: optimizeLegibility; color: rgb(17, 17, 17); font-family: \"Amazon Ember\", Arial, sans-serif; letter-spacing: normal; margin-bottom: 0px !important; font-size: 21px !important; line-height: 1.3 !important;\"><span id=\"productTitle\" class=\"a-size-large\" style=\"text-rendering: optimizeLegibility; font-size: 19px !important; line-height: 1.3 !important;\">FASHIONS OUTLET Women\'s Satin Dress</span></h1> ', 1, '', '2018-10-28 13:43:58', '0000-00-00 00:00:00', '89', '', 'Europe'),
(32, 'Men Navy Blue Checked Round Neck T-shirt', 'Men Navy Blue Checked Round Neck T-shirt', 58, 100, 100, 100, '12', 12, 0, '<h1 class=\"pdp-name\" style=\"box-sizing: inherit; font-size: 20px; margin-top: 0px; margin-bottom: 0px; color: rgb(83, 86, 101); padding: 5px 20px 14px 0px; opacity: 0.8; border-bottom: 1px solid rgb(212, 213, 217); font-family: Whitney; letter-spacing: normal;\">Men Navy Blue Checked Round Neck T-shirt</h1> ', 1, '', '2018-10-28 16:17:34', '0000-00-00 00:00:00', '89', '', 'Europe');

-- --------------------------------------------------------

--
-- Table structure for table `product_brands`
--

CREATE TABLE `product_brands` (
  `sno` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_brands`
--

INSERT INTO `product_brands` (`sno`, `product_id`, `brand_id`, `created_date`, `created_by`) VALUES
(1, 20, 22, '2018-10-28 14:15:37', 89),
(3, 23, 21, '2018-10-28 16:20:08', 89),
(4, 21, 22, '2018-10-28 18:24:14', 89);

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `product_img_sno` int(11) NOT NULL,
  `product_img_name` varchar(80) NOT NULL,
  `prod_sno` int(11) NOT NULL,
  `product_img_alt_tag` varchar(80) NOT NULL,
  `product_img_path` text NOT NULL,
  `product_img_created_date` datetime NOT NULL,
  `product_img_updated_date` datetime NOT NULL,
  `product_img_created_by` varchar(80) NOT NULL,
  `product_img_updated_by` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`product_img_sno`, `product_img_name`, `prod_sno`, `product_img_alt_tag`, `product_img_path`, `product_img_created_date`, `product_img_updated_date`, `product_img_created_by`, `product_img_updated_by`) VALUES
(1, 'skirt_1.jpeg', 9, '', '', '2018-10-28 14:39:47', '0000-00-00 00:00:00', '89', ''),
(3, 'sleeves_stylish_casual_shirt_1.jpeg', 11, '', '', '2018-10-28 18:21:51', '0000-00-00 00:00:00', '89', ''),
(11, 'maniac_men\'s_cotton_t-shirt_1.jpeg', 19, '', '', '2018-10-28 18:42:09', '0000-00-00 00:00:00', '89', ''),
(15, 'wexford_men\'s_cotton_polo__wex-wfs034c__1.jpeg', 23, '', '', '2018-10-28 20:33:22', '0000-00-00 00:00:00', '89', ''),
(23, 'men_navy_blue_checked_round_neck_t-shirt_1.jpeg', 32, '', '', '2018-10-28 20:48:04', '0000-00-00 00:00:00', '89', ''),
(24, 'cherokee_by_unlimited_girl_regular_fit_cotton_skirt_1.jpeg', 13, '', '', '2018-10-28 22:47:49', '0000-00-00 00:00:00', '89', ''),
(25, 'decot_paradise_womens_cotton_print_skirt_1.jpeg', 29, '', '', '2018-10-28 22:48:53', '0000-00-00 00:00:00', '89', ''),
(26, 'fashion_freak_mens_cotton_half_sleeve_polo_t-shirt_1.jpeg', 15, '', '', '2018-10-28 22:49:36', '0000-00-00 00:00:00', '89', ''),
(27, 'diverse_mens_printed_slim_fit_casual_shirt_1.jpeg', 18, '', '', '2018-10-28 22:50:56', '0000-00-00 00:00:00', '89', ''),
(28, 'louis_philippe_mens_printed_milano_fit_cotton_formal_shirt_1.jpeg', 24, '', '', '2018-10-28 22:51:53', '0000-00-00 00:00:00', '89', ''),
(29, 'newport_by_unlimited_women\'s_skinny_fit_shirts_1.jpeg', 20, '', '', '2018-10-28 22:53:02', '0000-00-00 00:00:00', '89', ''),
(30, 'lee_mens_skinny_fit_shirts_1.jpeg', 21, '', '', '2018-10-28 22:55:02', '0000-00-00 00:00:00', '89', ''),
(31, 'mens_skinny_fit_jeans_1.jpeg', 16, '', '', '2018-10-28 22:55:53', '0000-00-00 00:00:00', '89', ''),
(32, 'red_tape_mens_skinny_fit_jeans_1.png', 17, '', '', '2018-10-28 22:56:19', '0000-00-00 00:00:00', '89', ''),
(33, 'saree_1.jpeg', 10, '', '', '2018-10-28 22:58:46', '0000-00-00 00:00:00', '89', ''),
(34, 'harpa_womens_a-line_dress_1.jpeg', 12, '', '', '2018-10-28 22:59:27', '0000-00-00 00:00:00', '89', ''),
(35, 'traditional_anarkali_salwar_suit_dress_materials_1.jpeg', 14, '', '', '2018-10-28 23:00:10', '0000-00-00 00:00:00', '89', ''),
(36, 'generic_womens_silk_dress_material_1.jpeg', 22, '', '', '2018-10-28 23:00:40', '0000-00-00 00:00:00', '89', ''),
(37, 'fashions_outlet_womens_satin_dress_1.jpeg', 30, '', '', '2018-10-28 23:01:33', '0000-00-00 00:00:00', '89', '');

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `sno` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `rating` int(1) NOT NULL,
  `status` int(1) NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subcat_brands`
--

CREATE TABLE `subcat_brands` (
  `sno` int(11) NOT NULL,
  `subcat_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` varchar(2) NOT NULL,
  `updated_date` datetime NOT NULL,
  `updated_by` varchar(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subcat_brands`
--

INSERT INTO `subcat_brands` (`sno`, `subcat_id`, `brand_id`, `created_date`, `created_by`, `updated_date`, `updated_by`) VALUES
(1, 57, 21, '0000-00-00 00:00:00', '20', '0000-00-00 00:00:00', ''),
(2, 58, 21, '0000-00-00 00:00:00', '20', '0000-00-00 00:00:00', ''),
(3, 59, 22, '0000-00-00 00:00:00', '20', '0000-00-00 00:00:00', ''),
(4, 60, 22, '0000-00-00 00:00:00', '20', '0000-00-00 00:00:00', ''),
(5, 61, 22, '0000-00-00 00:00:00', '20', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `sub_cat`
--

CREATE TABLE `sub_cat` (
  `sub_cat_sno` int(2) NOT NULL,
  `sub_cat_name` varchar(100) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `sub_cat_created_date` datetime NOT NULL,
  `sub_cat_updated_date` datetime NOT NULL,
  `sub_cat_created_by` varchar(80) NOT NULL,
  `sub_cat_updated_by` varchar(80) NOT NULL,
  `sub_cat_position_no` int(3) NOT NULL,
  `sub_cat_status` int(2) NOT NULL COMMENT '1=>active,0=>inactive',
  `sub_cat_img` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_cat`
--

INSERT INTO `sub_cat` (`sub_cat_sno`, `sub_cat_name`, `cat_id`, `sub_cat_created_date`, `sub_cat_updated_date`, `sub_cat_created_by`, `sub_cat_updated_by`, `sub_cat_position_no`, `sub_cat_status`, `sub_cat_img`) VALUES
(57, 'skirts', 1, '2018-10-27 21:28:35', '2018-10-28 09:07:57', '89', '89', 1, 1, 'img_57.png'),
(58, 'T_shirts', 2, '2018-10-27 21:28:55', '2018-10-28 13:25:52', '89', '89', 2, 1, 'img_58.png'),
(59, 'Jeans', 2, '2018-10-27 21:29:19', '2018-10-28 09:08:45', '89', '89', 3, 1, 'img_59.png'),
(60, 'traditional-wear', 1, '2018-10-27 21:31:42', '2018-10-28 09:22:04', '89', '89', 4, 1, 'img_60.png'),
(61, 'Shirts', 3, '2018-10-27 21:32:00', '2018-10-28 09:09:01', '89', '89', 5, 1, 'img_61.png');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mobile_number` varchar(100) NOT NULL,
  `created` date NOT NULL,
  `user_type` enum('user','sellar') NOT NULL DEFAULT 'user'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `full_name`, `password`, `email`, `mobile_number`, `created`, `user_type`) VALUES
(36, 'lohita', 'd29f6cd3a2d31c612408fd4be413de78', 'lohita.gara@gmail.com', '92221827', '2018-10-28', 'user');

-- --------------------------------------------------------

--
-- Table structure for table `user_fav_products`
--

CREATE TABLE `user_fav_products` (
  `id` int(100) NOT NULL,
  `user_id` int(100) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_reviews`
--

CREATE TABLE `user_reviews` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `rating` varchar(100) NOT NULL,
  `comments` varchar(500) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_no`),
  ADD UNIQUE KEY `admin_id` (`admin_id`),
  ADD UNIQUE KEY `admin_phone` (`admin_phone`);

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`brand_sno`),
  ADD UNIQUE KEY `brand_name` (`brand_name`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`sno`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`cat_sno`),
  ADD UNIQUE KEY `cat_name` (`cat_name`);

--
-- Indexes for table `checkout_address`
--
ALTER TABLE `checkout_address`
  ADD PRIMARY KEY (`con_no`);

--
-- Indexes for table `forgot_pass`
--
ALTER TABLE `forgot_pass`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`menu_sno`),
  ADD UNIQUE KEY `menu_name` (`menu_name`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_no`);

--
-- Indexes for table `order_products`
--
ALTER TABLE `order_products`
  ADD PRIMARY KEY (`sno`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`prod_sno`),
  ADD UNIQUE KEY `prod_name` (`prod_name`);

--
-- Indexes for table `product_brands`
--
ALTER TABLE `product_brands`
  ADD PRIMARY KEY (`sno`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`product_img_sno`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`sno`);

--
-- Indexes for table `subcat_brands`
--
ALTER TABLE `subcat_brands`
  ADD PRIMARY KEY (`sno`);

--
-- Indexes for table `sub_cat`
--
ALTER TABLE `sub_cat`
  ADD PRIMARY KEY (`sub_cat_sno`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_fav_products`
--
ALTER TABLE `user_fav_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_reviews`
--
ALTER TABLE `user_reviews`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_no` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
  MODIFY `brand_sno` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `sno` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=477;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `cat_sno` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `checkout_address`
--
ALTER TABLE `checkout_address`
  MODIFY `con_no` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `forgot_pass`
--
ALTER TABLE `forgot_pass`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `menu_sno` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1540750378;

--
-- AUTO_INCREMENT for table `order_products`
--
ALTER TABLE `order_products`
  MODIFY `sno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1502703040;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `prod_sno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `product_brands`
--
ALTER TABLE `product_brands`
  MODIFY `sno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `product_img_sno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `sno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `subcat_brands`
--
ALTER TABLE `subcat_brands`
  MODIFY `sno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sub_cat`
--
ALTER TABLE `sub_cat`
  MODIFY `sub_cat_sno` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `user_fav_products`
--
ALTER TABLE `user_fav_products`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_reviews`
--
ALTER TABLE `user_reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
