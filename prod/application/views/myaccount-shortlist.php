<?php include 'includes/header.php'?>
<!-- main starts-->



<style>
#snackbar {
    visibility: hidden;
    min-width: 250px;
    margin-left: -125px;
    background-color: green;
    color: #fff;
    text-align: center;
    border-radius: 2px;
    padding: 16px;
    position: fixed;
    z-index: 1;
    left: 50%;
    bottom: 30px;
    font-size: 17px;
}

#snackbar.show {
    visibility: visible;
    -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
    animation: fadein 0.5s, fadeout 0.5s 2.5s;
}

@-webkit-keyframes fadein {
    from {bottom: 0; opacity: 0;} 
    to {bottom: 30px; opacity: 1;}
}

@keyframes fadein {
    from {bottom: 0; opacity: 0;}
    to {bottom: 30px; opacity: 1;}
}

@-webkit-keyframes fadeout {
    from {bottom: 30px; opacity: 1;} 
    to {bottom: 0; opacity: 0;}
}

@keyframes fadeout {
    from {bottom: 30px; opacity: 1;}
    to {bottom: 0; opacity: 0;}
}


img{
 
width: 100%;
object-fit: contain;
}


.img-responsive{

    display: block;
    max-width: 100%;
    height:280px;
}
</style>




<main>
  <div class="container"> 
    <!-- My account myorders starts here-->
    <div class="row">
      <div class="col-md-3 myorders-nav">
        <h3 class="bold">MY ACCOUNT</h3>
        <?php include 'myaccount-leftnav.php'?>
      </div>
      <div class="col-md-9"> 
        
        <!--short list block starts-->
      <!--  <div class="shortlist">
          <div class="shortlist-header">
            <h4 class="bold pull-left">SHORTLIST <small>(2 Items)</small></h4>
            <div class="pull-right sort-short">
              <select class="form-control">
                <option>Sort By</option>
                <option>Date Added</option>
                <option>Price Low to High</option>
                <option>Price High to Low</option>
              </select>
            </div>
          </div> !-->
          
          <!-- short list body starts-->
          <div class="short-listbody row">
           
           
           	<?php  foreach($get_products as $productDisplayResult){ $query = $this->db->query("SELECT * FROM `product_images` 
			  where prod_sno='".$productDisplayResult['prod_sno']."'"); $resultImg = $query->row();
			   
			  
			  ?>
                         <div class="col-md-4 pblock">
                <div class="product-block">
                  <div class="short-list text-right">    </div>
                  <figure class="pro-figure"><a href="<?php echo base_url()?><?php echo str_replace(" ","-",strtolower($productDisplayResult['cat_name'])) ?>/<?php echo str_replace(" ","-",strtolower($productDisplayResult['sub_cat_name'])) ?>/<?php echo str_replace(" ","-",strtolower($productDisplayResult['prod_name'])) ?>/<?php echo str_replace(" ","-",strtolower($productDisplayResult['prod_sno'])) ?>/detailed">
				  <img   width='280px' height="280px" style="object-fit:cover !important;"  class="img-responsive" src="<?php echo base_url()?>assets/images/gallery/<?php echo str_replace(" ","_",strtolower($productDisplayResult['sub_cat_name'])) ?>/<?php echo str_replace(" ","_",strtolower($productDisplayResult['prod_name'])) ?>/<?php echo $resultImg->product_img_name ?>"></a></figure>
                  <div class="list-desc"> <span class="discount-flag">Alchol Per:<?php echo $productDisplayResult['per_of_alc']?>%</span> <a href="<?php echo base_url()?><?php echo str_replace(" ","-",strtolower($productDisplayResult['cat_name'])) ?>/<?php echo str_replace(" ","-",strtolower($productDisplayResult['sub_cat_name'])) ?>/<?php echo str_replace(" ","-",strtolower($productDisplayResult['prod_name'])) ?>/<?php echo str_replace(" ","-",strtolower($productDisplayResult['prod_sno'])) ?>/detailed" class="quickview">QUICK VIEW</a> <a class="product-title" href="javascript:void(0)">
				  <?php echo $productDisplayResult['prod_name']?></a>
                    <div class="product-price">  <span class="dis-price">AED <?php echo $productDisplayResult['prod_dis_price']?></span> </div>
                    
                            <div class="btn-group btn-group-sm">
                        <a href="<?php echo base_url();?>Myaccount_shortlist/delete_prod/<?php echo $productDisplayResult['prod_sno'];?>">     <button value="REMOVE FROM SHORLIST" class="btn btn-danger">  REMOVE FROM SHORLIST </button> </a>
                            </div>
                    
                    
                                      </div>
                </div>
              </div>
              
              <?php } ?>
           
           
           <!-- <div class="col-md-4">
              <div class="product-block">
                <div class="short-list text-right"><a href="#"><i class="fa fa-heart-o"></i>&nbsp;Added on 03-03-2016</a></div>
                <figure class="pro-figure"><a href="javascript:void(0)"><img class="img-responsive" src="<?php echo base_url()?>assets/frontend/images/tv-image2.png"></a></figure>
                <div class="list-desc"> <a class="product-title" href="javascript:void(0)">Micromax 32T7290MHD 81 cm (32) HD Ready LED Television Wi...</a>
                  <div class="product-price"> <span class="real-price">Rs: 24,900</span> <span class="dis-price">Rs: 19,500</span> </div>
                  <div class="buy-rating"> <i class="fa fa-star blue"></i> <i class="fa fa-star blue"></i> <i class="fa fa-star blue"></i> <i class="fa fa-star blue"></i> <i class="fa fa-star "></i> <span>(35) Review</span> </div>
                  <div class="btn-group btn-group-sm">
                    <input type="button" value="ADD CO CART" class="btn btn-default">
                    <input type="button" value="REMOVE FROM SHORTLIST" class="btn btn-default">
                  </div>
                </div>
              </div>
            </div> !-->
           <!-- <div class="col-md-4">
              <div class="product-block">
                <div class="short-list text-right"><a href="#"><i class="fa fa-heart-o"></i>&nbsp;Added on 03-03-2016</a></div>
                <figure class="pro-figure"><a href="javascript:void(0)"><img class="img-responsive" src="<?php echo base_url()?>assets/frontend/images/tv-image2.png"></a></figure>
                <div class="list-desc"> <a class="product-title" href="javascript:void(0)">Micromax 32T7290MHD 81 cm (32) HD Ready LED Television Wi...</a>
                  <div class="product-price"> <span class="real-price">Rs: 24,900</span> <span class="dis-price">Rs: 19,500</span> </div>
                  <div class="buy-rating"> <i class="fa fa-star blue"></i> <i class="fa fa-star blue"></i> <i class="fa fa-star blue"></i> <i class="fa fa-star blue"></i> <i class="fa fa-star "></i> <span>(35) Review</span> </div>
                  <div class="btn-group btn-group-sm">
                    <input type="button" value="ADD CO CART" class="btn btn-default">
                    <input type="button" value="REMOVE FROM SHORTLIST" class="btn btn-default">
                  </div>
                </div>
              </div>
            </div> !-->
           <!-- <div class="col-md-4">
              <div class="product-block">
                <div class="short-list text-right"><a href="#"><i class="fa fa-heart-o"></i>&nbsp;Added on 03-03-2016</a></div>
                <figure class="pro-figure"><a href="javascript:void(0)"><img class="img-responsive" src="<?php echo base_url()?>assets/frontend/images/tv-image2.png"></a></figure>
                <div class="list-desc"> <a class="product-title" href="javascript:void(0)">Micromax 32T7290MHD 81 cm (32) HD Ready LED Television Wi...</a>
                  <div class="product-price"> <span class="real-price">Rs: 24,900</span> <span class="dis-price">Rs: 19,500</span> </div>
                  <div class="buy-rating"> <i class="fa fa-star blue"></i> <i class="fa fa-star blue"></i> <i class="fa fa-star blue"></i> <i class="fa fa-star blue"></i> <i class="fa fa-star "></i> <span>(35) Review</span> </div>
                  <div class="btn-group btn-group-sm">
                    <input type="button" value="ADD CO CART" class="btn btn-default">
                    <input type="button" value="REMOVE FROM SHORTLIST" class="btn btn-default">
                  </div>
                </div>
              </div>
            </div> !-->
          </div>
          <!-- short list body ends --> 
          
        </div>
        <!-- short list block ends --> 
        
      </div>
    </div>
  </div>
  <!-- my account myorders ends here-->
  </div>
  
  <!-- cart page ends --> 
  
</main>
<!-- main ends -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script>
  
   $(document).ready(function(){
    
        <?php if($this->session->flashdata('delete_fav')) { ?>    
        $('#show_snack').click();
        <?php } ?>
    
    
     });
  </script>
 



<button id="show_snack" style="display:none !important;" onclick="myFunction()">Show Snackbar</button>

<div id="snackbar">  <?php echo  $this->session->flashdata('delete_fav'); ?>      </div>

<script>
function myFunction() {
    var x = document.getElementById("snackbar");
    x.className = "show";
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
}
</script>






<?php include 'includes/footer.php'?>
