<?php

 $this->load->view('includes/header');

$cat = $_REQUEST['cat'];
 ?>
<!-- main starts-->  

<style>
.loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 120px;
  height: 120px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}



img{
 
width: 100%;
object-fit: contain;
}


.img-responsive{

    display: block;
    max-width: 100%;
    height:280px;
   
</style>





<style>
#snackbar {
    visibility: hidden;
    min-width: 250px;
    margin-left: -125px;
    background-color: green;
    color: #fff;
    text-align: center;
    border-radius: 2px;
    padding: 16px;
    position: fixed;
    z-index: 1;
    left: 50%;
    bottom: 30px;
    font-size: 17px;
}

#snackbar.show {
    visibility: visible;
    -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
    animation: fadein 0.5s, fadeout 0.5s 2.5s;
}

@-webkit-keyframes fadein {
    from {bottom: 0; opacity: 0;} 
    to {bottom: 30px; opacity: 1;}
}

@keyframes fadein {
    from {bottom: 0; opacity: 0;}
    to {bottom: 30px; opacity: 1;}
}

@-webkit-keyframes fadeout {
    from {bottom: 30px; opacity: 1;} 
    to {bottom: 0; opacity: 0;}
}

@keyframes fadeout {
    from {bottom: 30px; opacity: 1;}
    to {bottom: 0; opacity: 0;}
}
</style>









<main> 
  <!-- product page starts -->
  <section class="products-page">
    <div class="container"> 
      <!--brudcrumb starts-->
      <div class="brcrumb-products"> 
        <!-- brudcrumb starts-->
        <ul>
          <li><a href="index.php">Home <i class="fa fa-angle-right"></i> </a></li>
          <li><a href="<?php echo base_url()?>alcohol"><?php echo ucfirst($this->uri->segment(1)) ?><i class="fa fa-angle-right"></i> </a></li>
          <li><a><?php echo ucfirst($this->uri->segment(2)) ?></a></li>
        </ul>
        <!-- brudcrumb ends --> 
        <!-- page search block starts-->
        
        <!-- page search block ends --> 
      </div>
      <!-- brudcrumb ends --> 
      <!-- product main starts-->
      <div class="row main-products"> 
        <!-- left navigatino starts-->
        <div style="width:10%" class="col-md-3">
          <aside class="filter-block"> 
            <!-- sub category starts-->
            <div class="filter-block-left">
              <ul>
                  
                <?php $segment = ucfirst($this->uri->segment(2));  ?>  
                  
                <li class="filter-title"><a class="bold" href="javascript:void(0)"><?php echo ucfirst($this->uri->segment(1)) ?></a> <span class="pull-right"><?php //echo count($get_sub_cat)?></span></li>
                <?php foreach($get_sub_cat as $val){
                
                       
                
                ?>
			 
				<li><a <?php if($segment==$val['sub_cat_name']){  ?> class="bold"   <?php } ?>   href="<?php echo base_url();?>wine/<?php echo $val['sub_cat_name'];?>/sub-cat"><?php echo $val['sub_cat_name']; ?></a></li>
                <?php }?>
              </ul>
            </div>
            <!-- sub category block ends --> 
            <!--  filter by brand starts-->
          <!--  <div class="filter-block-left">
              <ul class="filter2">
                <li class="filter-title"><span class="bold">Filter by Brand</span></li>
               <?php foreach($get_brands as $get_brands){?>
                <li><a href="javascript:void(0)">
                  <input class="pull-left" name='filter'  type="checkbox" value='<?php echo $get_brands['brand_name'] ?>'>
                 <?php echo $get_brands['brand_name'] ?></a></li>
			   <?php }?>
              </ul>
            </div> !-->
            <!-- filter by brand ends --> 
            <!-- filtger by size starts-->
           <!-- <div class="filter-block-left">
              <ul class="filter2">
                <li class="filter-title"><span class="bold">Filter by Size</span> </li>
				<?php foreach($get_quan_type as $get_quan_type){ ?>
                <li><a href="javascript:void(0)">
                  <input class="pull-left" name='size_value' value='<?php echo $get_quan_type['color_name'];?>' type="checkbox">
                  <?php echo $get_quan_type['color_name'];?></a></li>
                <?php } ?>
              </ul>
            </div> !-->
            <!-- filter by size ends --> 
            <!-- display features starts-->
           
           
            <!-- filter by functionality ends --> 
            <!-- filter by price starts-->
            
            <!-- filter by price ends--> 
            
          </aside>
        </div>
        <!-- left navigation ends --> 
        <!-- right side main product list starts-->
        <div  class="col-md-9" style="width:89%">
	
          <h3 class="pro-title"><?php echo $cat ?> <span>(<?php echo count($get_products); ?> Items)</span></h3>
          <!-- right main product block starts-->
          <div class="main-pro-block"> 
            <!-- sort by block starts-->
           
           <?php  
           $cat_name = $get_products[0]['cat_name']; 
           $sub_cat_name = $get_products[0]['sub_cat_name'];     
            ?>
           
           
           
           
            <div class="sortby"> <span>Sort By: </span>
               <ul>
                <li><a href="<?php echo base_url();?><?php echo $cat_name;?>/<?php echo $sub_cat_name;?>/sub-cat/low"> Price Low to High </a></li>
                <li><a href="<?php echo base_url();?><?php echo $cat_name;?>/<?php echo $sub_cat_name;?>/sub-cat/high">Price High to Low</a></li>
               </ul>
            </div> 
            
            
               
            <!-- main product list starts-->
			 <div class="row default">
  <span id='filter_value'>			               
  <!-- product starts-->
			  <?php 
			  
			  $get_user_id = $this->session->userdata('userId');
			  
			  
			  $get_fav_products = $this->db->query("select * from user_fav_products where user_id = '".$get_user_id."' group by product_id")->result();
			  
			  $get_fav_prod_ids = array();
			  
			 foreach($get_fav_products as $ke=>$vl){
			     
			  $get_fav_prod_ids[] = $vl->product_id;    
			     
			 }
			 
		 
			  
			  
			  foreach($get_products as $productDisplayResult){ $query = $this->db->query("SELECT * FROM `product_images` 
			  where prod_sno='".$productDisplayResult['prod_sno']."'"); $resultImg = $query->row();
			  
			    
			     $get_user_id = $this->session->userdata('userId');
			 
			    if(empty($get_user_id)){
			         $hre = base_url().'Listing/add_fav_prod/'.$productDisplayResult['prod_sno'];
			         
			         $class = "";
			         
			     }else{
			         
			         $class = "add_fav";
			         
			         $hre = "#";
			     }
			
			  

if (in_array($productDisplayResult['prod_sno'], $get_fav_prod_ids))
  {
      $class = "";
      $hre = "#";
      $sty = "green";
      $color = "white";
  }else{
      $sty = "none";
      $color = "black";
  }
 
			 
			 
			  
			  ?>
              <div class="col-md-4 pblock">
                <div class="product-block">
                  <div class="short-list text-right">   <a href="<?php echo  $hre; ?>" class="<?php echo $class;?>" data-prod-id="<?php echo $productDisplayResult['prod_sno']; ?>"> <i class="fa fa-heart-o"  style="background-color:<?php echo $sty;?>;color:<?php echo $color;?>;"></i> </a></div>
                  <figure class="pro-figure"><a href="<?php echo base_url()?><?php echo str_replace(" ","-",strtolower($productDisplayResult['cat_name'])) ?>/<?php echo str_replace(" ","-",strtolower($productDisplayResult['sub_cat_name'])) ?>/<?php echo str_replace(" ","-",strtolower($productDisplayResult['prod_name'])) ?>/<?php echo str_replace(" ","-",strtolower($productDisplayResult['prod_sno'])) ?>/detailed">
				  <img   width='280px' height="280px" style="object-fit:cover !important;"  class="img-responsive" src="<?php echo base_url()?>assets/images/gallery/<?php echo str_replace(" ","_",strtolower($productDisplayResult['sub_cat_name'])) ?>/<?php echo str_replace(" ","_",strtolower($productDisplayResult['prod_name'])) ?>/<?php echo $resultImg->product_img_name ?>"></a></figure>
                  <div class="list-desc">   <a href="<?php echo base_url();?><?php echo str_replace(" ","-",strtolower($productDisplayResult['cat_name'])) ?>/<?php echo str_replace(" ","-",strtolower($productDisplayResult['sub_cat_name'])) ?>/<?php echo str_replace(" ","-",strtolower($productDisplayResult['prod_name'])) ?>/<?php echo str_replace(" ","-",strtolower($productDisplayResult['prod_sno'])) ?>/detailed" class="quickview">QUICK VIEW</a> <a class="product-title" href="javascript:void(0)">
				  <?php echo $productDisplayResult['prod_name']?></a>
                    <div class="product-price">  <span class="dis-price">NOK <?php echo $productDisplayResult['prod_dis_price']?></span> </div>
                                      </div>
                </div>
              </div>
			  <?php }?>
              <!-- product ends --> 
             
              <!-- product starts-->
             
              <!-- product ends --> 
              
           
           
           
           
		<?php //}?>
		  </span>
		 </div>
		 
		  
		 
		 
	 
		  
            <!-- main product list ends --> 
            <!--<a class="loadmore" href="javascript:void(0)">LOAD MORE</a>-->
          </div>
          <!-- right main products block ends --> 
        </div>
        <!-- right side main product list ends --> 
      </div>
      <!-- product main ends --> 
    </div>
  </section>
  <!-- product page ends --> 
  
</main>
<!-- main ends -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
    
    
    
    $(".pull-left").on('change',function(){
        
        	var filterValue = [];
	    filterSize=[];
        $('input[name="filter"]:checked').each(function () {
            filterValue.push(this.value);
        });
        
         var filterSize = [];
        
        	$('input[name="size_value"]:checked').each(function () {
            filterSize.push(this.value);
         });
         
         
         
          if (filterValue.length !== 0) {
    
        var url = "<?php echo base_url();?>alcohol/filter_products";
         
         
             $.ajax({  
             type:"POST",  
             url:url,  
             data:{'fil_val':JSON.stringify(filterValue),'fil_size':JSON.stringify(filterSize)},  
            beforeSend: function() {
           $('#res').addClass('loader');
            },   
             success: function(data){  
               
                $('#res').removeClass('loader');   
               
               
              $('.result').html(data);
              
              $('.default').hide();
              
             } 
         
             });  
             
          }
         
        
    });    
    

});
</script>





<button id="show_snack" style="display:none;" onclick="myFunction()">Show Snackbar</button>

<div id="snackbar"> Product is Added to Favourite Successfully</div>

<script>
function myFunction() {
    var x = document.getElementById("snackbar");
    x.className = "show";
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
}
</script>





<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js">
</script>

<script>

 $(document).ready( function() {
   $(".add_fav").click(function(){
    
     //  alert('test');
       
       
    var prod_id = $(this).attr('data-prod-id');
    
         //   alert(prod_id);

     
        url =  "<?php echo base_url();?>Listing/add_fav_prod/"+prod_id;   
     
       dataString = "prod_id="+prod_id; 
     
     
     $.ajax({
      type: "POST",
      url: url,
      data: dataString,
      success: function( response ) {
         if(response=="success"){
        $("#show_snack").click();
         }else{
            $('.confirm').hide();    
        }
       }
    });
    
    
     
   });  
   
});

 
</script>















<?php $this->load->view('includes/footer'); ?>
