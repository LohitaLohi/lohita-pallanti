<!-- footer starts-->

<footer> 
  <!-- secure block starts-->
  <div class="secure">
    <div class="container">
      <div class="row">
        <div class="col-md-4">
            <div class="row align-items-start">
          <div class="col-xs-2 col-md-3"><img src="<?php echo base_url()?>assets/frontend/images/key-icon.png"></div>
          <div class="col-xs-9 col-md-9">
            <h3 class="secure-heading">100 % Secure Payment</h3>
            <p>All major credit & debit cards accepted</p>
          </div>
          </div>
        </div>
        <div class="col-md-4">
            <div class="row">
          <div class="col-xs-2 col-md-3"><img src="<?php echo base_url()?>assets/frontend/images/trustpay-icon.png"></div>
          <div class="col-xs-9 col-md-9">
            <h3 class="secure-heading">Trust Pay</h3>
            <p>100% Money Back Guarantee & 7days  Return Policy</p>
          </div>
          </div>
        </div>
        <div class="col-md-4">
            <div class="row">
          <div class="col-xs-2 col-md-3"><img src="<?php echo base_url()?>assets/frontend/images/customer-care-icon.png"></div>
          <div class="col-xs-9 col-md-9">
            <h3 class="secure-heading">Customer Support</h3>
            <p>24x7 Customer Support</p>
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- secure block ends --> 
  
  <!-- main footer links starts-->
  <div class="container">
    <div class="row footer-links">
      <div class="col-md-3">
        <h3 class="bold">Policy Information</h3>
        <ul>
          <li><a href="javascript:void(0)">Privacy Policy</a></li>
          <li><a href="javascript:void(0)">Terms of Sale</a></li>
          <li><a href="javascript:void(0)">Terms of Use</a></li>
          <li><a href="javascript:void(0)">Report Abuse & Takedown</a></li>
          <li><a href="javascript:void(0)">Policy</a></li>
          <li><a href="javascript:void(0)">CSR Policy</a></li>
          <li><a href="javascript:void(0)">T&C Adv/Media</a></li>
          <li><a href="javascript:void(0)">Communications</a></li>
        </ul>
      </div>
      <div class="col-md-2">
        <h3 class="bold">COMPANY</h3>
        <ul>
          <li><a href="javascript:void(0)">About Us</a></li>
          <li><a href="javascript:void(0)">Core Values</a></li>
          <li><a href="javascript:void(0)">Press</a></li>
          <li><a href="javascript:void(0)">Careers</a></li>
          <li><a href="javascript:void(0)">Blog</a></li>
          <li><a href="javascript:void(0)">Sitemap</a></li>
          <li><a href="javascript:void(0)">FAQ</a></li>
          <li><a href="javascript:void(0)">Contact Us</a></li>
        </ul>
      </div>
      <div class="col-md-3">
       
        <ul>
          
          <li><a href="javascript:void(0)">Media Enquiries</a></li>
          <li><a href="javascript:void(0)">Be an Affiliate</a></li>
          <li><a href="javascript:void(0)">Dealof the Day</a></li>
          
        </ul>
      </div>
      <div class="col-md-2">
        <h3 class="bold">FOLLOW US</h3>
        <ul>
          <li><a href="javascript:void(0)" target="_blank"><i class="fa fa-facebook"></i> Facebook</a></li>
          <li><a href="javascript:void(0)" target="_blank"><i class="fa fa-twitter"></i> Twitter</a></li>
          <li><a href="javascript:void(0)" target="_blank"><i class="fa fa-google-plus"></i> Googld Plus</a></li>
          <li><a href="javascript:void(0)" target="_blank"><i class="fa fa-linkedin"></i> Linkedin</a></li>
        </ul>
      </div>
      <div class="col-md-2">
        <h3 class="bold">Payment Methods</h3>
        <ul>
          <li><a>Net Banking</a></li>
          <li><a>Cash On Delivery</a></li>
          <li><a>EMI Conversion</a></li>
          <li><a>E-Gift Voucher</a></li>
          <li><a>Debit / Credit Card</a></li>
        </ul>
      </div>
    </div>
  </div>
  <!-- main footer links ends --> 
  
  <!-- bottom footer starts-->
  <div class="bottom-footer">
    <div class="container text-center">
      <figure><img src=""></figure>
      <p>all rights reserved @ Liqour.com 2018</p>
    </div>
  </div>
  <!-- bottom footer ends --> 
  
</footer>
<!-- footer ends--> 

<!-- script files starts--> 
<script type="text/javascript" src="<?php echo base_url()?>assets/frontend/js/jquery-1.11.3.min.js"></script> 
<script type="text/javascript" src="<?php echo base_url()?>assets/frontend/js/filter.js"></script> 
<script type="text/javascript" src="<?php echo base_url()?>assets/frontend/js/bootstrap.min.js"></script> 
<script type="text/javascript" src="<?php echo base_url()?>assets/frontend/js/html5-shiv.js"></script> 
<script type="text/javascript" src="<?php echo base_url()?>assets/frontend/js/custom.js"></script> 
<script type="text/javascript" src="<?php echo base_url()?>assets/frontend/js/bootstrap-slider-range.js">

<!-- script files ends--> 


</body></html>