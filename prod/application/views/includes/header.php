<?php 
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>clothes</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- style sheets starts -->
<link rel="stylesheet" href="<?php echo base_url()?>assets/frontend/css/style.css">
<link rel="stylesheet" href="<?php echo base_url()?>assets/frontend/css/bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url()?>assets/frontend/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url()?>assets/frontend/css/fonts.css">
<link rel="icon" type="<?php echo base_url();?>assest/frontend/image/png" sizes="16x16" 
<a href="<?php echo base_url();?>assets/frontend/images/clothes/logo.jpg">

<!-- style sheets ends -->

</head>


	<?php $get_catgories = $this->db->query('SELECT * FROM `category` WHERE `cat_status` =  1')->result();
	        
	
		 ?>

<body>
<!-- header starts -->
<header class="header-container"> 
  <!-- top header starts-->
  <div class="top-header">
    <div class="container">
      <div class="row"> 
        <!-- top header left starts-->
        <div class="col-md-6">
          <div class="dropdown block-currency-wrapper">
            <form id="currency" class="">
            <!--  <a class="block-currency dropdown-toggle" href="#" data-target="#" data-toggle="dropdown" role="button" aria-expanded="false"> AED<span class="caret"></span> </a> !-->
             
              <input type="hidden" name="code" value="">
              <input type="hidden" name="redirect" value="#">
            </form>
          </div>
          <p class="welcome-msg hidden-xs">Welcome Visitor!</p>
        </div>
       <div class="col-md-6 rightnav"> 
          
          <ul class="nav navbar-nav navbar-right">
              	<?php if(isset($_SESSION['userId'])){?>
            <li><a href="#">Wish List(0)</a></li>
             <?php } ?>
            
            <?php if(isset($_SESSION['userId'])){?>
            <li><a href="<?php echo base_url()?>Checkout">CheckOut</a></li>
            <?php } ?>
            
            <?php if(isset($_SESSION['userId'])){?>
            <li class="dropdown"> <a href="<?php echo base_url()?>Myaccount_orders"class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> My Account <i class="fa fa-chevron-down"></i></a>
              <ul class="dropdown-menu">
                <li><a href="#"> <i class="fa fa-cog"></i> &nbsp; Settings</a></li>
                <li><a href="<?php echo base_url()?>Myaccount_orders"> <i class="fa fa-user"></i> &nbsp; My Profile</a></li>
			 
			    <li><a href="<?php echo base_url()?>Login_new/logout"> <i class="fa fa-sign-out"></i> &nbsp; Logout</a></li>
			 
				
				<?php }else{ ?>
				
				
                <li><a href="<?php echo base_url()?>Login_new"> <i class="fa fa-sign-in"></i> &nbsp; User Login / Register</a></li>
                
               
                
                <li class="divider"></li>
				
				<?php } ?>
				
              </ul>
            </li>
          </ul>
          <!-- End Header Top Links --> 
        </div>
        <!-- top header right ends --> 
      </div>
    </div>
  </div>
  <!-- top header ends--> 
  <!-- middle header starts-->
  <div class="header container">
    <div class="row"> 
      <!-- logo starts-->
      <div class="col-md-2"> <a class="logo" href="<?php echo base_url()?>" title="clothes"><img style="height:50px" width="50px"class="img-responsive" src="<?php echo base_url()?>assets/frontend/images/clothes/logo.jpg" title="clothes" alt="clothes"></a> </div>
      <!-- logo ends --> 
      
      <!-- search block starts-->
      <div class="col-lg-6 col-sm-5 col-md-6 col-xs-12"> 
        <!-- Search-col -->
      <!--  <div class="search-box">
          <select class="cate-dropdown hidden-xs" name="category_id">
            <option value="0">All Categories</option>
            <option value="59">Wine</option>
            <option value="59">HARD</option>
            <option value="59">CHAMPAIGN</option>
            <option value="59">Non LiQOUR</option>
            
          </select>
          <input id="search_header" class="" type="text" name="search" maxlength="70" value="" placeholder="Search here..">
          <button id="submit-button-search-header" class="search-btn-bg"> <span>Search</span> </button>
        </div> !-->
        <!-- End Search-col --> 
      </div>
      <!-- search block ends --> 
      
      <!-- login top navigation starts -->
      <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
        <div class="login"> <?php
        
        $this->load->database();
        
        $user_id =  $this->session->userdata('userId');
        
        if(!empty($user_id)) {
        $get_user_data = $this->db->query("select * from users where id='".$user_id."'")->row();
        
        $get_user_name  = $get_user_data->full_name;
        
        }
        if($this->session->userdata('userId')) { /*echo ucfirst($get_user_name);*/ } else { ?> 
        
        <!--<a href="<?php echo base_url()?>Login_new"> <span>Login1 / Register</span></a> !-->
        
        
        <?php } ?> </div>
      </div>
      <!-- login top navigaton ends--> 
      
    </div>
  </div>
  <!-- middle header ends --> 
  <!-- navigation starts-->
  <nav class="navbar navbar-default">
    <div class="container">
      <div class="row">
        <div class="collapse navbar-collapse js-navbar-collapse">
            
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigationbar"> </button>
          
		    <?php foreach($get_catgories as $ke=>$vl) { ?>  
		  
		  <ul class="nav navbar-nav">
			 <li class="dropdown mega-dropdown"> <a href="#" class="dropdown-toggle" style="color:green" data-toggle="dropdown"><?php echo $vl->cat_name;?> <i class="fa fa-chevron-down"></i></a>
			 
              <ul class="dropdown-menu mega-dropdown-menu container">
                         
                 
                  
                  <li class="col-sm-3">
                  <ul> 
                       
                    <li class="dropdown-header"><?php echo $vl->cat_name;?></li>
                    
                    <?php $get_sub = $this->db->query("select * from sub_cat where sub_cat_status=1 and cat_id = '".$vl->cat_sno."'")->result();
                    
                          foreach($get_sub as $key=>$value){ ?>
                    
                    ?>
                    
                    
                    
                    <li><a href="<?php echo base_url()?><?php echo str_replace(" ","-",strtolower($vl->cat_name)) ?>/<?php echo str_replace(" ","-",strtolower($value->sub_cat_name)) ?>/sub-cat"><?php echo $value->sub_cat_name;?></a></li>
                    
                    <?php } ?>
                    
                  </ul>
                </li>
 
                   
                          
               
              </ul>
            </li>
             
          </ul>
		  
		   <?php } ?>
		  
          <!-- cart block -->
		  <?php 
		  
			$this->db->select("sum(quanity) as sum_cart");
			$this->db->from("cart C");
			if($this->session->userdata('userSno')){
				
				$this->db->where('C.order_id', $this->session->userdata('userSno'));
			}
			else{
				
				$this->db->where('C.order_id', $this->session->userdata('cart_order_no'));
			}
			$query=$this->db->get();
			$resultName = $query->row();
		  
		  
		  ?>
          <div class="top-cart-contain">
            <div class="mini-cart">
              <div id="cart">
                <div  class="basket dropdown-toggle"> 
				<a href="<?php echo base_url()?>cart">
                  <div class="cart-box"> <span id="cart-total"><strong><?php echo $resultName->sum_cart ?></strong>item</span> </div>
                  </a> </div>
                <ul class="top-cart-content arrow_box">
                  <li>
                    <p class="text-center noitem">Your shopping cart is empty!</p>
                  </li>
                </ul>
              </div>
            </div>
            <div style="display:none" id="ajaxconfig_info"><a href="#/"></a>
              <input type="hidden" value="">
              <input type="hidden" value="1" id="enable_module">
              <input type="hidden" value="1" class="effect_to_cart">
              <input type="hidden" value="Go to shopping cart" class="title_shopping_cart">
            </div>
          </div>
          <!-- cart block ends --> 
        </div>
        <!-- /.nav-collapse --> 
      </div>
    </div>
  </nav>
  <!-- navigation ends --> 
  <!-- header services starts 
  <div class="header-service">
    <div class="container">
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="content"> <span> <i class="fa fa-truck"></i> Free Shipping on order over Rs:500</span> </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="content"> <i class="fa fa-comment"></i> <span>Customer Support Service</span> </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="content"> <i class="fa fa-money"></i> <span>3 days Money Back Guarantee</span> </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="content"> <i class="fa fa-star"></i> <span>5% discount on order over Rs:500</span> </div>
        </div>
      </div>
    </div>
  </div>
  <!-- header services ends --> 
</header>
<!-- header ends--> 