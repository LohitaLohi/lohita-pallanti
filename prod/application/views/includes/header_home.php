
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Clothes</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- style sheets starts -->
<link rel="stylesheet" href="<?php echo base_url()?>assets/frontend/css/style.css">
<link rel="stylesheet" href="<?php echo base_url()?>assets/frontend/css/bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url()?>assets/frontend/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url()?>assets/frontend/css/fonts.css">
<link rel="icon" type="<?php echo base_url()?>assest/frontend/image/png" sizes="16x16" 
href="<?php echo base_url()?>assets/frontend/images/clothes/logo.jpg">
<!-- style sheets ends -->

</head>


	<?php $get_catgories = $this->db->query('SELECT * FROM `category` WHERE `cat_status` =  1')->result();
	        
	
		 ?>

<body>
<!-- header starts -->
<header class="header-container"> 
  <!-- top header starts-->
  <div class="top-header">
    <div class="container">
      <div class="row"> 
        <!-- top header left starts-->
        <div class="col-md-6">
          <div class="dropdown block-currency-wrapper">
            <form id="currency" class="">
              <a class="block-currency dropdown-toggle" href="#" data-target="#" data-toggle="dropdown" role="button" aria-expanded="false"> NOK<span class="caret"></span> </a>
             
              <input type="hidden" name="code" value="">
              <input type="hidden" name="redirect" value="#">
            </form>
          </div>
          <p class="welcome-msg hidden-xs">Welcome visitor!</p>
        </div>
       <div class="col-md-6 rightnav"> 
          
          <ul class="nav navbar-nav navbar-right">
              	<?php if(isset($_SESSION['userId'])){?>
            <li><a href="#">Wish List(0)</a></li>
             <?php } ?>
            
            <?php if(isset($_SESSION['userId'])){?>
            <li><a href="<?php echo base_url()?>Checkout">CheckOut</a></li>
            <?php } ?>
            
            <?php if(isset($_SESSION['userId'])){?>
            <li class="dropdown"> <a href="<?php echo base_url()?>Myaccount_orders"class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> My Account <i class="fa fa-chevron-down"></i></a>
              <ul class="dropdown-menu">
                <li><a href="#"> <i class="fa fa-cog"></i> &nbsp; Settings</a></li>
                <li><a href="<?php echo base_url()?>Myaccount_orders"> <i class="fa fa-user"></i> &nbsp; My Profile</a></li>
			 
			    <li><a href="<?php echo base_url()?>Login_new/logout"> <i class="fa fa-sign-out"></i> &nbsp; Logout</a></li>
			 
				
				<?php }else{ ?>
				
				
                <li><a href="<?php echo base_url()?>Login_new"> <i class="fa fa-sign-in"></i> &nbsp; User Login / Register</a></li>
                
                  
                
                <li class="divider"></li>
				
				<?php } ?>
				
              </ul>
            </li>
          </ul>
          <!-- End Header Top Links --> 
        </div>
        <!-- top header right ends --> 
      </div>
    </div>
  </div>
  <!-- top header ends--> 
  <!-- middle header starts-->
 
  <!-- header services ends --> 
</header>
<!-- header ends--> 