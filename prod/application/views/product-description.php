<?php $mt='2';include"helper.php";include"header.php";include"leftpanel.php";?>
  
  <!-- main dashboard starts-->
  <main>
    <section class="mainpanel">
      <div class="content-panel"> 
        <!-- brudcrumb starts-->
        <ul class="brcrumb whiteblock">
          <li><a href="dashboard.php"><i class="fa fa-home"></i>Home</a></li>
          <li>Products</li>
          <li>New Product</li>
        </ul>
        <!-- brudcrumb ends--> 
        
        <!-- data table starts-->
        <section class="whiteblock">
          <article class="page-introduction">
            <h3>PRODUCT NAME</h3>
           
          </article>
          
          <!-- table section starts--> 
          
          
          <div class="row">
            <div class="newform">
            
             <div class="form-group col-lg-3">
              	<label>Main Product Category<span class="mandatory">*</span></label>
              	<select class="form-control">
                	<option>Electronics</option>
                	<option>Furniture</option>
                </select>
              </div>
              
                <div class="form-group col-lg-3 col-sm-6">
              	<label>Sub Category<span class="mandatory">*</span></label>
              	<select class="form-control">
                	<option>Electronics</option>
                	<option>Furniture</option>
                </select>
              </div>
            
              <div class="form-group col-lg-3 col-sm-6">
              	<label>Model Input type<span class="mandatory">*</span></label>
              	<input type="text" class="form-control">
              </div>
              
              <div class="form-group col-lg-3 col-sm-6">
              	<label>Screen Type<span class="mandatory">*</span></label>
              	<input type="text" class="form-control">
              </div>
              
               <div class="form-group col-lg-3 col-sm-6">
              	<label>Display Size<span class="mandatory">*</span></label>
              	<input type="text" class="form-control">
              </div>
              
               <div class="form-group col-lg-3 col-sm-6">
              	<label>Display Features<span class="mandatory">*</span></label>
              	<input type="text" class="form-control">
              </div>
              
               <div class="form-group col-lg-3 col-sm-6">
              	<label>Screen Resolution<span class="mandatory">*</span></label>
              	<input type="text" class="form-control">
              </div>
              
              <div class="form-group col-lg-3 col-sm-6">
              	<label>In the box<span class="mandatory">*</span></label>
              	<input type="text" class="form-control">
              </div>
              
              <div class="form-group col-lg-3 col-sm-6">
              	<label>Aspet Ratio<span class="mandatory">*</span></label>
              	<input type="text" class="form-control">
              </div>
              
              <div class="form-group col-lg-3 col-sm-6">
              	<label>Dynamic Ratio<span class="mandatory">*</span></label>
              	<input type="text" class="form-control">
              </div>
              
               <div class="form-group col-lg-3 col-sm-6">
              	<label>View Angle(<span class="mandatory">*</span></label>
              	<input type="text" class="form-control">
              </div>
              
              <div class="form-group col-lg-3 col-sm-6">
              	<label>Refresh Rate(<span class="mandatory">*</span></label>
              	<input type="text" class="form-control">
              </div>
              
               <div class="form-group col-lg-12 col-sm-6">
              	<label>Additionals<span class="mandatory">*</span></label>
               <textarea class="form-control"></textarea>
              </div>
              
              <div class="col-md-12"><h2 class="h2">Connectivity</h2></div>
              
              <div class="form-group col-lg-3 col-sm-6">
              	<label>HDMI<span class="mandatory">*</span></label>
              	<input type="text" class="form-control">
              </div>
              
              <div class="form-group col-lg-3 col-sm-6">
              	<label>USB<span class="mandatory">*</span></label>
              	<input type="text" class="form-control">
              </div>
              
               
              <div class="form-group col-lg-3 col-sm-6">
              	<label>Composit Video Output<span class="mandatory">*</span></label>
              	<input type="text" class="form-control">
              </div>
              
              <div class="form-group col-lg-3 col-sm-6">
              	<label>Component Video Output<span class="mandatory">*</span></label>
              	<input type="text" class="form-control">
              </div>
              
               <div class="form-group col-lg-3 col-sm-6">
              	<label>Audio Output<span class="mandatory">*</span></label>
              	<input type="text" class="form-control">
              </div>
              
               <div class="form-group col-lg-3 col-sm-6">
              	<label>Headphone Output<span class="mandatory">*</span></label>
              	<input type="text" class="form-control">
              </div>
              
               <div class="form-group col-lg-3 col-sm-6">
              	<label>Other Connections<span class="mandatory">*</span></label>
              	<input type="text" class="form-control">
              </div>
              
               <div class="col-md-12"><h2 class="h2">Internet Options</h2></div>
               
               
               <div class="form-group col-lg-3 col-sm-6">
              	<label>Ether Net <span class="mandatory">*</span></label>
              	<input type="text" class="form-control">
              </div>
              
               <div class="form-group col-lg-3 col-sm-6">
              	<label>Wifi <span class="mandatory">*</span></label>
              	<input type="text" class="form-control">
              </div>
              
                 <div class="form-group col-lg-3 col-sm-6">
              	<label>Applications <span class="mandatory">*</span></label>
              	<input type="text" class="form-control">
              </div>
              
               <div class="form-group col-lg-12 col-sm-6">
              	<label>Additionals<span class="mandatory">*</span></label>
               <textarea class="form-control"></textarea>
              </div>
              
              <div class="col-md-12"><h2 class="h2">Playback formats</h2></div>
              
               <div class="form-group col-lg-3 col-sm-6">
              	<label>Video Playback formats<span class="mandatory">*</span></label>
               <input type="text" class="form-control">
              </div>
              
                <div class="form-group col-lg-3 col-sm-6">
              	<label>Audio Playback formats<span class="mandatory">*</span></label>
               <input type="text" class="form-control">
              </div>
              
              <div class="form-group col-lg-3 col-sm-6">
              	<label>Picture Playback formats<span class="mandatory">*</span></label>
               <input type="text" class="form-control">
              </div>
              
               <div class="form-group col-lg-12 col-sm-6">
              	<label>Additionals<span class="mandatory">*</span></label>
               <textarea class="form-control"></textarea>
              </div>
              
              <div class="col-md-12"><h2 class="h2">Dimensions</h2></div>
              
              <div class="form-group col-lg-3 col-sm-6">
              	<label>W x H x D (Without Stand)<span class="mandatory">*</span></label>
              <input type="text" class="form-control">
              </div>
              
               
              <div class="form-group col-lg-3 col-sm-6">
              	<label>W x H x D (With Stand)<span class="mandatory">*</span></label>
              <input type="text" class="form-control">
              </div>
              
               <div class="form-group col-lg-3 col-sm-6">
              	<label>Weight(With Stand)<span class="mandatory">*</span></label>
              <input type="text" class="form-control">
              </div>
              
               <div class="form-group col-lg-3 col-sm-6">
              	<label>Weight(With Out Stand)<span class="mandatory">*</span></label>
              <input type="text" class="form-control">
              </div>
              
            
              
              
             
              
               
              
              <div class="form-group form-btn col-lg-6 col-sm-6">
              	<button>SUBMIT</button>
              </div>
              
               
              
            </div>
          </div> 
          
        
          
         
          <!-- table section ends--> 
          
        </section>
        <!-- data table ends--> 
        
      </div>
    </section>
  </main>
  <!-- main dashboard ends-->
  
<?php
include"footer.php"
?>