<!doctype html>
<html class="fixed">
	<head>

		<?php Include('includes/headerlinks.php');?>
		<link rel="stylesheet" href="<?= $this->config->item('admin_assets');?>vendor/bootstrap-fileupload/bootstrap-fileupload.min.css" />
		<?php Include('includes/headerlinks2.php');?>
	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<?php include('includes/header.php');?>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php include('includes/menu_section.php');?>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Add Product Images </h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo $admin_url;?>">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><a href="<?php echo $admin_url;?>product"><span>Products</span></a></li>
								<li><a href="<?php echo $admin_url;?>product_images/<?= $product_ID;?>"><span>Images</span></a></li>
								<li><span>Add Product Images</span></li>
							</ol>
						</div>
					</header>

					<!-- start: page -->
						<div class="row">
							<div class="col-xs-12">
								<section class="panel">
									<header class="panel-heading">
										<h2 class="panel-title">Add Product Images</h2>
									</header>
									<div class="panel-body">
										<?php  
										$action = $admin_url."product_images_insert"; 
									 	echo form_open_multipart($action,array('id'=>"fm_prod_add",'class'=>"form-horizontal form-bordered custom_form",'autocomplete'=>"off"));
										?>
											<div class="form-group">
												<label class="col-md-3 control-label">Images <span class="colon">:</span></label>
												<div class="col-md-6">
													<div class="fileupload fileupload-new" data-provides="fileupload">
														<div class="input-append">
															<div class="uneditable-input">
																<i class="fa fa-file fileupload-exists"></i>
																<span class="fileupload-preview"></span>
															</div>
															<span class="btn btn-default btn-file">
																<span class="fileupload-exists">Change</span>
																<span class="fileupload-new">Select file</span>
																<input type="file" name="prod_images[]" class="custom_required " multiple="" accept="image/*" required="">
															</span>
															<a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
														</div>
													</div>
												</div>
											</div>
											<input type="hidden" name="prod_sno" value="<?= $product_ID;?>" >
											<div class="form-group">
												<label class="col-md-3 control-label"></label>
												<div class="col-md-6">
													<?php if(isset($errors)){ echo "<p class='errors_p'>".$errors."</p>"; }?>
													<button type="submit" onclick='return validate_form();'  class="btn btn-primary custom_submit_btm" name="menu_add">Submit</button>
												</div>
											</div>
										<?php echo form_close();?>
									</div>
								</section>
							</div>
						</div>
					<!-- end: page -->
				</section>
			</div>
		</section>

		<?php include('includes/footerlinks.php');?>
		<script src="<?= $this->config->item('admin_assets');?>vendor/jquery-autosize/jquery.autosize.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
		<?php include('includes/footerlinks2.php');?>


		<!-- Examples -->
		<script src="<?= $this->config->item('admin_assets');?>javascripts/forms/examples.advanced.form.js"></script>

	</body>
</html>