<!doctype html>
<html class="fixed">
	<head>

		<?php Include('includes/headerlinks.php');?>
		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="<?php echo $this->config->item('admin_assets');?>vendor/select2/select2.css" />
		<link rel="stylesheet" href="<?php echo $this->config->item('admin_assets');?>vendor/jquery-datatables-bs3/assets/css/datatables.css" />
		<?php Include('includes/headerlinks2.php');?>
	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<?php include('includes/header.php');?>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php include('includes/menu_section.php');?>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Menu</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo $this->config->item('admin_url');?>">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span>Menu</span></li>
							</ol>
						</div>
					</header>

					<!-- start: page -->
						<section class="panel">
							<header class="panel-heading">
								<div class="panel-actions">
									<a href="<?php echo $admin_url;?>menu_add" class="add">Add Menu</a>
								</div>
							<h2 class="panel-title">Menu</h2>
							</header>
							<div class="panel-body">
									<?php /*echo '<pre>';print_r($menu_list[0]); echo '</pre>';*/?>
								<table class="table table-bordered table-striped mb-none" id="datatable-default">
									<thead>
										<tr>
											<th class="center">S No.</th>
											<th>Menu</th>
											<th class="center">Position</th>
											<th class="center">Status</th>
											<th class="center">Actions</th>
										</tr>
									</thead>
									<tbody>
										<?php $i=0; foreach ($menu_list as $menuitem) { $i++;?>
										<tr class="gradeX">
											<td class="center"><?= $i;?></td>
											<td><?= ucfirst($menuitem['menu_name']); ?></td>
											<td class="center"><?= $menuitem['menu_position_no']; ?></td>
											<td class="center"><?php if($menuitem['menu_status'] =='1'){echo 'Active';}else{echo 'In-active';} ?></td>
											<td class="center actions_td">
												<a title="Edit" href="<?php echo $this->config->item('admin_url');?>menu_edit/<?= $menuitem['menu_sno']; ?>" class="edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
												<?php if($menuitem['menu_status'] =='1'){ ?>
												<a title="Make In-active" href="<?php echo $this->config->item('admin_url');?>menu_inactive/<?= $menuitem['menu_sno']; ?>" onclick="return confirm_inactive();" class="delete active"><i class="fa fa-check" aria-hidden="true"></i></a>
												<?php }else{ ?>
												<a title="Make Active" href="<?php echo $this->config->item('admin_url');?>menu_active/<?= $menuitem['menu_sno']; ?>" onclick="return confirm_active();" class="delete inactive"><i class="fa fa-times" aria-hidden="true"></i></a>
												<?php } ?>
											</td>
										</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</section>
					<!-- end: page -->
				</section>
			</div>

		</section>
		<?php include('includes/footerlinks.php');?>
		<!-- Specific Page Vendor -->
		<script src="<?php echo $this->config->item('admin_assets');?>vendor/select2/select2.js"></script>
		<script src="<?php echo $this->config->item('admin_assets');?>vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>
		<script src="<?php echo $this->config->item('admin_assets');?>vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>
		<script src="<?php echo $this->config->item('admin_assets');?>vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>

		<!-- Examples -->
		<script src="<?php echo $this->config->item('admin_assets');?>javascripts/tables/examples.datatables.default.js"></script>
		<script src="<?php echo $this->config->item('admin_assets');?>javascripts/tables/examples.datatables.row.with.details.js"></script>
		<script src="<?php echo $this->config->item('admin_assets');?>javascripts/tables/examples.datatables.tabletools.js"></script>
		<?php include('includes/footerlinks2.php');?>
	</body>
</html>