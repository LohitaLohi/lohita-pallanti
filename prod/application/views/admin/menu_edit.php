<!doctype html>
<html class="fixed">
	<head>

		<?php Include('includes/headerlinks.php');?>
		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="<?php echo $this->config->item('admin_assets');?>vendor/select2/select2.css" />
		<?php Include('includes/headerlinks2.php');?>
	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<?php include('includes/header.php');?>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php include('includes/menu_section.php');?>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Edit Menu</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo $admin_url;?>">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><a href="<?php echo $admin_url;?>menu"><span>Menu</span></a></li>
								<li><span>Edit Menu</span></li>
							</ol>
						</div>
					</header>

					<!-- start: page -->
					<div class="row">
						<div class="col-xs-12">
							<section class="panel">
								<header class="panel-heading">
									<h2 class="panel-title">Edit Menu</h2>
								</header>
								<div class="panel-body">
									<?php $menu_item_data = $menu_item_data['0']; ?>
									<?php $action =$this->config->item('admin_url')."menu_update"; 
									 echo form_open($action,array('id'=>"fm_menu_add",'class'=>"form-horizontal form-bordered custom_form",'autocomplete'=>"off")); ?>
										<div class="form-group">
											<label class="col-md-3 control-label">Menu Name <span class="colon">:</span></label>
											<div class="col-md-6">
												<input class="form-control custom_required" value="<?= $menu_item_data['menu_name'];?>" name="menu_name" />
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label">Position<span class="colon">:</span></label>
											<div class="col-md-6">
												<input class="form-control number custom_required" maxlength="1"  onkeypress="return onlyNos(event,this);" value="<?= $menu_item_data['menu_position_no'];?>" name="menu_position" />
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label">Menu Status<span class="colon">:</span></label>
											<div class="col-md-6">
												<?php $status = $menu_item_data['menu_status'];?>
												<select data-plugin-selectTwo class="form-control populate custom_required" name="menu_status" >
													<option value="1" <?php if($status == '1'){echo 'selected';}?>>Active</option>
													<option value="0" <?php if($status == '0'){echo 'selected';}?>>In-active</option>
												</select>
											</div>
										</div>
										<input type="hidden" name="menu_id" value="<?= $menu_item_data['menu_sno'];?>" />
										<div class="form-group">
											<label class="col-md-3 control-label"></label>
											<div class="col-md-6">
												<p class="error_p"><?php echo $this->session->flashdata('error0');?></p>
												<button type="submit" onclick='return validate_form();' class="btn btn-primary custom_submit_btm" name="menu_add">Update</button>
											</div>
										</div>
									<?php echo form_close();?>
								</div>
							</section>
						</div>
					</div>
					<!-- end: page -->
				</section>
			</div>
		</section>

		<?php include('includes/footerlinks.php');?>
		<!-- Specific Page Vendor -->
		<script src="<?php echo $this->config->item('admin_assets');?>vendor/select2/select2.js"></script>
		<?php include('includes/footerlinks2.php');?>
		<!-- Examples -->
		<script src="<?php echo $this->config->item('admin_assets');?>javascripts/forms/examples.advanced.form.js"></script>

	</body>
</html>