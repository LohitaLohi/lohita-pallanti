<!doctype html>
<html class="fixed">
	<head>

		<?php Include('includes/headerlinks.php');?>
		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="<?php echo $this->config->item('admin_assets');?>vendor/select2/select2.css" />
		<?php Include('includes/headerlinks2.php');?>
	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<?php include('includes/header.php');?>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php include('includes/menu_section.php');?>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Add Category</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo $admin_url;?>">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><a href="<?php echo $admin_url;?>category"><span>Category</span></a></li>
								<li><span>Add Category</span></li>
							</ol>
						</div>
					</header>

					<!-- start: page -->
						<div class="row">
							<div class="col-xs-12">
								<section class="panel">
									<header class="panel-heading">
										<h2 class="panel-title">Add Category</h2>
									</header>
									<div class="panel-body">
										<?php /*echo '<pre>'; print_r($menu_list); echo '</pre>';*/?>
										<?php $action =$admin_url."category_insert"; 
									 echo form_open($action,array('id'=>"fm_cat_add",'class'=>"form-horizontal form-bordered custom_form",'autocomplete'=>"off")); ?>
											<div class="form-group">
												<label class="col-md-3 control-label">Menu Item <span class="colon">:</span></label>
												<div class="col-md-6">
													<select data-plugin-selectTwo class="form-control populate" name="menu_id" required="required">
														<option value="">Select Menu</option>
														<?php foreach ($menu_list as $menu_data) {
															echo '<option value="'.$menu_data['menu_sno'].'">'.ucfirst($menu_data["menu_name"]).'</option>';
														}?>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-3 control-label">Category Name <span class="colon">:</span></label>
												<div class="col-md-6">
													<input class="form-control custom_required" name="cat_name" />
												</div>
											</div>
						
											<div class="form-group">
												<label class="col-md-3 control-label">Position<span class="colon">:</span></label>
												<div class="col-md-6">
													<input class="form-control number custom_required" onkeypress="return onlyNos(event,this);"  name="cat_position_no" />
												</div>
											</div>
											
											<div class="form-group">
												<label class="col-md-3 control-label"></label>
												<div class="col-md-6">
													<button type="submit" onclick='return validate_form();'  class="btn btn-primary custom_submit_btm" name="category_add">Submit</button>
												</div>
											</div>
						
										<?php echo form_close();?>
									</div>
								</section>
							</div>
						</div>
					<!-- end: page -->
				</section>
			</div>
		</section>

		<?php include('includes/footerlinks.php');?>
		<!-- Specific Page Vendor -->
		<script src="<?php echo $this->config->item('admin_assets');?>vendor/select2/select2.js"></script>
		<?php include('includes/footerlinks2.php');?>
		<!-- Examples -->
		<script src="<?php echo $this->config->item('admin_assets');?>javascripts/forms/examples.advanced.form.js"></script>

	</body>
</html>