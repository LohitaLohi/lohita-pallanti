<!doctype html>
<html class="fixed">
	<head>

		<?php Include('includes/headerlinks.php');?>
		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="<?= $this->config->item('admin_assets');?>vendor/select2/select2.css" />
		<link rel="stylesheet" href="<?= $this->config->item('admin_assets');?>vendor/jquery-datatables-bs3/assets/css/datatables.css" />
		<?php Include('includes/headerlinks2.php');?>
		
	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<?php include('includes/header.php');?>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php include('includes/menu_section.php');?>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Orders</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a  data-toggle="tooltip" title="Home" href="<?php echo $admin_url;?>">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span>Orders</span></li>
							</ol>
						</div>
					</header>

					<!-- start: page -->
						<section class="panel">
							<header class="panel-heading"> 
							<h2 class="panel-title">Orders</h2>
							</header>
							<div class="panel-body">
										<div class="startandenddate filter-gopi" style=" margin: 0px 0 30px 0; text-align: right; line-height: 34px; font-weight: 700; font-size: 15px; border: 1px solid #ddd; padding: 15px;">
								
									<form method="post" name="" action="<?php echo base_url()?>admin/accountant_orders">
								
									<div class="col-md-2">
										<label>Start Date</label>
										<input name="pr_start_date" value="<?php echo $_POST['pr_start_date']?>" id="txtFromDate" placeholder="MM/DD/YYYY"  type="text"  class="form-control">
									</div>
									<div class="col-md-2">
										<label>End Date</label>
										<input name="pr_deadline" value="<?php echo $_POST['pr_deadline']?>" id="txtToDate" placeholder="MM/DD/YYYY"  type="text"  class="form-control">
									</div>
									
									
									<div class="col-md-1">
										<button type="submit" name="search_data" class="btn btn-primary">Search</button>
									</div>
									</form>
								
									<div class="clearfix"></div>
								</div>
								<table class="center table table-bordered table-striped mb-none" id="datatable-default">
									<thead>
										<tr>
											<th class="center">S No.</th>
											<th>Order No</th>
											<th width="20%">Date</th>
											<th class="center">Price (NOV)</th>
											<th class="center">Shipping Status</th>
											<th class="center">Payment Status</th>
											<th class="center">Order Status</th>
											 </tr>
									</thead>
									<tbody>
										<?php $c =0; foreach ($orders as $order_info) { $c++; ?>
											<td class="center"><?= $c; ?></td>
											<td><?= ucfirst($order_info['order_no']);?></td>
											<td><?php echo date('d-m-Y h:i a',strtotime($order_info['order_date_time']));?></td>
											<td class="text-right"><?= number_format($order_info['grand_total'],2);?></td>
											<td>
												<?php 
												if($order_info['shipping_status'] == '1'){
													echo 'Dispached';
												} else if($order_info['shipping_status'] == '2'){
													echo 'Delivered';
												} else if($order_info['delivery_boy_id'] != '0'){
													echo 'Assigned To Delivery Boy';
												} else if($order_info['shipping_status'] == '0'){
													echo '--';
												}
												else {
													echo 'Delivered';
												} ?>
											</td>
											<td><?php 
												if($order_info['payment_type'] == '1'){echo 'Pending';}else{echo 'Completed';}
												?>
											</td>
											<td><?php 
												if($order_info['order_status'] == '0'){
													echo 'Order Placed';
												} else if($order_info['order_status'] == '1'){
													echo 'Processing';
												} else if($order_info['order_status'] == '2'){
													echo 'Completed';
												} else if($order_info['order_status'] == '3'){
													echo 'Canceled';
												}?>
											</td>
											 
											 
										</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</section>
					<!-- end: page -->
				</section>
			</div>

		</section>
		<?php include('includes/footerlinks.php');?>
		<!-- Specific Page Vendor -->
		<script src="<?= $this->config->item('admin_assets');?>vendor/select2/select2.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>

		<!-- Examples -->
		<script src="<?= $this->config->item('admin_assets');?>javascripts/tables/examples.datatables.default.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>javascripts/tables/examples.datatables.row.with.details.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>javascripts/tables/examples.datatables.tabletools.js"></script>
		<?php include('includes/footerlinks2.php');?>
<link href="<?php echo base_url();?>assets/datepicker/jquery-ui.css" rel="Stylesheet" type="text/css" />
<link rel="stylesheet" href="http://proppick.in/gadgetcareplan/assets/vendor/bootstrap-datepicker/css/datepicker3.css" />
<script>

     $(document).ready(function(){
    $("#txtFromDate").datepicker({
        numberOfMonths: 1,
		//minDate:-30,
        onSelect: function(selected) {
          $("#txtToDate").datepicker("option","minDate", selected)
        }
    });
    $("#txtToDate").datepicker({ 
        numberOfMonths: 1,
		//minDate:0,
        onSelect: function(selected) {
           $("#txtFromDate").datepicker("option","maxDate", selected)
        }
    });  
});
    </script>
	</body>
</html>