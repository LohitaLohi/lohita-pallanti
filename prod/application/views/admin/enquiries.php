<!doctype html>
<html class="fixed">
	<head>

		<?php Include('includes/headerlinks.php');?>
		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="<?= $this->config->item('admin_assets');?>vendor/select2/select2.css" />
		<link rel="stylesheet" href="<?= $this->config->item('admin_assets');?>vendor/jquery-datatables-bs3/assets/css/datatables.css" />
		<?php Include('includes/headerlinks2.php');?>
	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<?php include('includes/header.php');?>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php include('includes/menu_section.php');?>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Enquiries</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo $admin_url;?>">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span>Enquiries</span></li>
							</ol>
						</div>
					</header>

					<!-- start: page -->
						<section class="panel">
							<header class="panel-heading">
								<div class="panel-actions">
									<a href="<?php echo $admin_url;?>enquiry_add" class="add">Add Enquiry</a>
								</div>
							<h2 class="panel-title">Enquiries</h2>
							</header>
							<div class="panel-body">
								<table class="table table-bordered table-striped mb-none" id="datatable-default">
									<thead>
										<tr>
											<th class="center">S No.</th>
											<th>Name</th>
											<th class="center">Email</th>
											<th class="center">Phone No.</th>
											<th class="center">Description</th>
											<th class="center">status</th>
											<th class="center">Actions</th>
										</tr>
									</thead>
									<tbody>
										<?php $bb=0; foreach ($enquiries as $enquiry) { $bb++;?>
										<tr class="gradeX">
											<td class="center"><?= $bb;?></td>
											<td><?= ucwords($enquiry['name']);?></td>
											<td><?= $enquiry['email'];?></td>
											<td><?= $enquiry['phone'];?></td>
											<td><?= $enquiry['description'];?></td>
											<td>
												<?php /* $action = $admin_url."enquiry_status_change"; 
									 			echo form_open($action,array('id'=>"fm_brand_add",'class'=>"form-horizontal form-bordered custom_form",'autocomplete'=>"off"));*/?>
												<input type="hidden" name="enq_id" value="<?= $enquiry['sno']?>" />
												<select name="status"  class="form-control status_change"  data-id="<?= $enquiry['sno']?>"  >
													<option value="New" <?php if($enquiry['status'] == 'New'){echo 'selected';}?>>New</option>
													<option value="On-Going" <?php if($enquiry['status'] == 'On-Going'){echo 'selected';}?>>On-Going</option>
													<option value="Completed" <?php if($enquiry['status'] == 'Completed'){echo 'selected';}?>>Completed</option>
													<option value="Not Related" <?php if($enquiry['status'] == 'Not Related'){echo 'selected';}?>>Not Related</option>
												</select>
												<?php /* form_close();*/?>
											</td>
											<td class="center actions_td">	
												<a title="Make Active" href="<?php echo $admin_url;?>enquiry_delete/<?= $enquiry['sno']; ?>" onclick="return confirm_delete();" class="delete inactive"><i class="fa fa-trash" aria-hidden="true"></i></a>
											</td>
										</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</section>
					<!-- end: page -->
				</section>
			</div>

		</section>
		<?php include('includes/footerlinks.php');?>
		<!-- Specific Page Vendor -->
		<script src="<?= $this->config->item('admin_assets');?>vendor/select2/select2.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>

		<!-- Examples -->
		<script src="<?= $this->config->item('admin_assets');?>javascripts/tables/examples.datatables.default.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>javascripts/tables/examples.datatables.row.with.details.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>javascripts/tables/examples.datatables.tabletools.js"></script>
		<?php include('includes/footerlinks2.php');?>
<script>
$(document).ready(function(){
	var baseurl = "<?php echo base_url();?>";
	$('.status_change').change(function(){
		var status = $(this).val();
		var id = $(this).data('id');
		var mailurl = baseurl+"/admin/enquiry_status_change/"+id+"/"+status;
		window.location.href = mailurl;
	});
});
</script>
	</body>
</html>