<!doctype html>
<html class="fixed">
	<head>

		<?php Include('includes/headerlinks.php');?>
		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="<?= $this->config->item('admin_assets');?>vendor/select2/select2.css" />
		<link rel="stylesheet" href="<?= $this->config->item('admin_assets');?>vendor/jquery-datatables-bs3/assets/css/datatables.css" />
		<?php Include('includes/headerlinks2.php');?>
	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<?php include('includes/header.php');?>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php include('includes/menu_section.php');?>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Colors</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo $admin_url;?>">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span>Colors</span></li>
							</ol>
						</div>
					</header>
					<!-- start: page -->
					<section class="panel">
						<div class="panel-body width-f">
								<form class="form-horizontal form-bordered" method="post" enctype="multipart/form-data" action="<?php echo base_url();?>admin/user_insert">
											<div class="panel-body gopi-mb">
												<div class="clients-head">
													<h3>Information</h3>
												</div>
												<div class="row">
													<div class="col-sm-6">
														<div class="form-group">
															<label class="col-md-4 control-label">Name <span class="red">*</span> : </label>
															<div class="col-md-8">
																<input value="<?php echo $this->input->post('name')?>" required type="text" class="form-control" name="name">
															</div>
														</div>
														
														<div class="form-group">
															<label class="col-md-4 control-label">Email * : </label>
															<div class="col-md-8">
																<input value="<?php echo $this->input->post('email')?>" required type="email" class="form-control" name="email">
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-4 control-label">Phone No : </label>
															<div class="col-md-8">
																<input value="<?php echo $this->input->post('phone_no')?>" required type="text" class="form-control" name="phone_no">
															</div>
														</div>
															<div class="form-group">
															<label class="col-md-4 control-label">Admin City : </label>
															<div class="col-md-8">
																<input type="text"  class="form-control" value="" name="admin_city">
															</div>
														</div>
													</div>
													<div class="col-sm-6">
														<div class="form-group">
															<label class="col-md-4 control-label">Date of Joining : </label>
															<div class="col-md-8">
																<div class="input-group">
																	<span class="input-group-addon">
																		<i class="fa fa-calendar"></i>
																	</span>
																	<input value="<?php echo $this->input->post('date_of_joining')?>" required type="text" data-plugin-datepicker="" class="form-control" name="date_of_joining">
																</div>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-4 control-label">Roles *: </label>
															<div class="col-md-8">
																<select required class="form-control" name="admin_type">
																	<option value="">Select  Type</option>
																	<option <?php if($this->input->post('admin_type')=="Delivery Boy"){ echo "selected";}?> value="Delivery Boy">Delivery Boy</option>
																	
																</select>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-4 control-label">Upload Photo : </label>
															<div class="col-md-8">
																<div class="fileupload fileupload-new" data-provides="fileupload">
																	<div class="input-append">
																		<div class="uneditable-input gopib">
																			<i class="fa fa-file fileupload-exists"></i>
																			<span class="fileupload-preview"></span>
																		</div>
																		<span class="btn btn-default btn-file">
																			<span class="fileupload-exists">Change</span>
																			<span class="fileupload-new">Select file</span>
																			<input type="file" name="admin_img">
																		</span>
																		<a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>	
											</div>
											<div class="panel-body gopi-mb">
												<div class="clients-head">
													<h3>Personal Information</h3>
												</div>
												<div class="row">
													<div class="col-sm-6">
														<div class="form-group">
															<label class="col-md-4 control-label">Father Name : </label>
															<div class="col-md-8">
																<input type="text" value="<?php echo $this->input->post('name')?>" class="form-control" name="father_name">
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-4 control-label">Blood Group : </label>
															<div class="col-md-8">
																<input type="text" value="<?php echo $this->input->post('name')?>" class="form-control" name="blood_group">
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-4 control-label">Sex : </label>
															<div class="col-md-8">
																<select class="form-control" name="gender">
																	<option value=""></option>
																	<option <?php if($this->input->post('gender')=="Male"){ echo "selected";}?>  value="Male">Male</option>
																	<option <?php if($this->input->post('gender')=="Female"){ echo "selected";}?> value="Female">Female</option>
																</select>
															</div>
														</div>
													</div>
													<div class="col-sm-6">
														<div class="form-group">
															<label class="col-md-4 control-label">DOB : </label>
															<div class="col-md-8">
																<div class="input-group">
																	<span class="input-group-addon">
																		<i class="fa fa-calendar"></i>
																	</span>
																	<input type="text" value="<?php echo $this->input->post('name')?>" data-plugin-datepicker="" class="form-control" name="dob">
																</div>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-4 control-label">Personal No : </label>
															<div class="col-md-8">
																<input type="text" value="<?php echo $this->input->post('name')?>" class="form-control" name="personal_phone">
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-4 control-label">Address : </label>
															<div class="col-md-8">
																<input type="text" value="<?php echo $this->input->post('name')?>" class="form-control" name="address">
															</div>
														</div>
													</div>
													<div class="col-sm-12">
														<div class="form-group">
															<div class="col-xs-4 col-xs-offset-8">
																<div class="add-lead-btn">
																<div class="error">
																
																<input  type="submit" class="mb-xs mt-xs btn btn-success" value="Submit" />
																<?php echo $this->session->flashdata('user_name');?>
																</div>
																	
																	
																</div>
															</div>
														</div>
													</div>
												</div>	
											</div>
										</form>	
							
							
								
								
								
							
						
						</div>
					</section>	
					<!-- end: page -->
				</section>
			</div>

			
		</section>

		<!-- Vendor -->
<?php include('includes/footerlinks.php');?>
		<!-- Specific Page Vendor -->
		<script src="<?= $this->config->item('admin_assets');?>vendor/select2/select2.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>

		<!-- Examples -->
		<script src="<?= $this->config->item('admin_assets');?>javascripts/tables/examples.datatables.default.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>javascripts/tables/examples.datatables.row.with.details.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>javascripts/tables/examples.datatables.tabletools.js"></script>
		<?php include('includes/footerlinks2.php');?>		
<style>
.clients-head h3 {
    margin: 0 0 20px 0;
    background: #0e80d5;
    padding: 6px 0 6px 10px;
    color: #fff;
    font-size: 22px;
}
.panel-bodygopi {
    margin: 30px 0 0 0;
}
.error {
    color: red;
}
.add-lead-btn.nav-right {
    float: right;
    margin-top: 10px;
}
</style>		
		
	</body>
<script>
function myFunction() {
    var x = document.getElementById("myFile").value;
    document.getElementById("demo").innerHTML = x;
}
</script>
<script>
var app = angular.module('myApp', []);
//numbers-only
app.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9]/g, '');

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }            
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});
//letters-only
app.directive('lettersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^a-zA-Z]/, '');

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }            
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});

          app.controller('AppController', ['$scope', function($scope) {
		  $scope.products = [{
			value: 'prod_1',
			label: 'Product 1'
		  }, {
			value: 'prod_2',
			label: 'Product 2'
		  }];  
		    
			var self = this;
			self.reset = function(){
			self.user={id:id};
			$scope.myForm.$setPristine(); //reset Form
			}
			
			$scope.myFunc = function() {
			 $scope.x=10;
			}
					
      }]);
app.directive('validFile', function () {
   return {
       require: 'ngModel',
       link: function (scope, el, attrs, ngModel) {
           ngModel.$render = function () {
               ngModel.$setViewValue(el.val());
           };

           el.bind('change', function () {
               scope.$apply(function () {
                   ngModel.$render();
               });
           });
       }
   };
});
app.directive('imageonload', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.bind('load', function() {
                alert('image is loaded');
            });
            element.bind('error', function(){
                alert('image could not be loaded');
            });
        }
    };
});
</script>
</html>