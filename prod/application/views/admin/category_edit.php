<!doctype html>
<html class="fixed">
	<head>

		<?php Include('includes/headerlinks.php');?>

		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="<?php echo $this->config->item('admin_assets');?>vendor/select2/select2.css" />
		<?php Include('includes/headerlinks2.php');?>
	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<?php include('includes/header.php');?>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php include('includes/menu_section.php');?>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Edit Category</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo $admin_url;?>">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><a href="<?php echo $admin_url;?>category"><span>Category</span></a></li>
								<li><span>Edit Category</span></li>
							</ol>
						</div>
					</header>

					<!-- start: page -->
						<div class="row">
							<div class="col-xs-12">
								<section class="panel">
									<header class="panel-heading">
										<h2 class="panel-title">Edit Category</h2>
									</header>
									<div class="panel-body">
											<?php 
												$cat_data =$cat_data['0']; 
												/*
												echo '<pre>';
												print_r($cat_data);
												print_r($menu_list); echo '</pre>';
												*/
											?>
										<?php $action =$admin_url."category_update"; 
									 echo form_open($action,array('id'=>"fm_cat_add",'class'=>"form-horizontal form-bordered custom_form",'autocomplete'=>"off")); ?>
											<div class="form-group">
												<label class="col-md-3 control-label">Menu Item <span class="colon">:</span></label>
												<div class="col-md-6">
													<select data-plugin-selectTwo class="form-control populate " name="menu_id" required="required">
														<option value="">Select Menu</option>
														<?php foreach ($menu_list as $menu_data) { ?>
															<option value="<?= $menu_data['menu_sno'];?>" <?php if($menu_data['menu_sno']==$cat_data['menu_id']){echo 'selected';}?>><?= ucfirst($menu_data["menu_name"]);?></option>
														<?php } ?>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-3 control-label">Category Name <span class="colon">:</span></label>
												<div class="col-md-6">
													<input class="form-control custom_required" value="<?= $cat_data['cat_name']?>" name="cat_name" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-3 control-label">Position<span class="colon">:</span></label>
												<div class="col-md-6">
													<input class="form-control number custom_required" onkeypress="return onlyNos(event,this);" value="<?= $cat_data['cat_position_no']?>" name="cat_position_no" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-3 control-label">Category Status<span class="colon">:</span></label>
												<div class="col-md-6">
													<?php $status = $cat_data['cat_status'];?>
													<select data-plugin-selectTwo class="form-control populate custom_required" name="cat_status" >
														<option value="1" <?php if($status == '1'){echo 'selected';}?> >Active</option>
														<option value="0" <?php if($status == '0'){echo 'selected';}?> >In-active</option>
													</select>
												</div>
											</div>
											<input type="hidden" name="cat_id" value="<?= $cat_data['cat_sno']?>">
											<div class="form-group">
												<label class="col-md-3 control-label"></label>
												<div class="col-md-6">
													<button type="submit" onclick='return validate_form();'  class="btn btn-primary custom_submit_btm" name="category_add">Update</button>
												</div>
											</div>
						
										</form>
									</div>
								</section>
							</div>
						</div>
					<!-- end: page -->
				</section>
			</div>
		</section>

		<?php include('includes/footerlinks.php');?>
		<!-- Specific Page Vendor -->
		<script src="<?php echo $this->config->item('admin_assets');?>vendor/select2/select2.js"></script>
		<?php include('includes/footerlinks2.php');?>
		<!-- Examples -->
		<script src="<?php echo $this->config->item('admin_assets');?>javascripts/forms/examples.advanced.form.js"></script>

	</body>
</html>