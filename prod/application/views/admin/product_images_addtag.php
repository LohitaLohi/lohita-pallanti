<!doctype html>
<html class="fixed">
	<head>

		<?php Include('includes/headerlinks.php');?>

		<!-- Specific Page Vendor CSS -->
		<?php Include('includes/headerlinks2.php');?>
	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<?php include('includes/header.php');?>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php include('includes/menu_section.php');?>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Add Tag</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo $admin_url;?>">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><a href="<?php echo $admin_url;?>product"><span>Products</span></a></li>
								<li><a href="<?php echo $admin_url;?>product_images/<?= $product_ID;?>"><span>Product Images</span></a></li>
								<li><span>Add Tag</span></li>
							</ol>
						</div>
					</header>

					<!-- start: page -->
						<div class="row">
							<div class="col-xs-12">
								<section class="panel">
									<header class="panel-heading">
										<h2 class="panel-title">Add Tag</h2>
									</header>
									<div class="panel-body">
										<?php 
										$action = $admin_url."product_images_addtag_insert"; 
									 	echo form_open_multipart($action,array('id'=>"fm_prod_add",'class'=>"form-horizontal form-bordered custom_form",'autocomplete'=>"off"));
										?>
											<div class="form-group">
												<label class="col-md-3 control-label">Tag Name <span class="colon">:</span></label>
												<div class="col-md-6">
													<input class="form-control custom_required" value="" name="product_img_alt_tag" />
												</div>
											</div>
											<input type="hidden" name="product_id" value="<?= $product_ID;?>" />
											<input type="hidden" name="image_id" value="<?= $image_ID;?>" />
											<div class="form-group">
												<label class="col-md-3 control-label"></label>
												<div class="col-md-6">
													<button type="submit" onclick='return validate_form();'  class="btn btn-primary custom_submit_btm" name="pincode_add">Submit</button>
												</div>
											</div>
										<?php form_close();?>
									</div>
								</section>
							</div>
						</div>
					<!-- end: page -->
				</section>
			</div>
		</section>

		<?php include('includes/footerlinks.php');?>
		<?php include('includes/footerlinks2.php');?>
		<!-- Examples -->
		<script src="<?= $this->config->item('admin_assets');?>javascripts/forms/examples.advanced.form.js"></script>

	</body>
</html>