<!doctype html>
<html class="fixed">
	<head>

		<?php Include('includes/headerlinks.php');?>

		<!-- Specific Page Vendor CSS -->
		<?php Include('includes/headerlinks2.php');?>
	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<?php include('includes/header.php');?>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php include('includes/menu_section.php');?>

				<section role="main" class="content-body">
				
					<?php include("includes/breadcrumbs.php")?>

					<!-- start: page -->
					<section class="panel">
						<div class="panel-body width-f">
						
							<form class="form-horizontal form-bordered" method="post" enctype="multipart/form-data" action="<?php echo base_url();?>admin/emp_agent_update">
								
											<div class="panel-body gopi-mb">
												<div class="clients-head">
													<h3>Information</h3>
												</div>
												<div class="row">
													<div class="col-sm-6">
														<div class="form-group">
															<label class="col-md-4 control-label">Name <span class="red">*</span> : </label>
															<div class="col-md-8">
																<input type="text" class="form-control" value="<?php echo $user_data->admin_name ?>" name="name">
															</div>
														</div>
														
														<div class="form-group">
															<label class="col-md-4 control-label">Email * : </label>
															<div class="col-md-8">
																<input type="text" readonly class="form-control" value="<?php echo $user_data->admin_id ?>" name="email">
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-4 control-label">Phone No : </label>
															<div class="col-md-8">
																<input type="text" readonly class="form-control" value="<?php echo $user_data->admin_phone ?>" name="phone_no">
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-4 control-label">Admin City : </label>
															<div class="col-md-8">
																<input type="text"  class="form-control" value="<?php echo $user_data->admin_city ?>" name="admin_city">
															</div>
														</div>
													</div>
													<div class="col-sm-6">
														<div class="form-group">
															<label class="col-md-4 control-label">Date of Joining : </label>
															<div class="col-md-8">
																<div class="input-group">
																	<span class="input-group-addon">
																		<i class="fa fa-calendar"></i>
																	</span>
																	<input type="text" data-plugin-datepicker="" class="form-control" value="<?php echo date('m/d/Y', strtotime($user_data->date_of_joining)); ?>" name="date_of_joining">
																</div>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-4 control-label">Roles : </label>
															<div class="col-md-8">
															<?php $admintype = $user_data->admin_type ?>
																<input type="text" readonly class="form-control" value="<?php echo ucfirst($user_data->admin_type) ?>" name="admin_type">
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-4 control-label">Upload Photo : </label>
															<div class="col-md-8">
																<img style="width:150px;" src="<?php echo base_url();?>assets/images/emp_pic/<?php echo $user_data->admin_img; ?>" alt="<?php echo $user_data->admin_img; ?>" />
																<div class="fileupload fileupload-new" data-provides="fileupload">
																	<div class="input-append">
																		<div class="uneditable-input gopib">
																			<i class="fa fa-file fileupload-exists"></i>
																			<span class="fileupload-preview"></span>
																		</div>
																		<span class="btn btn-default btn-file">
																			<span class="fileupload-exists">Change</span>
																			<span class="fileupload-new">Select file</span>
																			<input type="file"  name="admin_img">
																		</span>
																		<a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
																	</div>
																</div>
															</div>
														</div>
														<input type="hidden" name="old_img" value="<?php echo $user_data->admin_img; ?>" >
													</div>
												</div>	
											</div>
											<div class="panel-body gopi-mb">
												<div class="clients-head">
													<h3>Personal Information</h3>
												</div>
												<div class="row">
													<div class="col-sm-6">
														<div class="form-group">
															<label class="col-md-4 control-label">Father Name : </label>
															<div class="col-md-8">
																<input type="text" class="form-control" value="<?php echo $user_data->father_name ?>" name="father_name">
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-4 control-label">Blood Group : </label>
															<div class="col-md-8">
																<input type="text" class="form-control" value="<?php echo $user_data->blood_group ?>" name="blood_group">
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-4 control-label">Sex : </label>
															<div class="col-md-8">
																<?php $gender = $user_data->gender; ?>
																<select class="form-control" name="gender">
																	<option value="Male"  <?php if($gender=='Male'){echo 'selected';}?> >Male</option>
																	<option value="Female"  <?php if($gender=='Female'){echo 'selected';}?> >Female</option>
																</select>
															</div>
														</div>
													</div>
													<div class="col-sm-6">
														<div class="form-group">
															<label class="col-md-4 control-label">DOB : </label>
															<div class="col-md-8">
																<div class="input-group">
																	<span class="input-group-addon">
																		<i class="fa fa-calendar"></i>
																	</span>
																	<input type="text" data-plugin-datepicker="" class="form-control" 
																	value="<?php echo date('m/d/Y', strtotime($user_data->dob)); ?>" name="dob">
																</div>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-4 control-label">Personal No : </label>
															<div class="col-md-8">
																<input type="text" class="form-control" value="<?php echo $user_data->personal_phone ?>" name="personal_phone">
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-4 control-label">Address : </label>
															<div class="col-md-8">
																<input type="text" class="form-control" value="<?php echo $user_data->address ?>" name="address">
															</div>
														</div>
													</div>
													<input type="hidden" name="Admin_No" value="<?php echo $user_data->admin_no; ?>" >
													<div class="col-sm-12">
														<div class="form-group">
															<div class="col-xs-2 col-xs-offset-9">
																<div class="add-lead-btn">
																	<input type="submit" class="mb-xs mt-xs btn btn-success" value="Submit" />
																</div>
															</div>
														</div>
													</div>
												</div>	
											</div>
										</form>	
						</div>
					</section>	
					<!-- end: page -->
				</section>
			</div>

			
		</section>

		<!-- Vendor -->
<?php include('includes/footerlinks2.php');?>
<?php include('includes/footerlinks.php');?>
		<!-- Specific Page Vendor -->
		<script src="<?= $this->config->item('admin_assets');?>vendor/select2/select2.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>

		<!-- Examples -->
		<script src="<?= $this->config->item('admin_assets');?>javascripts/tables/examples.datatables.default.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>javascripts/tables/examples.datatables.row.with.details.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>javascripts/tables/examples.datatables.tabletools.js"></script>
		
<style>
.clients-head h3 {
    margin: 0 0 20px 0;
    background: #0e80d5;
    padding: 6px 0 6px 10px;
    color: #fff;
    font-size: 22px;
}
.panel-bodygopi {
    margin: 30px 0 0 0;
}
.add-lead-btn.nav-right {
    float: right;
    margin-top: 10px;
}
</style>		
		
	</body>
</html>