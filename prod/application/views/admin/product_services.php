<!doctype html>
<html class="fixed">
	<head>

		<?php Include('includes/headerlinks.php');?>
		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="<?= $this->config->item('admin_assets');?>vendor/select2/select2.css" />
		<link rel="stylesheet" href="<?= $this->config->item('admin_assets');?>vendor/jquery-datatables-bs3/assets/css/datatables.css" />
		<?php Include('includes/headerlinks2.php');?>
	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<?php include('includes/header.php');?>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php include('includes/menu_section.php');?>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Product Services</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo $admin_url;?>">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li>
									<a href="<?php echo $admin_url;?>product">
										<span>Products</span>
									</a>
								</li>
								<li><span>Product Services</span></li>
							</ol>
						</div>
					</header>

					<!-- start: page -->
						<section class="panel">
							<header class="panel-heading">
								<div class="panel-actions">
									<a href="<?php echo $admin_url;?>product_service_add/<?= $product_ID;?>" class="add">Add Service</a>
								</div>
								<h2 class="panel-title">Product Services</h2>
							</header>
							<div class="panel-body">
								<?php/* echo '<pre>'; print_r($pro_services[0]); echo '</pre>';*/?>
								<table class="table table-bordered table-striped mb-none" id="datatable-default">
									<thead>
										<tr>
											<th class="center">S No.</th>
											<th>Product</th>
											<th>Subcategory</th>
											<th>Services</th>
											<th class="center">Actions</th>
										</tr>
									</thead>
									<tbody>
										<?php $ps=0;foreach ($pro_services as $pro_service) { $ps++;?>
										<tr class="gradeX">
											<td class="center"><?= $ps;?></td>
											<td><?= ucfirst($pro_service['prod_name']);?></td>
											<td><?= ucfirst($pro_service['sub_cat_name']);?></td>
											<td><?= ucfirst($pro_service['service']);?></td>
											<td class="center actions_td">
												<a href="<?php echo $admin_url;?>product_service_delete/<?= $pro_service['service_sno'];?>/<?= $pro_service['prod_sno'];?>" onclick="return confirm_active();" class="delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
											</td>
										</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</section>
					<!-- end: page -->
				</section>
			</div>

		</section>
		<?php include('includes/footerlinks.php');?>
		<!-- Specific Page Vendor -->
		<script src="<?= $this->config->item('admin_assets');?>vendor/select2/select2.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>

		<!-- Examples -->
		<script src="<?= $this->config->item('admin_assets');?>javascripts/tables/examples.datatables.default.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>javascripts/tables/examples.datatables.row.with.details.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>javascripts/tables/examples.datatables.tabletools.js"></script>
		<?php include('includes/footerlinks2.php');?>
	</body>
</html>