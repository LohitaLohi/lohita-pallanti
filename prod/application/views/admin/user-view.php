<!doctype html>
<html class="fixed">
	<head>
		<?php include('includes/headerfiles.php');?>
	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<?php include('includes/header.php')?>
			
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<aside id="sidebar-left" class="sidebar-left">
					<?php include('includes/sidemenu.php')?>
				
				</aside>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<?php include("includes/breadcrumbs.php")?>
					<!-- start: page -->
						<div class="live-chat-page add-page">
							<div class="row">
								<div class="col-xs-12">
									<section class="panel">
										<form class="form-horizontal form-bordered spans" action="#">
											<div class="panel-body gopi-mb">
												<div class="clients-head">
													<h3>Information</h3>
												</div>
												
												<div class="row">
													<div class="col-sm-6">
														<div class="form-group">
															<label class="col-md-4 control-label" for="inputDefault">Name : </label>
															<div class="col-md-6">
																<span><?= $user_data->admin_name;?></span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-4 control-label" for="inputDefault">Designation : </label>
															<div class="col-md-6">
																<span><?= $user_data->emp_designation;?></span>
															</div>
														</div>
														
														<div class="form-group">
															<label class="col-md-4 control-label" for="inputDefault">Rules : </label>
															<div class="col-md-6">
																<span><?php echo $user_data->admin_type?></span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-4 control-label" for="inputDefault">Phone No. : </label>
															<div class="col-md-6">
																<span><?php echo $user_data->admin_phone?></span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-4 control-label" for="inputDefault">Date of joining : </label>
															<div class="col-md-6">
																<span><?php echo $user_data->date_of_joining?></span>
															</div>
														</div>
													</div>
													<div class="col-sm-6">
														<div class="form-group">
															<label class="col-md-4 control-label" for="inputDefault">Email : </label>
															<div class="col-md-6">
																<span><?php echo $user_data->admin_id ?></span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-4 control-label" for="inputDefault">Photo : </label>
															<div class="col-md-6">
																<span><img style="max-width:80%;" src="<?php echo base_url();?>uploads/emp_images/<?php echo $user_data->admin_img; ?>" alt=""></span>
															</div>
														</div>
													</div>
												</div>	
											</div>
											<div class="panel-body gopi-mb">
												<div class="clients-head">
													<h3>Personal Information</h3>
												</div>
												<div class="row">
													<div class="col-sm-6">
														<div class="form-group">
															<label class="col-md-4 control-label" for="inputDefault">Father Name : </label>
															<div class="col-md-6">
																<span><?php echo $user_data->father_name?></span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-4 control-label" for="inputDefault">Date of Birth : </label>
															<div class="col-md-6">
																<span><?php echo $user_data->dob?></span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-4 control-label" for="inputDefault">Blood Group : </label>
															<div class="col-md-6">
																<span><?php echo $user_data->blood_group?></span>
															</div>
														</div>
													</div>
													<div class="col-sm-6">
														<div class="form-group">
															<label class="col-md-4 control-label" for="inputDefault">Personal No : </label>
															<div class="col-md-6">
																<span><?php echo $user_data->personal_phone?></span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-4 control-label" for="inputDefault">Sex : </label>
															<div class="col-md-6">
																<span><?php echo $user_data->gender?></span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-4 control-label" for="inputDefault">Address : </label>
															<div class="col-md-6">
																<span><?php echo $user_data->address?></span>
															</div>
														</div>
														<?php if($user_data->admin_type=='Manager' or $this->session->userdata('admin_type')=='Admin' ) { ?>
														<div class="col-sm-12">
														<div class="form-group">
															<div class="col-xs-2 col-xs-offset-9">
																<div class="add-lead-btn">
																	<a class="edit-page" href="<?php echo base_url()?>admin/user_edit/<?php echo $user_data->admin_no?>">Edit</a>
																</div>
															</div>
														</div>
													</div>
														<?php } ?>
													</div>
												</div>	
											</div>
										</form>	
									</section>
								</div>
							</div>
						</div>
					<!-- end: page -->
					<div class="clearfixels"></div>
				</section>
				<div class="clearfixels"></div>
			</div>

			
		</section>

		<?php include('includes/footerfiles.php');?>

	</body>
</html>