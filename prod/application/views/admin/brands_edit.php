<!doctype html>
<html class="fixed">
	<head>

		<?php Include('includes/headerlinks.php');?>

		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="<?= $this->config->item('admin_assets');?>vendor/bootstrap-fileupload/bootstrap-fileupload.min.css" />
		<?php Include('includes/headerlinks2.php');?>
	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<?php include('includes/header.php');?>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php include('includes/menu_section.php');?>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Edit Brand</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo $admin_url;?>">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><a href="<?php echo $admin_url;?>brands"><span>Brand</span></a></li>
								<li><span>Edit Brand</span></li>
							</ol>
						</div>
					</header>

					<!-- start: page -->
						<div class="row">
							<div class="col-xs-12">
								<section class="panel">
									<header class="panel-heading">
										<h2 class="panel-title">Edit Brand</h2>
									</header>
									<div class="panel-body">
											<?php $brand_data = $brand_data['0'];?>
										<?php $action = $admin_url."brand_insert"; 
									 	echo form_open_multipart($action,array('id'=>"fm_brand_add",'class'=>"form-horizontal form-bordered custom_form",'autocomplete'=>"off")); ?>
											<div class="form-group">
												<label class="col-md-3 control-label">Brand Name <span class="colon">:</span></label>
												<div class="col-md-6">
													<input class="form-control custom_required" name="brand_name" value="<?= $brand_data['brand_name'];?>" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-3 control-label">Present Brand Image <span class="colon">:</span></label>
												<div class="col-md-6">
													<img style="max-width:100%;" src="<?= base_url();?>assets/images/brands/<?= $brand_data['brand_img'];?>">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-3 control-label">Upload New Brand Image<span class="colon">:</span></label>
												<div class="col-md-6">
													<div class="fileupload fileupload-new" data-provides="fileupload">
														<div class="input-append">
															<div class="uneditable-input">
																<i class="fa fa-file fileupload-exists"></i>
																<span class="fileupload-preview"></span>
															</div>
															<span class="btn btn-default btn-file">
																<span class="fileupload-exists">Change</span>
																<span class="fileupload-new">Select file</span>
																<input class=" " id="image_required1" name="brand_img" accept="image/*" type="file" />
															</span>
															<a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
														</div>
													</div>
												</div>
											</div>
											<input type="hidden" name="old_brand_image" value="<?= $brand_data['brand_img'];?>">
											<input type="hidden" name="brand_sno" value="<?= $brand_data['brand_sno'];?>">
											<div class="form-group">
												<label class="col-md-3 control-label"></label>
												<div class="col-md-6">
													<p style="text-align:left;color:red;font-size:13px;"><?php if(isset($error)){ echo $error;}?></p>
													<button type="submit" onclick='return validate_form();'  class="btn btn-primary custom_submit_btm" name="brand_update">Update</button>
												</div>
											</div>
										<?php form_close();?>
									</div>
								</section>
							</div>
						</div>
					<!-- end: page -->
				</section>
			</div>
		</section>

		<?php include('includes/footerlinks.php');?>
		
		<!-- Specific Page Vendor -->
		<script src="<?= $this->config->item('admin_assets');?>vendor/jquery-autosize/jquery.autosize.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
		
		<?php include('includes/footerlinks2.php');?>


		<!-- Examples -->
		<script src="<?= $this->config->item('admin_assets');?>javascripts/forms/examples.advanced.form.js"></script>

	</body>
</html>