<!doctype html>
<html class="fixed">
	<head>

		<?php Include('includes/headerlinks.php');?>
		<?php Include('includes/headerlinks2.php');?>
	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<?php include('includes/header.php');?>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php include('includes/menu_section.php');?>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Edit Service</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo $admin_url;?>">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><a href="<?php echo $admin_url;?>product"><span>Products</span></a></li>
								<li><a href="<?php echo $admin_url;?>product_features"><span>Services</span></a></li>
								<li><span>Edit Service</span></li>
							</ol>
						</div>
					</header>

					<!-- start: page -->
						<div class="row">
							<div class="col-xs-12">
								<section class="panel">
									<header class="panel-heading">
										<h2 class="panel-title">Edit Service</h2>
									</header>
									<div class="panel-body">
										<form class="form-horizontal form-bordered custom_form" action="#" method="post">
											<div class="form-group">
												<label class="col-md-3 control-label">Service <span class="colon">:</span></label>
												<div class="col-md-6">
													<input class="form-control custom_required" name="menu_name" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-3 control-label"></label>
												<div class="col-md-6">
													<button type="submit" onclick='return validate_form();'  class="btn btn-primary custom_submit_btm" name="menu_add">Submit</button>
												</div>
											</div>
						
										</form>
									</div>
								</section>
							</div>
						</div>
					<!-- end: page -->
				</section>
			</div>
		</section>

		<?php include('includes/footerlinks.php');?>
		
		<?php include('includes/footerlinks2.php');?>


		<!-- Examples -->
		<script src="<?= $this->config->item('admin_assets');?>javascripts/forms/examples.advanced.form.js"></script>

	</body>
</html>