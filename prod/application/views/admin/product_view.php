<!doctype html>
<html class="fixed">
	<head>

		<?php Include('includes/headerlinks.php');?>
		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="<?= $this->config->item('admin_assets');?>vendor/select2/select2.css" />
		<link rel="stylesheet" href="<?= $this->config->item('admin_assets');?>vendor/bootstrap-multiselect/bootstrap-multiselect.css" />
		<?php Include('includes/headerlinks2.php');?>
	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<?php include('includes/header.php');?>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php include('includes/menu_section.php');?>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Add Product</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo $admin_url;?>">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><a href="<?php echo $admin_url;?>product"><span>Products</span></a></li>
								<li><span>Add Product</span></li>
							</ol>
						</div>
					</header>

					<!-- start: page -->
					<div class="row">
						<div class="col-xs-12">
							<section class="panel">
								<header class="panel-heading">
									<h2 class="panel-title">Add Product</h2>
								</header>
								<div class="panel-body">
									<?php 
									$product_details = $product_details[0]; 
									/*
									 echo '<pre>'; 
									 print_r($product_details); echo '</pre>';
									 */?>
									<div class="form-group">
										<label class="col-md-3 control-label">Category <span class="colon">:</span></label>
										<div class="col-md-6">
											<p><?= ucwords($product_details['cat_name']);?></p>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Subcategory <span class="colon">:</span></label>
										<div class="col-md-6" stylesheet="position:relative" id="prod_sub_cat_div">
											<p><?= ucwords($product_details['sub_cat_name']);?></p>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-3 control-label">Brands<span class="colon">:</span></label>
										<div class="col-md-6" id="brands_div">
											<div class="brands_view">
												<?php if(count($product_brands)){?>
												<ul class="brands_view_ul">
													<?php foreach ($product_brands as $product_brand) { ?>
													<li class="brands_view_li"><?= ucfirst($product_brand['brand_name']);?></li>
													<?php } ?>
												</ul>
												<?php } else { echo "<p>No brands available.</p>";}?>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Product Name <span class="colon">:</span></label>
										<div class="col-md-6">
											<p><?= ucwords($product_details['prod_name']);?></p>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Product Tags <span class="colon">:</span></label>
										<div class="col-md-6">
											<p><?= $product_details['prod_tags'];?></p>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Colors<span class="colon">:</span></label>
										<div class="col-md-6" id="brands_div">
											<?php if(count($product_colors)){?>
											<div class="colors_view">
												<ul class="colors_view_ul">
													<?php foreach ($product_colors as $product_color) { ?>
													<li class="colors_view_li">
														<p class="c_name"><?= ucfirst($product_color['color_name']);?></p>
														<p class="c_code" style="background-color:<?= ucfirst($product_color['color_code']);?>"></p>
													</li>
													<?php } ?>
												</ul>
												<?php } else { echo "<p>No colors available.</p>";}?>
											</div>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-3 control-label">Price (Rs.)<span class="colon">:</span></label>
										<div class="col-md-6">
											<p><?= $product_details['prod_price'];?></p>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Discount (%)<span class="colon">:</span></label>
										<div class="col-md-6">
											<p><?= $product_details['prod_discount'];?></p>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Final Price (Rs.)<span class="colon">:</span></label>
										<div class="col-md-6">
											<p><?= $product_details['prod_dis_price'];?></p>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Stock<span class="colon">:</span></label>
										<div class="col-md-6">
											<p><?= $product_details['prod_stock'];?></p>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Stock Remaining<span class="colon">:</span></label>
										<div class="col-md-6">
											<p><?= $product_details['prod_stock_rem'];?></p>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Description<span class="colon">:</span></label>
										<div class="col-md-9">
											<div><?= $product_details['prod_des'];?></divx>
										</div>
									</div>
								</div>
							</section>
						</div>
					</div>
					<!-- end: page -->
				</section>
			</div>
		</section>

		<?php include('includes/footerlinks.php');?>
		<!-- Specific Page Vendor -->
		<script src="<?= $this->config->item('admin_assets');?>vendor/select2/select2.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>
		<?php include('includes/footerlinks2.php');?>
		<!-- Examples -->
		<script src="<?= $this->config->item('admin_assets');?>javascripts/forms/examples.advanced.form.js"></script>

		<script>
			$( document ).ready(function() {
			$('#summernote').summernote({height: 300});
			});
		</script>
		<script src="<?= $this->config->item('admin_assets');?>wysiwyg_editor/summernote.min.js"></script>
		<link href="<?= $this->config->item('admin_assets');?>wysiwyg_editor/summernote.css" rel="stylesheet">
		<link href="<?= $this->config->item('admin_assets');?>wysiwyg_editor/summernote-bs3.css" rel="stylesheet">
<script>
$(document).ready(function(){
	$('#prod_cat_sno').change(function(){
		var cat_id = $(this).val();
		var gg = $("input[name=csrf_test_name]").val();
		var form_data = {
	        cat_id: cat_id,
	        csrf_test_name:gg
	    };
	    $('#s2id_prod_sub_cat_sno #select2-chosen-2').html('Select Subcategory');
	    $('#s2id_prod_brands .select2-choices .select2-search-choice').html('')
	    $('#s2id_prod_brands .select2-choices .select2-search-choice').css({'padding':'0px'});
		$.ajax({
			type:"POST",
			url:"<?php echo base_url();?>admin/ajax_subcat_list",
			data:form_data
		}).done(function(msg){
			$('#chinthala').fadeOut();
			$("#prod_sub_cat_sno").html(msg);
		});
	});
	$('#prod_sub_cat_sno').change(function(){
		var sub_cat_id = $(this).val();
		var sss = $("input[name=csrf_test_name]").val();
		var form_data0 = {
	        sub_cat_id: sub_cat_id,
	        csrf_test_name:sss
	    };
	    $('#s2id_prod_brands .select2-choices .select2-search-choice').html('')
	    $('#s2id_prod_brands .select2-choices .select2-search-choice').css({'padding':'0px'});
		$.ajax({
			type:"POST",
			url:"<?php echo base_url();?>admin/ajax_brands_list",
			data:form_data0
		}).done(function(msg1){
			$("#prod_brands").html(msg1);
		});
	});
});
/*	  	*/
</script>

<script>
$(document).ready(function() {
	$("#prod_price, #prod_dis_price").keyup(function() {
		var p = $("#prod_price").val();
		var q = $("#prod_dis_price").val();
		var x= Math.round(100- (q/p*100));
		$("#prod_discount").val(x);
	});
	$("#prod_discount").keyup(function() {
		var p = $("#prod_price").val();
		var q = $("#prod_discount").val();
		var x= Math.round(p-(q*(p/100)));
		$("#prod_dis_price").val(x);
	});
});
</script>
	</body>
</html>