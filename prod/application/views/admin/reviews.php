<!doctype html>
<html class="fixed">
	<head>

		<?php Include('includes/headerlinks.php');?>
		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="<?= $this->config->item('admin_assets');?>vendor/select2/select2.css" />
		<link rel="stylesheet" href="<?= $this->config->item('admin_assets');?>vendor/jquery-datatables-bs3/assets/css/datatables.css" />
		<?php Include('includes/headerlinks2.php');?>
	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<?php include('includes/header.php');?>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php include('includes/menu_section.php');?>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Reviews</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo $admin_url;?>">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span>Reviews</span></li>
							</ol>
						</div>
					</header>

					<!-- start: page -->
						<section class="panel">
							<header class="panel-heading">
							<h2 class="panel-title">Reviews</h2>
							</header>
							<div class="panel-body">
								<?php  /*echo '<pre>'; print_r($reviews); echo '</pre>'; */?>
								<table class="table table-bordered table-striped mb-none center" id="datatable-default">
									<thead>
										<tr>
											<th class="center">S No.</th>
											<th class="center">Product Name</th>
											<th>Name</th>
											<th class="center">Description</th>
											<th class="center">Rating</th>
											<th class="center">Status</th>
											<th class="center">Actions</th>
										</tr>
									</thead>
									<tbody>
										<?php 
										$bb=0; foreach ($reviews as $review) { $bb++; ?>
										<tr class="gradeX">
											<td class="center"><?= $bb;?></td>
											<td><?=  ucwords($review['prod_name']);?></td>
											<td><?=  ucfirst($review['first_name']).' '.ucfirst($review['last_name']);?></td>
											<td><?=  $review['description'];?></td>
											<td class="center"><?=  $review['rating'];?></td>
											<td class="center">
												<?php if($review['status'] =='0'){ echo 'New';} 
													else if($review['status'] =='1'){echo 'Active'; } 
													else if($review['status'] =='2'){echo 'Rejected';} 
												?>
											</td>
											<td class="center actions_td">									
												<?php if($review['status'] =='0'){ ?>
												<a data-toggle="tooltip" title="Make Active" href="<?php echo $this->config->item('admin_url');?>review_active/<?= $review['sno']; ?>" onclick="return confirm_active();" class="delete waiting"><i class="fa fa-check " aria-hidden="true"></i></a>
												<a data-toggle="tooltip" title="Make Reject" href="<?php echo $this->config->item('admin_url');?>review_reject/<?= $review['sno']; ?>" onclick="return confirm_inactive();" class="delete waiting"><i class="fa fa-times" aria-hidden="true"></i></a>

												<?php  } else if($review['status'] =='1'){ ?>
												<a data-toggle="tooltip" title="Make Reject" href="<?php echo $this->config->item('admin_url');?>review_reject/<?= $review['sno']; ?>" onclick="return confirm_active();" class="delete active"><i class="fa fa-check" aria-hidden="true"></i></a>

												<?php } else if($review['status'] =='2'){ ?>
												<a data-toggle="tooltip" title="Make Active" href="<?php echo $this->config->item('admin_url');?>review_active/<?= $review['sno']; ?>" onclick="return confirm_active();" class="delete inactive"><i class="fa fa-times" aria-hidden="true"></i></a>
												<?php  } ?>

												<a data-toggle="tooltip" title="Delete" href="<?php echo $this->config->item('admin_url');?>review_delete/<?= $review['sno']; ?>" onclick="return confirm_delete();" class="delete delete2"><i class="fa fa-trash" aria-hidden="true"></i></a>
											</td>
										</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</section>
					<!-- end: page -->
				</section>
			</div>

		</section>
		<?php include('includes/footerlinks.php');?>
		<!-- Specific Page Vendor -->
		<script src="<?= $this->config->item('admin_assets');?>vendor/select2/select2.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>

		<!-- Examples -->
		<script src="<?= $this->config->item('admin_assets');?>javascripts/tables/examples.datatables.default.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>javascripts/tables/examples.datatables.row.with.details.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>javascripts/tables/examples.datatables.tabletools.js"></script>
		<?php include('includes/footerlinks2.php');?>
	</body>
</html>