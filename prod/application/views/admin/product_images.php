<!doctype html>
<html class="fixed">
	<head>

		<?php Include('includes/headerlinks.php');?>
		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="<?= $this->config->item('admin_assets');?>vendor/select2/select2.css" />
		<link rel="stylesheet" href="<?= $this->config->item('admin_assets');?>vendor/jquery-datatables-bs3/assets/css/datatables.css" />
		<?php Include('includes/headerlinks2.php');?>
	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<?php include('includes/header.php');?>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php include('includes/menu_section.php');?>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Product Images</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo $admin_url;?>">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li>
									<a href="<?php echo $admin_url;?>product">
										<span>Products</span>
									</a>
								</li>
								<li><span>Product Images</span></li>
							</ol>
						</div>
					</header>
					<!-- start : page -->
					<section class="panel">
						<header class="panel-heading">
							<div class="panel-actions">
								<a href="<?php echo $admin_url;?>product_images_add/<?= $product_ID?>" class="add">Add Images</a>
							</div>
						<h2 class="panel-title">Product Images</h2>
						</header>
						<div class="panel-body">
							 
							<table class="table table-bordered table-striped mb-none" id="datatable-default">
								<thead>
									<tr>
										<th class="center">S.No</th>
										<th class="center">Image Name</th>
										<th class="center">Image Alt Tag</th>
										<th class="center"  style="width:20%;">Image</th>
										<th class="center">Actions</th>
									</tr>
								</thead>
								<tbody>
									<?php $pi=0; foreach ($pro_images as $pro_image) { $pi++;?>
									<tr class="gradeX">
										<td class="center" style="vertical-align: middle;"><?= $pi;?></td>
										<td style="vertical-align: middle;"><?= $pro_image['product_img_name'];?></td>
										<td style="vertical-align: middle;">
											<?php $img_alt = $pro_image['product_img_alt_tag'];
											if($img_alt == ''){ 
												echo "---<br><br> <a href='".$admin_url."product_images_addtag/".$pro_image['prod_sno']."/".$pro_image['product_img_sno']."'>Add Tag</a>";
											} else {
												echo $img_alt."<br><br> <a href='".$admin_url."product_images_edittag/".$pro_image['prod_sno']."/".$pro_image['product_img_sno']."'>Edit Tag</a>";
											}
											?>
										</td>
										<td style="width:20%;">
											<?php
											$except = array(' ','(',')','\\', '/', ':', '*', '?', '"', '<', '>', '|');
											$sub_cat_name = str_replace($except, "_", strtolower($pro_image['sub_cat_name']));
											$product_name = str_replace($except, "_", strtolower($pro_image['prod_name']));
											?>
											<img src="<?php echo base_url().'assets/images/gallery/'.$sub_cat_name.'/'.$product_name.'/'.$pro_image['product_img_name'];?>" style="max-width: 100%" alt="Road No:One" >
										</td>
										<td class="center actions_td">
											<a href="<?php echo $admin_url;?>product_image_delete/<?= $pro_image['prod_sno'];?>/<?= $pro_image['product_img_sno'];?>" class="delete"  onclick="return confirm_delete();" ><i class="fa fa-trash" aria-hidden="true"></i></a>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</section>
					<!-- end: page -->
				</section>
			</div>

		</section>
		<?php include('includes/footerlinks.php');?>
		<!-- Specific Page Vendor -->
		<script src="<?= $this->config->item('admin_assets');?>vendor/select2/select2.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>

		<!-- Examples -->
		<script src="<?= $this->config->item('admin_assets');?>javascripts/tables/examples.datatables.default.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>javascripts/tables/examples.datatables.row.with.details.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>javascripts/tables/examples.datatables.tabletools.js"></script>
		<?php include('includes/footerlinks2.php');?>
	</body>
</html>