<?php 
//echo "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
$purl_file = "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
if (strpos($purl_file,'index.php') !== false){ $dashboard_active = 'nav-active';}else{$dashboard_active = '';}
if (strpos($purl_file,'menu') !== false){ $menu_active = 'nav-active';}else{$menu_active = '';} 
// menu,menu_add, menu_edit
if (strpos($purl_file,'/category') !== false){ $category_active = 'nav-active';}else{$category_active = '';} 
if (strpos($purl_file,'brands') !== false){ $brands_active = 'nav-active';}else{$brands_active = '';} 
if (strpos($purl_file,'subcategory') !== false){ $subcategory_active = 'nav-active';}else{$subcategory_active = '';} 
if (strpos($purl_file,'colors') !== false){ $colors_active = 'nav-active';}else{$colors_active = '';} 
if (strpos($purl_file,'pincode') !== false){ $pincode_active = 'nav-active';}else{$pincode_active = '';} 
if (strpos($purl_file,'product') !== false){ $product_active = 'nav-active';}else{$product_active = '';} 
if (strpos($purl_file,'subscriber') !== false){ $subscriber_active = 'nav-active';}else{$subscriber_active = '';} 
if (strpos($purl_file,'enquir') !== false){ $enquiry_active = 'nav-active';}else{$enquiry_active = '';} 
if (strpos($purl_file,'reviews') !== false){ $reviews_active = 'nav-active';}else{$reviews_active = '';} 
if (strpos($purl_file,'banners') !== false){ $banners_active = 'nav-active';}else{$banners_active = '';} 
if (strpos($purl_file,'enquiries') !== false){ $enquiries_active = 'nav-active';}else{ $enquiries_active = '';} 
if (strpos($purl_file,'users') !== false){ $users_active = 'nav-active';}else{$users_active = '';} 
if (strpos($purl_file,'orders') !== false || strpos($purl_file,'order_info') !== false || strpos($purl_file,'order_info') !== false || strpos($purl_file,'order_info') !== false){ $orders_active = 'nav-active';} else{ $orders_active = '';} 
if(strpos($presentURL,'employee')==true)
{
	 $EmpSupport = 'nav-active';
}
else{
	$EmpSupport = ''; 
}	
//echo $this->session->userdata('admin_type_id'); exit();
?>

<aside id="sidebar-left" class="sidebar-left">
	<div class="sidebar-header">
		<div class="sidebar-title">
			Navigation
		</div>
		<div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
			<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
		</div>
	</div>
	<div class="nano">
		<div class="nano-content">
			<nav id="menu" class="nav-main" role="navigation">
				<ul class="nav nav-main">
					<!--
					<li class="nav-parent">
						<a>
							<i class="fa fa-copy" aria-hidden="true"></i>
							<span>Pages</span>
						</a>
						<ul class="nav nav-children">
							<li>
								<a href="<?php echo $this->config->item('admin_url');?>pages-signup.html">
									 Sign Up
								</a>
							</li>
						</ul>
					</li>
					-->
					<li class="<?= $dashboard_active;?>">
						<a href="<?php echo $this->config->item('admin_url');?>index">
							<i class="fa fa-dashboard" aria-hidden="true"></i>
							<span>Dashboard </span>
						</a>
					</li>
					<?php if($this->session->userdata('admin_type_id')=="1"){?>
					
					<li class="<?= $menu_active;?>">
						<a href="<?php echo $this->config->item('admin_url');?>menu">
							<i class="fa fa-tty" aria-hidden="true"></i>
							<span>Menu</span>
						</a>
					</li>
					<li class="<?= $category_active;?>">
						<a href="<?php echo $this->config->item('admin_url');?>category">
							<i class="fa fa-caret-square-o-down" aria-hidden="true"></i>
							<span>Category</span>
						</a>
					</li>
					<li class="<?= $brands_active;?>">
						<a href="<?php echo $this->config->item('admin_url');?>brands">
							<i class="fa fa-building" aria-hidden="true"></i>
							<span>Brands</span>
						</a>
					</li>
					<li class="<?= $subcategory_active;?>">
						<a href="<?php echo $this->config->item('admin_url');?>subcategory">
							<i class="fa fa-caret-square-o-down" aria-hidden="true"></i>
							<span>Subcategory</span>
						</a>
					</li>
				
					<li class="<?= $product_active;?>">
						<a href="<?php echo $this->config->item('admin_url');?>product">
							<i class="fa fa-image" aria-hidden="true"></i>
							<span>Products</span>
						</a>
					</li>
					
					<?php
					
					 
					  ?>
					
					
						<?php if($this->uri->segment(3)=='user'){
					    
					    $user_active = 'nav-active';
					    
					}  ?>
					 
					
                           
				
				    <li class="<?php echo $orders_active ?>">
						<a href="<?php echo $this->config->item('admin_url');?>orders">
							<i class="fa fa-reorder" aria-hidden="true"></i>
							<span>Orders</span>
						</a>
					</li>
				
				
					
					<?php } else{ ?>
					
			        <?php } if($this->session->userdata('admin_type_id')=="4"){ ?>
					
					<?php } ?>
				
				
				
				</ul>
			</nav>
		</div>
	</div>
</aside>