
<!-- Vendor -->
<script src="<?php echo $this->config->item('admin_assets');?>vendor/jquery/jquery.js"></script>
<script src="<?php echo $this->config->item('admin_assets');?>vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
<script src="<?php echo $this->config->item('admin_assets');?>vendor/bootstrap/js/bootstrap.js"></script>
<script src="<?php echo $this->config->item('admin_assets');?>vendor/nanoscroller/nanoscroller.js"></script>
<script src="<?php echo $this->config->item('admin_assets');?>vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?php echo $this->config->item('admin_assets');?>vendor/magnific-popup/magnific-popup.js"></script>
<script src="<?php echo $this->config->item('admin_assets');?>vendor/jquery-placeholder/jquery.placeholder.js"></script>
