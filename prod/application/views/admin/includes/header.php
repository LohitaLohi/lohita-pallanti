
<link rel="icon" type="<?php echo base_url();?>assest/frontend/image/png" sizes="16x16" 
href="<?php echo base_url();?>assets/frontend/images/clothes/logo.jpg">

<header class="header">
	<div class="logo-container">
		<a href="../" class="logo">
			<img src="<?php echo base_url();?>assets/frontend/images/clothes/logo.jpg" height="60" width="60" alt="Porto Admin" />
		</a>
		<div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
			<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
		</div>
	</div>
	<!-- start: search & user box -->
	<div class="header-right">
		<div id="userbox" class="userbox">
			<a href="#" data-toggle="dropdown">
				<figure class="profile-picture">
					<img src="<?php echo $this->config->item('admin_assets');?>images/!logged-user.jpg" alt="Joseph Doe" class="img-circle" data-lock-picture="assets/images/!logged-user.jpg" />
				</figure>
				<div class="profile-info" data-lock-name="John Doe" data-lock-email="johndoe@okler.com">
					<span class="name"> <?php echo ucfirst($this->session->userdata('admin_name'))?></span>
					<?php if($this->session->userdata('admin_type_id')=="4"){?>
					<span class="role">Delivery Boy</span>
					<?php }?>
					<?php if($this->session->userdata('admin_type_id')=="1"){?>
					<span class="role">Admin</span>
					<?php }?>
				</div>
				<i class="fa custom-caret"></i>
			</a>
			<div class="dropdown-menu">
				<ul class="list-unstyled">
					<li class="divider"></li>
					<li>
						<a role="menuitem" tabindex="-1" href="<?php echo base_url();?>admin/logout"><i class="fa fa-power-off"></i> Logout</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- end: search & user box -->
</header>

<?php 
$admin_url = $this->config->item('admin_url');
?>