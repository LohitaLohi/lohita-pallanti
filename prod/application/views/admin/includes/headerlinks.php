<!-- Basic -->
<meta charset="UTF-8">

<title>Clothes</title>
<meta name="keywords" content="BostonBooks" />
<meta name="description" content="BostonBooks">
<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

<!-- Web Fonts  -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

<!-- Vendor CSS -->
<link rel="stylesheet" href="<?php echo $this->config->item('admin_assets');?>vendor/bootstrap/css/bootstrap.css" />

<link rel="stylesheet" href="<?php echo $this->config->item('admin_assets');?>vendor/font-awesome/css/font-awesome.css" />
<link rel="stylesheet" href="<?php echo $this->config->item('admin_assets');?>vendor/magnific-popup/magnific-popup.css" />
<link rel="stylesheet" href="<?php echo $this->config->item('admin_assets');?>vendor/bootstrap-datepicker/css/datepicker3.css" />



