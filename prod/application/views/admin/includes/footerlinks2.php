<!-- Theme Base, Components and Settings -->
<script src="<?php echo $this->config->item('admin_assets');?>javascripts/theme.js"></script>

<!-- Theme Custom -->
<script src="<?php echo $this->config->item('admin_assets');?>javascripts/theme.custom.js"></script>

<!-- Theme Initialization Files -->
<script src="<?php echo $this->config->item('admin_assets');?>javascripts/theme.init.js"></script>



<script>
function confirm_inactive()
{
	var confirmDel = confirm("Are you sure to In-activate.?");
	if(confirmDel){ return true; }
	else{ return false; }
}
function confirm_active()
{
	var confirmDel = confirm("Are you sure to Activate.?");
	if(confirmDel){ return true; }
	else{ return false; }
}
function confirm_delete()
{
	var confirmDel = confirm("Are you sure to delete.?");
	if(confirmDel){	return true; }
	else{ return false; }
}

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip({html: true}); 
});
</script>