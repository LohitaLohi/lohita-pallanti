<!-- Theme CSS -->
<link rel="stylesheet" href="<?php echo $this->config->item('admin_assets');?>stylesheets/theme.css" />

<!-- Skin CSS -->
<link rel="stylesheet" href="<?php echo $this->config->item('admin_assets');?>stylesheets/skins/default.css" />

<!-- Theme Custom CSS -->
<link rel="stylesheet" href="<?php echo $this->config->item('admin_assets');?>stylesheets/theme-custom.css">

<!-- Head Libs -->
<script src="<?php echo $this->config->item('admin_assets');?>vendor/modernizr/modernizr.js"></script>