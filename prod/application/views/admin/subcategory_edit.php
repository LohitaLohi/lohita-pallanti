<!doctype html>
<html class="fixed">
	<head>

		<?php Include('includes/headerlinks.php');?>
		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="<?= $this->config->item('admin_assets');?>vendor/select2/select2.css" />
		<link rel="stylesheet" href="<?= $this->config->item('admin_assets');?>vendor/bootstrap-multiselect/bootstrap-multiselect.css" />
		<?php Include('includes/headerlinks2.php');?>
	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<?php include('includes/header.php');?>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php include('includes/menu_section.php');?>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Edit SubCategory</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo $admin_url;?>">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><a href="<?php echo $admin_url;?>subcategory"><span>SubCategory</span></a></li>
								<li><span>Edit SubCategory</span></li>
							</ol>
						</div>
					</header>

					<!-- start: page -->
						<div class="row">
							<div class="col-xs-12">
								<section class="panel">
									<header class="panel-heading">
										<h2 class="panel-title">Edit SubCategory</h2>
									</header>
									<div class="panel-body">
										<?php 
											//echo '<pre>';
											//print_r($brands_dropdown); 
											//print_r($category_dropdown); 
											//print_r($subcat_info);
											$subcat_info = $subcat_info['0'];
											//echo '</pre>'; 
										?>
										<?php $action = $admin_url."subcat_update"; 
									 	echo form_open($action,array('id'=>"fm_brand_add",'class'=>"form-horizontal form-bordered custom_form",'autocomplete'=>"off","enctype"=>"multipart/form-data")); ?>
											<div class="form-group">
												<label class="col-md-3 control-label">Category <span class="colon">:</span></label>
												<div class="col-md-6">
													<select data-plugin-selectTwo class="form-control populate" name="cat_id" required="required">
														<option value="">Select Category</option>
														<?php foreach ($category_dropdown as $category_dt) { ?>
															<option value="<?= $category_dt["cat_sno"];?>" <?php if($subcat_info['cat_id'] == $category_dt["cat_sno"]){echo 'selected';}?> ><?= ucwords($category_dt["cat_name"]);?></option>
														<?php } ?>
													</select>
												</div>
											</div>

											<div class="form-group">
												<label class="col-md-3 control-label">SubCategory Name <span class="colon">:</span></label>
												<div class="col-md-6">
													<input class="form-control custom_required" value="<?= $subcat_info['sub_cat_name'];?>" name="subcategory_name" />
												</div>
											</div>
<?php
//echo '<pre>';
$newArray = array();
//$diff = array();
$finalbrandArray = array();
$subcarbrands0 = $subcat_info['brand_names'];
foreach($subcarbrands0 as $key => $val){
    $newArray[$val['brand_sno'] . '-' . $val['brand_name']] = $val;
    //$newArray[$val['brand_sno'] . '-' . $val['brand_name']]['sub_cat_brand_id'] = $val;
}
//$project_banks0 = array();
$cc= -1;
foreach($brands_dropdown as $key => $val){ $cc++;
    if(isset($newArray[$val['brand_sno'] . '-' . $val['brand_name']])){
        //$project_banks0[] = $newArray[$val['brand_sno'] . '-' . $val['brand_name']];
        $finalbrandArray[$cc] = $val;
        $finalbrandArray[$cc]['exist'] = "1";
        //$finalbrandArray[$cc]['sub_cat_brand_id'] = $newArray[$val['brand_sno'] . '-' . $val['brand_name']]['sub_cat_brand_id'];
    }
	else{
		//echo 'Not Equal <br>';
		//$diff[] = $val;
		$finalbrandArray[$cc] = $val;
		$finalbrandArray[$cc]['exist'] = "0";
		//$finalbrandArray[$cc]['sub_cat_brand_id'] = $newArray[$val['brand_sno'] . '-' . $val['brand_name']]['sub_cat_brand_id'];
	}
}
/*
print_r($newArray);
print_r($diff);
print_r($project_banks0);
print_r($finalbrandArray);
echo '</pre>';
*/

?>
											<div class="form-group">
												<label class="col-md-3 control-label">Brands <span class="colon">:</span></label>
												<div class="col-md-6">
													<select name="brand_id[]" multiple data-plugin-selectTwo class="form-control populate " required="required">
														<?php foreach ($finalbrandArray as $brands_dt) { ?>
															<option value="<?= $subcat_info['sub_cat_sno'].'__'.$brands_dt["brand_sno"].'__'.$brands_dt["brand_name"];?>" <?php if($brands_dt["exist"] == "1"){echo 'selected';}?> ><?= ucwords($brands_dt["brand_name"]);?></option>
														<?php } ?>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-3 control-label">Position<span class="colon">:</span></label>
												<div class="col-md-6">
													<input class="form-control number custom_required" onkeypress="return onlyNos(event,this);"  name="subcat_pos" value="<?= $subcat_info['sub_cat_position_no'];?>" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-3 control-label">Category Status<span class="colon">:</span></label>
												<div class="col-md-6">
													<?php $status = $subcat_info['sub_cat_status'];?>
													<select data-plugin-selectTwo class="form-control populate custom_required" name="sub_cat_status" >
														<option value="1" <?php if($status == '1'){echo 'selected';}?> >Active</option>
														<option value="0" <?php if($status == '0'){echo 'selected';}?> >In-active</option>
													</select>
												</div>
											</div>
											<label class="col-md-3 control-label">SubCategory Name <span class="colon">:</span></label>	
											
											<input type="hidden" name="sub_cat_sno" value="<?= $subcat_info['sub_cat_sno'];?>" />
											
											  <div class="form-group col-md-6">
											      
											      <input type="file" name="sub_cat_img"> (Size Should be 400px X 400px);
											      
											   
												
											      
											  </div>     
											
											
											
											<div class="form-group">
												<label class="col-md-3 control-label"></label>
												
												
												
												<div class="col-md-6">
													<button type="submit" onclick='return validate_form();'  class="btn btn-primary custom_submit_btm" name="subcategory_update">Submit</button>
												</div>
											</div>
						
										<?php echo form_close();?>
									</div>
								</section>
							</div>
						</div>
					<!-- end: page -->
				</section>
			</div>
		</section>

		<?php include('includes/footerlinks.php');?>
		<!-- Specific Page Vendor -->
		<script src="<?= $this->config->item('admin_assets');?>vendor/select2/select2.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>
		<?php include('includes/footerlinks2.php');?>
		<!-- Examples -->
		<script src="<?= $this->config->item('admin_assets');?>javascripts/forms/examples.advanced.form.js"></script>

	</body>
</html>