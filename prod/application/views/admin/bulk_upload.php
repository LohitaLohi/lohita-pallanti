<!doctype html>
<html class="fixed">
	<head>

		<?php Include('includes/headerlinks.php');?>

		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="<?= $this->config->item('admin_assets');?>vendor/bootstrap-fileupload/bootstrap-fileupload.min.css" />
		<?php Include('includes/headerlinks2.php');?>
	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<?php include('includes/header.php');?>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php include('includes/menu_section.php');?>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Edit Brand</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo $admin_url;?>">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><a href="<?php echo $admin_url;?>products"><span>Products</span></a></li>
								<li><span>Bulk Upload Products </span></li>
							</ol>
						</div>
					</header>

					<!-- start: page -->
						<div class="row">
							<div class="col-xs-12">
								<section class="panel">
									<header class="panel-heading">
										<h2 class="panel-title">Bulk Upload </h2>
									</header>
									<div class="panel-body">
										 
										<?php $action = $admin_url."product_bulk"; 
									 	echo form_open_multipart($action,array('id'=>"fm_brand_add",'class'=>"form-horizontal form-bordered custom_form",'autocomplete'=>"off")); ?>
										 
											<div class="form-group">
												<label class="col-md-3 control-label">Upload products<span class="colon">:</span></label>
												<div class="col-md-6">
													<div class="fileupload fileupload-new" data-provides="fileupload">
														<div class="input-append">
															<div class="uneditable-input">
																<i class="fa fa-file fileupload-exists"></i>
																<span class="fileupload-preview"></span>
															</div>
															<span class="btn btn-default btn-file">
															 
																<span class="fileupload-new">Select file (upload xls or csv files)</span>
																
																
																<input   id="excel_file" name="product_file"   type="file" />
															</span>
														 
														</div>
													</div>
												</div>
											</div>
										 
											<div class="form-group">
												<label class="col-md-3 control-label"></label>
												<div class="col-md-6">
													<p style="text-align:left;color:red;font-size:13px;"><?php if(isset($error)){ echo $error;}?></p>
													<button type="submit"   class="btn btn-primary custom_submit_btm" name="bulk_upload"> Upload Products </button>
												</div>
											</div>
										<?php form_close();?>
									</div>
								</section>
							</div>
						</div>
					<!-- end: page -->
				</section>
			</div>
		</section>

		<?php include('includes/footerlinks.php');?>
		
		<!-- Specific Page Vendor -->
		<script src="<?= $this->config->item('admin_assets');?>vendor/jquery-autosize/jquery.autosize.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
		
		<?php include('includes/footerlinks2.php');?>


		<!-- Examples -->
		<script src="<?= $this->config->item('admin_assets');?>javascripts/forms/examples.advanced.form.js"></script>




<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
					
						<script>
					 
$(document).ready(function(){
    
    
    
      $('#excel_file').change(function(){
          
          
          
          
           var names = ['xls', 'csv'];
           var name = $(this).val().split('.').pop();
           if ($.inArray(name, names)!='-1') {
                
                alert("Please Upload Xls or csv Formatted files");
                
               $('.custom_submit_btm').show();
         }else{
             
               alert("Please Upload Xls or csv Formatted files");
             
              $('.err_msg').show();
              $('.custom_submit_btm').hide();
            }
            
  
            
            
      });
    
     
    
});
       
</script>
	</body>
</html>