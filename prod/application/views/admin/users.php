<!doctype html>
<html class="fixed">
	<head>

		<?php Include('includes/headerlinks.php');?>
		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="<?= $this->config->item('admin_assets');?>vendor/select2/select2.css" />
		<link rel="stylesheet" href="<?= $this->config->item('admin_assets');?>vendor/jquery-datatables-bs3/assets/css/datatables.css" />
		<?php Include('includes/headerlinks2.php');?>
	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<?php include('includes/header.php');?>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php include('includes/menu_section.php');?>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2><?php echo $this->uri->segment(3); ?>s</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo $admin_url;?>">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span><?php echo $this->uri->segment(3); ?>s</span></li>
							</ol>
						</div>
					</header>

                         <?php  $type = $this->uri->segment(3);  ?>
                        

					<!-- start: page -->
						<section class="panel">
							<header class="panel-heading">
							<h2 class="panel-title"><?php echo $this->uri->segment(3); ?>s</h2>
							</header>
							<div class="panel-body">
								<?php  /*echo '<pre>'; print_r($users); echo '</pre>'; */?>
								<table class="table table-bordered table-striped mb-none " id="datatable-default">
									<thead>
										<tr>
											<th class="center">S No.</th>
											<th class="">Name</th>
											<th class="">Email</th>
											<th class="center">Phone</th>
											
											<th class="center">Document</th>
											
											<th class="center">Actions</th>
										</tr>
									</thead>
									<tbody>
										<?php 
										$bb=0; foreach ($users as $user) { $bb++; ?>
										<tr class="gradeX">
											<td class="center"><?= $bb;?></td> 
											<td><?= ucfirst($user['full_name']);?></td>
											<td><?= $user['email'];?></td>
											<td class="center"><?= $user['mobile_number'];?></td>  
											
										<td>	
										   <?php if($type=="user"){ ?>
										  
										  	 <?php $get_documents = $this->db->query("select * from user_details where user_id='".$user['id']."'")->row();       
										  	 
										  	              $get_license_name = $get_documents->alcohol_license;
										  	 
										  	 ?>
										  	
										  	 	<a target="_blank" href="<?php echo base_url();?>assets/images/alocohal_license_images/<?php echo $get_license_name;  ?>"> <?php echo  $get_license_name;?> </a>     
										  	
										  	<?php } ?>
										  	
										  	 <?php if($type=="sellar"){ ?>
										  
										  		 <?php $get_documents = $this->db->query("select * from sell_docs where seller_id='".$user['id']."'")->row();       
										  	 
										  	              $get_license_name = $get_documents->document_name;
										  	 
										  	 ?>
										  	
										  	<a target="_blank" href="<?php echo base_url();?>assets/images/Seller_images/<?php echo $get_license_name;  ?>"> <?php echo  $get_license_name;?> </a>     
										  	
										  	
										  	<?php } ?>
										  	
										  	</td>
										  	
											<td class="center actions_td">
												<a title="Edit" href="<?= base_url();?>admin/users_view/<?= $user['id'];?>" class="view"><i class="fa fa-eye" aria-hidden="true"></i></a>
												
											</td>
										</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</section>
					<!-- end: page -->
				</section>
			</div>

		</section>
		<?php include('includes/footerlinks.php');?>
		<!-- Specific Page Vendor -->
		<script src="<?= $this->config->item('admin_assets');?>vendor/select2/select2.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>

		<!-- Examples -->
		<script src="<?= $this->config->item('admin_assets');?>javascripts/tables/examples.datatables.default.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>javascripts/tables/examples.datatables.row.with.details.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>javascripts/tables/examples.datatables.tabletools.js"></script>
		<?php include('includes/footerlinks2.php');?>
	</body>
</html>