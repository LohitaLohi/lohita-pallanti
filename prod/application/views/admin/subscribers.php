<!doctype html>
<html class="fixed">
	<head>

		<?php Include('includes/headerlinks.php');?>
		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="<?= $this->config->item('admin_assets');?>vendor/select2/select2.css" />
		<link rel="stylesheet" href="<?= $this->config->item('admin_assets');?>vendor/jquery-datatables-bs3/assets/css/datatables.css" />
		<?php Include('includes/headerlinks2.php');?>
	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<?php include('includes/header.php');?>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php include('includes/menu_section.php');?>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Subscribers</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo $admin_url;?>">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span>Subscribers</span></li>
							</ol>
						</div>
					</header>

					<!-- start: page -->
						<section class="panel">
							<header class="panel-heading">
							<h2 class="panel-title">Subscribers</h2>
							</header>
							<div class="panel-body">
								<?php  /*echo '<pre>'; print_r($subcat_list); echo '</pre>'; */?>
								<table class="table table-bordered table-striped mb-none" id="datatable-default">
									<thead>
										<tr>
											<th class="center">S No.</th>
											<th>Email</th>
											<th class="center">Date</th>
											<th class="center">Status</th>
											<th class="center">Actions</th>
										</tr>
									</thead>
									<tbody>
										<?php $bb=0; foreach ($subscribers as $subscriber) { $bb++;?>
										<tr class="gradeX">
											<td class="center"><?= $bb;?></td>
											<td><?=  $subscriber['email'];?></td>
											<td><?=  $subscriber['created_date'];?></td>
											<td class="center"><?php if($subscriber['status'] =='1'){echo 'Active';}else{echo 'In-Active';} ?></td>
											<td class="center actions_td">									
												<?php if($subscriber['status'] =='1'){ ?>
												<a title="Make In-active" href="<?php echo $this->config->item('admin_url');?>subscriber_inactive/<?= $subscriber['sno']; ?>" onclick="return confirm_inactive();" class="delete active"><i class="fa fa-check" aria-hidden="true"></i></a>
												<?php }else{ ?>
												<a title="Make Active" href="<?php echo $this->config->item('admin_url');?>subscriber_active/<?= $subscriber['sno']; ?>" onclick="return confirm_active();" class="delete inactive"><i class="fa fa-times" aria-hidden="true"></i></a>
												<?php } ?>
												<a title="Make Active" href="<?php echo $this->config->item('admin_url');?>subscriber_delete/<?= $subscriber['sno']; ?>" onclick="return confirm_delete();" class="delete inactive"><i class="fa fa-trash" aria-hidden="true"></i></a>
											</td>
										</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</section>
					<!-- end: page -->
				</section>
			</div>

		</section>
		<?php include('includes/footerlinks.php');?>
		<!-- Specific Page Vendor -->
		<script src="<?= $this->config->item('admin_assets');?>vendor/select2/select2.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>

		<!-- Examples -->
		<script src="<?= $this->config->item('admin_assets');?>javascripts/tables/examples.datatables.default.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>javascripts/tables/examples.datatables.row.with.details.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>javascripts/tables/examples.datatables.tabletools.js"></script>
		<?php include('includes/footerlinks2.php');?>
	</body>
</html>