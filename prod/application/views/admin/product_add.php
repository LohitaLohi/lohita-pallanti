<!doctype html>
<html class="fixed">
	<head>

		<?php Include('includes/headerlinks.php');?>
		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="<?= $this->config->item('admin_assets');?>vendor/select2/select2.css" />
		<link rel="stylesheet" href="<?= $this->config->item('admin_assets');?>vendor/bootstrap-multiselect/bootstrap-multiselect.css" />
		<?php Include('includes/headerlinks2.php');?>
	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<?php include('includes/header.php');?>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php include('includes/menu_section.php');?>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Add Product</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo $admin_url;?>">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><a href="<?php echo $admin_url;?>product"><span>Products</span></a></li>
								<li><span>Add Product</span></li>
							</ol>
						</div>
					</header>

					<!-- start: page -->
					<div class="row">
						<div class="col-xs-12">
							<section class="panel">
								<header class="panel-heading">
									<h2 class="panel-title">Add Product</h2>
								</header>
								<div class="panel-body">
									<?php 
										$action = $admin_url."product_insert"; 
									 	echo form_open($action,array('id'=>"fm_prod_add",'class'=>"form-horizontal form-bordered custom_form",'autocomplete'=>"off"));
									?>
										<div class="form-group">
											<label class="col-md-3 control-label">Category <span class="colon">:</span></label>
											<div class="col-md-6">
												<select data-plugin-selectTwo name="prod_cat_sno" id="prod_cat_sno" class="form-control populate " required="required">
													<option value="">Select Category</option>
													<?php foreach ($category_dropdown as $cat_data0) { ?>
														<option value="<?= $cat_data0['cat_sno'];?>"><?= $cat_data0['cat_name'];?></option>
													<?php } ?>
												</select>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label">Subcategory <span class="colon">:</span></label>
											<div class="col-md-6" stylesheet="position:relative" id="prod_sub_cat_div">
												<select data-plugin-selectTwo name="prod_sub_cat_sno" id="prod_sub_cat_sno" class="form-control populate " required="required">
													<option value="">Select Subcategory</option>
												</select>
											</div>
										</div>
										
										<div class="form-group">
											<label class="col-md-3 control-label">Brands<span class="colon">:</span></label>
											<div class="col-md-6" >
												<select  class="form-control populate " placeholder="Select Brands" required="required" name="quanity_type" placeholder="Select Brands">
											<?php foreach ($ajax_brands_list as $ajax_brand_list) { ?>
												<option value="<?= $ajax_brand_list['brand_id'];?>"><?= $ajax_brand_list['brand_name'];?></option>
											<?php } ?>
										</select>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label">Product Name <span class="colon">:</span></label>
											<div class="col-md-6">
												<input class="form-control custom_required" name="prod_name" />
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label">Product Tags <span class="colon">:</span></label>
											<div class="col-md-6">
												<textarea class="form-control custom_required" name="prod_tags" placeholder="Enter tags with seperated comma(,)."></textarea>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label">Origin Of Country<span class="colon">:</span></label>
											<div class="col-md-6">
												<input class="form-control custom_required" name="origin_of_country" />
											</div>
										</div>
										
										 
										<div class="form-group">
											<label class="col-md-3 control-label">Price in NOK<span class="colon">:</span></label>
											<div class="col-md-6">
												<input class="form-control number custom_required" onkeypress="return onlyNos(event,this);" id="prod_dis_price" name="prod_dis_price" />
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label">Stock<span class="colon">:</span></label>
											<div class="col-md-6">
												<input class="form-control number custom_required" onkeypress="return onlyNos(event,this);" id="stock" name="prod_stock" />
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label">Description<span class="colon">:</span></label>
											<div class="col-md-9">
												<textarea name="prod_des" id="summernote" cols="150" rows="10"> </textarea>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label"></label>
											<div class="col-md-6">
												<?php if($errors != ''){ echo '<p class="p_error">'.$errors.'</p>';} ?>
												<button type="submit" onclick='return validate_form();'  class="btn btn-primary custom_submit_btm" name="product_add">Submit</button>
											</div>
										</div>
									<?php form_close();?>
								</div>
							</section>
						</div>
					</div>
					<!-- end: page -->
				</section>
			</div>
		</section>

		<?php include('includes/footerlinks.php');?>
		<!-- Specific Page Vendor -->
		<script src="<?= $this->config->item('admin_assets');?>vendor/select2/select2.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>
		<?php include('includes/footerlinks2.php');?>
		<!-- Examples -->
		<script src="<?= $this->config->item('admin_assets');?>javascripts/forms/examples.advanced.form.js"></script>

		<script>
			$( document ).ready(function() {
			$('#summernote').summernote({height: 300});
			});
		</script>
		<script src="<?= $this->config->item('admin_assets');?>wysiwyg_editor/summernote.min.js"></script>
		<link href="<?= $this->config->item('admin_assets');?>wysiwyg_editor/summernote.css" rel="stylesheet">
		<link href="<?= $this->config->item('admin_assets');?>wysiwyg_editor/summernote-bs3.css" rel="stylesheet">
<script>
$(document).ready(function(){
	$('#prod_cat_sno').change(function(){
		var cat_id = $(this).val();
		var gg = $("input[name=csrf_test_name]").val();
		var form_data = {
	        cat_id: cat_id,
	        csrf_test_name:gg
	    };
	    $('#s2id_prod_sub_cat_sno #select2-chosen-2').html('Select Subcategory');
	    $('#s2id_prod_brands .select2-choices .select2-search-choice').html('')
	    $('#s2id_prod_brands .select2-choices .select2-search-choice').css({'padding':'0px'});
		$.ajax({
			type:"POST",
			url:"<?php echo base_url();?>admin/ajax_subcat_list",
			data:form_data
		}).done(function(msg){
			$('#chinthala').fadeOut();
			$("#prod_sub_cat_sno").html(msg);
		});
	});
	$('#prod_sub_cat_sno').change(function(){
		var sub_cat_id = $(this).val();
		var sss = $("input[name=csrf_test_name]").val();
		var form_data0 = {
	        sub_cat_id: sub_cat_id,
	        csrf_test_name:sss
	    };
	    $('#s2id_prod_brands .select2-choices .select2-search-choice').html('')
	    $('#s2id_prod_brands .select2-choices .select2-search-choice').css({'padding':'0px'});
		$.ajax({
			type:"POST",
			url:"<?php echo base_url();?>admin/ajax_brands_list",
			data:form_data0
		}).done(function(msg1){
			$("#prod_brands").html(msg1);
		});
	});
});
/*	  	*/
</script>

<script>
$(document).ready(function() {
	$("#prod_price, #prod_dis_price").keyup(function() {
		var p = $("#prod_price").val();
		var q = $("#prod_dis_price").val();
		var x= 100- (q/p*100);
		x = Number((x).toFixed(2));
		$("#prod_discount").val(x);
	});
	$("#prod_discount").keyup(function() {
		var p = $("#prod_price").val();
		var q = $("#prod_discount").val();
		var x= p-(q*(p/100));
		x = Number((x).toFixed(2));
		$("#prod_dis_price").val(x);
	});
});
</script>
	</body>
</html>