<!doctype html>
<html class="fixed">
	<head>

		<?php Include('includes/headerlinks.php');?>
		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="<?php echo $this->config->item('admin_assets');?>vendor/select2/select2.css" />
		<?php Include('includes/headerlinks2.php');?>
	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<?php include('includes/header.php');?>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php include('includes/menu_section.php');?>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Add Menu</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo $admin_url;?>">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><a href="<?php echo $admin_url;?>menu"><span>Menu</span></a></li>
								<li><span>Add Menu</span></li>
							</ol>
						</div>
					</header>

					<!-- start: page -->
						<div class="row">
							<div class="col-xs-12">
								<section class="panel">
									<header class="panel-heading">
										<h2 class="panel-title">Add Menu</h2>
									</header>
									<div class="panel-body">
									<?php $action = $admin_url."menu_insert"; 
									 echo form_open($action,array('id'=>"fm_menu_add",'class'=>"form-horizontal form-bordered custom_form",'autocomplete'=>"off")); ?>
											<div class="form-group">
												<label class="col-md-3 control-label">Menu Name <span class="colon">:</span></label>
												<div class="col-md-6">
													<input class="form-control custom_required" name="menu_name" />
												</div>
											</div>
						
											<div class="form-group">
												<label class="col-md-3 control-label">Position<span class="colon">:</span></label>
												<div class="col-md-6">
													<input class="form-control number custom_required" onkeypress="return onlyNos(event,this);"  name="menu_position" />
												</div>
											</div>
											
											<div class="form-group">
												<label class="col-md-3 control-label"></label>
												<div class="col-md-6">
													<p class="error_p"><?php echo $this->session->flashdata('error0');?></p>
													<button type="submit" onclick='return validate_form();'  class="btn btn-primary custom_submit_btm" name="menu_add">Submit</button>
												</div>
											</div>
																	
										<?php echo form_close(); ?>
									</div>
								</section>
							</div>
						</div>
					<!-- end: page -->
				</section>
			</div>
		</section>

		<?php include('includes/footerlinks.php');?>
		<!-- Specific Page Vendor -->
		<script src="<?php echo $this->config->item('admin_assets');?>vendor/select2/select2.js"></script>
		<?php include('includes/footerlinks2.php');?>

		<!-- Examples -->
		<script src="<?php echo $this->config->item('admin_assets');?>javascripts/forms/examples.advanced.form.js"></script>

	</body>
</html>