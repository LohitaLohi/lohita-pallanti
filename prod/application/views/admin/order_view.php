<!doctype html>
<html class="fixed">
	<head>

		<?php Include('includes/headerlinks.php');?>
		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="<?= $this->config->item('admin_assets');?>vendor/select2/select2.css" />
		<link rel="stylesheet" href="<?= $this->config->item('admin_assets');?>vendor/bootstrap-multiselect/bootstrap-multiselect.css" />
		<?php Include('includes/headerlinks2.php');?>
		<style>
		.colon {float:right:padding-right:15px;}
		</style>
		
	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<?php include('includes/header.php');?>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php include('includes/menu_section.php');?>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Order View</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo $admin_url;?>">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><a href="<?php echo $admin_url;?>orders"><span>Orders</span></a></li>
								<li><span>Order View</span></li>
							</ol>
						</div>
					</header>
<?php 
									
										$order_info1 = $order_info[0]; 
										$this->db->select('*');
										$this->db->from('users'); 
										$this->db->where('id',$order_info1['user_id']); 
										$query = $this->db->get();
										$userInfo = $query->row();
									   
									 ?>
					<!-- start: page -->
					<div class="row">
						<div class="col-xs-12">
							<section class="panel">
								<header class="panel-heading">
									<h2 class="panel-title">Order View(<?= $order_info1['order_no'];?>)</h2>
								</header>
								<div class="panel-body">
									
								
									<div class="form-group">
										<div class="orders-table-div">
											<table>
												<thead>
													<tr>
														<th class="twidth50">Product Name</th>
														<th class="twidth10">Product Price (AED))</th>
														
														<th class="twidth10">Quantity</th>
														<th class="twidth10">After Discount (AED)</th>
													</tr>
												</thead>
												<tbody>
													<?php foreach ($order_info as $order_info_single) { ?>
													<tr>
														<td><?php echo $order_info_single['prod_name'];?> <?php if($order_info_single['color_name'] != ''){echo '( '.ucwords($order_info_single['color_name']).' )';}?></td>
														<td><?php echo number_format($order_info_single['product_cost'],2);?></td>
														<td><?php echo $order_info_single['product_quanity'];?></td>
														<td><?php echo number_format($order_info_single['product_quanity']*$order_info_single['product_cost'],2);?></td>
													</tr>
													<?php } ?>
												</tbody>
											</table>
											<div class="table-below-sub">
												<div class="table-below-line"><span>Subtotal </span>	<span>AED <?= number_format(($order_info[0]['grand_total']-$order_info[0]['shipping_cost']),2);?> </span></div>
												<div class="table-below-line"><span>Shipping & Handling</span>	<span>AED <?= number_format($order_info[0]['shipping_cost'],2);?></span></div>
												<div class="table-below-fline"><span>Grand Total</span>	<span>AED <?= number_format($order_info[0]['grand_total'],2);?></span>
												</div>
											</div>
										</div>
									</div>
									
							</section>
						</div>
					</div>
					<!-- end: page -->
				</section>
			</div>
		</section>

		<?php include('includes/footerlinks.php');?>
		<!-- Specific Page Vendor -->
		<script src="<?= $this->config->item('admin_assets');?>vendor/select2/select2.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>
		<?php include('includes/footerlinks2.php');?>
		<!-- Examples -->
		<script src="<?= $this->config->item('admin_assets');?>javascripts/forms/examples.advanced.form.js"></script>

		<script>
			$( document ).ready(function() {
			$('#summernote').summernote({height: 300});
			});
		</script>
		<script src="<?= $this->config->item('admin_assets');?>wysiwyg_editor/summernote.min.js"></script>
		<link href="<?= $this->config->item('admin_assets');?>wysiwyg_editor/summernote.css" rel="stylesheet">
		<link href="<?= $this->config->item('admin_assets');?>wysiwyg_editor/summernote-bs3.css" rel="stylesheet">
<script>
$(document).ready(function(){
	$('#prod_cat_sno').change(function(){
		var cat_id = $(this).val();
		var gg = $("input[name=csrf_test_name]").val();
		var form_data = {
	        cat_id: cat_id,
	        csrf_test_name:gg
	    };
	    $('#s2id_prod_sub_cat_sno #select2-chosen-2').html('Select Subcategory');
	    $('#s2id_prod_brands .select2-choices .select2-search-choice').html('')
	    $('#s2id_prod_brands .select2-choices .select2-search-choice').css({'padding':'0px'});
		$.ajax({
			type:"POST",
			url:"<?php echo base_url();?>admin/ajax_subcat_list",
			data:form_data
		}).done(function(msg){
			$('#chinthala').fadeOut();
			$("#prod_sub_cat_sno").html(msg);
		});
	});
	$('#prod_sub_cat_sno').change(function(){
		var sub_cat_id = $(this).val();
		var sss = $("input[name=csrf_test_name]").val();
		var form_data0 = {
	        sub_cat_id: sub_cat_id,
	        csrf_test_name:sss
	    };
	    $('#s2id_prod_brands .select2-choices .select2-search-choice').html('')
	    $('#s2id_prod_brands .select2-choices .select2-search-choice').css({'padding':'0px'});
		$.ajax({
			type:"POST",
			url:"<?php echo base_url();?>admin/ajax_brands_list",
			data:form_data0
		}).done(function(msg1){
			$("#prod_brands").html(msg1);
		});
	});
});
/*	  	*/
</script>

<script>
$(document).ready(function() {
	$("#prod_price, #prod_dis_price").keyup(function() {
		var p = $("#prod_price").val();
		var q = $("#prod_dis_price").val();
		var x= Math.round(100- (q/p*100));
		$("#prod_discount").val(x);
	});
	$("#prod_discount").keyup(function() {
		var p = $("#prod_price").val();
		var q = $("#prod_discount").val();
		var x= Math.round(p-(q*(p/100)));
		$("#prod_dis_price").val(x);
	});
});
</script>
<script>

$(document).ready(function(){
	
    $("#delivery_date").datepicker({
		
       
        onSelect: function(selected) {
          $("#delivery_date").datepicker("option","minDate", selected)
        }
    });
    
});
    </script>
	</body>
</html>