<!doctype html>
<html class="fixed">
	<head>

		<?php Include('includes/headerlinks.php');?>
		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="<?= $this->config->item('admin_assets');?>vendor/select2/select2.css" />
		<link rel="stylesheet" href="<?= $this->config->item('admin_assets');?>vendor/jquery-datatables-bs3/assets/css/datatables.css" />
		<?php Include('includes/headerlinks2.php');?>
	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<?php include('includes/header.php');?>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php include('includes/menu_section.php');?>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Colors</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo $admin_url;?>">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span>Colors</span></li>
							</ol>
						</div>
					</header>

						
						<section class="panel">
							<div class="panel-body categories width-f">
								<div class="add-btn">
									
									
										<a href="<?php echo base_url()?>admin/employee_add" class="Add-p"><i class="fa fa-plus" aria-hidden="true"></i>Add Staff</a>
									
								</div>
								
								<table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
									<thead>
										<tr>
											<th  class="center">S.No</th>
											<th  class="center">Emp Id</th>
											<th  class="center">Email</th>
											<th  class="center">Area</th>
											<th  class="center">Name</th>
											<th  class="center">Phone No</th>
											<th  class="center">Role</th>
											<th  class="center">Joining Date</th>
											<th  class="center">Employee Status</th>
											
											<th  class="center">Action</th>
										</tr>
									</thead>
									<tbody  class="center">
										<?php $sno = ''; foreach($user_data as $user){ $sno++; ?>
										<tr class="gradeX">
											<td><?php echo $sno;?></td>
											<td><?= $user['admin_id'];?></td>
											<td><?= $user['admin_id'];?></td>
											<td><?= $user['admin_city'];?></td>
											<td><?= ucfirst($user['admin_name']);?></td>
											<td class="center"><?= $user['admin_phone'];?></td>
											<td class="center"><?= ucfirst($user['admin_type']);?></td>
											<td class="center"><?= $user['date_of_joining'];?></td>
											<td>
											<form method="post" name="" action="<?php echo base_url()?>admin/employee_details_delete">
											<input type="hidden" name="id" value="<?php echo $user['admin_no']?>">
											<select onchange="this.form.submit();" name="change_status">
											<option  value="">Change Status</option>
											<option <?php if($user['admin_status']=='active'){ echo "selected"; }?> value="active">Active</option>
											<option <?php if($user['admin_status']=='inactive'){ echo "selected"; }?> value="inactive">InActive</option>
											</select>
											</form>
											</td>
											<?php if($this->uri->segment(2)=="agents_details"){ ?>
											<td class="sec-actions"><a class="edit" href="<?php echo base_url()?>admin/agent_details_edit/<?= $user['admin_no'];?>">Edit</a>
												<a class="view vv" href="<?php echo base_url()?>admin/agent_details_view/<?= $user['admin_no'];?>">View</a>
												
											</td>
											<?php }
                                             else{ ?>
												 <td class="sec-actions"><a class="edit" href="<?php echo base_url()?>admin/employee_details_edit/<?= $user['admin_no'];?>">Edit</a>
												<a class="view vv" href="<?php echo base_url()?>admin/employee_details_view/<?= $user['admin_no'];?>">View</a>
												
											</td>
											<?php  }
											?>
										</tr>
										<?php } ?>
										
										
									</tbody>
								</table>
							</div>
						</section>
						
						
					<!-- end: page -->
				</section>
			</div>

			
		</section>

<?php include('includes/footerlinks.php');?>
		<!-- Specific Page Vendor -->
		<script src="<?= $this->config->item('admin_assets');?>vendor/select2/select2.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>

		<!-- Examples -->
		<script src="<?= $this->config->item('admin_assets');?>javascripts/tables/examples.datatables.default.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>javascripts/tables/examples.datatables.row.with.details.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>javascripts/tables/examples.datatables.tabletools.js"></script>
		<?php include('includes/footerlinks2.php');?>
	</body>
</html>