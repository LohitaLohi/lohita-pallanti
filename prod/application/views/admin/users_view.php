<!doctype html>
<html class="fixed">
	<head>

		<?php Include('includes/headerlinks.php');?>
		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="<?= $this->config->item('admin_assets');?>vendor/select2/select2.css" />
		<link rel="stylesheet" href="<?= $this->config->item('admin_assets');?>vendor/bootstrap-multiselect/bootstrap-multiselect.css" />
		<?php Include('includes/headerlinks2.php');?>
	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<?php include('includes/header.php');?>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php include('includes/menu_section.php');?>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Add Product</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo $admin_url;?>">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><a href="<?php echo $admin_url;?>product"><span>Products</span></a></li>
								<li><span>Add Product</span></li>
							</ol>
						</div>
					</header>

					<!-- start: page -->
					<div class="row">
						<div class="col-xs-12">
							<section class="panel">
								<header class="panel-heading">
									<h2 class="panel-title">Add Product</h2>
								</header>
								<div class="panel-body">
									<?php 
									$user_info = $user_info[0]; 
								    ?>
									<div class="form-group">
										<label class="col-md-3 control-label">Name <span class="colon">:</span></label>
										<div class="col-md-6">
											<p><?php echo ucwords($user_info['full_name']);?></p>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Email <span class="colon">:</span></label>
										<div class="col-md-6" stylesheet="position:relative" id="prod_sub_cat_div">
											<p><?= ucwords($user_info['email']);?></p>
										</div>
									</div>
 									<div class="form-group">
										<label class="col-md-3 control-label">Address <span class="colon">:</span></label>
										<div class="col-md-6">
											<p><?php 
											if($user_info['address_line1'] !=''){echo ucwords($user_info['address_line1']);}
											if($user_info['address_line2'] !=''){echo '<br>'.ucwords($user_info['address_line2']);}
											if($user_info['address_line3'] !=''){echo '<br>'.ucwords($user_info['address_line3']);}
											if($user_info['city'] !=''){echo '<br>'.ucwords($user_info['city']);}
											if($user_info['state'] !=''){echo '<br>'.ucwords($user_info['state']);}
											if($user_info['pincode'] !=''){echo '<br>'.ucwords($user_info['pincode']);}
											?></p>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">OTP Status <span class="colon">:</span></label>
										<div class="col-md-6">
											<p><?php if($user_info['otp_status']){echo '<span style="color:#00cc00">Verified</span>';} else { echo '<span style="color:#ff0000">Not Verified</span>';};?></p>
										</div>
									</div> 
									<div class="form-group">
										<label class="col-md-3 control-label">Status<span class="colon">:</span></label>
										<div class="col-md-6"> 
											<p><?php if($user_info['status']){echo '<span style="color:#00cc00">Verified</span>';} else { echo '<span style="color:#ff0000">Not Verified</span>';};?></p>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Created Date<span class="colon">:</span></label>
										<div class="col-md-6">
											<p><?= $user_info['created'];?></p>
										</div>
									</div> 
								</div>
							</section>
						</div>
					</div>
					<!-- end: page -->
				</section>
			</div>
		</section>

		<?php include('includes/footerlinks.php');?>
		<!-- Specific Page Vendor -->
		<script src="<?= $this->config->item('admin_assets');?>vendor/select2/select2.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>
		<?php include('includes/footerlinks2.php');?>
		<!-- Examples -->
		<script src="<?= $this->config->item('admin_assets');?>javascripts/forms/examples.advanced.form.js"></script>

		<script>
			$( document ).ready(function() {
			$('#summernote').summernote({height: 300});
			});
		</script>
		<script src="<?= $this->config->item('admin_assets');?>wysiwyg_editor/summernote.min.js"></script>
		<link href="<?= $this->config->item('admin_assets');?>wysiwyg_editor/summernote.css" rel="stylesheet">
		<link href="<?= $this->config->item('admin_assets');?>wysiwyg_editor/summernote-bs3.css" rel="stylesheet">
<script>
$(document).ready(function(){
	$('#prod_cat_sno').change(function(){
		var cat_id = $(this).val();
		var gg = $("input[name=csrf_test_name]").val();
		var form_data = {
	        cat_id: cat_id,
	        csrf_test_name:gg
	    };
	    $('#s2id_prod_sub_cat_sno #select2-chosen-2').html('Select Subcategory');
	    $('#s2id_prod_brands .select2-choices .select2-search-choice').html('')
	    $('#s2id_prod_brands .select2-choices .select2-search-choice').css({'padding':'0px'});
		$.ajax({
			type:"POST",
			url:"<?php echo base_url();?>admin/ajax_subcat_list",
			data:form_data
		}).done(function(msg){
			$('#chinthala').fadeOut();
			$("#prod_sub_cat_sno").html(msg);
		});
	});
	$('#prod_sub_cat_sno').change(function(){
		var sub_cat_id = $(this).val();
		var sss = $("input[name=csrf_test_name]").val();
		var form_data0 = {
	        sub_cat_id: sub_cat_id,
	        csrf_test_name:sss
	    };
	    $('#s2id_prod_brands .select2-choices .select2-search-choice').html('')
	    $('#s2id_prod_brands .select2-choices .select2-search-choice').css({'padding':'0px'});
		$.ajax({
			type:"POST",
			url:"<?php echo base_url();?>admin/ajax_brands_list",
			data:form_data0
		}).done(function(msg1){
			$("#prod_brands").html(msg1);
		});
	});
});
/*	  	*/
</script>

<script>
$(document).ready(function() {
	$("#prod_price, #prod_dis_price").keyup(function() {
		var p = $("#prod_price").val();
		var q = $("#prod_dis_price").val();
		var x= Math.round(100- (q/p*100));
		$("#prod_discount").val(x);
	});
	$("#prod_discount").keyup(function() {
		var p = $("#prod_price").val();
		var q = $("#prod_discount").val();
		var x= Math.round(p-(q*(p/100)));
		$("#prod_dis_price").val(x);
	});
});
</script>
	</body>
</html>