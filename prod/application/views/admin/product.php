<!doctype html>
<html class="fixed">
	<head>

		<?php Include('includes/headerlinks.php');?>
		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="<?= $this->config->item('admin_assets');?>vendor/select2/select2.css" />
		<link rel="stylesheet" href="<?= $this->config->item('admin_assets');?>vendor/jquery-datatables-bs3/assets/css/datatables.css" />
		<?php Include('includes/headerlinks2.php');?>
	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<?php include('includes/header.php');?>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php include('includes/menu_section.php');?>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Products</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo $admin_url;?>">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span>Products</span></li>
							</ol>
						</div>
					</header>

					<!-- start: page -->
						<section class="panel">
							<header class="panel-heading">
								
								<div class="panel-actions">
									<a href="<?php echo $admin_url;?>product_add" class="add">Add Product</a> 	<a href="<?php echo $admin_url;?>product_bulk" class="add">Bulk Upload</a>
								</div>
								
							 
								
							<h2 class="panel-title">Products</h2>
							</header>
							<div class="panel-body">
								<?php 
								/*
									echo '<pre>';
									print_r($products_list[0]);
									echo count($products_list);
									echo '</pre>';
								*/
								?>
								<table class="table table-bordered table-striped mb-none" id="datatable-default">
									<thead>
										<tr>
											<th class="center">S No.</th>
											<th width="30%">Product</th>
											<th>Subcategory</th>
											<th class="center">Price NOV.</th>
											<th class="center padding0" >Stock</th>
											
											<th class="center padding0">Images</th>
											<th class="center padding0">Type</th>
											
											<th class="center padding0 tworows" style="width: 50px;">Actions</th>
										</tr>
									</thead>
									<tbody>
										<?php $pc=0;foreach ($products_list as $product_INFO) { $pc++; ?>
										<tr class="gradeX">
											<td class="center"><?= $pc;?></td>
											<td ><?= $product_INFO['prod_name'];?></td>
											<td><?= $product_INFO['sub_cat_name'];?></td>
											<td class="center"><?= $product_INFO['prod_dis_price'];?></td>
											<td class="center padding0"><?= $product_INFO['prod_stock'];?></td>
											<td class="center padding0"><a href="<?php echo $admin_url;?>product_images/<?= $product_INFO['prod_sno'];?>">View</a></td>
											<td class="center padding0"><?php if($product_INFO['prod_status'] =='1'){echo 'Active';}else{echo 'In-active';} ?></td>
											
											<td class="center padding0 actions_td tworows" style="width: 50px;">
												<a data-toggle="tooltip" title="View" href="<?php echo $admin_url;?>product_view/<?= $product_INFO['prod_sno'];?>" class="view"><i class="fa fa-eye" aria-hidden="true"></i></a>
												<a data-toggle="tooltip" title="Edit" href="<?php echo $admin_url;?>product_edit/<?= $product_INFO['prod_sno'];?>" class="edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
												
												<a data-toggle="tooltip" title="Delete" href="<?php echo $admin_url;?>product_delete/<?= $product_INFO['prod_sno'];?>" onclick="return confirm_delete();" class="delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
											</td>
										</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</section>
					<!-- end: page -->
				</section>
			</div>

		</section>
		<?php include('includes/footerlinks.php');?>
		<!-- Specific Page Vendor -->
		<script src="<?= $this->config->item('admin_assets');?>vendor/select2/select2.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>

		<!-- Examples -->
		<script src="<?= $this->config->item('admin_assets');?>javascripts/tables/examples.datatables.default.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>javascripts/tables/examples.datatables.row.with.details.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>javascripts/tables/examples.datatables.tabletools.js"></script>
		<?php include('includes/footerlinks2.php');?>
	</body>
</html>