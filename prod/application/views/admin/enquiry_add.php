<!doctype html>
<html class="fixed">
	<head>

		<?php Include('includes/headerlinks.php');?>
		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="<?= $this->config->item('admin_assets');?>vendor/select2/select2.css" />
		<link rel="stylesheet" href="<?= $this->config->item('admin_assets');?>vendor/bootstrap-multiselect/bootstrap-multiselect.css" />
		<?php Include('includes/headerlinks2.php');?>
	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<?php include('includes/header.php');?>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php include('includes/menu_section.php');?>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Add Enquiry</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo $admin_url;?>">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><a href="<?php echo $admin_url;?>subcategory"><span>Enquiry</span></a></li>
								<li><span>Add Enquiry</span></li>
							</ol>
						</div>
					</header>

					<!-- start: page -->
						<div class="row">
							<div class="col-xs-12">
								<section class="panel">
									<header class="panel-heading">
										<h2 class="panel-title">Add Enquiry</h2>
									</header>
									<div class="panel-body">
										<?php /*print_r($brands_dropdown); echo '<br><br>'; print_r($category_dropdown);*/?>
										<?php $action = $admin_url."enquiry_insert"; 
									 	echo form_open($action,array('id'=>"fm_brand_add",'class'=>"form-horizontal form-bordered custom_form",'autocomplete'=>"off")); ?>
											<div class="form-group">
												<label class="col-md-3 control-label">Name <span class="colon">:</span></label>
												<div class="col-md-6">
													<input class="form-control custom_required" name="name" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-3 control-label">Email <span class="colon">:</span></label>
												<div class="col-md-6">
													<input class="form-control email custom_required" name="email" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-3 control-label">Phone Number <span class="colon">:</span></label>
												<div class="col-md-6">
													<input class="form-control phone custom_required" maxlength="10" onkeypress="return onlyNos(event,this);"  name="phone" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-3 control-label">Description<span class="colon">:</span></label>
												<div class="col-md-6">
													
													<textarea name="description" class="form-control custom_required" ></textarea>
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-3 control-label">Phone Number <span class="colon">:</span></label>
												<div class="col-md-6">
													<select name="status" class="form-control custom_required"  >
														<option value="">Select Status</option>
														<option value="New">New</option>
														<option value="On-Going">On-Going</option>
														<option value="Completed">Completed</option>
														<option value="Not Related">Not Related</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-3 control-label"></label>
												<div class="col-md-6">
													<?php if(isset($errors)){ echo "<p class='errors_p'>".$errors."</p>"; }?>
													<button type="submit" onclick='return validate_form();'  class="btn btn-primary custom_submit_btm" name="enquiry_add">Submit</button>
												</div>
											</div>
						
										<?php echo form_close();?>
									</div>
								</section>
							</div>
						</div>
					<!-- end: page -->
				</section>
			</div>
		</section>

		<?php include('includes/footerlinks.php');?>
		<!-- Specific Page Vendor -->
		<script src="<?= $this->config->item('admin_assets');?>vendor/select2/select2.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>
		<?php include('includes/footerlinks2.php');?>
		<!-- Examples -->
		<script src="<?= $this->config->item('admin_assets');?>javascripts/forms/examples.advanced.form.js"></script>

	</body>
</html>