<!doctype html>
<html class="fixed">
	<head>

		<?php Include('includes/headerlinks.php');?>
		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="<?= $this->config->item('admin_assets');?>vendor/select2/select2.css" />
		<link rel="stylesheet" href="<?= $this->config->item('admin_assets');?>vendor/bootstrap-multiselect/bootstrap-multiselect.css" />
		<?php Include('includes/headerlinks2.php');?>
	</head>
	<body>
		<section class="body">
			<!-- start: header -->
			<?php include('includes/header.php');?>
			<!-- end: header -->
			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php include('includes/menu_section.php');?>
				<!-- end: sidebar -->
				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Edit Product</h2>
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo $admin_url;?>">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><a href="<?php echo $admin_url;?>product"><span>Products</span></a></li>
								<li><span>Edit Product</span></li>
							</ol>
						</div>
					</header>
					<!-- start: page -->
					<div class="row">
						<div class="col-xs-12">
							<section class="panel">
								<header class="panel-heading">
									<h2 class="panel-title">Edit Product</h2>
								</header>
								<div class="panel-body">
									<?php $product_details = $product_details[0]; 
									//echo '<pre>';  print_r($product_details); echo '</pre>';?>
									<?php 
										$action = $admin_url."product_update"; 
									 	echo form_open($action,array('id'=>"fm_prod_add",'class'=>"form-horizontal form-bordered custom_form",'autocomplete'=>"off"));
									?>
										<div class="form-group0">
											<label class="col-md-3 control-label"></span></label>
											<div class="col-md-6">
												<?php 
													$prod_not_updated = $this->session->userdata('product_data_not_updated');
													$prod_updated = $this->session->userdata('product_data_updated');
													if($prod_not_updated){
														echo "<p class='error_p'>".$prod_not_updated."</p>";
														$this->session->unset_userdata('product_data_not_updated');
													}
													if($prod_updated){
														echo "<p class='p_success'>".$prod_updated."</p>";
														$this->session->unset_userdata('product_data_updated');
													}
												?>
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label">Category <span class="colon">:</span></label>
											<div class="col-md-6">
												<input type="text" name="prod_cat_sno" id="prod_cat_sno" value="<?= ucwords($product_details['cat_name']);?>" class="form-control populate " required="required" readonly />
												<?php /*
												<select data-plugin-selectTwo name="prod_cat_sno" id="prod_cat_sno" class="form-control populate " required="required" >
													<option value="">Select Category</option>
													<?php foreach ($category_dropdown as $cat_data0) { ?>
														<option value="<?= $cat_data0['cat_sno'];?>" <?php if($cat_data0['cat_sno'] == $product_details['cat_id']){echo 'selected';}?> ><?= $cat_data0['cat_name'];?></option>
													<?php } ?>
												</select>
												*/?>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label">Subcategory <span class="colon">:</span></label>
											<div class="col-md-6" stylesheet="position:relative" id="prod_sub_cat_div">
												<input type="text" name="prod_sub_cat_name" id="prod_sub_cat_sno" value="<?= ucwords($product_details['sub_cat_name']);?>" class="form-control populate " required="required" readonly />
												<?php /*
												<select data-plugin-selectTwo name="prod_sub_cat_sno" id="prod_sub_cat_sno" class="form-control populate " required="required" >
													<option value="">Select Subcategory</option>
													<?php foreach ($sub_cat_list as $sub_cat0) {?>
													<option value="<?= $sub_cat0['sub_cat_sno'];?>" <?php if($sub_cat0['sub_cat_sno'] == $product_details['prod_sub_cat_sno']){echo 'selected';}?> ><?= $sub_cat0['sub_cat_name']?></option>
													<?php } ?>
												</select>
												*/ ?>
											</div>
										</div>
<?php // echo '<pre>';print_r($subcat_brands_list); print_r($product_brands); 
$newArray0 = array();
$finalbrandsArray = array();
foreach($product_brands as $key => $val){
    $newArray0[$val['brand_id'] . '-' . $val['brand_name']] = $val;
}
$cc= -1;
foreach($subcat_brands_list as $key => $val){ $cc++;
    if(isset($newArray0[$val['brand_id'] . '-' . $val['brand_name']])){
        $finalbrandsArray[$cc] = $val;
        $finalbrandsArray[$cc]['exist'] = "1";
    }
	else{
		$finalbrandsArray[$cc] = $val;
		$finalbrandsArray[$cc]['exist'] = "0";
	}
}
/*
print_r($finalbrandsArray);
 echo '</pre>'; 
 */
?>
										<div class="form-group">
											<label class="col-md-3 control-label">Brands<span class="colon">:</span></label>
											<div class="col-md-6" id="brands_div">
												<select data-plugin-selectTwo class="form-control populate " placeholder="Select Brands" required="required" name="prod_brands[]" placeholder="Select Brands" id="prod_brands">
													<?php foreach ($finalbrandsArray as $finalbrandArray) { ?>
														<option value="<?= $finalbrandArray['brand_id'];?>__<?= $finalbrandArray['brand_name'];?>" <?php if($finalbrandArray['exist']=="1"){echo 'selected';}?>><?= ucfirst($finalbrandArray['brand_name']);?></option>
													<?php } ?>
												</select>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label">Product Name <span class="colon">:</span></label>
											<div class="col-md-6">
												<input class="form-control custom_required" value="<?= $product_details['prod_name'];?>" name="prod_name" />
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label">Product Tags <span class="colon">:</span></label>
											<div class="col-md-6">
												<textarea class="form-control custom_required" name="prod_tags" placeholder="Enter tags with seperated comma(,)."><?= $product_details['prod_tags'];?></textarea>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label">Origin Of Country<span class="colon">:</span></label>
											<div class="col-md-6">
												<input class="form-control custom_required" value="<?= $product_details['origin_of_country'];?>" name="origin_of_country" />
											</div>
										</div>
										 
<?php /*/ print_r($colors_list); print_r($product_colors); */

$newArray = array();
$finalcolorsArray = array();
foreach($product_colors as $key => $val){
    $newArray[$val['color_id'] . '-' . $val['color_name']] = $val;
}
$cc= -1;
foreach($colors_list as $key => $val){ $cc++;
    if(isset($newArray[$val['color_sno'] . '-' . $val['color_name']])){
        $finalcolorsArray[$cc] = $val;
        $finalcolorsArray[$cc]['exist'] = "1";
    }
	else{
		$finalcolorsArray[$cc] = $val;
		$finalcolorsArray[$cc]['exist'] = "0";
	}
}
?>
										
										
										<div class="form-group">
											<label class="col-md-3 control-label">Final Price<span class="colon">:</span></label>
											<div class="col-md-6">
												<input class="form-control number custom_required" onkeypress="return onlyNos(event,this);" id="prod_dis_price" name="prod_dis_price" value="<?= $product_details['prod_dis_price'];?>" />
											</div>
										</div>
										 
										<div class="form-group">
											<label class="col-md-3 control-label">Stock<span class="colon">:</span></label>
											<div class="col-md-6">
												<input class="form-control number custom_required" onkeypress="return onlyNos(event,this);" id="stock" name="prod_stock" value="<?= $product_details['prod_stock'];?>" />
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label">Description<span class="colon">:</span></label>
											<div class="col-md-9">
												<textarea name="prod_des" id="summernote" cols="150" rows="10"><?= $product_details['prod_des'];?></textarea>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label">Product Status<span class="colon">:</span></label>
											<div class="col-md-6">
												<?php $status = $product_details['prod_status'];?>
												<select data-plugin-selectTwo class="form-control populate custom_required" name="prod_status" >
													<option value="1" <?php if($status == '1'){echo 'selected';}?>>Active</option>
													<option value="0" <?php if($status == '0'){echo 'selected';}?>>In-active</option>
												</select>
											</div>
										</div>
										<input type="hidden" name="prod_sno" value="<?= $product_details['prod_sno'];?>" />
										<input type="hidden" name="cat_id" value="<?= $product_details['cat_id'];?>" />
										<input type="hidden" name="old_prod_name" value="<?= $product_details['prod_name'];?>" />
										<input type="hidden" name="sub_cat_name" value="<?= $product_details['sub_cat_name'];?>" />
										<input type="hidden" name="prod_sub_cat_sno" value="<?= $product_details['prod_sub_cat_sno'];?>" />

										<div class="form-group">
											<label class="col-md-3 control-label"></label>
											<div class="col-md-6">
												<button type="submit" onclick='return validate_form();'  class="btn btn-primary custom_submit_btm" name="product_update">Update</button>
											</div>
										</div>
									<?php form_close();?>
								</div>
							</section>
						</div>
					</div>
					<!-- end: page -->
				</section>
			</div>
		</section>

		<?php include('includes/footerlinks.php');?>
		<!-- Specific Page Vendor -->
		<script src="<?= $this->config->item('admin_assets');?>vendor/select2/select2.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>
		<?php include('includes/footerlinks2.php');?>
		<!-- Examples -->
		<script src="<?= $this->config->item('admin_assets');?>javascripts/forms/examples.advanced.form.js"></script>

		<script>
			$( document ).ready(function() {
			$('#summernote').summernote({height: 300});
			});
		</script>
		<script src="<?= $this->config->item('admin_assets');?>wysiwyg_editor/summernote.min.js"></script>
		<link href="<?= $this->config->item('admin_assets');?>wysiwyg_editor/summernote.css" rel="stylesheet">
		<link href="<?= $this->config->item('admin_assets');?>wysiwyg_editor/summernote-bs3.css" rel="stylesheet">
<script>
$(document).ready(function(){
	$('#prod_cat_sno').change(function(){
		var cat_id = $(this).val();
		var gg = $("input[name=csrf_test_name]").val();
		var form_data = {
	        cat_id: cat_id,
	        csrf_test_name:gg
	    };
	    $('#s2id_prod_sub_cat_sno #select2-chosen-2').html('Select Subcategory');
	    $('#s2id_prod_brands .select2-choices .select2-search-choice').html('')
	    $('#s2id_prod_brands .select2-choices .select2-search-choice').css({'padding':'0px'});
		$.ajax({
			type:"POST",
			url:"<?php echo base_url();?>admin/ajax_subcat_list",
			data:form_data
		}).done(function(msg){
			$('#chinthala').fadeOut();
			$("#prod_sub_cat_sno").html(msg);
		});
	});
	$('#prod_sub_cat_sno').change(function(){
		var sub_cat_id = $(this).val();
		var sss = $("input[name=csrf_test_name]").val();
		var form_data0 = {
	        sub_cat_id: sub_cat_id,
	        csrf_test_name:sss
	    };
	    $('#s2id_prod_brands .select2-choices .select2-search-choice').html('')
	    $('#s2id_prod_brands .select2-choices .select2-search-choice').css({'padding':'0px'});
		$.ajax({
			type:"POST",
			url:"<?php echo base_url();?>admin/ajax_brands_list",
			data:form_data0
		}).done(function(msg1){
			$("#prod_brands").html(msg1);
		});
	});
});
/*	  	*/
</script>

<script>
$(document).ready(function() {
	$("#prod_price, #prod_dis_price").keyup(function() {
		var p = $("#prod_price").val();
		var q = $("#prod_dis_price").val();
		var x= 100- (q/p*100);
		x = Number((x).toFixed(2)); 
		$("#prod_discount").val(x);
	});
	$("#prod_discount").keyup(function() {
		var p = $("#prod_price").val();
		var q = $("#prod_discount").val();
		var x= p-(q*(p/100));
		x = Number((x).toFixed(2)); 
		$("#prod_dis_price").val(x);
	});
});
</script>
	</body>
</html>