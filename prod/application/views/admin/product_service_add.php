<!doctype html>
<html class="fixed">
	<head>

		<?php Include('includes/headerlinks.php');?>
		<?php Include('includes/headerlinks2.php');?>
	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<?php include('includes/header.php');?>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php include('includes/menu_section.php');?>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Add Service</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo $admin_url;?>">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><a href="<?php echo $admin_url;?>product"><span>Products</span></a></li>
								<li><a href="<?php echo $admin_url;?>product_features/<?= $product_ID;?>"><span>Services</span></a></li>
								<li><span>Add Service</span></li>
							</ol>
						</div>
					</header>

					<!-- start: page -->
						<div class="row">
							<div class="col-xs-12">
								<section class="panel">
									<header class="panel-heading">
										<h2 class="panel-title">Add Service</h2>
									</header>
									<div class="panel-body">
										<?php 
										$action = $admin_url."product_service_insert"; 
									 	echo form_open($action,array('id'=>"fm_prod_add",'class'=>"form-horizontal form-bordered custom_form",'autocomplete'=>"off"));
										?>
											<div class="services_main_div">
												<div class="form-group service0">
													<label class="col-md-3 control-label">Service <span class="colon">:</span></label>
													<div class="col-md-5">
														<input class="form-control custom_required" id="service" name="service[]" />
													</div>
													<div class="col-md-3 add_remove">
														<p class="add">Add</p>
														<p class="remove">Remove</p>
														<div class="clearfix"></div>
													</div>
												</div>
												<input type="hidden" name="prod_sno" value="<?= $product_ID;?>">
												<div class="form-group">
													<label class="col-md-3 control-label"></label>
													<div class="col-md-6">
														<button type="submit" onclick='return validate_form();' class="btn btn-primary custom_submit_btm" value="" name="menu_add">Submit</button>
													</div>
												</div>
											</div>
										<?php echo form_close();?>
									</div>
								</section>
							</div>
						</div>
					<!-- end: page -->
				</section>
			</div>
		</section>
<style>
.displaynone { display : none; }
.add_remove p.add, .add_remove p.remove {float: left; margin: 0 15px 0 0; width: 70px; color: #fff; text-align: center; height: 33px; line-height: 32px;  border-radius: 3px; }
.add_remove p.add { background: green; }
.add_remove p.remove { background: red; }
</style>
		<?php include('includes/footerlinks.php');?>
		
		<?php include('includes/footerlinks2.php');?>


		<!-- Examples -->
		<script src="<?= $this->config->item('admin_assets');?>javascripts/forms/examples.advanced.form.js"></script>
<script type="text/javascript">
$(document).ready(function() {
check_rows();
	$(".add").click(function() {
	  $('.services_main_div .service0:last').clone(true).insertAfter('.services_main_div .service0:last');
	  $('.services_main_div .service0:last #service').val('');
	  check_rows();
	  return false;
	});
	$('.remove').click(function(){
		$(this).parents('.service0').remove();
		check_rows();
	});

});

function check_rows(){
	var rowCount = $('.services_main_div .service0').length;
	//alert(rowCount);
	if(rowCount == 1){ $('.remove').addClass('displaynone'); } else { $('.remove').removeClass('displaynone'); }
}

</script>
	</body>
</html>
