<!doctype html>
<html class="fixed">
<head>
	<!-- Basic -->
	<meta charset="UTF-8">

	<title>clothes Admin</title>
	<meta name="keywords" content="" />
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Mobile Metas -->
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<link rel="icon" type="image/png" href="<?= base_url();?>assets/admin/frontend/images/fav.png"/>
	<!-- Web Fonts  -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">
	<!-- Vendor CSS -->
	<link rel="stylesheet" href="<?= base_url();?>assets/admin/vendor/bootstrap/css/bootstrap.css" />
	<link rel="stylesheet" href="<?= base_url();?>assets/admin/vendor/font-awesome/css/font-awesome.css" />
	<!-- Theme CSS -->
	<link rel="stylesheet" href="<?= base_url();?>assets/admin/stylesheets/theme.css" />
	<link rel="stylesheet" href="<?= base_url();?>assets/admin/stylesheets/theme-custom.css" />
	<!-- Skin CSS -->
	<link rel="stylesheet" href="<?= base_url();?>assets/admin/stylesheets/skins/default.css" />
	<!-- Theme Custom CSS -->
	<link rel="stylesheet" href="<?= base_url();?>assets/admin/stylesheets/theme-custom.css">
	<!-- Head Libs -->
	<script src="<?= base_url();?>assets/admin/vendor/modernizr/modernizr.js"></script>
	 
</head>	
<body id="register_bg">
<!-- start: page -->
<section class="body-sign">
	<div class="center-sign">
		<div class="panel panel-sign">
			<div class="panel-body">
				
				<p>SIGN IN TO CONTINUE.</p>
				<?php //$baseurl = base_url();
				$action = base_url()."admin_login/login_submit"; 
				echo form_open($action,array('id'=>"admin_login_fm",'class'=>"form-horizontal form-bordered0 custom_form",'autocomplete'=>"off")); ?>
					<div class="form-group mb-lg user-f">
						<div class="input-group input-group-icon">
							<span class="input-group-addon">
								<span class="icon icon-lg">
									<i class="fa fa-user"></i>
								</span>
							</span>
							<input name="username" type="text" class="form-control input-lg custom_required admin_username" placeholder="Username">
						</div>
					</div>

					<div class="form-group mb-lg pass-l">
						<div class="input-group input-group-icon">
							<span class="input-group-addon">
								<span class="icon icon-lg">
									<i class="fa fa-lock"></i>
								</span>
							</span>
							<input name="password" type="password" class="form-control custom_required input-lg admin_password" placeholder="Password">
						</div>
					</div>

					<div class="row">
						<div class="col-sm-12">
							<?php if($this->session->userdata('error_message')){ ?>
							<p class="error_p"><?php echo $this->session->userdata('error_message');?></p>
							<?php $this->session->unset_userdata('error_message'); } ?>
							<button type="submit" name="login_sub" onclick='return admin_login_function();' class="btn btn-primary arrow-log">Sign In</button>
						</div>
					</div>
				<?php echo form_close();?>
			</div>
		</div>
		<p class="text-center text-muted mt-md mb-md">© Copyright 2018. All Rights Reserved.</p>
	</div>
	
</section>
<!-- end: page -->

<!-- Vendor -->
<script src="<?= base_url();?>assets/admin/vendor/jquery/jquery.js"></script>
<script src="<?= base_url();?>assets/admin/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
<script src="<?= base_url();?>assets/admin/vendor/bootstrap/js/bootstrap.js"></script>
<script src="<?= base_url();?>assets/admin/vendor/nanoscroller/nanoscroller.js"></script>
<script src="<?= base_url();?>assets/admin/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?= base_url();?>assets/admin/vendor/magnific-popup/magnific-popup.js"></script>
<script src="<?= base_url();?>assets/admin/vendor/jquery-placeholder/jquery.placeholder.js"></script>

<!-- Theme Base, Components and Settings -->
<script src="<?= base_url();?>assets/admin/javascripts/theme.js"></script>

<!-- Theme Custom -->
<script src="<?= base_url();?>assets/admin/javascripts/theme.custom.js"></script>

<!-- Theme Initialization Files -->
<script src="<?= base_url();?>assets/admin/javascripts/theme.init.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>

<style>
.center-sign img {    text-align: center;    margin: 0 auto;    display: block;    margin-bottom: 20px;    padding: 5px 3px; max-width: 100%; background: #0e80d5; }
.body-sign .panel-sign .panel-body {    padding-top: 22px;border-radius: 5px;    padding-bottom: 25px; }
.body-sign .center-sign {    margin-top: 150px;    padding: 0; }
.body-sign {    display: table;    height: 100vh;    margin: 0 auto;    padding: 0 15px;    width: 100%;    background: url(assets/images/proppicklogo_sing.jpg);    position: relative;    background-size: cover;    background-attachment: fixed; }
.center-sign {    margin-top: 15vh;    position: absolute;    left: 0;    right: 0; }
.center-sign {    width: 380px;    margin: 0 auto; }
.body-sign .panel-sign .panel-body {    border: 0;} 
.adm-logo {    background: #fff;    border-radius: 5px 5px 0 0;    border-bottom: 1px solid #ccc;}
.admin-bag {    box-shadow: 0px 0px 5px 3px #ccc;    border-radius: 6px; }
.input-group-icon .input-group-addon span.icon, .input-search .input-group-addon span.icon {    position: absolute;    top: 0;    bottom: 0;    left: 0;    border: 0;    z-index: 3;    width: auto;color:#fff;    display: inline-block;    vertical-align: middle;    text-align: center;    padding: 6px 12px;    background: #0e80d5;    line-height: 20px;    -webkit-box-sizing: content-box;    -moz-box-sizing: content-box;    box-sizing: content-box;    pointer-events: none; }
.panel.panel-sign p {    font-size: 14px;    text-align: center;    font-weight: 600;    margin-top: -5px;    margin-bottom: 13px; }
.input-group-icon .input-group-addon + input.form-control, .input-search .input-group-addon + input.form-control {    margin-left: 39px;    padding-left: 14px;    width: 88%;    height: 42px;    border-radius: 0; }
button.btn.btn-primary.hidden-xs.arrow-log {    width: 100%;    border: 0;    border-radius: 0;    padding: 9px 0;    margin-bottom: 10px;    background: #ffbb00;    color: #333;    font-weight: 600;    text-transform: uppercase;    transition: all 0.5s ease; }
button.btn.btn-primary.hidden-xs.arrow-log:hover {    color: #fff;    background: #d2e6ac; }
body:before {	content: "";    top: 0;    left: 0;    right: 0;    bottom: 0;    position: absolute; }
body { background:#444 position: relative;overflow-y: hidden; background-image: url("assets/frontend/images/access_bg.jpg"); }
p.text-center.text-muted.mt-md.mb-md {    color: #fafafa !important; }
.custom_error { bottom: 1px solid red; }
.form-bordered .form-group {border : none !important;}


</style>		
		
<script >
function admin_login_function(){
	var admin_usernamae = $('.admin_username').val();
	var admin_password = $('.admin_password').val();
	if(admin_usernamae=='')
	{
		$('.admin_username').addClass('custom_error');
		return false;
	} else {
		$('.admin_username').removeClass('custom_error');
	}
	if(admin_password=='')
	{
		$('.admin_password').addClass('custom_error');
		return false;
	} else {
		$('.admin_password').removeClass('custom_error');
	}
	if(admin_usernamae !='' && admin_password !='')
	{
		$('.admin_username').removeClass('custom_error');
		$('.admin_password').removeClass('custom_error');
		return true;
	}
}
$('.custom_required').blur(function(){
	var admin_usernamae = $('.admin_username').val();
	var admin_password = $('.admin_password').val();
	if(admin_usernamae != '' && admin_password != ''){
		$('.error_p').html('');
	}
});

</script>	

</body>
</html>