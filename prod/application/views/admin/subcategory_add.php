<!doctype html>
<html class="fixed">
	<head>

		<?php Include('includes/headerlinks.php');?>
		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="<?= $this->config->item('admin_assets');?>vendor/select2/select2.css" />
		<link rel="stylesheet" href="<?= $this->config->item('admin_assets');?>vendor/bootstrap-multiselect/bootstrap-multiselect.css" />
		<?php Include('includes/headerlinks2.php');?>
	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<?php include('includes/header.php');?>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php include('includes/menu_section.php');?>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Add SubCategory</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo $admin_url;?>">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><a href="<?php echo $admin_url;?>subcategory"><span>SubCategory</span></a></li>
								<li><span>Add SubCategory</span></li>
							</ol>
						</div>
					</header>

					<!-- start: page -->
						<div class="row">
							<div class="col-xs-12">
								<section class="panel">
									<header class="panel-heading">
										<h2 class="panel-title">Add SubCategory</h2>
									</header>
									<div class="panel-body">
										<?php /*print_r($brands_dropdown); echo '<br><br>'; print_r($category_dropdown);*/?>
										<?php $action = $admin_url."subcat_insert"; 
									 	echo form_open($action,array('id'=>"fm_brand_add",'class'=>"form-horizontal form-bordered custom_form",'autocomplete'=>"off")); ?>
											<div class="form-group">
												<label class="col-md-3 control-label">Category <span class="colon">:</span></label>
												<div class="col-md-6">
													<select data-plugin-selectTwo class="form-control populate " name="cat_id" required="required">
														<option value="">Select Category</option>
														<?php foreach ($category_dropdown as $category_dt) {
															echo '<option value="'.$category_dt["cat_sno"].'">'.ucwords($category_dt["cat_name"]).'</option>';
														}?>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-3 control-label">SubCategory Name <span class="colon">:</span></label>
												<div class="col-md-6">
													<input class="form-control custom_required" name="subcategory_name" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-3 control-label">Brands <span class="colon">:</span></label>
												<div class="col-md-6">
													<select name="brand_id[]" multiple data-plugin-selectTwo class="form-control populate " required="required">
														<?php foreach ($brands_dropdown as $brands_dt) {
															echo '<option value="'.$brands_dt["brand_sno"].'">'.ucwords($brands_dt["brand_name"]).'</option>';
														}?>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-3 control-label">Position<span class="colon">:</span></label>
												<div class="col-md-6">
													<input class="form-control number custom_required" onkeypress="return onlyNos(event,this);"  name="subcat_pos" />
												</div>
											</div>
											
											<div class="form-group">
												<label class="col-md-3 control-label"></label>
												<div class="col-md-6">
													<button type="submit" onclick='return validate_form();'  class="btn btn-primary custom_submit_btm" name="subcategory_add">Submit</button>
												</div>
											</div>
						
										<?php echo form_close();?>
									</div>
								</section>
							</div>
						</div>
					<!-- end: page -->
				</section>
			</div>
		</section>

		<?php include('includes/footerlinks.php');?>
		<!-- Specific Page Vendor -->
		<script src="<?= $this->config->item('admin_assets');?>vendor/select2/select2.js"></script>
		<script src="<?= $this->config->item('admin_assets');?>vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>
		<?php include('includes/footerlinks2.php');?>
		<!-- Examples -->
		<script src="<?= $this->config->item('admin_assets');?>javascripts/forms/examples.advanced.form.js"></script>

	</body>
</html>