
<?php include 'includes/header.php'?>
<!-- main starts-->
<main> 
  <!-- slider block starts-->
  <div class="container">
    <div class="row"> 
      <!-- main slider starts-->
      <div class="col-md-12 main-slider">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel"> 
          <!-- Indicators -->
          <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            <li data-target="#carousel-example-generic" data-slide-to="3"></li>
            <li data-target="#carousel-example-generic" data-slide-to="4"></li>
          </ol>
          <!-- Wrapper for slides -->
          <div class="carousel-inner">
            <div class="item active"> <a href="#"><img src="<?php echo base_url()?>assets/frontend/images/wines/1.jpg" alt="First slide"></a> </div>
            <div class="item"> <a href="#"><img src="<?php echo base_url()?>assets/frontend/images/wines/redwinesforwhite-header.jpg" alt="Second slide"> </a> </div>
            <div class="item"> <a href="#"><img src="<?php echo base_url()?>assets/frontend/images/wines/shop-wines-fb.jpg" alt="Third slide"> </a> </div>
            <div class="item"> <a href="#"><img src="<?php echo base_url()?>assets/frontend/images/wines/our-wine.jpg" alt="Fourth Slide"> </a> </div>
            
          </div>
          <!-- Controls --> 
          <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev"> <i class="fa fa-chevron-left"></i> </a> <a class="right carousel-control" href="#carousel-example-generic" data-slide="next"> <i class="fa fa-chevron-right"></i></a> </div>
        <!-- /carousel --> 
      </div>
      <!-- main slider ends--> 
      <!-- slider block ad block starts-->
     
      <!-- slider block ad block ends --> 
    </div>
  </div>
  <!-- slider block ends--> 
  
  <!-- secondary advertisement starts-->
  <div class="container second-ad">
    <div class="row">
      <div class="col-md-6">
        <figure><a href="#"><img src="<?php echo base_url()?>assets/frontend/images/ad01.png"></a></figure>
      </div>
      <div class="col-md-6">
        <figure><a href="#"><img src="<?php echo base_url()?>assets/frontend/images/ad02.png"></a></figure>
      </div>
    </div>
  </div>
  <!-- secondary adverisements ends --> 
  
  <!-- categories in home page starts-->
  <div class="container"> 
    
    <!-- categories title starts-->
    <div class="cat-title"> <span>Categories</span> <span class="allcat pull-right"><a href="javascript:void(0)">VIEW ALL CATEGORIES</a></span></div>
    <!-- categories title ends --> 
    
    <!-- all categories starts-->
    <div class="row">
      <div class="col-md-3">
        <div class="index-categories">
          <figure><a href="#"><img src="<?php echo base_url()?>assets/frontend/images/wines/beer-pic-1-300x255.jpg" title="Electronics"></a></figure>
          <article>
            <h3>Beer</h3>
          </article>
        </div>
      </div>
      <div class="col-md-3">
        <div class="index-categories">
          <figure><a href="#"><img src="<?php echo base_url()?>assets/frontend/images/wines/images (1).jpg" title="Electronics"></a></figure>
          <article>
            <h3>Wine</h3>
          </article>
        </div>
      </div>
      <div class="col-md-3">
        <div class="index-categories">
          <figure><a href="#"><img src="<?php echo base_url()?>assets/frontend/images/wines/mediumCutout.jpg" title="Electronics"></a></figure>
          <article>
            <h3>CHAMPAIGN</h3>
          </article>
        </div>
      </div>
      <div class="col-md-3">
        <div class="index-categories">
          <figure><a href="#"><img src="<?php echo base_url()?>assets/frontend/images/wines/61-owknAl9L._SX425_.jpg" title="Electronics"></a></figure>
          <article>
            <h3>Hard</h3>
          </article>
        </div>
      </div>
      
    </div>
    <!-- all categories ends --> 
    
  </div>
  <!-- caregories in home page ends--> 
  
  <!-- rent a car starts-->
  <div class="container">
  
    
  </div>
  <!-- rent a car end s--> 
  
</main>
<!-- main ends --> 

<?php include 'includes/footer.php'?>