<?php $this->load->view('includes/header');?>
<!-- main starts-->
<main>
  <div class="container"> 
    <!-- My account myorders starts here-->
    <div class="row">
      <div class="col-md-3 myorders-nav">
        <h3 class="bold">MY ACCOUNT</h3>
        <?php include 'myaccount-leftnav.php'?>
      </div>
      <div class="col-md-9">
      
      <div class="change-password">
      <!-- default address starts-->
      
      <div class="col-md-6 login-block text-left">
                    <h3>Change Password</h3>
                    
                    <h4 style="color:green !important;">
                    
                     <?php if(isset($success_msg)) { echo $success_msg;  } ?>    
                        
                    </h6> 
                    
                    
                    <form   method="post">
					  
                      
                      <div class="logblock">
                        <input type="password" class='new'  required name="new_password"placeholder="New Password">
                      </div>
                      <div class="logblock">
                        <input required type="password" class='con' name= "confirm_password"  placeholder="Confirm New Password">
                        
                       <span id="error_msg" style="color:red !important;display:none !important;">
                           
                           Password & Confirm Passwords should be match.
                           
                          </span> 
                                
                        
                      </div>
                     
                      <div class="logblock">
                        <button class="sub_change" type="submit" name="change_password">SUBMIT</button>
                      </div>
                      
                      <!-- recover your social account-->
                      
                    </form>
                  </div>
                   <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
    <script>
    
    $(document).ready(function(){
        
        $('.sub_change').click(function(){
            
          var pass = $('.new').val();
          
          var con = $('.con').val();
           
            
           
            if(pass!=con){
                  $('#error_msg').show();
                  return false;
            }else{
                $('#error_msg').hide();
                  return true;
            }
            
            
            
        });        
    
        
        
    });
    
    
    
    
   </script>    
                  
      	
        
        
        
       
        </div>
        
        
       
      <!-- default address ends -->
        
        
      </div>
    </div>
  </div>
  <!-- my account myorders ends here-->
  </div>
  
  <!-- cart page ends --> 
  
</main>
<!-- main ends -->

<?php include 'includes/footer.php'?>
