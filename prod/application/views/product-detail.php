<?php  $this->load->view('includes/header');  


?>

<style>

@import url(//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css);

fieldset, label { margin: 0; padding: 0; }
body{ margin: 20px; }
h1 { font-size: 1.5em; margin: 10px; }

/****** Style Star Rating Widget *****/

.rating { 
  border: none;
  float: left;
}

.rating > input { display: none; } 
.rating > label:before { 
  margin: 5px;
  font-size: 1.25em;
  font-family: FontAwesome;
  display: inline-block;
  content: "\f005";
}

.rating > .half:before { 
  content: "\f089";
  position: absolute;
}

.rating > label { 
  color: #ddd; 
 float: right; 
}

/***** CSS Magic to Highlight Stars on Hover *****/

.rating > input:checked ~ label, /* show gold star when clicked */
.rating:not(:checked) > label:hover, /* hover current star */
.rating:not(:checked) > label:hover ~ label { color: #FFD700;  } /* hover previous stars in list */

.rating > input:checked + label:hover, /* hover current star when changing rating */
.rating > input:checked ~ label:hover,
.rating > label:hover ~ input:checked ~ label, /* lighten current selection */
.rating > input:checked ~ label:hover ~ label { color: #FFED85;  } 

</style>



<style>
#snackbar {
    visibility: hidden;
    min-width: 250px;
    margin-left: -125px;
    background-color: green;
    color: #fff;
    text-align: center;
    border-radius: 2px;
    padding: 16px;
    position: fixed;
    z-index: 1;
    left: 50%;
    bottom: 30px;
    font-size: 17px;
}

.review_count {
    
    margin-top: 7px;
    margin-left: 3px;
}



#snackbar.show {
    visibility: visible;
    -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
    animation: fadein 0.5s, fadeout 0.5s 2.5s;
}

@-webkit-keyframes fadein {
    from {bottom: 0; opacity: 0;} 
    to {bottom: 30px; opacity: 1;}
}

@keyframes fadein {
    from {bottom: 0; opacity: 0;}
    to {bottom: 30px; opacity: 1;}
}

@-webkit-keyframes fadeout {
    from {bottom: 30px; opacity: 1;} 
    to {bottom: 0; opacity: 0;}
}

@keyframes fadeout {
    from {bottom: 30px; opacity: 1;}
    to {bottom: 0; opacity: 0;}
}
</style>











<!-- main starts-->
<main> 
  <!-- product page starts -->
  <section class="products-page">
    <div class="container"> 
      <!--brudcrumb starts-->
      <div class="brcrumb-products"> 
        <!-- brudcrumb starts-->
        <ul>
          <li><a href="<?php echo base_url()?>">Home <i class="fa fa-angle-right"></i> </a></li>
          <li><a href="<?php echo base_url()?>alcohol">Alchol<i class="fa fa-angle-right"></i> </a></li>
          <li><a href="<?php echo base_url()?><?php echo str_replace(" ","-",strtolower($get_single_product->cat_name)) ?>/<?php echo str_replace(" ","-",strtolower($get_single_product->sub_cat_name)) ?>/sub-cat"><?php echo $get_single_product->sub_cat_name ?><i class="fa fa-angle-right"></i> </a></li>
          <li><a><?php echo ucfirst($get_single_product->prod_name)?></a></li>
        </ul>
        <!-- brudcrumb ends --> 
        
      </div>
      <!-- brudcrumb ends --> 
      
      <!-- product detail starts here-->
      <div class="product-detail-block"> 
        <!-- product gallery and information starts-->
        <div class="row"> 
          <!-- product detail gallery starts-->
          <div class="col-md-3 text-center">
            <div class="product-gallery">
                
              <?php  
			    
				$get_user_id = $this->session->userdata('userId');
				
			 
			  
			  
			  $get_fav_products = $this->db->query("select * from user_fav_products where user_id = '".$get_user_id."' group by product_id")->result();
			  
			  $get_fav_prod_ids = array();
			  
			 foreach($get_fav_products as $ke=>$vl){
			     
			  $get_fav_prod_ids[] = $vl->product_id;    
			     
			 }
				
				
			    if(empty($get_user_id)){
			         $hre = base_url().'Listing/add_fav_prod/'.$get_single_product->prod_sno;
			         $class = "";
			     }else{
			          $class = "add_fav";
			          $hre = "#";
			     }
			     
			     
			     
if (in_array($get_single_product->prod_sno, $get_fav_prod_ids))
  {
      $class = "";
      $hre = "#";
      $sty = "green";
      $color = "white";
  }else{
      $sty = "none";
      $color = "black";
  }
			     
		 ?>
                
              <div class="short-list text-right"> 
			  
			   <a  class="<?php echo $class;?>" data-prod-id="<?php echo $get_single_product->prod_sno; ?>"> 
			  <i class="fa fa-heart-o" style="background-color:<?php echo $sty;?>;color:<?php echo $color;?>;" ></i> </a>
			  </div>
              <!-- gallery carousel starts-->
			  <?php 
			    $query = $this->db->query("SELECT * FROM `product_images` 
			  where prod_sno='".$get_single_product->prod_sno."'"); $resultImg = $query->result_array();
			  ?>
              <div class="carousel slide article-slide" id="article-photo-carousel"> 
                
                <!-- Wrapper for slides -->
                <div class="carousel-inner cont-slider">
				 <?php $i="0";foreach($resultImg as $val){ $i++;?>
                  <div class="item <?php if($i==1){ echo 'active';}?>"> <img alt="" title="" 
				  src="<?php echo base_url()?>assets/images/gallery/<?php echo str_replace(" ","_",strtolower($get_single_product->sub_cat_name)) ?>/<?php echo str_replace(" ","_",strtolower($get_single_product->prod_name)) ?>/<?php echo $val['product_img_name'] ?>"> </div>
				 <?php }?>
                </div>
                <!-- Indicators -->
                <ol class="carousel-indicators">
				<?php $i="";foreach($resultImg as $val){ $i++;?>
                  <li class="<?php if($i==1){ echo 'active';}?>" data-slide-to="<?php echo $i ?>" data-target="#article-photo-carousel"> 
				  <img alt="" src="<?php echo base_url()?>assets/images/gallery/<?php echo str_replace(" ","_",strtolower($get_single_product->sub_cat_name)) ?>/<?php echo str_replace(" ","_",strtolower($get_single_product->prod_name)) ?>/<?php echo $val['product_img_name'] ?>"> </li>
				<?php }?>
                </ol>
              </div>
              <!-- gallery carousel ends --> 
            </div>
          </div>
          <!-- product detail gallery ends --> 
          <!-- product detail right starts-->
           
          
                 
                     
          
          <div class="col-md-6">
            <div class="product-detail-right">
			<?php 
					$this->db->select('*');
					$this->db->from('brand');
					$this->db->where('brand_status',1);
					$this->db->order_by('brand_sno', 'asc');
					$query = $this->db->get();
					$getBrandName = $query->row();
			?>
			
			
			
			
			
              <h2 class="bold detail-title"><b>Item Name </b>: <?php echo ucfirst($get_single_product->prod_name)?></h2>
             
              <!-- specifications primary starts-->
              <ul class="primary-specifications">
			  
                <li><b>Brand </b>: <?php echo $getBrandName->brand_name ?> </li>
                  <li><b>Origin Of Country </b>: <?php echo ucfirst($get_single_product->origin_of_country)?></li>
                 
                
               
              </ul>
              <!-- specifications primary ends --> 
              <!-- price block starts-->
              <div class="detail-price">
			  <input type="hidden" id="offer_price_old" value="<?php echo $get_single_product->prod_dis_price; ?>">
			  <span id="offer-price" class="offer-price">  NOK <span id="offer_price">
			  <?php echo $get_single_product->prod_dis_price ?></span></span>  </div>
			  
			  
		
			  
			  
			  
			  
              <!-- price block ends--> 
              <!-- emi offers starts-->
              
              <!-- emi offers ends --> 
              <!-- check delivery starts-->
		  <form action="<?php echo base_url();?>cart/add_to_cart" method="post">
		  <div class="prod_quantity_show"> 
		  <p class="prod_quantity_label">Quantity <span>:</span></p>
					<div class="prod_quant_inc">
						<div class="prod_quantity_change">
							<input type="text" style="width:20%" id="quantity" name="quantity" value="1" class="pro_quantity number" onkeypress="return onlyNos(event,this);" maxlength="2" autocomplete="off">
							
							<div class="prod_quan_inc_dec">
							   <button type="button" class="prod_quan_dec_p">-</button>
								<button type="button" class="prod_quan_inc_p">+</button>
								
								<div class="clearfix"></div>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
					<div class="clearfix"></div>
		           <input type="hidden" style="width:20%" id="product_sno" name="product_sno" value="<?php echo $get_single_product->prod_sno ?>" class="pro_quantity number"  >
			  </div>
			  <style>
			  .prod_quantity_show {
					margin: 0 0 20px 0;
				}
				.prod_quantity_label {
					float: left;
					width: 80px;
					line-height: 42px;
					margin: 0;
					font-weight: 500;
				}
				.prod_quant_inc {
					float: left;
					/* width: calc(100% - 80px);*/
					width: 100%;
				}
				.prod_quantity_change {
					border: 1px solid #fff;
					    height: 39px;
						width: 40%;
					background: #fff;
					border-radius: 23px;
					box-shadow: 0px 0px 3px #6d6d6d;
				}
				.pro_quantity.number {
					float: left;
					width: 34px !important;
					padding: 5px 0;
					border: none;
					text-align: center;
					height: 38px;
					outline: none !important;
					border-radius: 50%;
				}
				input, button, select, textarea {
					font-size: inherit;
					line-height: inherit;
				}
				.prod_quan_inc_dec {
					float: right;
					width: calc(100% - 34px);
					margin: 0 0 0 0px;
					border: none;
					height: 38px;
					text-align: right;
					padding: 0 10px 0 0;
				}
				input, button, select, textarea {
					font-size: inherit;
					line-height: inherit;
				}
				

				.prod_quan_inc_dec button {
					margin: 7px 3px;
					padding: 0;
					cursor: pointer;
					width: 24px;
					height: 24px;
					line-height: 24px;
					font-size: 20px;
					color: #fff;
					display: inline-block;
					border-radius: 50%;
					text-align: center;
					border: none;
					outline: none;
					background-color: green;
				}

			  </style>
              <!-- check delivery ends --> 
              <!-- button block starts-->
              <div class="btn-block-detail">
   
            
                <button type="submit" class="add_cart">ADD TO CART</button> 
                
                </form>
              </div>
              <!-- button block ends --> 
              
              
               
              
              
            </div>
          </div>
          
            
        
              
            <div class="col-md-3">
                 <div class="product-detail-right">
                 <ul class="primary-specifications">     
                 <li>
                 <?php  $prd_user = $get_single_product->prod_created_by;  
                 
                      
                 
                 
                 if($get_single_product->prod_created_by==89){ echo "Admin"; }else{    
                     
                     $get_sellar_name = $this->db->query("select * from users where id = '".$prd_user."'")->row();  
                                                     echo $get_sellar_name->full_name; 
                                    };?> 
                                    
                                    
                    <?php
                    $prod_id = $get_single_product->prod_sno;
                    $get_prd_rating =  $this->db->query("select AVG(rating) as avg from user_reviews where prod_id = '".$prod_id."'")->row();  
                    $rating = round($get_prd_rating->avg);
                    
                  
                    
                    $get_count =  $this->db->query("select   * from user_reviews where prod_id = '".$prod_id."'")->result();  
                   
                    $get_prod_review_count = count($get_count);
                    
                    if($get_prod_review_count==""){
                         $get_prod_review_count = 0;
                      }
                   
                    
                   
                    ?>       </li>
                     
                   
                   
                  
                     
                     
                      
        <li style="width:100% !important;">   <fieldset class="rating">
    <input type="radio" id="star5" <?php if($rating=="5") { ?>  checked  <?php } ?>  name="rating" value="5"><label class = "full" for="star5" title="Awesome - 5 stars"></label>
     
    <input type="radio" id="star4"  <?php if($rating=="4") { ?>  checked  <?php } ?>  name="rating" value="4"><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
   
    <input type="radio" id="star3"   <?php if($rating=="3") { ?>  checked  <?php } ?> name="rating" value="3"><label class = "full" for="star3" title="Meh - 3 stars"></label>
    
    <input type="radio" id="star2" <?php if($rating=="2") { ?>  checked  <?php } ?>  name="rating" value="2"><label class = "full" for="star2" title=" bad - 2 stars"></label>
    
    <input type="radio" id="star1" <?php if($rating=="1") { ?>  checked  <?php } ?>  name="rating" value="1"><label class = "full" for="star1" title=" big time - 1 star"></label>
                  
                  
                  
                   </fieldset>  
                   
                    <label class="review_count"> - (<?php echo $get_prod_review_count;?> Ratings)  </label> 
                   
                   </li> 
                   
                     <li style="width:100% !important;">    
                            
                     </li>          
                         
                    </div>     
            </div> 
                        
          
          
          <!-- product detail right ends --> 
        </div>
        <!-- product gallery and information ends --> 
        
        <!-- poroduct detail specifications starts-->
        <div class="product-specifications">
          <h2 class="title-pro-speci">Description</h2>
          <div class="col-md-12">
            <table width="100%">
            
              <tr>
                <td colspan="2" width="20%"><h3 class="speci-title"><?php echo $get_single_product->prod_des?> </h3></td>
              </tr>
             
            </table>
          </div>
        </div>
        <!-- product detail specifications ends --> 
        
        <!-- ratings and reviews starts-->
        <!-- review block ends --> 
          
        </div>
        <!-- ratings and reviews ends --> 
        
       
        <!-- detail similar products block ends --> 
      </div>
      
      <!-- product detail ends here--> 
      
    </div>
  </section>
  <!-- product page ends --> 
  
</main>
<!-- main ends -->

<?php include 'includes/footer.php'?>
<script src="http://mahithawebsol.com/water_online/assets/detailed/jquery-1.9.1.min.js"></script>
<script>

/*===========     ENTER ONLY NUMBERS     ============*/
function onlyNos(e, t) {
	try {
		if (window.event) {
			var charCode = window.event.keyCode;
		}
		else if (e) {
			var charCode = e.which;
		}
		else { return true; }
		if (charCode > 31 && ((charCode < 48 && charCode != 46 ) || charCode > 57 )) {
			return false;
		}
		return true;
	}
	catch (err) {
		alert(err.Description);
	}
}
/*===========     ENTER ONLY CHARACHERS (EXCEPT NUMBERS)     ============*/

$(document).ready(function(){
	
	$('#quantity').blur(function(){
		
		var q = $('#quantity').val();
		if(q<1 || q==''){$('#quantity').val('1')}
	});
    $('.prod_quan_inc_p').click(function(){
		var quan = $('#quantity').val();
		var product_cost = $('#offer_price_old').val();
			
		if(quan < 99){ quantity = parseInt(quan)+1; } else { quantity = quan; }
		$('#quantity').val(quantity);
		var productFinal = parseFloat(product_cost) * parseFloat(quantity);
		
	
		$('#offer_price').html(productFinal);
	});
	$('.prod_quan_dec_p').click(function(){
		var quan = $('#quantity').val();
		var product_cost = $('#offer_price_old').val();
		var productFinal = parseFloat(offer_price)*parseInt(quan);
		if(quan > 1){ quantity = parseInt(quan)-1; } else { quantity = 1; }
		$('#quantity').val(quantity);
		var productFinal = parseFloat(product_cost) * parseFloat(quantity);
		$('#offer_price').html(productFinal);
	});
	
});
</script>










<button id="show_snack" style="display:none;" onclick="myFunction()">Show Snackbar</button>

<div id="snackbar"> Product is Added to Favourite Successfully</div>

<script>
function myFunction() {
    var x = document.getElementById("snackbar");
    x.className = "show";
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
}
</script>
</script>

<script>

 $(document).ready( function() {
	 
   $(".add_fav").click(function(){
  
    var prod_id = $(this).attr('data-prod-id');
  
        url =  "<?php echo base_url();?>Listing/add_fav_prod/"+prod_id;   
     
       dataString = "prod_id="+prod_id; 
     
     
     $.ajax({
      type: "POST",
      url: url,
      data: dataString,
      success: function( response ) {
         if(response=="success"){
        $("#show_snack").click();
         }else{
            $('.confirm').hide();    
        }
       }
    });
    
    
     
   });  
  
 

});

 
</script>








