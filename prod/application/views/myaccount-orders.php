<?php  $this->load->view('includes/header'); ?>

<style>

@import url(//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css);

fieldset, label { margin: 0; padding: 0; }
body{ margin: 20px; }
h1 { font-size: 1.5em; margin: 10px; }

/****** Style Star Rating Widget *****/

.rating { 
  border: none;
  float: left;
}

.rating > input { display: none; } 
.rating > label:before { 
  margin: 5px;
  font-size: 1.25em;
  font-family: FontAwesome;
  display: inline-block;
  content: "\f005";
}

.rating > .half:before { 
  content: "\f089";
  position: absolute;
}

.rating > label { 
  color: #ddd; 
 float: right; 
}

/***** CSS Magic to Highlight Stars on Hover *****/

.rating > input:checked ~ label, /* show gold star when clicked */
.rating:not(:checked) > label:hover, /* hover current star */
.rating:not(:checked) > label:hover ~ label { color: #FFD700;  } /* hover previous stars in list */

.rating > input:checked + label:hover, /* hover current star when changing rating */
.rating > input:checked ~ label:hover,
.rating > label:hover ~ input:checked ~ label, /* lighten current selection */
.rating > input:checked ~ label:hover ~ label { color: #FFED85;  } 

</style>


 <style>
#snackbar {
    visibility: hidden;
    min-width: 250px;
    margin-left: -125px;
    background-color: green;
    color: #fff;
    text-align: center;
    border-radius: 2px;
    padding: 16px;
    position: fixed;
    z-index: 1;
    left: 50%;
    bottom: 30px;
    font-size: 17px;
}

#snackbar.show {
    visibility: visible;
    -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
    animation: fadein 0.5s, fadeout 0.5s 2.5s;
}

@-webkit-keyframes fadein {
    from {bottom: 0; opacity: 0;} 
    to {bottom: 30px; opacity: 1;}
}

@keyframes fadein {
    from {bottom: 0; opacity: 0;}
    to {bottom: 30px; opacity: 1;}
}

@-webkit-keyframes fadeout {
    from {bottom: 30px; opacity: 1;} 
    to {bottom: 0; opacity: 0;}
}

@keyframes fadeout {
    from {bottom: 30px; opacity: 1;}
    to {bottom: 0; opacity: 0;}
}
</style>


<!-- main starts-->
<main>
  <div class="container"> 
    <!-- My account myorders starts here-->
    <div class="row">
      <div class="col-md-3 myorders-nav">
        <h3 class="bold">MY ACCOUNT</h3>
        <?php include 'myaccount-leftnav.php'?>
      </div>
      <div class="col-md-9 right-myaccount">
      
      <div class="row"><h3 class="bold"><?php echo ucfirst($_SESSION['adminName'])?> ORDERS</h3></div>
	  <?php 
	      
	       
	  $grandTotal = 0;
	  
 
	  
	  
	  foreach($products as $val){  $query = $this->db->query("SELECT * FROM `product_images` 
			  where prod_sno='".$val['prod_sno']."'"); $resultImg = $query->row();
			 
			 
			  
			 $grandTotal +=  $val['prod_dis_price'] * $val['quantity'];
			 
			 
	 
			 
			 ?>
			  
        <div class="row">
          <div class="myorde">
            <div class="myorder-header">
              <div class="pull-left">
                <p>Order ID: <?php echo $val['sno']?> <span>(<?php echo count($products) ?>) Items</span></p>
                <small>Placed on <?php echo date("d M Y",strtotime($val['order_date_time'])) ?></small> </div>
              <a href="<?php echo base_url();?>detailed/order_confirm2/<?php echo $val['order_no']; ?>" class="btn btn-danger pull-right" type="button" value="DETAILS"> DETAILS </a>
            </div>
            <div class="col-md-3">
              <figure>
			  <img class="img-responsive" src="<?php echo base_url()?>assets/images/gallery/<?php echo str_replace(" ","_",strtolower($val['sub_cat_name'])) ?>/<?php echo str_replace(" ","_",strtolower($val['prod_name'])) ?>/<?php echo $resultImg->product_img_name ?>"></figure>
            </div>
            <div class="col-md-9 myorder-details">
              <h5><?php echo $val['prod_name']?></h5>
             <!-- <input type="btn" class="btn btn-default" value="GET INVOICE"> !-->
              <div class="deliv">
                <p class="pull-left">Status:  <?php if($val['order_status']==0){ echo "Pending"; }else if($val['order_status']==1)  {echo "Dispatched";}else{ echo "Delivered"; } ?> </p>
                <p class="pull-right"> <?php if($val['order_status']==2){?>   <span class="bold text-right">Delivered On:</span> <?php echo date("d M Y",strtotime($val['shipping_date'])) ?> <?php } ?> </p>
              </div>
              <div>
                <p><small>7-Day Easy Returns Policy period has ended. You cannot return / replace your product now.</small></p>
                 
                <button type="btn" data-toggle="modal"  data-prod-name = "<?php echo $val['prod_name']; ?>" data-target="#myModal" data-src="<?php echo base_url()?>assets/images/gallery/<?php echo str_replace(" ","_",strtolower($val['sub_cat_name'])) ?>/<?php echo str_replace(" ","_",strtolower($val['prod_name'])) ?>/<?php echo $resultImg->product_img_name ?>"  data-id="<?php echo $val['prod_sno'];?>"   class="btn btn-success user_review"> Write a Review </button>  
                
                
              </div>
            </div>
          </div>
        </div>
	  <?php }?>
         
      </div>
    </div>
  </div>
  <!-- my account myorders ends here-->
  </div>
  
  <!-- cart page ends --> 
  
</main>
<!-- main ends -->


 
  
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 
 
 
 
 

<div class="container">
  
  
  

  
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
  
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h6   style="color:black !important;"> Write a product review </h6>
        </div>
        
        <div class="modal-footer">
            
         <form method="post" action ="<?php echo base_url();?>Myaccount_orders/prod_review">
            
            <div class="row col-md-12">
                
                <div class="row col-md-3">    
           <div class="form-group">
               
          
            <img height="50" id="prod_img" width="50"> 
            
           </div>
           </div>
                
                
                
                
            <div class="row col-md-3">
                
                 <div class="form-group">
                
                      <div class="prd_name" style="color:#333 !important;font-size:20px !important;">   </div>
                </div>
                </div>
                
            
            </div>
           
            <div class="row col-md-12">
           <div class="form-group">
          
           <fieldset class="rating">
    <input type="radio" id="star5" name="rating" value="5" /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
     
    <input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
   
    <input type="radio" id="star3"  checked name="rating" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
    
    <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="  bad - 2 stars"></label>
    
    <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="  big time - 1 star"></label>
   
</fieldset> 

        </div>
        
         </div>  
        
        <div class="row col-md-12">
        <div class="form-group">
            
            
            <input type="hidden" name="prod_id" id="prod_id"> 
            
             <textarea  name="comments" cols="85" placeholder="Write your review" rows="3"  style="color:black !important;" >  </textarea> 
        
       
       
        </div>    
        
            </div>    
            
            
            
            
            
            
          <button type="submit" class="btn btn-success">Submit</button>
          
          </form>   
          
          
        </div>
      </div>
      
    </div>
  </div>
  
</div>

</body>
</html>



 <script>
  
   $(document).ready(function(){
       
      
     
      
      $('.user_review').click(function(){
          
          
           
       
         
        var get_src = $(this).attr('data-src');
        
        var get_name = $(this).attr('data-prod-name');
        
        var prod_id = $(this).attr('data-id');
          
           // alert(get_src);
          
 
        $('#prod_img').attr("src",get_src);
        
           $('#prod_id').val(prod_id);
        
           $('.prd_name').html(get_name);
          
      }); 
       
       
       
       
       
       <?php if($this->session->flashdata('user_review')) { ?>    
            
           
        $('#show_snack').click();
        
        <?php } ?>
     
       
       
       
       
       
       
       
   });
  
  
  
  </script>
 


<button id="show_snack" style="display:none !important;" onclick="myFunction()">Show Snackbar</button>

<div id="snackbar">  <?php echo  $this->session->flashdata('user_review'); ?>      </div>

<script>
function myFunction() {
    var x = document.getElementById("snackbar");
    x.className = "show";
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
}
</script>

