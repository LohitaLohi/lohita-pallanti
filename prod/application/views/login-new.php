<?php
 $this->load->view('includes/header'); ?>
<!-- main starts-->
<?php if(!(isset($_SESSION['userId'])))
{ ?>
<main> 

		<script src="https://code.jquery.com/jquery-2.1.1.js"></script>
		<script src="https://code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
		<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.1/themes/ui-darkness/jquery-ui.css">

		<script>
		var $j = jQuery.noConflict();

		$j(function() {
		$j("#exp_date").datepicker({
		dateFormat:"yy-mm-dd"
		});

	 
		});
		</script>
   
  <!-- login starts-->
  	<div class="row">
    	<div class="container loginpage">
        	<div class="bhoechie-tab-container">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
              <div class="list-group"> <a href="#" class="list-group-item active text-center"> <i class="fa fa-sign-in"></i><br/>
                Login Your Account </a> <a href="#" class="list-group-item text-center"> <i class="fa fa-user-plus"></i><br/>
                Create New Account </a> <a href="#" class="list-group-item text-center"> <i class="fa fa-unlock-alt"></i><br/>
                Forgot Password? </a> </div>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab"> 
              <!-- flight section -->
              <div class="bhoechie-tab-content active">
                <center>
                  <!-- login starts here-->
                  <div class="col-md-6 login-block text-left">
                    <h3>User Login</h3>
                    <form name='user_login' id='user_login' method='post' name="myform" action='<?php echo base_url()?>Login_new/login_submit'>
                      <div class="logblock">
                        <input type="text" placeholder="Enter email" id="user_name" name='user_id' required>
                      </div>
                      <div class="logblock">
                        <input type="password" placeholder="Enter Password" id='user_pwd' name='user_pwd' required>
                      </div>
                      <div class="logblock">
                        <input type='submit' id="submit"  class="btn btn-success onclick="validate()" name='user_login' value='Login'>
						<span style='color:red'>
					   <?php echo $this->session->flashdata('error_message'); ?>
				       </span>
				
                             </div>
						<script language = "JavaScript">
                        </script>

                      <div class="logblock"> New to Online Snap? <a href="javascript:void(0)">SIGNUP</a> </div>
                      <!-- recover your social account-->
                      <div class="logblock">
                        <div class="box">
                          <!--<div class="input-group"> <span class="input-group-addon addon-facebook"> <i class="fa fa-fw fa-2x fa-facebook fa-fw"></i> </span> <a class="btn btn-lg btn-block btn-facebook" href="#"> Login with Facebook</a> </div>
                          <div class="input-group"> <span class="input-group-addon addon-twitter"> <i class="fa fa-fw fa-2x fa-twitter fa-fw"></i> </span> <a class="btn btn-lg btn-block btn-twitter" href="#"> Login with Twitter</a> </div>-->
                        </div>
                        <!-- recover your social account ends --> 
                      </div>
                    </form>
                  </div>
                  <!-- login ends -->
                  
                  <div class="col-md-6"> 
                    <!-- login priority -->
                    <ul class="login-instructions text-left">
                      <li> <i class="fa fa-truck"></i> <span> Manage Your Orders<br>
                        Easily Track Orders, Create Returns </span> </li>
                      <li> <i class="fa fa-bell"></i> <span> Make Informed Decisions<br>
                        Get Relevant Alerts And Recommendations </span> </li>
                      <li> <i class="fa fa-heart"></i> <span> Engage Socially<br>
                        With Wishlists, Reviews, Ratings </span> </li>
                    </ul>
                    <!-- login priority ends --> 
                  </div>
                </center>
              </div>
              <!-- train section -->
              <div class="bhoechie-tab-content">
                <center>
                  <!-- login starts here-->
                  <div class="col-md-6 login-block text-left">
                    <h3>User Register</h3>
                    
                    <h5 style="color:red !important;">	<?php echo $this->session->flashdata('reg_error'); ?> </h5>
                    
                    <form method='post' action="<?php echo base_url();?>Login_new/add_user"  enctype="multipart/form-data" id='reg_validate' name="myForm" >
				  	<div class="logblock">
                        <input type="text" placeholder="Full Name"  id="user_name"   name='user_name' required>
                        
                        	<span   style="display:none;color:red !important;" class="uname"> Full Name contains characters only</span>
                       </div>
                      <div class="logblock">
                        <input type="text" placeholder="Mobile Number" id="user_phone"   name="user_phon" required>
                      </div>
                      <div class="logblock">
                          
                          
                          
                        <input type="text" id='user_email' placeholder="Email Address"  name='user_email' required>
                        
					 	<span   style="display:none;color:red !important;" class="email">  Enter Valid Email  </span>
					 
					 
                      </div>
                      <div class="logblock">
                        <input type="password" placeholder="Create Password"  id='user_pwd' name='user_pwd' required>
                      </div>
                      <div class="logblock pass">
                        <input type="password" placeholder="Confirm Password" id='user_cpwd' name='user_cpwd' required>
						<span id='pw_check' style="display:none;color:red !important;" class="upass_error"> Password & confirm Password Should be match.</span>
                      </div>
                      
                     
                     
                    
                    
                     
                     
                     
                     
                     
                      
                      
                    
                      <div class="logblock">
                        <input type='submit' id="submit" class="btn btn-success"   name='user_registeration' class="user_register" value='Register'>
				
				 &nbsp;&nbsp;Already have an account?<a href="javascript:void(0)"> LOGIN</a> </div>
					
					<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
					
						<script>
					 
$(document).ready(function(){
    
    
    
     
    
    
    
    
    
    
    
      <?php if(isset($reg_err)){ ?>
     
        $('.fa-user-plus').click();
                       
      <?php }  ?>
    
    
    $(".user_register").click(function(){
        var upass = $('#user_pwd').val();
        var uconfirm_password = $('#user_cpwd').val();
       
       var email = $('#user_email').val();
       
       
         var   email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
       
      
             var errors = '';
      
           if (!email_regex.test(email)) {
           
              errors = true;
            
              $('.email').show();
           }else{
                
                $('.email').hide();
           }
               
         
        
            if(upass == uconfirm_password){
                errors = true;
            
               
                
            $('.upass_error').show();
            }else{
                
             $('.upass_error').hide();
            }
      
     // alert(errors);
         
        if(errors==''){
              return true;      
          }else{
               return false;
           }  
      
       
       
       
   });
    
    
    
});
       
</script>

                      
                      <!-- recover your social account-->
                      
                    </form>
					
                  </div>
                  <!-- login ends -->
                  
                  <div class="col-md-6"> 
                    <!-- login priority -->
                    <ul class="login-instructions text-left">
                      <li> <i class="fa fa-truck"></i> <span> Manage Your Orders<br>
                        Easily Track Orders, Create Returns </span> </li>
                      <li> <i class="fa fa-bell"></i> <span> Make Informed Decisions<br>
                        Get Relevant Alerts And Recommendations </span> </li>
                      <li> <i class="fa fa-heart"></i> <span> Engage Socially<br>
                        With Wishlists, Reviews, Ratings </span> </li>
                    </ul>
                    <!-- login priority ends --> 
                  </div>
                </center>
              </div>
              
              <!-- hotel search -->
              <div class="bhoechie-tab-content">
                <center>
                  <center>
                    <!-- login starts here-->
                    <div class="col-md-6 login-block text-left">
                      <h3>Forgot Password?</h3>
                      
                      <span class="fsuccess_msg" style="display:none;color:green;"> A link send to your email to change your password  </span>
                      
                      <span class="ferr_msg" style="display:none;color:red;">  Your not Registered in our website </span>
                      
                      <form method="post" id="get_password">
                        <div class="logblock">
                          <input type="text" required name="forget_password" placeholder="Enter Registered Email Address">
                        </div>
                       <!-- <div class="logblock"> You will get New Password to your Registered Mobile</div> !-->
                        <div class="logblock">
                            
                             
                            
                          <button type="submit" class="btn btn-success class="get_password">GET NEW PASSWORD</button>
                        </div>
                      </form>
                    </div>
                    <!-- login ends -->
                    
                    <div class="col-md-6"> 
                      <!-- login priority -->
                      <ul class="login-instructions text-left">
                        <li> <i class="fa fa-truck"></i> <span> Manage Your Orders<br>
                          Easily Track Orders, Create Returns </span> </li>
                        <li> <i class="fa fa-bell"></i> <span> Make Informed Decisions<br>
                          Get Relevant Alerts And Recommendations </span> </li>
                        <li> <i class="fa fa-heart"></i> <span> Engage Socially<br>
                          With Wishlists, Reviews, Ratings </span> </li>
                      </ul>
                      <!-- login priority ends --> 
                    </div>
                  </center>
                </center>
              </div>
            </div>
          </div>
        </div>
    </div>
  <!-- login ends -->
  
</main>
<?php } 
else
{
	// header("location:myaccount-orders.php");
}
?>
<!-- main ends -->
 


<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js">
</script>

<script>

 $(document).ready( function() {
 
 
 
  $("#get_password").submit(function(e){
  
  e.preventDefault();
   var form =  $("#get_password");
           
   url =  "<?php echo base_url();?>Checkout/get_password";   
           
    $.ajax({
      type: "POST",
      url: url,
      data: form.serialize(),
      success: function( response ) {
         if(response=="success"){
           
          $('.fsuccess_msg').show();
          $('.ferr_msg').hide();
         
          }else{
            
            $('.fsuccess_msg').hide();
           $('.ferr_msg').show();
                
        }
       }
    });
    
    
    return false;
  
   });  
  
    
 
    
});

 
</script>

















<script type="text/javascript" src="<?php echo base_url()?>assets/frontend/js/emailcheck.js"></script> 
<?php include 'includes/footer.php'?>
<script src="<?php echo base_url()?>assets/frontend/js/lib/jquery.js"></script>
<script src="<?php echo base_url()?>assets/frontend/js/jquery.validate.js"></script>
<script src="<?php echo base_url()?>assets/frontend/js/validations.js"></script>