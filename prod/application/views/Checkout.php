 <?php include ('includes/header.php') ?>
<!-- main starts-->
<main> 
  <!-- payment process starts-->
  
  <div class="container payment">
    <div class="bhoechie-tab-container">
      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
        <div class="list-group"> 
		<a href="<?php echo base_url()?>checkout/checkout_add" class="list-group-item active text-center"> 
		<span>1</span>Confirm Address </a> 
		 
		<a href="<?php echo base_url()?>Checkout" style="display:none;"  class="list-group-item text-center make_payment"> 
		<span>2</span>Make Payment </a>		</div>
      </div>
      
      <?php 
            
            $this->load->database();
            $get_session_id = $this->session->userdata('userId');
         $get_addresses = $this->db->query("select * from checkout_address where user_id='".$get_session_id."' order by con_no desc")->row();
          ?>
      
     
      
      
      <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab"> 
        <!-- flight section -->
        <div class="bhoechie-tab-content active">
          <center>
            <!-- login starts here-->
             <div class="login-block text-left">
              <div class="col-md-12">
                <h3>   Delivery Address <small>We will deliver your order here</small> </h3>
              </div>
              
               
                
              
                 <form class="row" id="check_out" method="post">
                  
                  <h3 class="confirm" style="display:none;color:green !important;"> Address is Saved Successfully. </h3>
                 
                  
                <div class="logblock col-md-6">
                  <label>Pincode</label>
                  <input type="text" name="confirm_pincode_name" value="<?php echo  $get_addresses->confirm_pincode_name; ?>"  required placeholder="Pincode">
                </div>
                <div class="logblock col-md-6">
                  <label>Name</label>
                  <input type="text" name="confirm_name" value="<?php echo  $get_addresses->confirm_name; ?>"  required placeholder="Name">
                </div>
                <div class="logblock col-md-12">
                  <label>Address</label>
                  <textarea placeholder="Address"    required name="confirm_address" multiple="" class="form-control">  <?php echo $get_addresses->confirm_address; ?>     </textarea>
                </div>
                <div class="logblock col-md-6">
                  <label>Landmark</label>
                  <input type="text" name="confirm_landmark"  value="<?php echo  $get_addresses->confirm_landmark; ?>" required placeholder="Landmark">
                </div>
                <div class="logblock col-md-6">
                  <label>City</label>
                  <input type="text"  required name="confirm_city"   value="<?php echo  $get_addresses->confirm_city; ?>" placeholder="City">
                </div>
               
                <div class="logblock col-md-6">
                  <label>State</label>
                  <input type="text"  required name="confirm_state" value="<?php echo  $get_addresses->confirm_state; ?>" placeholder="State">
                </div>
               
               <input type="hidden" name="check_address" value="<?php echo  $get_addresses->con_no; ?>">
               
                <div class="logblock col-md-6">
                  <label>Mobile Number</label>
                  <input type="text"  required name="confirm_mobile_number" value="<?php echo  $get_addresses->confirm_mobile_number; ?>" placeholder="Mobile Number">
                </div>
                <div class="logblock col-md-6">
                  <label>Address Type</label>
                  <select name="confirm_address_type"  required class="form-control">
                    <option  <?php if($get_addresses->confirm_address_type=='home'){ echo "selected"; } ?> value="home">Home / Personal</option>
                    <option value="office" <?php if($get_addresses->confirm_address_type=='office'){ echo "selected"; } ?>>Office / Commercial</option>
                  </select>
                </div>
                <div class="logblock col-md-12" style="margin-top:10px;">
                  <button type="submit" class="bold check_out">SAVE AND CONTINUE</button>
                </div>
              </form>
            </div>
            <!-- login ends -->
          </center>
        </div>
        <!-- train section -->
         
        
        <!-- hotel search -->
         
                  <div class="tab-pane fade" id="profile">
                    <h3 class="head text-center">Pay using Debit Card</h3>
                    <ul class="card-list">
                      <li>
                        <input type="radio">
                        <img src="<?php echo base_url()?>assets/frontend/images/visa-icon.png"> </li>
                      <li>
                        <input type="radio">
                        <img src="<?php echo base_url()?>assets/frontend/images/master-card-ic.png"> </li>
                      <li>
                        <input type="radio">
                        <img src="<?php echo base_url()?>assets/frontend/images/ae-icon.png"> </li>
                    </ul>
                    <form class="card-form">
                      <div>
                        <label>Card Number</label>
                        <input type="text" placeholder="Card Number">
                      </div>
                      <div>
                        <div  class="ex-head">
                          <label>Expiry Date</label>
                          <div class="expcard">
                            <select class="form-control">
                              <option>MM</option>
                              <option>1</option>
                              <option>2</option>
                              <option>3</option>
                              <option>4</option>
                            </select>
                            <select class="form-control">
                              <option>YY</option>
                              <option>1</option>
                              <option>2</option>
                              <option>3</option>
                              <option>4</option>
                            </select>
                          </div>
                        </div>
                        <div class="cvv">
                          <label>CVV</label>
                          <input type="text" placeholder="CVV">
                        </div>
                      </div>
                      <div>
                        <button class=" btn btn-danger">MAKE PAYMENT</button>
                      </div>
                    </form>
                  </div>
                    <div class="tab-pane fade" id="messages">
                   <h3 class="head text-center">Pay using Net Banking</h3>
                   
                    <form class="card-form">
                     
                      <div>
                        <div style="width:100%;">
                          <label>Select Bank</label>
                          <div>
                            
                          </div>
                        </div>
                      </div>
                      <div>
                        <button class=" btn btn-danger">MAKE PAYMENT</button>
                      </div>
                    </form>
                  </div>
                 
                  </div>
                
                  <div class="clearfix"></div>
                </div>
              </div>
              
              <!-- finalpay ends --> 
              </center> 
            </div>
            <!-- Review Order Ends -->
			 <div class="bhoechie-tab-content">
          <center>
            <!-- Review Order Starts-->
            <div class="login-block text-left ">
              <div class="col-md-12 ">
                <h3> Make Payment </h3>
              </div>
              <div class="text-center upay col-md-12"> <span>You Pay </span>NOK: <?php echo $this->session->userdata("grand_total")?>/- </div>
              <!-- final pay starts-->
              
              <div class="board col-md-12 paycard"> 
                <!-- <h2>Welcome to IGHALO!<sup>™</sup></h2>-->
                <div class="board-inner">
                  <ul class="nav nav-tabs" id="myTab">
                    <li class="active"> <a href="#home" data-toggle="tab" title="Credit Card"> Credit Card</a></li>
                    <li><a href="#settings" data-toggle="tab" title="Cash on Delivery"> Cash on Delivery</a></li>
                  </ul>
                </div>
                <div class="tab-content">
                  <div class="tab-pane fade in active" id="home">
                    <h3 class="head text-center">Pay using Credit Card</h3>
                    <ul class="card-list">
                      <li>
                        <input type="radio">
                        <img src="<?php echo base_url()?>assets/frontend/images/visa-icon.png"> </li>
                      <li>
                        <input type="radio">
                        <img src="<?php echo base_url()?>assets/frontend/images/master-card-ic.png"> </li>
                      <li>
                        <input type="radio">
                        <img src="<?php echo base_url()?>assets/frontend/images/ae-icon.png"> </li>
                    </ul>
                    <form class="card-form">
                      <div>
                        <label>Card Number</label>
                        <input type="text" placeholder="Card Number">
                      </div>
                      <div>
                        <div  class="ex-head">
                          <label>Expiry Date</label>
                          <div class="expcard">
                            <select class="form-control">
                              <option>MM</option>
                              <option>1</option>
                              <option>2</option>
                              <option>3</option>
                              <option>4</option>
                            </select>
                            <select class="form-control">
                              <option>YY</option>
                              <option>1</option>
                              <option>2</option>
                              <option>3</option>
                              <option>4</option>
                            </select>
                          </div>
                        </div>
                        <div class="cvv">
                          <label>CVV</label>
                          <input type="text" placeholder="CVV">
                        </div>
                      </div>
                      <div>
                        <button class=" btn btn-danger">MAKE PAYMENT</button>
                      </div>
                    </form>
                  </div>
                  <div class="tab-pane fade" id="profile">
                    <h3 class="head text-center">Pay using Debit Card</h3>
                    <ul class="card-list">
                      <li>
                        <input type="radio">
                        <img src="<?php echo base_url()?>assets/frontend/images/visa-icon.png"> </li>
                      <li>
                        <input type="radio">
                        <img src="<?php echo base_url()?>assets/frontend/images/master-card-ic.png"> </li>
                      <li>
                        <input type="radio">
                        <img src="<?php echo base_url()?>assets/frontend/images/ae-icon.png"> </li>
                    </ul>
                    <form class="card-form">
                      <div>
                        <label>Card Number</label>
                        <input type="text" placeholder="Card Number">
                      </div>
                      <div>
                        <div  class="ex-head">
                          <label>Expiry Date</label>
                          <div class="expcard">
                            <select class="form-control">
                              <option>MM</option>
                              <option>1</option>
                              <option>2</option>
                              <option>3</option>
                              <option>4</option>
                            </select>
                            <select class="form-control">
                              <option>YY</option>
                              <option>1</option>
                              <option>2</option>
                              <option>3</option>
                              <option>4</option>
                            </select>
                          </div>
                        </div>
                        <div class="cvv">
                          <label>CVV</label>
                          <input type="text" placeholder="CVV">
                        </div>
                      </div>
                      <div>
                        <button class=" btn btn-danger">MAKE PAYMENT</button>
                      </div>
                    </form>
                  </div>
                    <div class="tab-pane fade" id="messages">
                   <h3 class="head text-center">Pay using Net Banking</h3>
                   
                    <form class="card-form">
                     
                      <div>
                        <div style="width:100%;">
                          <label>Select Bank</label>
                          <div>
                            <select class="form-control">
                              
                            </select>
                          </div>
                        </div>
                      </div>
                      <div>
                        <button class=" btn btn-danger">MAKE PAYMENT</button>
                      </div>
                    </form>
                  </div>
                  <div class="tab-pane fade" id="settings">
                    <h3 class="head text-center">Drop comments!</h3>
                    <p class="narrow text-center">Lorem ipsum dolor sit amet, his ea mollis fabellas principes. Quo mazim facilis tincidunt ut, utinam saperet facilisi an vim. </p>
                    <p class="text-center"> <a href="<?php echo base_url()?>/detailed/order_confirm" class="btn btn-success btn-outline-rounded green"> Payment <span style="margin-left:10px;" class="glyphicon glyphicon-send"></span></a> </p>
                  </div>
                  </div>
                
                  <div class="clearfix"></div>
                </div>
              </div>
              
              <!-- finalpay ends --> 
               </center>
            </div>
         
        </div>
      </div>
    </div>
  </div>
  
  <!--payment ends --> 
  
</main>
<!-- main ends -->







<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js">
</script>

<script>

 $(document).ready( function() {
 
 
 
  $("#check_out").submit(function(e){
  
                
                e.preventDefault();
                 
           
           var form =  $("#check_out");
           
        url =  "<?php echo base_url();?>Checkout/add_user_address";   
           
          
          
          
  
    $.ajax({
      type: "POST",
      url: url,
      data: form.serialize(),
      success: function( response ) {
         if(response=="sucess"){
      //   $("input[type=text], textarea").val("");    
         $('.confirm').show(); 
         
           $('.make_payment').show();
         $('.make_payment').click();
         
         }else{
            $('.confirm').hide();    
        }
       }
    });
    
    
    return false;
  
   });  
  
     
 
    

});

 
</script>

 

<?php $this->load->view('includes/footer'); ?>


 
