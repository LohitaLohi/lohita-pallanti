
<?php $this->load->view('includes/header');?>
<!-- main starts-->
<section class="main-home">
	<div class="container">
		<div class="row">
			<div class="home-main">
				
				
				<div class="col-sm-12">
				<?php foreach($get_catgories as $val){ 
				$this->db->select("*");
				$this->db->from("sub_cat");
				$this->db->where("sub_cat_status",'1');
				$this->db->where("cat_id",$val['cat_sno']);
				$query=$this->db->get();
				
				$result = $query->result_array();  ?>
					<div class="home-right">
						<div class="row categories-list">
							<h2><?php echo $val['cat_name'] ?>(<?php  echo count($result); ?>)</h2>
							<?php $i=0;foreach($result as $val23){ $i++;
							
							
							
							?>
							<div class="col-sm-4 pad-right">
								<div class="categories">
									<a href="<?php echo base_url()?><?php echo str_replace(" ","-",strtolower($val['cat_name'])) ?>/<?php echo str_replace(" ","-",strtolower($val23['sub_cat_name'])) ?>/sub-cat">
										<div class="plus_overlay"></div>
										<div class="plus_overlay_icon"></div>
										<img src="<?php echo base_url()?>assets/frontend/images/clothes/img_<?php echo $val23['sub_cat_sno'];?>.png" title="clothes" alt="clothes">
										<span class="title-name"><?php echo $val23['sub_cat_name'] ?></span>
									</a>
								</div>
							</div>
							<?php }?>
							
							<div class="clearfix"></div>
						</div>
						
					</div>
					<?php } ?>
				</div>
				
			</div>
		</div>
	</div>
</section>
<!-- main ends --> 


<?php include 'includes/footer.php'?>