<!DOCTYPE html>
<html>
<body bgcolor='#f7f7f7' style='border:2px solid green !important;font-family:calibri'>
<table align='center' cellpadding='0' cellspacing='0' class='container-for-gmail-android' width='100%' >
  <tr>
    <td align='left' valign='top' width='100%' style='background:#08c65b;'>
      <center>
        <table cellspacing='0' cellpadding='0' width='100%' background='#08c65b'>
          <tr>
            <td width='100%' height='80' valign='top' style='text-align:center;vertical-align:middle;'>
              <center>
                <table cellpadding='0' cellspacing='0' width='600' class='w320'>
                  <tr>
                    <td class='pull-left mobile-header-padding-left' style='vertical-align:middle;'>
                      <a href='<?php echo base_url();?>'><img height="50" width="50" src='<?php echo base_url()?>assets/frontend/images/clothes/logo.jpg' alt='clothes'></a>
                    </td>
                    
                    
                     <td class='pull-left mobile-header-padding-left' style='vertical-align:middle;'>
                      <a href='<?php echo base_url();?>'> Back To Home </a>
                    </td>
                    
                  </tr>  
                </table>
              </center>
            </td>
          </tr>
        </table>
      </center>
    </td>
  </tr>

  <tr>
    <td align='center' valign='top' width='100%' style='background-color:#ffffff;border-top:1px solid #e5e5e5;border-bottom:1px solid #e5e5e5;'>
      <center>
        <table cellpadding='0' cellspacing='0' width='600' class='w320' style='margin:10px 0';>
		 <tr>
            <td class='free-text' style='padding:20px 0 10px 0;'>
            Hi <?php echo $this->session->userdata("username")?>,<br>
             
            </td>
          </tr>
            <tr>
              <td class='item-table'>
                <table cellspacing='0' cellpadding='0' width='100%' class='ss'>
					<tr style='background:#08c65b;'>
						<td class='title-dark' width='50%' style=' padding:10px 10px;color:#fff;'>
						   Item
						</td>
						<td class='title-dark' width='25%' style=' padding:10px 10px;color:#fff;border:1px solid #fff;border-bottom:0;border-top:0;'>
						  Qty
						</td>
						<td class='title-dark' width='25%' style='padding:10px 10px;color:#fff;border-right:0px solid #FFF;'>
						  Total
						</td>
					  </tr>
					  <?php $i=""; $finalPrice=""; $mainPrice="";foreach($get_product_details as $val){
						  
						  $i++;
						  $finalPrice += $val["product_cost"]*$val['product_quanity'];
						  $mainPrice += $val["product_cost"];
						
						  
						  ?>
					<tr>
						<td class='item-col item' style='border:1px solid green;padding:10px;border-right:0;<?php if($i>1){echo 'border-top:0';}?>'>
						  <table cellspacing='0' cellpadding='0' width='100%'>
							<tr>
							  <td class='product'>
								<span style='color:#4d4d4d;font-weight:bold;'><?php echo $val['prod_name'] ?></span>
							  </td>
							</tr>
						  </table>
						</td>
						<td class='item-col quantity' style='border:1px solid green;padding:10px;text-align:center;<?php if($i>1){echo 'border-top:0';}?>  '>
						  <?php echo $val['product_quanity']?>
						</td>
						<td class='item-col' style='border:1px solid green;padding:10px;border-left:0;<?php if($i>1){echo 'border-top:0';}?>'>
						 NOK <?php echo $val['product_quanity']*$val['product_cost']?>
						</td>
					 </tr>
					  <?php }?>
					<tr>
						<td class='item-col item'>
						</td>
						<td class='item-col quantity' style='text-align:right;padding:10px;border:1px solid green;line-height:25px;border-top:0;'>
							<span class='total-space'>Subtotal</span> <br />
							<span class='total-space'>Shipping Charges</span><br />
							<span class='total-space' style='font-weight:bold;color:#4d4d4d'>Total</span>
						</td>
						<td class='item-col price' style='text-align:left;padding:10px;border:1px solid green;line-height:25px;border-top:0;border-left:0;'>
							<span class='total-space'>NOK <?php echo $finalPrice; ?></span> <br />
							<span class='total-space'>NOK <?php echo $data->service_cost;?></span>  <br />
							<span class='total-space' style='font-weight:bold;color:#4d4d4d'> NOK <?php echo $finalPrice+$data->service_cost;?></span>
						</td>
					</tr>
                </table>
              </td>
            </tr>

        </table>
      </center>
    </td>
  </tr>
    <tr>
    <td align='center' valign='top' width='100%' style='background-color:#fff;' class='content-padding'>
      <center>
        <table cellspacing='0' cellpadding='0' width='600' class='w320'>
         
          <tr>
            <td class='w320'>
              <table cellpadding='0' cellspacing='0' width='100%'>
                <tr>
                  <td class='mini-container-left' style='padding:0 10px 10px 0;width:48%;'>
                    <table cellpadding='0' cellspacing='0' width='100%'>
                      <tr>
                        <td class='mini-block-padding'>
                          <table cellspacing='0' cellpadding='0' width='100%' style='border-collapse:separate !important;'>
                            <tr>
                              <td class='mini-block' style='padding:0px'>
                                <span class='header-sm' style='background:#08c65b;display:block;color:#fff;text-align:center;padding:10px 0;'>Delivery Address </span>
                                <p style='border:1px solid green;margin:0;padding:15px;text-align:left;'> 
                               <!-- <?php echo $data->address_line1 ?> <br/><?php echo $data->address_line2 ?><br/><?php echo $data->city ?> <br/><?php echo $data->state ?><br/><?php echo $data->pincode ?><br/> !-->
                               
                               <?php 
                               
                                $user_id = $this->session->userdata('userId');	      
        	  $vl = $this->db->query("select * from checkout_address where user_id = '".$user_id."'")->row();
        	   
                              ?>  <?php echo $vl->confirm_address;?>,<?php echo $vl->confirm_city;?>, <?php echo $vl->confirm_state;?>, <?php echo $vl->confirm_pincode_name;?> <?php echo $vl->confirm_landmark;?>, Phone No: <?php echo $vl->confirm_mobile_number;?>
                               
                                </p>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td class='mini-container-right' style='padding:0px 0px 10px 15px;'>
                    <table cellpadding='0' cellspacing='0' width='100%'>
                      <tr>
                        <td class='mini-block-padding'>
                          <table cellspacing='0' cellpadding='0' width='100%' style='border-collapse:separate !important;'>
                            <tr>
                              <td class='mini-block' style='padding:0px'>
                                <span class='header-sm' style='background:#08c65b;display:block;color:#fff;text-align:center;padding:10px 0;'>Delivery Time</span>
                                <p style='border:1px solid green;margin:0;padding:15px;'>
                                 <span class='header-sm'><b>Order ID</b></span> <br />
                               <?php echo $this->session->userdata("orderNo")?> <br /><br />
                                
                                </p>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </center>
    </td>
  </tr>
  <tr>
    <td align='center' valign='top' width='100%' style='background-color:#08c65b;'>
      <center>
        <table cellspacing='0' cellpadding='0' width='600' class='w320'>
          <tr>
            <td style='padding:25px 0 25px;color:#fff;text-align: center;'>
              <span>Thank You!</span>              
            </td>
          </tr>
        </table>
      </center>
    </td>
  </tr>
</table>
</div>
</body>
</html>