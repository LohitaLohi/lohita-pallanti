<?php include 'includes/header.php'?>
<!-- main starts-->
<main> 
  <!-- cart page starts-->
  
  

  
 
  	<div class="container cart">
    	<h2 class="carttitle bold">Shopping Cart <span>(<?php echo count($products)?>)</span></h2>
        <!-- table starts -->
        <table width="100%" class="cart-table">
        	<thead>
            	<tr>
                	<th width="30%">Item Details</th>
					<th>Type</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    
                    <th>Sub Total</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
			<?php foreach($products as $val){  $query = $this->db->query("SELECT * FROM `product_images` 
			  where prod_sno='".$val['prod_sno']."'"); $resultImg = $query->row();
			  
			 $grandTotal +=  $val['prod_dis_price']* $val['qun_cart'] ;
			 
			 $this->session->set_userdata("grand_total",$grandTotal);
			 
			 ?>
		     
			  
			 
        	<tr>
            	<td>
                	<figure class="cart-pic">
					<img src="<?php echo base_url()?>assets/images/gallery/<?php echo str_replace(" ","_",strtolower($val['sub_cat_name'])) ?>/<?php echo str_replace(" ","_",strtolower($val['prod_name'])) ?>/<?php echo $resultImg->product_img_name ?>"></figure>
                    <div class="pro-cart">
                    	<span><?php echo $val['prod_name']?></span>
                        <small>  <?php echo $val['quanity'].' '.$val['quanity_type']?></small>
                    </div>
                </td>
                <td><?php echo $val['pck_typ']?></td>
                <td><strong>NOK:</strong> <?php echo $val['prod_dis_price']?></td>
				<td>
                <div class="qty-holder">
					<a class="qty-dec-btn" id="updateadd_dec_<?php echo $val['sno']?>" title="Dec">-</a>
					<input type="text" style="width:20%" id="quantity_<?php echo $val['sno']?>" class="qty-input" value="<?php echo $val['qun_cart']?>">
					<a class="qty-inc-btn" id="updateadd_<?php echo $val['sno']?>" title="Inc">+</a>
				</div>
				</td>
               <td>NOK:<?php echo $val['prod_dis_price']* $val['qun_cart'] ?></td>
                <td><a id="remove_product_<?php echo $val['sno']?>" href="javascript:void(0)">
				<i class="fa fa-times" id="" ></i></a></td>
            </tr>
            <?php }?>
           
        </table>
         <div class="final-pay">
        	<div class="col-md-9">Delivery and payment options can be selected later </div>
            <div class="col-md-3">
            	<div class="total-table">
                	<table width="100%">
                    	
                       
                         <tr>
                        	<td class="grtotal">Grand Total</td>
                            <td class="text-right bold grtotal">NOK: <?php echo  $grandTotal?></td>
                        </tr>
                        <tr>
                            <td></td>
                            </tr>
                         <tr>
                             <tr>
                            <td></td>
                            </tr>
                         <tr>
                        	<td><a href="<?php echo base_url()?>Checkout" class="btncart">Checkout</a></td>
                            <td><a href="<?php echo base_url()?>" class="btncart">Cancel</a></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

  
  <!-- cart page ends -->
  
</main>
<!-- main ends -->
<style>
.qty-holder {
    display: inline-block;
    width: 125px;
    white-space: nowrap;
    vertical-align: middle;
    font-size: 0;
}
.qty-dec-btn, .qty-inc-btn {
    display: inline-block;
    width: 30px;
    height: 30px;
    background: #f4f4f4;
    border: 1px solid #ccc;
    color: #777;
    line-height: 30px;
    border-radius: 0;
    margin: 0;
    font-size: 14px;
    font-weight: 500;
    text-decoration: none;
    text-align: center;
    vertical-align: middle;
}
body a {
    outline: none !important;
}
.qty-input {
    display: inline-block;
    vertical-align: middle;
    width: 35px !important;
    font-size: 14px;
    text-align: center;
    color: #777;
    height: 30px;
    border-radius: 0;
    border: 1px solid #ccc;
    margin: 0 -1px;
    outline: none;
}
</style>
<script src="http://mahithawebsol.com/water_online/assets/detailed/jquery-1.9.1.min.js"></script>
<script>
$('a[id^="updateadd_"]').on('click', function(){
	
	
	var quan = $(this).attr('id');;
	if (quan.indexOf('dec') > -1)
      {
        quanityId =quan.replace("updateadd_dec_","");
	    quanityId2 ="#quantity_"+quanityId;
	    var quan2 = $(quanityId2).val();
		if(quan2==1 || quan2==0){
			quantity = quan2;
			Ajaxquantity = quan2;
		}
		else{
			quantity = parseInt(quan2)-1;
			Ajaxquantity = parseInt(quan2)-1;
		}
      }
	  else{
			quanityId =quan.replace("updateadd_","");
			quanityId2 ="#quantity_"+quanityId;
			var quan2 = $(quanityId2).val();
			quantity = parseInt(quan2)+1;
			Ajaxquantity = parseInt(quan2)+1;
	  }
	$(quanityId2).val(quantity);
	var gg = $("input[name=csrf_test_name]").val();
	var form_data = {
	        
			Ajaxquantity:Ajaxquantity,
			quanityId:quanityId,
			
	    };
		
	$.ajax({
			type:"post",
			url:"<?php echo base_url()?>cart",
			data:form_data,
	        success: function (msg) {                    
          window.location.reload(true);
        }
    });
});
$('a[id^="remove_product_"]').on('click', function(){
	
	var cartSno = $(this).attr('id');;
	cartSno1 =cartSno.replace("remove_product_"," ");
	
	var form_data = {
	       
			cartSno1:cartSno1
	    };
		
	$.ajax({
			type:"post",
			url:"<?php echo base_url()?>cart",
			data:form_data
	       }).done(function(msg)
			{
				
				window.location.reload(true);
		});
});

</script>
<?php include 'includes/footer.php'?>
