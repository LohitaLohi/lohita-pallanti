<?php $this->load->view('includes/header');?>
<!-- main starts-->
<main>
  <div class="container"> 
    <!-- My account myorders starts here-->
    <div class="row">
      <div class="col-md-3 myorders-nav">
        <h3 class="bold">MY ACCOUNT</h3>
         <?php // include 'myaccount-leftnav.php'?>
         
         <?php $this->load->view('myaccount-leftnav');?>
         
         
      </div>
	    
       
      <div class="col-md-9">
       
      <div class="saveaddress" id="sav-address">
       
       
       	<h4>Your Addresses</h4>
       	
       	
      		<p style="color:green !important;">	<?php echo $this->session->flashdata('msg'); ?> </p>
      
      <?php 
        	 $user_id = $this->session->userdata('userId');	      
        	  $user_address = $this->db->query("select * from checkout_address where user_id = '".$user_id."'")->result();
        	 
        	 foreach($user_address as $ke=>$vl){ ?>
      
      	<address class="col-md-4">
        
         	<p><span class="bold "><?php echo $vl->confirm_name;?>,</span>  <?php echo $vl->confirm_address;?>,<?php echo $vl->confirm_city;?>, <?php echo $vl->confirm_state;?>, <?php echo $vl->confirm_pincode_name;?> <?php echo $vl->confirm_landmark;?>, Phone No: <?php echo $vl->confirm_mobile_number;?></p>
            
             <div class="row btngr">
            	<input type="button" value="Change" class="btn btn-default">
                <input type="button" value="Delivery Here" class="btn btn-default">
            </div>
        </address>
        
        <?php } ?>  
        
        
      <!--   <address class="col-md-4">
        	<h4>Secondary Address</h4>
        	<p><span class="bold ">Praveen Guptha,</span> 10-2/A, Pragathi Nagar, Nizampet, Hyderabad, Telangana, 500090 Near Rajiv Gruha Kalpa, Phone No: 9642123254</p>
            <div class="row btngr">
            	<input type="button" value="Edit" class="btn btn-default">
                <input type="button" value="Delete" class="btn btn-default">
            </div>
        </address>  !-->
		
        
        <input type="submit" value="CREATE NEW ADDRESS" class="btn btn-default" id="btn-newaddress">
        </div>
		

		
	    
        <div class="row" id="add-new">
        	<div class="col-md-6 login-block text-left">
                    <h3>Register New Address</h3>
                    <form action="<?php echo base_url()?>Myaccount_address/myaccount_add" method="post">
                      <div class="logblock">
                        <input type="text" required name="confirm_pincode_name" placeholder="Pincode">
                      </div>
                      <div class="logblock">
                        <input type="text" required name="confirm_name" placeholder="Name">
                      </div>
                      <div class="logblock">
                       <textarea class="form-control" required = "required" name="confirm_address" placeholder="Enter Address"></textarea>
                      </div>
                      <div class="logblock">
                        <input type="text" required name="confirm_landmark" placeholder="Landmark">
                      </div>
                      <div class="logblock">
                        <input type="text" required name="confirm_city" placeholder="City">
                      </div>
                      <div class="logblock">
                        <select name="confirm_state" required class="form-control">
                        	<option>Select State</option>
                        	<option>Andhra pradesh</option>
                            <option>Telangana</option>
                        </select>
                      </div>
                      <div class="logblock">
                        <input type="text" required name="confirm_mobile_number" placeholder="Mobile Number">
                      </div>
                       <div class="logblock">
                        <select name="confirm_address_type" required class="form-control">
                        	<option>Address Type</option>
                        	<option>Home</option>
                            <option>Office</option>
                             <option>Other</option>
                        </select>
                      </div>
                      
                    <!--   <div class="logblock">
                        <input type="checkbox" name="address" value="Make this my default address" style="float:left; margin-right:5px"><span>Make this my default address</span>
                      </div> !-->
                      
                     
                      <div class="logblock">
                        <button>REGISTER NEW ADDRESS</button>
                      </div>
                      
                      <!-- recover your social account-->
                      
                    </form>
                  </div>
				   
        </div>
        
       
      <!-- default address ends -->
        
        
      </div>
	     </div>
  </div>
  <!-- my account myorders ends here-->
  </div>
  <!-- <form  action="<?php echo base_url()?>Myaccount_address/myaccount_view" method="post">
	
		<?php foreach($myaccount_data as $val) {?>
		<address class="col-md-4">
        	<h4>Secondary Address</h4>
			<p><?php echo $val['confirm_pincode_name']?>
			<?php echo $val['confirm_name']?>
			<?php echo $val['confirm_address']?>
			<?php echo $val['confirm_landmark']?>
			<?php echo $val['confirm_city']?>
			<?php echo $val['confirm_state']?>
			<?php echo $val['confirm_mobile_number']?>
			<?php echo $val['confirm_address_type']?></p>
        			
            <div class="row btngr">
            	<a href="<?php echo base_url() ?>Myaccount_address/myaccount_edit/<?php echo $val['con_no'];?>">edit</a>
                <a href="<?php echo base_url() ?>Myaccount_address/myaccountaddress_delete/<?php echo $val['con_no'];?>">delete </a>       
				</div><?php }?>
        </address>
     </form> !-->
    
  
  <!-- cart page ends --> 
  
</main>
<!-- main ends -->
<?php include 'includes/footer.php'?>
