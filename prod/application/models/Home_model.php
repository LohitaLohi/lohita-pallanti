<?php 
class Home_model extends CI_Model {

function add_user($data){
	
    $this->db->insert('users', $data);
     return $this->db->insert_id();
	
}
function login_submit($username,$pwd,$type){
	
 
	
	$this->db->select('*');
	$this->db->from('users');
	$this->db->where('email', $username);
	$this->db->where('password', $pwd);
 	$this->db->where('user_type', $type);
	
	$query = $this->db->get()->row();
	
	
 
	
	
	return $query; 
	
  
	
 
} 
public function get_currentpwd($userid){
	 
	 $this->db->select("*");
	 $this->db->from("users");
	 $this->db->where("id",$userid);
	 
	  $query=$this->db->get();
	  //echo $this->db->last_query();
	 //exit();
	 if($query->num_rows()>0){
	  return $query->row();
	 }
  }
  public function update_pwd($new_password,$userid){
      $data=array('password'=>$new_password);
	  $this->db->where('id',$userid);
	 return  $this->db->update('users',$data);
	 // echo $this->db->last_query();
	 //exit();
	  }



 public function myaccount_cpw_edit($edit,$pwd){
	 
	 $this->db->select("*");
	 $this->db->from("users");
	 $this->db->where("id",$edit);
	 $this->db->where('password', $pwd);
	  $quer=$this->db->get();
	  //echo $this->db->last_query();
	 //exit();
	  $result=$quer->row();
	  return $result;
  }
  public function myaccount_cpw_editid($data,$editid){
	  $this->db->where('id',$editid);
	 return  $this->db->update('users',$data);
	 // echo $this->db->last_query();
	 //exit();
	  }
	  public function myaccount_add($data){
	   $this->db->insert("checkout_address",$data);
	    return $this->db->insert_id();
     }
     
      public function myaccount_update($data,$check_id){
          
     $this->db->where('con_no',$check_id);  
	 $this->db->update("checkout_address",$data);
	  return  $check_id;
     }
     
     
     
public function myaccount_view(){
	$this->db->select("*");
	$this->db->from("checkout_address");
	$query=$this->db->get();
	return $query->result_array();
	
  }


public function checkout_add($data){
	
	 
	
	return $this->db->insert("checkout_address",$data);
	
	
}

public function myaccount_edit($edit){
	$this->db->select("*");
	$this->db->from("checkout_address");
	$this->db->where("con_no",$edit);
	$quer=$this->db->get();
	$result=$quer->row();
	return $result;
}
public function myaccount_editid($data,$editid){
	$this->db->where("con_no",$editid);
	return $this->db->update("checkout_address",$data);

}
public function myaccountaddress_delete($editid){
	
	$this->db->where("con_no",$editid);
	return $this->db->delete("checkout_address");
}


public function  user_data_update($userData){
	
	$email=$this->session->userdata('user_logged_in');
	$this->db->where('email', $email);
    $this->db->update('users', $userData);
	$this->db->select("id,phone_number");
	$this->db->from("users");
	$this->db->where("email",$email);
	$query=$this->db->get();
	return  $query->row();
	
	
}
public function get_catgories(){
	
   
	$this->db->select("*");
	$this->db->from("category");
	$this->db->where("cat_status",'1');
	$query=$this->db->get();
	
 
	
	return  $query->result_array();
	
}
public function get_sub_cat(){

		$this->db->select("*");
		$this->db->from("sub_cat S");
		$this->db->join("category C","S.cat_id=C.cat_sno");
		$this->db->where("S.sub_cat_status",'1');
		$this->db->where("C.cat_name",$this->uri->segment(1));
		$query=$this->db->get();
		//echo $this->db->last_query();
		return  $query->result_array();	
 	
}
public function userOtpConfirm($emailTo,$userPhoneNo,$otpUpdateSignUp){
	 $this->db->where('email',$emailTo);	 
     $this->db->update('users',$otpUpdateSignUp);
	 $this->db->select("id,phone_number");
	 $this->db->from("users");
	 $this->db->where("email",$emailTo);
	 $query=$this->db->get();
	
	 return  $query->row();
}
public function  fb_checking(){
	$email=$this->session->userdata('user_logged_in');
	$sql="select email from `users` where email='$email'";
	$query = $this->db->query($sql);
	
	
	if ($query->num_rows() > 0){
    return true;
	}
}

public function get_user_details(){
	
	$this->db->select("*");
	$this->db->from("users");
	$this->db->where("email",$this->session->userdata('userId'));
	$query=$this->db->get();
	return $query->row();
}
public function get_check_user_details(){
	
	$this->db->select("*");
	$this->db->from("users");
	$this->db->where("email",$this->input->post('user_email'));
	$this->db->or_where("phone_number",$this->input->post('user_number'));
	$query=$this->db->get();
	return $query->result_array();
}

public function login($userId,$userPassword){
	
	$this->db->select('first_name,otp_status,email,count(email) as count_email,phone_number,id');
	$this->db->from('users');
	$this->db->where('password',$userPassword);
	$this->db->where('otp_status','1');
	$this->db->where('email',$userId);
	$this->db->or_where('phone_number',$userId);
	$query = $this->db->get();
    return $query->row();
}

public function products($get_cat_name,$get_sub_cat,$order){
 	$this->db->select('*'); 
	$this->db->from('products P'); 
	$this->db->join("sub_cat S","P.prod_sub_cat_sno=S.sub_cat_sno","left");
	$this->db->join("category C","S.cat_id=C.cat_sno","left");
	$this->db->where('P.prod_status',1);
	$this->db->where('C.cat_name',$get_cat_name);
	$this->db->where('S.sub_cat_name',$get_sub_cat);
	
	if($order !=''){
	    
	    if($order=='low'){
	           
	           $this->db->order_by('P.prod_dis_price',"asc");        
	        
	    }else{
	        
	       $this->db->order_by('P.prod_dis_price',"desc");
	    }
	}
	 
	
	
 	$query = $this->db->get();
	
	
	
	
	
 	return $query->result_array();
	
	
	
	
	
}







public function fav_products($products){
 	$this->db->select('*'); 
	$this->db->from('products P'); 
	$this->db->join("sub_cat S","P.prod_sub_cat_sno=S.sub_cat_sno","left");
	$this->db->join("category C","S.cat_id=C.cat_sno","left");
	$this->db->where('P.prod_status',1);
 	$this->db->where_in('P.prod_sno',$products);
	 
     	$query = $this->db->get();
 	    return $query->result_array();
	
    }








public function get_single_product(){
	
	$this->db->select('*'); 
	$this->db->from('products P'); 
	$this->db->join("sub_cat S","P.prod_sub_cat_sno=S.sub_cat_sno","left");
	$this->db->join("category C","S.cat_id=C.cat_sno","left");
	$this->db->where('P.prod_sno',$this->uri->segment(4)); 
	$this->db->where('P.prod_status',1); 
	$query = $this->db->get();
	return $query->row();
	
}
public function get_brands(){
	
	$this->db->select('*');
	$this->db->from('brand');
	$this->db->where('brand_status',1);
	$this->db->order_by('brand_sno', 'asc');
	$query = $this->db->get();
	return $query->result_array();
	
}
public function get_quan_type(){
	
	$this->db->select('*');
	$this->db->from('colors');
	$this->db->where('color_status',1);
	$this->db->order_by('color_status', 'DESC');
	$this->db->order_by('color_sno', 'ASC');
	$query = $this->db->get();
	return $query->result_array();
	
	
	
}



function update_user_data($data){

 $this->db->where('id',$this->session->userdata('userSno'));	
 $this->db->update('users',$data);	

 return  $this->db->affected_rows();	
 

}

function orders(){
	$userid = $this->session->userdata('userSno');
	$this->db->select('*');
	$this->db->from('orders O');
	//$this->db->join('order_products OP', 'OP.order_no = O.order_no', 'left');
	$this->db->where('O.user_id',$userid);
	$query = $this->db->get();
	return $query->result_array();
}
function get_order_info(){
	$orderID = $this->uri->segment('2');
	$userID = $this->session->userdata('userSno');
	$this->db->select("*");
	$this->db->from('orders O'); 
	$this->db->join('order_products OP', 'OP.order_no = O.order_no', 'left');
	$this->db->join("prod_sizes S","OP.prod_size=S.siz_sno",'left');
	$this->db->where('O.order_no', $orderID);
	$this->db->where('O.user_id', $userID);
	$query = $this->db->get();
	//echo $this->db->last_query();
	//exit();
	return $query->result_array();

}

}
?>