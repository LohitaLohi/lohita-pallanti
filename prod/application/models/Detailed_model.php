<?php 
class Detailed_model extends CI_Model {
 
function product_view($product_id){
	$this->db->select('*');
	$this->db->from('products P');
	$this->db->where('P.prod_sno', $product_id);
	$query = $this->db->get();
	return $query->result_array();
}

function product_features($product_id){
	
	$this->db->select('F.feature_field_name, F.feature_field_des');
	$this->db->from('features F'); 
	$this->db->where('prod_sno', $product_id);
	$this->db->where('feature_status', '1');
	$query = $this->db->get();
	return $query->result_array();
	
}
function product_services($product_id){
	$this->db->select('PS.service_sno, PS.service');
	$this->db->from('product_services PS'); 
	$this->db->where('prod_sno', $product_id); 
	$query = $this->db->get();
	return $query->result_array();
}
function product_sizes($product_id){
	
	$this->db->select('PS.sizes, PS.prod_cost,PS.siz_sno,PS.prod_cost');
	$this->db->from('prod_sizes PS'); 
	$this->db->where('prod_id', $product_id); 
	$query = $this->db->get();
	return $query->result_array();
	
}
function product_images($product_id){
	
	$this->db->select('PI.product_img_sno, PI.product_img_name, PI.product_img_alt_tag');
	$this->db->from('product_images PI'); 
	$this->db->where('prod_sno', $product_id); 
	$query = $this->db->get();
	return $query->result_array();
	
}
function related_products($sub_cat_id){
	
	$this->db->select('P.prod_sno, P.prod_name, P.prod_sub_cat_sno, SC.sub_cat_name, P.prod_price, P.prod_discount, P.prod_dis_price, P.prod_sno, AVG(R.rating) as avg_r, PI.product_img_name, PI.product_img_alt_tag ');
	$this->db->from('products P'); 
	$this->db->join('product_images PI', 'P.prod_sno = PI.prod_sno', 'left'); 
	$this->db->join('sub_cat SC', 'SC.sub_cat_sno = P.prod_sub_cat_sno', 'left'); 
	$this->db->join('reviews R', 'R.product_id = P.prod_sno', 'left'); 
	$this->db->where('prod_sub_cat_sno', $sub_cat_id);
	$this->db->where('prod_status', '1');
	$this->db->group_by('P.prod_sno');
	$this->db->limit('10');
	$query = $this->db->get();
	return $query->result_array();
	
}
function check_pincode($pincode){
	$this->db->select('*');
	$this->db->from('pincodes');
	$this->db->where('pincode', $pincode);
	$query = $this->db->get();
	return $query->row();
}
function check_cart($productSno){
	
	$this->db->select('quanity');
	$this->db->from('cart');
	$this->db->where('product_id', $productSno);
	if($this->session->userdata('userSno')){
		$this->db->where('order_id', $this->session->userdata('userSno'));
	}
	else{
		$this->db->where('order_id', $this->session->userdata('cart_order_no'));
	}
	$query = $this->db->get();
	return $query->row();
	
}
function update_to_cart($cartdataupdate,$cartSno,$sizesId){
	
	if($this->session->userdata('userSno')){
		$this->db->where('order_id', $this->session->userdata('userSno'));
	}
	else{
		$this->db->where('order_id', $this->session->userdata('cart_order_no'));
	}
	$this->db->where('product_id', $cartSno);
	if($sizesId!=""){
	$this->db->where('size_id', $sizesId);
	}
	$this->db->update("cart",$cartdataupdate);
}
function update_to_cart_ajax($cartdataupdate,$cartSno){
	
	if($this->session->userdata('userSno')){
		$this->db->where('order_id', $this->session->userdata('userSno'));
	}
	else{
		$this->db->where('order_id', $this->session->userdata('cart_order_no'));
	}
	$this->db->where('sno', $cartSno);
	$this->db->update("cart",$cartdataupdate);
}

function update_to_cart_session($orderId){
	
	$this->db->where('order_id', $this->session->userdata('cart_order_no'));
	$this->db->update("cart",$orderId);
}
function add_to_cart($cartdata){
	
	$this->db->insert('cart',$cartdata);
	//echo $this->db->last_query();
	
	
	
}

public function get_ord_details(){
    
    	$this->db->select('O.*,P.*,OP.*,S.*,C.*');
	$this->db->from('orders O'); 

	
	$this->db->join('order_products OP', 'OP.order_no = O.order_no', 'left');
	$this->db->join('products P', 'P.prod_sno = OP.product_id', 'left');
	
		$this->db->join("sub_cat S","P.prod_sub_cat_sno=S.sub_cat_sno","left");
      	$this->db->join("category C","S.cat_id=C.cat_sno","left");
	
	

	$this->db->where('O.user_id', $this->session->userdata('userId')); 
 	$query=$this->db->get();
		return $query->result_array(); 
    
}


function get_cart_details(){
	
  /*	$this->db->select('O.*,P.*,OP.*,S.*,C.*');
	$this->db->from('orders O'); 

	
	$this->db->join('order_products OP', 'OP.order_no = O.order_no', 'left');
	$this->db->join('products P', 'P.prod_sno = OP.product_id', 'left');
	
		$this->db->join("sub_cat S","P.prod_sub_cat_sno=S.sub_cat_sno","left");
      	$this->db->join("category C","S.cat_id=C.cat_sno","left");
	
	

	$this->db->where('O.user_id', $this->session->userdata('userId')); 
 	$query=$this->db->get();
		return $query->result_array(); */
 	
 	$this->db->select("*,C.quanity as qun_cart");
 	$this->db->from("cart C");
 	$this->db->join("products P","C.product_id=P.prod_sno");
 	$this->db->join("sub_cat S","P.prod_sub_cat_sno=S.sub_cat_sno","left");
     $this->db->join("category Ca","S.cat_id=Ca.cat_sno","left");
	if($this->session->userdata('userId')){
		
		$this->db->where('C.order_id', $this->session->userdata('userId'));
	}
	else{
		
		$this->db->where('C.order_id', $this->session->userdata('cart_order_no'));
	} 
		$query=$this->db->get();
		return $query->result_array();

}

function cart_count(){
	
    $this->db->select("SUM(quanity)  total_quanity");
	$this->db->from("cart");

	if($this->session->userdata('userSno')){
		
		$this->db->where('order_id', $this->session->userdata('userSno'));
	}
	else{
		$this->db->where('order_id', $this->session->userdata('cart_order_no'));
	}
	
	$query=$this->db->get();
	
	return $query->row();

}
function delete_cart_item($cartSnol){
	
	$this->db->where('sno',$cartSnol);
	 $this->db->delete("cart");
	 return $this->db->affected_rows;
}

function order_inert_product($orderProduct){
	
	$this->db->insert("order_products",$orderProduct);
	
}

function order_inert($order){
	
	return $this->db->insert("orders",$order);
	
}
function delete_cart(){
	
	 $this->db->where('order_id',$this->session->userdata("userSno"));
	return  $this->db->delete("cart");
	 
}

function get_order_details(){
	
	if(!empty($this->session->userdata("orderNo"))){
	    
	    $order_num = $this->session->userdata("orderNo");
	}else{
	    
	     $order_num = $order_no;
	}
	
	
	$this->db->select("*");
	$this->db->from("orders O");
	$this->db->join('users U', 'O.user_id = U.id'); 
	$this->db->where('O.order_no', $order_num);
	$query=$this->db->get();
	return $query->row();

}

function get_order_product_details(){
	
	$this->db->select("*");
	$this->db->from('orders O'); 
	$this->db->join('order_products OP', 'OP.order_no = O.order_no', 'left');
	$this->db->join("products P","OP.product_id=P.prod_sno",'left'); 
	$this->db->where('O.order_no', $this->session->userdata("orderNo"));
	$query=$this->db->get();
	
	return $query->result_array();
	
}



}


?>