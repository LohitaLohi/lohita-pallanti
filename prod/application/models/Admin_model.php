<?php 
class Admin_model extends CI_Model {
/*==========  MENU SECTION START  ========*/
function login($username,$pwd){
	$this->db->select('*');
	$this->db->from('admin');
	$this->db->where('admin_id', $username);
	$this->db->where('admin_password', $pwd);
	$query = $this->db->get();
	return $query->row();
}
function menu(){
	$this->db->select('*');
	$this->db->from('menu');
	$this->db->order_by('menu_status', 'DESC');
	$this->db->order_by('menu_position_no', 'ASC');
	$query = $this->db->get();
	return $query->result_array();
}
function menu_active_items(){
	$this->db->select('*');
	$this->db->from('menu');
	$this->db->order_by('menu_status', 'DESC');
	$this->db->order_by('menu_position_no', 'ASC');
	$this->db->where('menu_status', '1');
	$query = $this->db->get();
	return $query->result_array();
}
function menu_insert($data){
	return $this->db->insert('menu', $data);
}
function menu_data($menu_id){
	$this->db->select('*');
	$this->db->from('menu');
	$this->db->where('menu_sno', $menu_id);
	$query = $this->db->get();
	return $query->result_array();
}
function menu_update($data,$menu_id){
	$this->db->where('menu_sno',$menu_id);
	$this->db->update('menu',$data);
	return $this->db->affected_rows();
}
function menu_delete($menu_id){
	$this->db->where('menu_sno',$menu_id);
	$this->db->update('menu',$data);
	return $this->db->affected_rows();
}
/*============    MENU SECTION END    ========*/
/*===========  CATEGORY SECTION START ========*/
function category(){
	
	$this->db->select('C.*,M.menu_name');
	$this->db->from('category C');
	$this->db->join('menu M', 'C.menu_id = M.menu_sno');
	$this->db->order_by('cat_status', 'DESC');
	$this->db->order_by('cat_position_no', 'ASC');
	$query = $this->db->get();
	return $query->result_array();
}
function cat_insert($data){
	return $this->db->insert('category', $data);
}
function cat_data($cat_id){
	$this->db->select('*');
	$this->db->from('category');
	$this->db->where('cat_sno', $cat_id);
	$query = $this->db->get();
	return $query->result_array();
}
function category_update($data,$cat_id){
	$this->db->where('cat_sno',$cat_id);
	$this->db->update('category',$data);
	return $this->db->affected_rows();
}
/*============  CATEGORY SECTION END  ========*/
/*============  BRANDS SECTION START  ========*/
function brands(){
	$this->db->select('*');
	$this->db->from('brand');
	$this->db->order_by('brand_status', 'DESC');
	$this->db->order_by('brand_sno', 'ASC');
	$query = $this->db->get();
	return $query->result_array();
}
function brands_insert($data){
	return $this->db->insert('brand', $data);
}
function brand_info($brand_id){
	$this->db->select('*');
	$this->db->from('brand');
	$this->db->where('brand_sno', $brand_id);
	$query = $this->db->get();
	return $query->result_array();
}
function brands_update($data, $brand_id){
	$this->db->where('brand_sno',$brand_id);
	$this->db->update('brand',$data);
	return $this->db->affected_rows();
}
function brand_update($data,$brand_id){
	$this->db->where('brand_sno',$brand_id);
	$this->db->update('brand',$data);
	return $this->db->affected_rows();
}
/*============  BRANDS SECTION END  ========*/
/*============  SUBCATEGORIES SECTION START  ========*/
function subcategories(){
	
	$this->db->select('SC.sub_cat_sno, SC.sub_cat_name, SC.cat_id, SC.sub_cat_position_no, SC.sub_cat_status, C.cat_name');
	$this->db->from('sub_cat SC');
	$this->db->join('category C', 'C.cat_sno = SC.cat_id');
	$this->db->order_by('sub_cat_status', 'DESC');
	$this->db->order_by('sub_cat_sno', 'ASC');
	$query = $this->db->get();
	$final_array = $query->result_array();
	$xxx = -1;
	foreach ($final_array as $sub_Array) {  $xxx++;
		$subCAT = $sub_Array['sub_cat_sno'];
		$this->db->select('B.brand_name');
		$this->db->from('subcat_brands SB');
		$this->db->join('brand B', 'B.brand_sno = SB.brand_id');
		$this->db->where('SB.subcat_id',$subCAT);
		$query0 = $this->db->get();
		$sub_cat_brands = $query0->result_array();
		$final_array[$xxx]['brand_names'] = $sub_cat_brands;
	}
	return $final_array;
}
function brands_dropdown(){
	$this->db->select('*');
	$this->db->from('brand');
	$this->db->where('brand_status','1');
	$this->db->order_by('brand_status', 'DESC');
	$this->db->order_by('brand_sno', 'ASC');
	$query = $this->db->get();
	return $query->result_array();
}
function category_dropdown(){
	$this->db->select('C.*,M.menu_name');
	$this->db->from('category C');
	$this->db->join('menu M', 'C.menu_id = M.menu_sno');
	$this->db->where('C.cat_status','1');
	$this->db->order_by('cat_status', 'DESC');
	$this->db->order_by('cat_position_no', 'ASC');
	$query = $this->db->get();
	return $query->result_array();
}
function subcat_insert($data){
	$this->db->insert('sub_cat', $data);
	return $this->db->insert_id();
}
function subcat_brands_insert($brandslist){
	return $this->db->insert_batch('subcat_brands', $brandslist);
}
function subcat_brands_delete_multiple($brands_delete_array){
	$delrows = count($brands_delete_array);
	if($delrows>0){
		for($i=0;$i<$delrows;$i++){
			$this->db->where_in('sno', $brands_delete_array[$i]);
	        $this->db->delete('subcat_brands');
		}
    }
    return $this->db->affected_rows();
}
function subcat_info($subcat_id){
	$this->db->select('SC.*, C.cat_name');
	$this->db->from('sub_cat SC');
	$this->db->join('category C', 'C.cat_sno = SC.cat_id');
	$this->db->where('SC.sub_cat_sno', $subcat_id);
	$this->db->order_by('sub_cat_status', 'DESC');
	$this->db->order_by('sub_cat_sno', 'ASC');
	$query = $this->db->get();
	$final_array = $query->result_array();
	$xxx = -1;
	foreach ($final_array as $sub_Array) {  
		$xxx++;
		$subCAT = $sub_Array['sub_cat_sno'];
		$this->db->select('SB.sno as sub_cat_brand_id, SB.brand_id as brand_sno, B.brand_name');
		$this->db->from('subcat_brands SB');
		$this->db->join('brand B', 'B.brand_sno = SB.brand_id');
		$this->db->where('SB.subcat_id',$subCAT);
		$query0 = $this->db->get();
		$sub_cat_brands = $query0->result_array();
		$final_array[$xxx]['brand_names'] = $sub_cat_brands;
	}
	return $final_array;
}
function subcat_update($data,$sub_cat_sno){
	$this->db->where('sub_cat_sno',$sub_cat_sno);
	$this->db->update('sub_cat', $data);
	return $this->db->affected_rows();
}
/*
function subcat_brands_delete($sub_cat_sno){
	$this->db->where('sub_cat_sno',$sub_cat_sno);
	$this->db->delete('sub_cat', $data);
	return $this->db->affected_rows();
}
*/
function subcat_status_update($data,$subcat_id){
	$this->db->where('sub_cat_sno',$subcat_id);
	$this->db->update('sub_cat',$data);
	return $this->db->affected_rows();
}
function subcat_brands_delete($brands_delete_array){
	if (!empty($brands_delete_array)) {
        $this->db->where_in('sno', $brands_delete_array);
        $this->db->delete('subcat_brands');
        return $this->db->affected_rows();
    }
}
/*============  SUBCATEGORIES SECTION END  ========*/
/*============  COLOURS SECTION START  ========*/
function colors_insert($data){
	$this->db->insert('colors',$data);
	return $this->db->insert_id();
}
function sizes(){
	
	$this->db->select('*');
	$this->db->from('colors');
	$this->db->order_by('color_status', 'DESC');
	$this->db->order_by('color_sno', 'ASC');
	$query = $this->db->get();
	return $query->result_array();
	
}
function color_info($color_id){
	$this->db->select('*');
	$this->db->from('colors');
	$this->db->where('color_sno', $color_id);
	$query = $this->db->get();
	return $query->result_array();
}
function colors_update($data, $color_id){
	$this->db->where('color_sno',$color_id);
	$this->db->update('colors',$data);
	return $this->db->affected_rows();
}
/*============  COLOURS SECTION END  ========*/
/*============ PINCODE SECTION START ========*/
function pincode_insert($data){
	$this->db->insert('pincodes',$data);
	return $this->db->insert_id();
}
function pincodes(){
	$this->db->select('*');
	$this->db->from('pincodes');
	$this->db->order_by('pin_status', 'DESC');
	$this->db->order_by('pin_sno', 'ASC');
	$query = $this->db->get();
	return $query->result_array();
}
function pincode_info($pin_sno){
	$this->db->select('*');
	$this->db->from('pincodes');
	$this->db->where('pin_sno', $pin_sno);
	$query = $this->db->get();
	return $query->result_array();
}
function pincode_update($data, $pin_sno){
	$this->db->where('pin_sno',$pin_sno);
	$this->db->update('pincodes',$data);
	return $this->db->affected_rows();
}
/*============ PINCODE SECTION END  ========*/
/*============ PRODUCT SECTION START  ========*/
function colors_dd(){
	$this->db->select('*');
	$this->db->from('colors');
	$this->db->where('color_status', '1');
	$query = $this->db->get();
	return $query->result_array();
}
function subcategory_dropdown(){
	$this->db->select('sub_cat_sno,sub_cat_name');
	$this->db->from('sub_cat');
	$this->db->where('sub_cat_status','1');
	$this->db->order_by('sub_cat_sno', 'ASC');
	$query = $this->db->get();
	return $query->result_array();
}
function ajax_subcat_list($cat_id){
	$this->db->select('*');
	$this->db->from('sub_cat');
	$this->db->where('cat_id',$cat_id);
	$this->db->where('sub_cat_status','1');
	$this->db->order_by('sub_cat_sno', 'ASC');
	$query = $this->db->get();
	return $query->result_array();
}
function ajax_brands_list(){
	
	$this->db->select('*');
	$this->db->from('brand');
	$this->db->order_by('brand_sno', 'ASC');
	$query = $this->db->get();
	return $query->result_array();
}
function brands_list_active(){
	$this->db->select('*');
	$this->db->from('brand');
	$this->db->where('brand_status','1');
	$this->db->order_by('brand_sno', 'ASC');
	$query = $this->db->get();
	return $query->result_array();
}
function products(){
	$this->db->select('P.*, SC.sub_cat_name');
	$this->db->from('products P');
	$this->db->join('sub_cat SC', 'SC.sub_cat_sno = P.prod_sub_cat_sno');
	$this->db->order_by('prod_status', 'DESC');
	$this->db->order_by('prod_sno', 'ASC');
	$query = $this->db->get();
	return $query->result_array();
}
function product_insert($data){
	$this->db->insert('products',$data);
	return $this->db->insert_id();
}
function product_colors_insert($product_colors){
	$this->db->insert_batch('product_colors', $product_colors);
	return $this->db->affected_rows();
}
function product_brands_insert($product_brands){
	$this->db->insert_batch('product_brands', $product_brands);
	return $this->db->affected_rows();
}
function product_features($product_id){
	$this->db->select('F.*, P.prod_name, SC.sub_cat_name');
	$this->db->from('features F');
	$this->db->join('products P', 'P.prod_sno = F.prod_sno');
	$this->db->join('sub_cat SC', 'SC.sub_cat_sno = P.prod_sub_cat_sno');
	$this->db->where('F.prod_sno',$product_id);
	$query = $this->db->get();
	return $query->result_array();
}
function product_feature_insert($data){
	$this->db->insert('features',$data);
	return $this->db->insert_id();
}
function product_feature_edit($feature_id){
	$this->db->select('*');
	$this->db->from('features');
	$this->db->where('feature_sno',$feature_id);
	$query = $this->db->get();
	return $query->result_array();
}
function product_feature_update($data, $feature_id){
	$this->db->where('feature_sno',$feature_id);
	$this->db->update('features',$data);
	return $this->db->affected_rows();
}
function product_feature_delete($feature_id){
	$this->db->where('feature_sno',$feature_id);
	$this->db->delete('features');
	return $this->db->affected_rows();
}
function product_services($product_id){
	$this->db->select('PS.*, P.prod_name, SC.sub_cat_name');
	$this->db->from('product_services PS');
	$this->db->join('products P', 'P.prod_sno = PS.prod_sno');
	$this->db->join('sub_cat SC', 'SC.sub_cat_sno = P.prod_sub_cat_sno');
	$this->db->where('PS.prod_sno',$product_id);
	$query = $this->db->get();
	return $query->result_array();
}
function product_services_insert($product_Services){
	$this->db->insert_batch('product_services', $product_Services);
	return $this->db->affected_rows();
}
function product_sevice_delete($service_id){
	$this->db->where('service_sno',$service_id);
	$this->db->delete('product_services');
	return $this->db->affected_rows();
}
function product_images($prod_id){
	$this->db->select('PI.*, P.prod_name, SC.sub_cat_name');
	$this->db->from('product_images PI');
	$this->db->join('products P', 'P.prod_sno = PI.prod_sno');
	$this->db->join('sub_cat SC', 'SC.sub_cat_sno = P.prod_sub_cat_sno');
	$this->db->where('PI.prod_sno',$prod_id);
	$query = $this->db->get();
	return $query->result_array();
}
function get_product_details($product_id){
	$this->db->select('P.*,SC.sub_cat_name');
	$this->db->from('products P');
	$this->db->join('sub_cat SC', 'SC.sub_cat_sno = P.prod_sub_cat_sno');
	$this->db->where('prod_sno', $product_id);
	$query = $this->db->get();
	return $query->row();
}
function product_images_insert($galleryAry){
	$this->db->insert_batch('product_images', $galleryAry);
	return $this->db->affected_rows();
}
function product_image_exist_or_not($image_id){
	$this->db->select('*');
	$this->db->from('product_images PI');
	$this->db->join('products P', 'PI.prod_sno = P.prod_sno');
	$this->db->where('PI.product_img_sno', $image_id);
	$query = $this->db->get();
	return $query->row();
}
function product_images_addtag_insert($data, $image_id){
	$this->db->where('product_img_sno',$image_id);
	$this->db->update('product_images',$data);
	return $this->db->affected_rows();
}
function get_image_full_data($product_id, $image_id){
	$this->db->select('PI.*, P.prod_name, SC.sub_cat_sno, SC.sub_cat_name');
	$this->db->from('product_images PI');
	$this->db->join('products P', 'PI.prod_sno = P.prod_sno');
	$this->db->join('sub_cat SC', 'SC.sub_cat_sno = P.prod_sub_cat_sno');
	$this->db->where('PI.product_img_sno', $image_id);
	$query = $this->db->get();
	return $query->row();
}
function product_image_delete($image_id){
	$this->db->where('product_img_sno',$image_id);
	$this->db->delete('product_images');
	return $this->db->affected_rows();
}
function product_details($product_id){
	$this->db->select('P.*, SC.sub_cat_name,C.cat_name, C.cat_sno cat_id');
	$this->db->from('products P');
	$this->db->join('sub_cat SC', 'SC.sub_cat_sno = P.prod_sub_cat_sno');
	$this->db->join('category C', 'SC.cat_id = C.cat_sno');
	$this->db->where('P.prod_sno', $product_id);
	$query = $this->db->get();
	return $query->result_array();
}
function product_brands($product_id){
	$this->db->select('PB.*, B.brand_name');
	$this->db->from('product_brands PB');
	$this->db->join('brand B', 'B.brand_sno = PB.brand_id');
	$this->db->where('PB.product_id', $product_id);
	$query = $this->db->get();
	return $query->result_array();
}
function product_colors($product_id){
	$this->db->select('PC.*, C.color_name, C.color_code');
	$this->db->from('product_colors PC');
	$this->db->join('colors C', 'C.color_sno = PC.color_id');
	$this->db->where('PC.product_id', $product_id);
	$query = $this->db->get();
	return $query->result_array();
}
function brands_list_subcat($sub_cat_id){
	$this->db->select('SB.*,B.brand_name');
	$this->db->from('subcat_brands SB');
	$this->db->join('brand B', 'B.brand_sno = SB.brand_id');
	$this->db->where('subcat_id', $sub_cat_id);
	$query = $this->db->get();
	return $query->result_array();
}
function product_colors_delete($data){
	if (!empty($data)) {
        $this->db->where_in('sno', $data);
        $this->db->delete('product_colors');
        return $this->db->affected_rows();
    }
}
function product_colors_add($add_array){
	$this->db->insert_batch('product_colors', $add_array);
	return $this->db->affected_rows();
}
function product_brands_add($brands_add_array){
	$this->db->insert_batch('product_brands', $brands_add_array);
	return $this->db->affected_rows();
}
function product_brands_delete($data){
	if (!empty($data)) {
        $this->db->where_in('sno', $data);
        $this->db->delete('product_brands');
        return $this->db->affected_rows();
    }
}
function product_updated($data, $product_id){
	$this->db->where('prod_sno',$product_id);
	$this->db->update('products',$data);
	return $this->db->affected_rows();
}
function delete_product_brands($product_id){
	$this->db->where('product_id', $product_id);
	$this->db->delete('product_brands');
	return $this->db->affected_rows();
}
function delete_product_colors($product_id){
	$this->db->where('product_id', $product_id);
	$this->db->delete('product_colors');
	return $this->db->affected_rows();
}
function delete_product_service($product_id){
	$this->db->where('prod_sno', $product_id);
	$this->db->delete('product_services');
	return $this->db->affected_rows();
}
function delete_product_images($product_id){
	$this->db->where('prod_sno', $product_id);
	$this->db->delete('product_images');
	return $this->db->affected_rows();
}
function delete_product($product_id){
	$this->db->where('prod_sno', $product_id);
	$this->db->delete('products');
	return $this->db->affected_rows();
}
/*==========    PRODUCTS SECTION END    =========*/
/*========   SUBSCRIBER SECTION START  =========*/
function subscribers(){
	$this->db->select('*');
	$this->db->from('subscribers');
	$this->db->order_by('status', 'DESC');
	$this->db->order_by('sno', 'ASC');
	$query = $this->db->get();
	return $query->result_array();
}
function subscriber_update($data,$subs_id){
	$this->db->where('sno', $subs_id);
	$this->db->update('subscribers', $data);
	return $this->db->affected_rows();
}
function subscriber_delete($subs_id){
	$this->db->where('sno', $subs_id);
	$this->db->delete('subscribers');
	return $this->db->affected_rows();
}
/*========   SUBSCRIBER SECTION END  =========*/
/*========   ENQUIRIES SECTION START  =========*/
function enquiries(){
	$this->db->select('*');
	$this->db->from('enquiries');
	$this->db->order_by('sno', 'ASC');
	$query = $this->db->get();
	return $query->result_array();
}
function enquiry_insert($data){
	$this->db->insert('enquiries', $data);
	return $this->db->affected_rows();
}
function enquiry_update($data, $enq_id){
	$this->db->where('sno',$enq_id);
	$this->db->update('enquiries',$data);
	return $this->db->affected_rows();
}
function enquiry_delete($enq_id){
	$this->db->where('sno',$enq_id);
	$this->db->delete('enquiries');
	return $this->db->affected_rows();
}
/*========   ENQUIRIES SECTION START  =========*/

function reviews(){
	$this->db->select('R.*, U.first_name, U.last_name, P.prod_name');
	$this->db->from('reviews R');
	$this->db->join('users U', 'U.id = R.user_id');
	$this->db->join('products P', 'P.prod_sno = R.product_id');
	$this->db->order_by('status', 'ASC');
	$this->db->order_by('sno', 'ASC');
	$query = $this->db->get();
	return $query->result_array();
}
function review_update($data, $rev_id){
	$this->db->where('sno', $rev_id);
	$this->db->update('reviews', $data);
	return $this->db->affected_rows();
}
function review_delete($rev_id){
	$this->db->where('sno', $rev_id);
	$this->db->delete('reviews');
	return $this->db->affected_rows();
}



function banners(){
	$this->db->select('*');
	$this->db->from('banners');
	$this->db->order_by('status', 'DESC');
	$this->db->order_by('banner_sno', 'ASC');
	$query = $this->db->get();
	return $query->result_array();
}
function banners_insert($data){
	$this->db->insert('banners', $data);
	return $this->db->affected_rows();
}
function banner_image_info($banner_sno){
	$this->db->select('*');
	$this->db->from('banners');
	$this->db->where('banner_sno', $banner_sno);
	$query = $this->db->get();
	return $query->result_array();
}
function banner_tag_insert($data, $banner_sno){
	$this->db->where('banner_sno',$banner_sno);
	$this->db->update('banners',$data);
	return $this->db->affected_rows();
}
function banner_tag_deleted($banner_sno){
	$this->db->where('banner_sno',$banner_sno);
	$this->db->delete('banners');
	return $this->db->affected_rows();
}
function product_type($data,$pro_id){
	$this->db->where('prod_sno',$pro_id);
	$this->db->update('products',$data);
	return $this->db->affected_rows();
}
function orders(){ 

	$this->db->select('*');
	$this->db->from('orders'); 
	if($this->input->post('pr_start_date')!="")
	$startDate=date("Y-m-d",strtotime($this->input->post('pr_start_date')));
    if($this->input->post('pr_deadline')!="")
	$toDate=date("Y-m-d",strtotime($this->input->post('pr_deadline')));
	if($startDate!="" && $toDate!=""){
			
			$this->db->where("date(order_date_time) >=",$startDate);
            $this->db->where("date(order_date_time) <=",$toDate);
			
		}
		else if($toDate!=""){
			
		$this->db->where("date(order_date_time) <=",$toDate);
		
		}
		else if($startDate!=""){
			
		$this->db->where('date(order_date_time) >=',$startDate);
		
		}
		
		if($status!=""){
			
			$this->db->where("order_status",$status);
			
		}
		if($this->session->userdata('admin_type_id')=="4"){
			
			$this->db->where("delivery_boy_id",$this->session->userdata('admin_id'));
			
		}
	$this->db->order_by('order_date_time', 'DESC');
	
	$query = $this->db->get();

	return $query->result_array();
}
function order_info(){
	
	$orderID = $this->uri->segment('3');
	
 
	
	$userID = $this->session->userdata('userSno');
	$this->db->select('O.*,P.*,OP.*');
	$this->db->from('orders O'); 
	$this->db->join('order_products OP', 'OP.order_no = O.order_no', 'left');
	$this->db->join('products P', 'P.prod_sno = OP.product_id', 'left');

	$this->db->where('O.order_no', $orderID); 
	$query = $this->db->get();
	
  
	return $query->result_array();

}
function order_update($orderid, $data){
	$this->db->where('order_no',$orderid);
	$this->db->update('orders',$data);
	return $this->db->affected_rows();
}
function users($type){
	$this->db->select('*');
	$this->db->from('users'); 
	
	$this->db->where('user_type',$type);
	
	$query = $this->db->get();
	return $query->result_array();
}
 

function sum_order_price(){
		
		$this->db->select("sum(grand_total) as total_price");
		$this->db->from("orders O");
	
		
		if($this->input->post('pr_start_date')!="")
			$startDate=date("Y-m-d",strtotime($this->input->post('pr_start_date')));
			if($this->input->post('pr_deadline')!="")
			$toDate=date("Y-m-d",strtotime($this->input->post('pr_deadline')));
		
		$status=$this->input->post('status');
		if($startDate!="" && $toDate!=""){
			
			$this->db->where('date(O.order_date_time) >=',$startDate);
            $this->db->where('date(O.order_date_time) <=',$toDate);
			
		}
		else if($toDate!=""){
			
		$this->db->where("date(O.order_date_time) <=",$toDate);
		
		}
		else if($startDate!=""){
			
		$this->db->where('date(O.order_date_time) >=',$startDate);
		
		}
		
		if($status!=""){
			
			$this->db->where("O.order_status",$status);
			
		}
		if($this->session->userdata('admin_type_id')=="4"){
			
			$this->db->where("O.delivery_boy_id",$this->session->userdata('admin_id'));
			
		}
		$this->db->order_by("O.order_no",'asc');
		$query=$this->db->get();
		
		return $query->row();
  }

 
function checkStaffData(){
		
		$adminEmail = $this->input->post('email');
		$adminPhone = $this->input->post('phone_no');
		$this->db->select("admin_phone,admin_id,admin_no");
		$this->db->from("admin");
		$this->db->where("admin_phone",$adminPhone);
		$this->db->or_where("admin_id",$adminEmail);
		$query=$this->db->get();
		
		return $this->db->affected_rows();
		
	}
public function user_insert($data)
	{
		$this->db->insert("admin",$data);   
		return $this->db->affected_rows();
	}
public function employee_update($data, $AdminNo)
    {
		$this->db->where('admin_no',$AdminNo);
		$this->db->update('admin', $data);
       // return $this->db->affected_rows();
    }
public function employee_details_view()
    {
        $userID = $this->uri->segment('3');
        $this->db->select('*');
        $this->db->from('admin');
        $this->db->where('admin_no',$userID);
        $query = $this->db->get();
        return $query->row();
    }

}

?>