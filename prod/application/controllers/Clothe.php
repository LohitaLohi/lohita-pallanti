<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clothe extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
public function __Construct(){
        parent::__Construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('html');
		
//	   $this->load->database(); 	
       
	   $this->load->library('form_validation');
	   $this->load->library('facebook');
		date_default_timezone_set("Europe/Oslo");
		 error_reporting(0);
}
public function index(){
	  
	  
	 
	 	/*$data['myaccountData']=$this->Home_model->get_user_details();
		$data['products']=$this->Home_model->products();	*/	
		$data['get_catgories']=$this->Home_model->get_catgories();
		$this->load->view('home',$data);
}

public function filter_products(){
    
     if($this->input->post()){
    
          $fil_val = json_decode($this->input->post('fil_val'));
          
          $this->load->database();
    
        $get_fil_val = implode(',',$fil_val);
        
        
        
        
    
    
    $data['get_products'] = $this->db->query("SELECT * FROM products,brand,product_brands,category,sub_cat where category.cat_sno = sub_cat.cat_id and products.prod_sub_cat_sno = sub_cat.sub_cat_sno  and products.prod_sno = product_brands.product_id and brand.brand_sno = product_brands.brand_id and brand.brand_name IN  ('" . implode("','", $fil_val) . "') and products.prod_status = 1")->result();
   
      
   
   
         
        
  
      }
   
     $this->load->view('get_products',$data);
 
    
}




}
