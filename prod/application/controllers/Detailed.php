<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Detailed extends CI_Controller {
public function __Construct(){

        parent::__Construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('html');
		       
	   $this->load->library('form_validation');
	   $this->load->library('facebook');
		 
		
	    error_reporting(0);
        $this->load->model('Detailed_model');		
}
public function index(){
	
	$data['get_single_product']=$this->Home_model->get_single_product();
	$this->load->view('product-detail',$data);
  	
}
public function product_view(){
	
	$data['myaccountData']=$this->Home_model->get_user_details();
	$product_id = $this->uri->segment('3');
	$data['product_details'] = $this->Detailed_model->product_view($product_id);
	if(count($data['product_details'])){
		
		$data['product_features'] = $this->Detailed_model->product_features($product_id);
		$data['product_sizes'] = $this->Detailed_model->product_sizes($product_id);
		$data['product_services'] = $this->Detailed_model->product_services($product_id); 
		 
	}
	$this->load->view('newproduct',$data);
}
public function check_pincode(){
	
	$pincode = $this->input->post('pincode');
	$data['get_deli_price'] = $this->Detailed_model->check_pincode($pincode);
	if(count($data['get_deli_price']) > 0){
		$data['pin_checked'] = "success";
	} else {
		unset($_POST['pincode']);
		$data['pin_checked'] = "fail";
	}
	$this->load->view('ajax/pincode_check', $data);
}

public function add_to_cart(){
   
    if($this->session->userdata('userSno')){
		
		$orderId['order_id']=$this->session->userdata('userSno');
		$this->Detailed_model->update_to_cart_session($orderId); 
		$this->session->unset_userdata("cart_order_no");
	}
	
    if($this->input->post('quantity')!=""){
	 if($this->session->userdata("cart_order_no") || $this->session->userdata('userSno')){
		 
		  if($this->session->userdata('userSno')){
				  $orderId=$this->session->userdata('userSno');
			  }else{
			     
				 $this->session->set_userdata("cart_order_no",strtotime(date("Y-m-d")));
				 $orderId= $this->session->userdata('cart_order_no');
			  }
		  $productSno =$this->input->post('product_sno');
		  $sizesId =$this->input->post('size_id');
		  $checkProduct = $this->Detailed_model->check_cart($productSno,$sizesId);
		  $cartdata['product_id']=$this->input->post('product_sno');
		  $cartdata['order_id']=$orderId;
		  $cartdata['size_id']=$sizesId;
		  $quanity=$this->input->post('quantity');
		  unset($_POST);
		  if($checkProduct->quanity>0){
			  
			    $cartdataupdate['quanity']=$checkProduct->quanity+$quanity;
			    $this->Detailed_model->update_to_cart($cartdataupdate,$productSno,$sizesId); 
		  
		  }
		  else{
			  
			   $cartdata['quanity']=$quanity;
			   $this->Detailed_model->add_to_cart($cartdata);
		  }
	 }
	 else{
		      if($this->session->userdata('userSno')){
				  $orderId=$this->session->userdata('userSno');
			  }else{
			     
				 $this->session->set_userdata("cart_order_no",strtotime(date("Y-m-d")));
				 $orderId= $this->session->userdata('cart_order_no');
			  }
			  $cartdata['product_id']=$this->input->post('product_sno');
			  
			  $cartdata['quanity']=$this->input->post('quantity');
			  $cartdata['order_id']=$orderId;
			  $cartdata['date']=date("Y-m-d");
		      $ddd=$this->input->post('product_sno');
			  unset($_POST);
			  $this->Detailed_model->add_to_cart($cartdata); 
	    }
	    $data['myaccountData']=$this->Home_model->get_user_details();
		$cart_count=$this->Detailed_model->cart_count();
		$data['cart_details']=$this->Detailed_model->get_cart_details();
		$totalCount = $cart_count->total_quanity;
		$this->session->set_userdata("totalCount",$totalCount);
		redirect("detailed/product_view/".$productSno);
	}
	else if($this->input->post('cartSno1')!=""){
		
		 $cartSnol=$this->input->post('cartSno1');
		 $a=$this->Detailed_model->delete_cart_item($cartSnol);
		 
   		
	}
	else if($this->input->post('Ajaxquantity')!=""){
		
		 $cartdataupdate['quanity']=$this->input->post('Ajaxquantity');
		 
		 $cartSno=$this->input->post('quanityId');
		 $a=$this->Detailed_model->update_to_cart_ajax($cartdataupdate,$cartSno);
		
	}
	
		$data['myaccountData']=$this->Home_model->get_user_details();
		$cart_count=$this->Detailed_model->cart_count();
		$data['cart_details']=$this->Detailed_model->get_cart_details();
		$data['cart_limit']=$this->Detailed_model->get_limit();
		$totalCount = $cart_count->total_quanity;
		
		$this->session->set_userdata("totalCount",$totalCount);
		
			if($this->input->post('cartSno1')!=""){
					$this->load->view("ajax/ajax_addtocart",$data);
			}
			else if($this->input->post('Ajaxquantity')!=""){
					$this->load->view("ajax/ajax_addtocart",$data);
			}
			else{
					$this->load->view("addtocart",$data);
			}
		
		
}

public function payment(){	
    
	$url=$this->uri->segment(1);
	$this->session->set_userdata("payemnt_url",$url);	
   
	if($this->session->userdata("userSno")){
		if($this->session->userdata("userPhoneNo") && $this->session->userdata("grandTotal1")>=$this->session->userdata("cart_limit")){
			if($this->session->userdata("grandTotal1")){
				
		        $data['myaccountData']=$this->Home_model->get_user_details();
		        $data['cart_details']=$this->Detailed_model->get_cart_details();
				$this->session->set_userdata("order_confirm2","order_confirm");		
				$this->session->unset_userdata("payemnt_url",$data);				
				$this->load->view("payment",$data);
				
		}
	  else{
	       redirect("home");
	 }
		}
	 else{
	       redirect("home/user_otp_payment");
	 }
	}
	else{
		
		redirect("login");
	
	}
	
}		
/*
public function payment(){	
    
	$url=$this->uri->segment(1);
	$this->session->set_userdata("payemnt_url",$url);
	
	if($this->session->userdata("userSno")){
		if($this->session->userdata("userPhoneNo")){
			if($this->session->userdata("grandTotal1")){
		        $data['myaccountData']=$this->Home_model->get_user_details();
        $this->session->set_userdata("order_confirm2","order_confirm");		
		$this->session->unset_userdata("payemnt_url",$data);
		
		$this->load->view("payment",$data);
		}
	  else{
	       redirect("home");
	 }
		}
	 else{
	       redirect("home/user_otp_payment");
	 }
	}
	else{
		
		redirect("login");
	
	}
	
}*/
function order_confirm(){
	
	if($this->session->userdata("userSno")){
		            $time = strtotime(date("Y-m-d H:i:s"));
					$order['order_no']=$time;
					$this->session->set_userdata("orderNo",$time);
					$order['user_id']=$this->session->userdata("userSno");
					$gtotal = $this->session->userdata("grand_total");
					$order['grand_total']=$gtotal;
					$order['shipping_status']=0;
					$order['payment_type']=1;
					$order['order_date_time']=date("Y-m-d H:i:s");
					$order12=$this->Detailed_model->order_inert($order);
					$cart_details=$this->Detailed_model->get_cart_details();
				    foreach($cart_details as $val){
						
						$orderProduct['product_id']=$val['product_id'];
						$orderProduct['order_no']=$time;
						$orderProduct['user_id']=$this->session->userdata("userSno");
						$orderProduct['product_cost']=$val['prod_dis_price'];
						$orderProduct['product_quanity']=$val['qun_cart'];
						$orderProduct['date_time']=date("Y-m-d H:i:s");
						$orderProduct['prod_size']=$val['size_id']; 
						$orderConfirm=$this->Detailed_model->order_inert_product($orderProduct);
					   
					 }
					
				 
			        $deleteCart=$this->Detailed_model->delete_cart();
					$this->session->unset_userdata("pincodeCost");
					$this->session->unset_userdata("order_confirm2");
					$this->session->unset_userdata("order_confirm2");
					$this->session->set_userdata("pincode");
					$email=$this->session->userdata("userId");
					
					
					 
						redirect("detailed/order_confirm2");
					 
					
			 
			}
			else{
				
				redirect("home");
			}
		
	
}

function order_confirm2(){
     
      
         $getPoducts['data']=$this->Detailed_model->get_order_details();     
     
	
	
	$getPoducts['get_product_details']=$this->Detailed_model->get_order_product_details();				
    $this->load->view("email/order_email",$getPoducts);		
		
}

}

?>