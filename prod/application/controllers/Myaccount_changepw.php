<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Myaccount_changepw extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
public function __Construct(){
        parent::__Construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('html');
		
		    $this->load->library('session');
		    
		    $this->load->database();
       
	   $this->load->library('form_validation');
	   $this->load->library('facebook');
		date_default_timezone_set("Asia/Kolkata");
		 	error_reporting(0);
	    		
}
public function index(){
		
		if($this->input->post()){
		    
		    
		    $user_id = $this->session->userdata('userId');
		    
		   $password = md5($this->input->post('new_password')); 
		  
		    $change_array = array('password'=>$password);
		    
		    $this->db->where('id',$user_id);
		    
		    if($this->db->update('users',$change_array)){
		        
		        
		        $data['success_msg'] = "Your Password is changed successfully";
		        
		    }
		    
		    
		    
		    
		}
		
		
		
		$this->load->view('myaccount-changepw',$data);
}
 
    
}
