<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sellar extends CI_Controller {

public function __Construct(){
	
    parent::__Construct();
    $this->load->helper('url');
    $this->load->helper('form');
    $this->load->helper('html');
    $this->load->library('upload');
    $this->load->model('Sellar_model');
    $this->load->library('form_validation');
	date_default_timezone_set("Asia/Kolkata");
    $this->load->library('session');
   // $this->session->set_userdata('sellar_id','10');
	if(!($this->session->userdata('sellar_id')))
	{
		redirect("home/index");
	}
	
	error_reporting(0);
}
public function index()	{
	$this->load->view('sellar/index');
}
function logout(){
	$this->session->unset_userdata('sellar_id');
	redirect('home/index');
}
/*==========   MENU SECTION START   ======*/
function menu(){
	$data['menu_list'] = $this->Sellar_model->menu();
	$this->load->view('sellar/menu', $data);
}
function menu_add(){
	$this->load->view('sellar/menu_add');
}
function menu_insert(){
	$data['menu_name'] = $this->input->post('menu_name');
	$data['menu_position_no'] = $this->input->post('menu_position');
	$data['menu_status'] = '1';
	$data['menu_created_date'] = date('Y-m-d H:i:s');
	$data['menu_created_by'] = $this->session->userdata('sellar_id');
	$data_insert = $this->Sellar_model->menu_insert($data);
	if ($data_insert) {
		redirect('sellar/menu');
	} else{
		$this->session->set_flashdata('error0','Data Not Inserted');
		redirect('sellar/menu_add');
	}
}
function menu_edit(){
	$menu_id = $this->uri->segment('3');
	$data['menu_item_data'] = $this->Sellar_model->menu_data($menu_id);
	if(count($data['menu_item_data'])){
		$this->load->view('sellar/menu_edit',$data);
	} else {
		redirect('sellar/menu');
	}
}
function menu_update(){
	$menu_id = $this->input->post('menu_id');
	$data['menu_name'] = $this->input->post('menu_name');
	$data['menu_position_no'] = $this->input->post('menu_position');
	$data['menu_status'] = $this->input->post('menu_status');
	$data['menu_updated_date'] = date('Y-m-d H:i:s');
	$data['menu_updated_by'] = $this->session->userdata('sellar_id');
	$data_update = $this->Sellar_model->menu_update($data,$menu_id);
	$to_edit = 'sellar/menu_edit/'.$menu_id;
	if($data_update>0) {
		redirect('sellar/menu');
	} else {
		$this->session->set_flashdata('error0','Data Not Updated');
		redirect($to_edit);
	}
}
function menu_inactive(){
	$menu_id = $this->uri->segment('3');
	$data['menu_status'] = '0';
	$data['menu_updated_date'] = date('Y-m-d H:i:s');
	$data['menu_updated_by'] = $this->session->userdata('sellar_id');
	$deleted = $this->Sellar_model->menu_update($data,$menu_id);
	redirect('sellar/menu');
}
 


function menu_active(){
	$menu_id = $this->uri->segment('3');
	$data['menu_status'] = '1';
	$data['menu_updated_date'] = date('Y-m-d H:i:s');
	$data['menu_updated_by'] = $this->session->userdata('sellar_id');
	$deleted = $this->Sellar_model->menu_update($data,$menu_id);
	redirect('sellar/menu');
}
/*============  MENU SECTION END  ========*/

/*========== CATEGORY SECTION START ======*/
function category(){
	$data['category_list'] = $this->Sellar_model->category();
	$this->load->view('sellar/category', $data);
}
function category_add(){
	$data['menu_list'] = $this->Sellar_model->menu_active_items();
	$this->load->view('sellar/category_add', $data);
}
function category_insert(){
	$data['cat_name'] = $this->input->post('cat_name');
	$data['menu_id'] = $this->input->post('menu_id');
	$data['cat_position_no'] = $this->input->post('cat_position_no');
	$data['cat_status'] = '1';
	$data['cat_created_date'] = date('Y-m-d H:i:s');
	$data['cat_created_by'] = $this->session->userdata('sellar_id');
	$data_insert = $this->Sellar_model->cat_insert($data);
	if ($data_insert) {
		redirect('sellar/category');
	} else{
		$this->session->set_flashdata('error0','Data Not Inserted');
		redirect('sellar/category_add');
	}
}
function category_edit(){
	$cat_id = $this->uri->segment('3');
	$data['menu_list'] = $this->Sellar_model->menu_active_items();
	$data['cat_data'] = $this->Sellar_model->cat_data($cat_id);
	if(count($data['cat_data'])){
		$this->load->view('sellar/category_edit',$data);
	} else {
		redirect('sellar/category');
	}
}
function category_update(){
	$cat_id = $this->input->post('cat_id');
	$data['menu_id'] = $this->input->post('menu_id');
	$data['cat_name'] = $this->input->post('cat_name');
	$data['cat_position_no'] = $this->input->post('cat_position_no');
	$data['cat_status'] = $this->input->post('cat_status');
	$data['cat_updated_date'] = date('Y-m-d H:i:s');
	$data['cat_updated_by'] = $this->session->userdata('sellar_id');
	$data_update = $this->Sellar_model->category_update($data,$cat_id);
	$to_edit = 'sellar/category_edit/'.$cat_id;
	if($data_update > 0) {
		redirect('sellar/category');
	} else {
		$this->session->set_flashdata('error0','Data Not Updated');
		redirect($to_edit);
	}
}
function category_inactive(){
	$cat_id = $this->uri->segment('3');
	$data['cat_status'] = '0';
	$data['cat_updated_date'] = date('Y-m-d H:i:s');
	$data['cat_updated_by'] = $this->session->userdata('sellar_id');
	$deleted = $this->Sellar_model->category_update($data,$cat_id);
	redirect('sellar/category');
}
function category_active(){
	$cat_id = $this->uri->segment('3');
	$data['cat_status'] = '1';
	$data['cat_updated_date'] = date('Y-m-d H:i:s');
	$data['cat_updated_by'] = $this->session->userdata('sellar_id');
	$deleted = $this->Sellar_model->category_update($data,$cat_id);
	redirect('sellar/category');
}
/*========== CATEGORY SECTION END ======*/
/*========== BRANDS SECTION START ======*/
function brands(){
	$data['brands_info'] = $this->Sellar_model->brands();
	$this->load->view('sellar/brands', $data);
}
function brands_add(){
	$this->load->view('sellar/brands_add');
}
function brand_insert(){
	$brandname = $this->input->post('brand_name');
	$data['brand_name'] = $brandname;
	$fileame = '';
	if(!file_exists('assets/images/brands'))
	{
		mkdir('assets/images/brands');
	}
	if($_FILES['brand_img']['name']){
		$brandimgType = $_FILES['brand_img']['type'];
		$brandimgName = $_FILES['brand_img']['name'];
		$brandimgTmp = $_FILES['brand_img']['tmp_name'];
		$brandname = str_replace(' ', '_', strtolower(trim($data['brand_name'])));
		$x = time();
		$fileame = $x."_".$brandname."_".$brandimgName;
		list($width, $height, $type, $attr) = getimagesize($brandimgTmp);
		$path = "assets/images/brands/$fileame";
		if(move_uploaded_file($brandimgTmp,$path)){
			$data['brand_img'] = $fileame;
		}
	}
	if(isset($_POST['brand_add'])){
		$data['brand_created_date'] = date('Y-m-d H:i:s');
		$data['brand_created_by'] = $this->session->userdata('sellar_id');
		$data['brand_status'] = '1';
		$brand_inserted = $this->Sellar_model->brands_insert($data);
		if($brand_inserted){
			redirect('sellar/brands');
		} else {
			$error['error'] = "Data not inserted."; 
			$this->load->view('sellar/brands_add', $error);

		}
	}
	if(isset($_POST['brand_update'])){
		$data['brand_updated_date'] = date('Y-m-d H:i:s');
		$data['brand_updated_by'] = $this->session->userdata('sellar_id');
		//$data['brand_status'] = $this->input->post('brand_status');
		$old_img = $this->input->post('old_brand_image');
		if($fileame != ''){
			if($old_img){
				$file = "assets/images/brands/".$old_img;
				unlink($file);
			}
		} else {
			$data['brand_img']= $old_img;
		}
		$brand_id = $this->input->post('brand_sno');
		$brand_updated = $this->Sellar_model->brands_update($data, $brand_id);
		if($brand_updated>0){
			redirect('sellar/brands');
		} else {
			redirect("sellar/brand_edit/$brand_id");
		}
	}
}
function brand_edit(){
	$brand_id = $this->uri->segment('3');
	$data['brand_data'] = $this->Sellar_model->brand_info($brand_id);
	if(count($data['brand_data'])){
		$this->load->view('sellar/brands_edit',$data);
	} else {
		redirect('sellar/brands');
	}
}
function brand_inactive(){
	$brand_id = $this->uri->segment('3');
	$data['brand_status'] = '0';
	$data['brand_updated_date'] = date('Y-m-d H:i:s');
	$data['brand_updated_by'] = $this->session->userdata('sellar_id');
	$deleted = $this->Sellar_model->brand_update($data,$brand_id);
	redirect('sellar/brands');
}
function brand_active(){
	$brand_id = $this->uri->segment('3');
	$data['brand_status'] = '1';
	$data['brand_updated_date'] = date('Y-m-d H:i:s');
	$data['brand_updated_by'] = $this->session->userdata('sellar_id');
	$deleted = $this->Sellar_model->brand_update($data,$brand_id);
	redirect('sellar/brands');
}
/*========== BRANDS SECTION END ======*/
/*========== SUB-CATEGORY SECTION START ======*/
function subcategory(){
	$data['subcat_list'] = $this->Sellar_model->subcategories();
	$this->load->view('sellar/subcategory', $data);
}
function subcategory_add(){
	$data['brands_dropdown'] = $this->Sellar_model->brands_dropdown();
	$data['category_dropdown'] = $this->Sellar_model->category_dropdown();
	$this->load->view('sellar/subcategory_add', $data);
}
function subcat_insert(){
	$data['cat_id'] = $this->input->post('cat_id');
	$data['sub_cat_name'] = trim($this->input->post('subcategory_name'));
	$data['sub_cat_position_no'] = $this->input->post('subcat_pos');
	$data['sub_cat_created_date'] = date('Y-m-d H:i:s');
	$data['sub_cat_created_by'] = $this->session->userdata('sellar_id');
	$data['sub_cat_status'] = '1';
	$subcat_id = $this->Sellar_model->subcat_insert($data);
	if($subcat_id){
		$brands_list = $this->input->post('brand_id');
		$p=-1;
		foreach($brands_list as $bdlist) { $p++;
			$brandslist[$p] = array(
			    'subcat_id' => $subcat_id,
			    'brand_id' => $bdlist,
			    'created_date' =>date('Y-m-d H:i:s'),
			    'created_by' => $this->session->userdata('sellar_id')
			);
		}
		$subcat_brands_id = $this->Sellar_model->subcat_brands_insert($brandslist);
		if($subcat_brands_id){
			redirect('sellar/subcategory');
		} else {
			$data['brands_dropdown'] = $this->Sellar_model->brands_dropdown();
			$data['category_dropdown'] = $this->Sellar_model->category_dropdown();
			$data['errors'] = "Only brands are not inserted.";
			$this->load->view('sellar/subcategory_add', $data);
		}
	} else {
		$data['brands_dropdown'] = $this->Sellar_model->brands_dropdown();
		$data['category_dropdown'] = $this->Sellar_model->category_dropdown();
		$data['errors'] = "Data not inserted.";
		$this->load->view('sellar/subcategory_add', $data);
	}
}
function subcategory_edit(){
	$subcat_id = $this->uri->segment('3');
	$data['subcat_info'] = $this->Sellar_model->subcat_info($subcat_id);
	if(count($data['subcat_info'])){
		$data['brands_dropdown'] = $this->Sellar_model->brands_dropdown();
		$data['category_dropdown'] = $this->Sellar_model->category_dropdown();
		$this->load->view('sellar/subcategory_edit', $data);
	} else {
		redirect('sellar/subcategory');
	}
}
function subcat_update(){
	$sub_cat_sno = $this->input->post('sub_cat_sno');
	$data['cat_id'] = $this->input->post('cat_id');
	$data['sub_cat_name'] = trim($this->input->post('subcategory_name'));
	$data['sub_cat_position_no'] = $this->input->post('subcat_pos');
	$data['sub_cat_updated_date'] = date('Y-m-d H:i:s');
	$data['sub_cat_updated_by'] = $this->session->userdata('sellar_id');
	$data['sub_cat_status'] = $this->input->post('sub_cat_status');
	$subcat_id_update = $this->Sellar_model->subcat_update($data,$sub_cat_sno);

	if($subcat_id_update>0){
		$old_brands = $this->Sellar_model->brands_list_subcat($sub_cat_sno);
		$new_brands = $this->input->post('brand_id');
		/*=========== FIND THE NEW BRANDS & ADDED TO subcat_brands TABLE  ===========*/
		$brands_add_array = array();
		$newArray2 = array();
		foreach($old_brands as $key => $val){
		    $newArray2[$val['subcat_id'] . '__' . $val['brand_id'] . '__' . $val['brand_name']] = $val;
		}
		$e= -1;
		foreach($new_brands as $key => $val){ $cc++; 
		    if(!isset($newArray2[$val])){
		        $e++;
		        $brd_id_name = explode("__",$val);
				$brands_add_array[$e]['subcat_id'] = $sub_cat_sno;
				$brands_add_array[$e]['brand_id'] = $brd_id_name['1'];
				$brands_add_array[$e]['created_date'] = $this->session->userdata('sellar_id');
				$brands_add_array[$e]['created_by'] = date('Y-m-d H:i:s');
		    }
		}
		if(count($brands_add_array)){
			$this->Sellar_model->subcat_brands_insert($brands_add_array);
		}
		/*=========== FIND THE BRANDS & REMOVED FROM subcat_brands TABLE  ===========*/
		$newArray3 = array();
		$brands_delete_array = array();
		foreach($new_brands as $key => $val){
		    $newArray3[$val] = $val;
		}
		$ne= -1;
		foreach($old_brands as $key => $val){ 
			$cc++; 
		    if(!isset($newArray3[$val['subcat_id'] . '__' . $val['brand_id'] . '__' . $val['brand_name']])){
		        $ne++;
				$brands_delete_array[$ne] = $val['sno'];
		    }
		} 
		if(count($brands_delete_array)){
			$this->Sellar_model->subcat_brands_delete_multiple($brands_delete_array);
		} 
		redirect('sellar/subcategory');
	} else {
		$sub_cat_sno_edit = 'sellar/subcategory_edit/'.$sub_cat_sno;
		redirect($sub_cat_sno_edit);
	}
}
function subcat_inactive(){
	$subcat_id = $this->uri->segment('3');
	$data['sub_cat_status'] = '0';
	$data['sub_cat_updated_date'] = date('Y-m-d H:i:s');
	$data['sub_cat_updated_by'] = $this->session->userdata('sellar_id');
	$deleted = $this->Sellar_model->subcat_status_update($data,$subcat_id);
	redirect('sellar/subcategory');
}
function subcat_active(){
	$subcat_id = $this->uri->segment('3');
	$data['sub_cat_status'] = '1';
	$data['sub_cat_updated_date'] = date('Y-m-d H:i:s');
	$data['sub_cat_updated_by'] = $this->session->userdata('sellar_id');
	$deleted = $this->Sellar_model->subcat_status_update($data,$subcat_id);
	redirect('sellar/subcategory');
}
/*==========  SUB-CATEGORY SECTION END  ======*/
/*==========   COLORS SECTION START  =========*/
function sizes(){
	$data['colours'] = $this->Sellar_model->sizes();
	$this->load->view('sellar/sizes', $data);
}
function sizes_add(){
	$this->load->view('sellar/sizes_add');
}
function sizes_insert(){
	
	$data['color_name'] = $this->input->post('color_name');
	
	if(isset($_POST['color_add'])){
		
		$data['color_created_date'] = date('Y-m-d H:i:s');
		$data['color_created_by'] = $this->session->userdata('sellar_id');
		$data['color_status'] = '1';
		$color_inserted = $this->Sellar_model->colors_insert($data);
		if($color_inserted){
			redirect('sellar/sizes');
		} else {
			$data['errors'] = "Color not inserted.";
			$this->load->view('sellar/colors_add', $data);
		}
	}
	if(isset($_POST['color_update'])){
		$color_id = $this->input->post('color_id');
		$data['color_updated_date'] = date('Y-m-d H:i:s');
		$data['color_updated_by'] = $this->session->userdata('sellar_id');
		$data['color_status'] = $this->input->post('color_status');
		$color_updated = $this->Sellar_model->colors_update($data, $color_id);
		if($color_updated>0){
			redirect('sellar/colors');
		} else {
			$color_id = $this->uri->segment('3');
			$data['color_info'] = $this->Sellar_model->color_info($color_id);
			$colors_edit = 'sellar/colors_edit/'.$color_id;
			$data['errors'] = "Data not updated.";
			$this->load->view($colors_edit, $data);
		}
	}
}
function sizes_edit(){
	$color_id = $this->uri->segment('3');
	$data['color_info'] = $this->Sellar_model->sizes_info($color_id);
	$this->load->view('sellar/sizes_edit', $data);
}
function sizes_inactive(){
	$color_id = $this->uri->segment('3');
	$data['color_status'] = '0';
	$data['color_updated_date'] = date('Y-m-d H:i:s');
	$data['color_updated_by'] = $this->session->userdata('sellar_id');
	$updated = $this->Sellar_model->colors_update($data,$color_id);
	redirect('sellar/colors');
}
function sizes_active(){
	$color_id = $this->uri->segment('3');
	$data['color_status'] = '1';
	$data['color_updated_date'] = date('Y-m-d H:i:s');
	$data['color_updated_by'] = $this->session->userdata('sellar_id');
	$updated = $this->Sellar_model->colors_update($data,$color_id);
	redirect('sellar/colors');
}
/*==========    COLORS SECTION END   =========*/
/*==========    PINCODES SECTION START   =========*/
function pincode(){
	$data['pincodes_list'] = $this->Sellar_model->pincodes();
	$this->load->view('sellar/pincode', $data);
}
function pincode_add(){
	$this->load->view('sellar/pincode_add');
}
function pincode_insert(){
	$data['pincode'] = $this->input->post('pincode');
	$data['service_cost'] = $this->input->post('service_cost');
	if (isset($_POST['pincode_add'])) {
		$data['pin_created_date'] = date('Y-m-d H:i:s');
		$data['pin_created_by'] = $this->session->userdata('sellar_id');
		$data['pin_status'] = '1';
		$inserted = $this->Sellar_model->pincode_insert($data);
		if($inserted){
			redirect('sellar/pincode');
		} else {
			$data['errors'] = 'Data not inserted.';
			$this->load->view('sellar/pincode_add', $data);
		}
	}
	if (isset($_POST['pincode_update'])) {
		$pin_sno = $this->input->post('pin_sno');
		$data['pin_updated_date'] = date('Y-m-d H:i:s');
		$data['pin_updated_by'] = $this->session->userdata('sellar_id');
		$data['pin_status'] = $this->input->post('pin_status');
		$updated = $this->Sellar_model->pincode_update($data, $pin_sno);
		if($updated>0){
			redirect('sellar/pincode');
		} else {
			$editpage = 'sellar/pincode_edit'.$pin_sno;
			$data['pincode_info'] = $this->Sellar_model->pincode_info($pin_sno);
			$data['errors'] = 'Data not updated.';
			$this->load->view($editpage, $data);
		}
	}
}
function pincode_edit(){
	$pin_sno = $this->uri->segment('3');
	$data['pincode_info'] = $this->Sellar_model->pincode_info($pin_sno);
	$this->load->view('sellar/pincode_edit', $data);
}
function pincode_inactive(){
	
	$pin_id = $this->uri->segment('3');
	$data['pin_status'] = '0';
	$data['pin_updated_date'] = date('Y-m-d H:i:s');
	$data['pin_updated_by'] = $this->session->userdata('sellar_id');
	$updated = $this->Sellar_model->pincode_update($data,$pin_id);
	if($updated){
		redirect('sellar/pincode');
	} else {
		$data['errors'] = 'Data not updated.';
		$data['pincode_info'] = $this->Sellar_model->pincode_info($pin_id);
		$this->load->view('sellar/pincode_edit', $data);
	}
}
function pincode_active(){
	
	$pin_id = $this->uri->segment('3');
	$data['pin_status'] = '1';
	$data['pin_updated_date'] = date('Y-m-d H:i:s');
	$data['pin_updated_by'] = $this->session->userdata('sellar_id');
	$updated = $this->Sellar_model->pincode_update($data,$pin_id);
	if($updated){
		redirect('sellar/pincode');
	} else {
		$data['errors'] = 'Data not updated.';
		$data['pincode_info'] = $this->Sellar_model->pincode_info($pin_id);
		$this->load->view('sellar/pincode_edit', $data);
	}
}
/*==========    PINCODES SECTION END   =========*/
/*==========    PRODUCTS SECTION START   =========*/
function product(){
	$data['products_list'] = $this->Sellar_model->products();
	$this->load->view('sellar/product', $data);
}
function product_add(){
	
	$data['colors_list'] = $this->Sellar_model->colors_dd();
	$data['category_dropdown'] = $this->Sellar_model->category_dropdown();
	$data['subcategory_dropdown'] = $this->Sellar_model->subcategory_dropdown();
	$data['ajax_brands_list'] = $this->Sellar_model->ajax_brands_list();
	$this->load->view('sellar/product_add', $data);
}
function product_insert(){
	
	$data['prod_sub_cat_sno'] = $this->input->post('prod_sub_cat_sno');
	$data['prod_name'] = trim($this->input->post('prod_name'));
	$data['prod_price'] = $this->input->post('prod_dis_price');
	$data['prod_discount'] = $this->input->post('prod_dis_price');
	$data['prod_tags'] = $this->input->post('prod_tags');
	$data['prod_dis_price'] = $this->input->post('prod_dis_price');
	$data['prod_stock'] = $this->input->post('prod_stock');
	$data['prod_stock_rem'] = $this->input->post('prod_stock');
	$data['prod_des'] = $this->input->post('prod_des');
	$data['prod_created_date'] = date('Y-m-d H:i:s');
	$data['origin_of_country'] = $this->input->post('origin_of_country');
	$data['per_of_alc'] = $this->input->post('per_of_alc');
	$data['pck_typ'] = $this->input->post('pck_typ');;
	$data['quanity'] = $this->input->post('quanity');
	$data['quanity_type'] = $this->input->post('quanity_type');
	$data['prod_created_by'] = $this->session->userdata('sellar_id');
	$data['prod_status'] = '1';
	$prod_colors = $this->input->post('prod_colors');
	$prod_brands = $this->input->post('prod_brands');
 
 
 
	$product_id = $this->Sellar_model->product_insert($data);
	
 
	
	if(!empty($product_id)){
		$p=-1;
		$q=-1;
	/*	foreach($prod_colors as $prod_color) { $p++;
			$product_colors[$p] = array(
			    'product_id' => $product_id,
			    'color_id' => $prod_color,
			    'created_date' =>date('Y-m-d H:i:s'),
			    'created_by' => $this->session->userdata('sellar_id')
			);
		} */
		foreach($prod_brands as $prod_brand) { $q++;
			$product_brands[$q] = array(
			    'product_id' => $product_id,
			    'brand_id' => $prod_brand,
			    'created_date' =>date('Y-m-d H:i:s'),
			    'created_by' => $this->session->userdata('sellar_id')
			);
		}
	//	$product_color_id = $this->Sellar_model->product_colors_insert($product_colors);
		$product_brands_id = $this->Sellar_model->product_brands_insert($product_brands);
	 
			$data['category_dropdown'] = $this->Sellar_model->category_dropdown();
		 if($product_brands_id <= 0){
				$data['errors'] = "Product brands not inserted.";
				$this->load->view('sellar/product_add',$data);
		 
		} else {
			redirect('sellar/product');
		}
	} else {
		$data['brands_dropdown'] = $this->Sellar_model->brands_dropdown();
		$data['category_dropdown'] = $this->Sellar_model->category_dropdown();
		$data['errors'] = "Data not inserted.";
		$this->load->view('sellar/subcategory_add', $data);
	}
}
function product_update(){
	$product_id = $this->input->post('prod_sno');
	$data['prod_sub_cat_sno'] = $this->input->post('prod_sub_cat_sno');
	$new_prod_name = trim($this->input->post('prod_name'));
	$data['prod_name'] = $new_prod_name;
	$old_prod_name = $this->input->post('old_prod_name');
	$sub_cat_name = $this->input->post('sub_cat_name');
	$origin_of_country = $this->input->post('origin_of_country');
	$per_of_alc = $this->input->post('per_of_alc');
	$data['prod_price'] = $this->input->post('prod_price');
	$data['prod_discount'] = $this->input->post('prod_discount');
	$data['origin_of_country'] = $this->input->post('origin_of_country');
	$data['per_of_alc'] = $this->input->post('per_of_alc');
	$data['prod_tags'] = $this->input->post('prod_tags');
	$data['prod_dis_price'] = $this->input->post('prod_dis_price');
	$data['prod_stock'] = $this->input->post('prod_stock');
	$data['prod_des'] = $this->input->post('prod_des');
	$data['pck_typ'] = $this->input->post('pck_typ');
	$data['quanity'] = $this->input->post('quanity');
	$data['quanity_type'] = $this->input->post('quanity_type');
	$data['prod_updated_date'] = date('Y-m-d H:i:s');
	$data['prod_updated_by'] = $this->session->userdata('sellar_id');
	$data['prod_status'] = $this->input->post('prod_status');
	$except = array(' ','(',')','\\', '/', ':', '*', '?', '"', '<', '>', '|');
	$old_prod_name = str_replace($except, "_", strtolower($old_prod_name));
	$new_prod_name = str_replace($except, "_", strtolower($new_prod_name));
	$sub_cat_name = str_replace($except, "_", strtolower($sub_cat_name));
	if($old_prod_name != $new_prod_name){
		$old_path = 'assets/images/gallery/'.$sub_cat_name.'/'.$old_prod_name;
		$new_path = 'assets/images/gallery/'.$sub_cat_name.'/'.$new_prod_name;
		rename($old_path, $new_path);
	}

	$prod_colors = $this->input->post('prod_colors');
	$old_prod_colors = $this->Sellar_model->product_colors($product_id);

	$prod_brands = $this->input->post('prod_brands');
	$old_prod_brands = $this->Sellar_model->product_brands($product_id);

	/*=========== FIND THE BRANDS TO ADD & ADDED TO product_brands TABLE ===========*/
	$brands_add_array = array();
	$newArray2 = array();
	foreach($old_prod_brands as $key => $val){
	    $newArray2[$val['brand_id'] . '__' . $val['brand_name']] = $val;
	}
	$e= -1;
	foreach($prod_brands as $key => $val){ $cc++; 
	    if(!isset($newArray2[$val])){
	        $e++;
	        $brd_id_name = explode("__",$val);
			$brands_add_array[$e]['product_id'] = $product_id;
			$brands_add_array[$e]['brand_id'] = $brd_id_name['0'];
			$brands_add_array[$e]['created_by'] = $this->session->userdata('sellar_id');
			$brands_add_array[$e]['created_date'] = date('Y-m-d H:i:s');
	    }
	}
	if(count($brands_add_array)){
		$prod_brands_added = $this->Sellar_model->product_brands_add($brands_add_array);
	}
	/*=========== FIND THE BRANDS TO REMOVE & REMOVED FROM product_brands TABLE  ===========*/
	$newArray3 = array();
	$brands_delete_array = array();
	foreach($prod_brands as $key => $val){
	    $newArray3[$val] = $val;
	}
	$ne= -1;
	foreach($old_prod_brands as $key => $val){ $cc++; 
	    if(!isset($newArray3[$val['brand_id'] . '__' . $val['brand_name']])){
	        $ne++;
			$brands_delete_array[$ne] = $val['sno'];
	    }
	}
	if(count($brands_delete_array)){
		$prod_brands_deleted = $this->Sellar_model->product_brands_delete($brands_delete_array);
	}
	/*=========== FIND THE COLORS TO ADD & ADDED TO product_colors TABLE  ===========*/
	$colors_add_array = array();
	$newArray0 = array();
	foreach($old_prod_colors as $key => $val){
	    $newArray0[$val['color_id'] . '__' . $val['color_name']] = $val;
	}
	$e= -1;
	foreach($prod_colors as $key => $val){ $cc++; 
	    if(!isset($newArray0[$val])){
	        $e++;
	        $clr_id_name = explode("__",$val);
			$colors_add_array[$e]['product_id'] = $product_id;
			$colors_add_array[$e]['color_id'] = $clr_id_name['0'];
			$colors_add_array[$e]['created_by'] = $this->session->userdata('sellar_id');
			$colors_add_array[$e]['created_date'] = date('Y-m-d H:i:s');
	    }
	}
	if(count($colors_add_array) >0){
		$add_colors_count = $this->Sellar_model->product_colors_add($colors_add_array);
	}
	/*=========== FIND THE COLORS TO REMOVE & REMOVED FROM product_colors TABLE   ===========*/
	$newArray = array();
	$colors_delete_array = array();
	foreach($prod_colors as $key => $val){
	    $newArray[$val] = $val;
	}
	$ne= -1;
	foreach($old_prod_colors as $key => $val){ $cc++; 
	    if(!isset($newArray[$val['color_id'] . '__' . $val['color_name']])){
	        $ne++;
			$colors_delete_array[$ne] = $val['sno'];
	    }
	}
	if(count($colors_delete_array) >0){
		$removed_colors_count = $this->Sellar_model->product_colors_delete($colors_delete_array);
	}
	$product_update = $this->Sellar_model->product_updated($data, $product_id);
	if($product_update){
		$this->session->set_userdata('product_data_updated','Product data updated successfully.');
		$edit_url = 'sellar/product_edit/'.$product_id;
		redirect($edit_url);
	} else {
		$this->session->set_userdata('product_data_not_updated','Product data not updated successfully.');
		$edit_url = 'sellar/product_edit/'.$product_id;
		redirect($edit_url);
	}
}
function ajax_subcat_list(){
	$cat_id = $this->input->post('cat_id');
	$data['sub_cat_list'] = $this->Sellar_model->ajax_subcat_list($cat_id);
	$this->load->view('sellar/ajax/subcat_list', $data);
}
function ajax_brands_list(){
	$sub_cat_id = $this->input->post('sub_cat_id');
	$data['ajax_brands_list'] = $this->Sellar_model->ajax_brands_list($sub_cat_id);
	$this->load->view('sellar/ajax/brands_list', $data);
}
function product_view(){
	$product_id = $this->uri->segment('3');
	$data['product_details'] = $this->Sellar_model->product_details($product_id);
	$data['product_colors'] = $this->Sellar_model->product_colors($product_id);
	$data['product_brands'] = $this->Sellar_model->product_brands($product_id);
	if(count($data['product_details']) > 0){
		$this->load->view('sellar/product_view', $data);
	} else {
		redirect('sellar/product');
	}
}
function product_edit(){
	$product_id = $this->uri->segment('3');
	$data['product_details'] = $this->Sellar_model->product_details($product_id);
	if(count($data['product_details']) > 0){
		$cat_id = $data['product_details']['0']['cat_id'];
		$data['sub_cat_list'] = $this->Sellar_model->ajax_subcat_list($cat_id);
		$sub_cat_id = $data['product_details']['0']['prod_sub_cat_sno'];
		$data['subcat_brands_list'] = $this->Sellar_model->brands_list_subcat($sub_cat_id);
		$data['colors_list'] = $this->Sellar_model->colors_dd();
		$data['category_dropdown'] = $this->Sellar_model->category_dropdown();
		$data['subcategory_dropdown'] = $this->Sellar_model->subcategory_dropdown();
		$data['product_colors'] = $this->Sellar_model->product_colors($product_id);
		$data['product_brands'] = $this->Sellar_model->product_brands($product_id);
		$this->load->view('sellar/product_edit',$data);
	} else {
		redirect('sellar/product');
	}
}
function product_features(){
	$data['product_ID'] = $this->uri->segment('3');
	if($data['product_ID']){
		$data['pro_features'] = $this->Sellar_model->product_features($data['product_ID']);
		$this->load->view('sellar/product_features',$data);
	} else {
		redirect('sellar/product');
	}
}
function product_feature_add(){
	$data['product_ID'] = $this->uri->segment('3');
	if($data['product_ID']){
		$this->load->view('sellar/product_feature_add', $data);
		} else {
			redirect('sellar/product');
		}
}
function product_feature_insert(){
	$data['feature_field_name'] = $this->input->post('feature_field_name');
	$data['feature_field_des'] = $this->input->post('feature_field_des');
	$data['prod_sno'] = $this->input->post('prod_sno');
	$data['feature_created_date'] = date('Y-m-d H:i:s');
	$data['feature_created_by'] = $this->session->userdata('sellar_id');
	$data['feature_status'] = '1';
	$feature_inserted = $this->Sellar_model->product_feature_insert($data);
	if($feature_inserted){
		$urll= 'sellar/product_features/'.$data['prod_sno'];
		redirect($urll);
	} else {
		$urll= 'sellar/product_feature_add/'.$data['prod_sno'];
		$data0['product_ID'] = $this->uri->segment('3');
		$data0['errors'] = "Data not inserted.";
		$this->load->view('sellar/product_feature_add', $data0);
	}
}
function product_feature_edit(){
	$feature_id = $this->uri->segment('3');
	if($feature_id){
		$data['feature_info'] = $this->Sellar_model->product_feature_edit($feature_id);
		if(count($data['feature_info'])){
			$this->load->view('sellar/product_feature_edit', $data);
		} else {
			redirect('sellar/product');
		}
		} else {
			redirect('sellar/product');
		}
}
function product_feature_update(){
	$data['feature_field_name'] = $this->input->post('feature_field_name');
	$data['feature_field_des'] = $this->input->post('feature_field_des');
	$data['feature_updated_date'] = date('Y-m-d H:i:s');
	$data['feature_updated_by'] = $this->session->userdata('sellar_id');
	$data['feature_status'] = '1';
	$feature_id = $this->input->post('feature_sno');
	$prod_id = $this->input->post('prod_sno');
	$feature_updated = $this->Sellar_model->product_feature_update($data, $feature_id);
	if($feature_updated>0){
		$url = 'sellar/product_features/'.$prod_id;
		redirect($url);
	} else {
		$data['feature_info'] = $this->Sellar_model->product_feature_edit($feature_id);
		if(count($data['feature_info'])){
			$data['errors'] = 'Data not updated.';
			$this->load->view('sellar/product_feature_edit', $data);
		} else {
			redirect('sellar/product');
		}
	}
}
function product_feature_delete(){
	$feature_id = $this->uri->segment('3');
	$prod_id = $this->uri->segment('4');
	$feature_deletes = $this->Sellar_model->product_feature_delete($feature_id);
	$url = 'sellar/product_features/'.$prod_id;
	redirect($url);
}
function product_services(){
	$data['product_ID'] = $this->uri->segment('3');
	if($data['product_ID']){
		$data['pro_services'] = $this->Sellar_model->product_services($data['product_ID']);
		$this->load->view('sellar/product_services',$data);
	} else {
		redirect('sellar/product');
	}
}
function product_service_add(){
	$data['product_ID'] = $this->uri->segment('3');
	if($data['product_ID']){
		$this->load->view('sellar/product_service_add', $data);
		} else {
			redirect('sellar/product');
		}
}
function product_service_insert(){
	$prod_sno = $this->input->post('prod_sno');
	$Services = $this->input->post('service');
	$p=-1;
	foreach($Services as $Service) { $p++;
		$product_Services[$p] = array(
		    'prod_sno' => $prod_sno,
		    'service' => $Service,
		    'created_date' =>date('Y-m-d H:i:s'),
		    'created_by' => $this->session->userdata('sellar_id')
		);
	}
	$product_service_id = $this->Sellar_model->product_services_insert($product_Services);
	if($product_service_id>0){
		$urll = 'sellar/product_services/'.$prod_sno;
		redirect($urll);
	} else {
		$data['product_ID'] = $prod_sno;
		$this->load->view('sellar/product_service_add', $data);
	}
}
function product_service_delete(){
	$service_id = $this->uri->segment('3');
	$prod_id = $this->uri->segment('4');
	$service_deletes = $this->Sellar_model->product_service_delete($service_id);
	$url = 'sellar/product_services/'.$prod_id;
	redirect($url);
}
function product_images(){
	$data['product_ID'] = $this->uri->segment('3');
	$productDetails = $this->Sellar_model->get_product_details($data['product_ID']);
	$productexist = count($productDetails);
	if($productexist>0){
		$data['pro_images'] = $this->Sellar_model->product_images($data['product_ID']);
		$this->load->view('sellar/product_images',$data);
	} else {
		redirect('sellar/product');
	}
}
function product_images_add(){
	$data['product_ID'] = $this->uri->segment('3');
	if($data['product_ID']){
		$this->load->view('sellar/product_images_add', $data);
		} else {
			redirect('sellar/product');
		}
}
function product_images_insert(){
	$product_id = $this->input->post('prod_sno');
	$product_details = $this->Sellar_model->get_product_details($product_id);
	if(count($product_details) > 0){
		$except = array(' ','(',')','\\', '/', ':', '*', '?', '"', '<', '>', '|');
		$sub_cat_name = str_replace($except, "_", strtolower($product_details->sub_cat_name));
		$product_name = str_replace($except, "_", strtolower($product_details->prod_name));
		$url0 = 'assets/images/gallery/'.$sub_cat_name.'/'.$product_name;
		if(!file_exists($url0))
		{
			mkdir($url0,0777,TRUE); // mkdir($path,$mode,$recursive,$context);
		}
		$count = count($_FILES['prod_images']['name']);
		$galleryAry = array();
		date_default_timezone_set("Asia/Calcutta");
		for($i=0; $i<$count; $i++)
		{
			$fname=$_FILES['prod_images']['name'][$i];
			$tname=$_FILES['prod_images']['tmp_name'][$i];
			$type=$_FILES['prod_images']['type'][$i];			
			$x = time();
			if($type=="image/jpeg" || $type=="image/png" || $type=="image/gif")
			{
				$num = $i+1;
				$galleryAry[$i] = array();
				$onlytype = str_replace("image/", '', $type);
				$fname = $product_name."_".$num.".".$onlytype;
				move_uploaded_file($tname, $url0."/".$fname);
				$galleryAry[$i]['product_img_name'] = $fname;
				$galleryAry[$i]['prod_sno'] = $product_id;
				$galleryAry[$i]['product_img_created_by'] = $this->session->userdata('sellar_id');
				$galleryAry[$i]['product_img_created_date'] = date('Y-m-d H:i:s');
			}
		}
		$prod_images_inserted = $this->Sellar_model->product_images_insert($galleryAry);
		if($prod_images_inserted > 0){
			$urll = 'sellar/product_images/'.$product_id;
			redirect($urll);
		} else {
			$data['product_ID'] =  $product_id;
			$data['errors'] = 'Data not inserted.';
			$this->load->view('sellar/product_images_add', $data);
		}

	}
}
function product_images_addtag(){
	$product_id = $this->uri->segment('3');
	$image_id = $this->uri->segment('4');

	$productexist = count($this->Sellar_model->get_product_details($product_id));
	$imageexist = count($this->Sellar_model->product_image_exist_or_not($image_id));
	if($productexist > 0){
		if($imageexist > 0){
			$data['product_ID'] = $product_id;
			$data['image_ID'] = $image_id;
			$this->load->view('sellar/product_images_addtag',$data);
		} else{
			$urll = 'sellar/product_images/'.$product_id;
			redirect($urll);
		}
	} else {
		redirect('sellar/product');
	}
}
function product_images_addtag_insert(){
	$data['product_img_alt_tag'] = $this->input->post('product_img_alt_tag');
	$data['product_img_updated_date'] = date('Y-m-d H:i:s');
	$data['product_img_updated_by'] = $this->session->userdata('sellar_id');
	$product_id = $this->input->post('product_id');
	$image_id = $this->input->post('image_id');
	$tag_inserted = $this->Sellar_model->product_images_addtag_insert($data, $image_id);
	if($tag_inserted > 0){
		$urll = 'sellar/product_images/'.$product_id;
		redirect($urll);
	} else {
		$data['product_ID'] = $product_id;
		$data['image_ID'] = $image_id;
		$this->load->view('sellar/product_images_addtag',$data);
	}
}
function product_images_edittag(){
	$product_id = $this->uri->segment('3');
	$image_id = $this->uri->segment('4');
	$productexist = count($this->Sellar_model->get_product_details($product_id));
	$data['image_info'] = $this->Sellar_model->product_image_exist_or_not($image_id);
	$imageexist = count($data['image_info']);
	if($productexist > 0){
		if($imageexist > 0){
			$data['product_ID'] = $product_id;
			$data['image_ID'] = $image_id;
			$this->load->view('sellar/product_images_edittag',$data);
		} else{
			$urll = 'sellar/product_images/'.$product_id;
			redirect($urll);
		}
	} else {
		redirect('sellar/product');
	}
}
function product_image_delete(){
	$product_id = $this->uri->segment('3');
	$image_id = $this->uri->segment('4');
	$fulldata = $this->Sellar_model->get_image_full_data($product_id, $image_id);
	$rcount = count($fulldata);
	$sub_cat_id = $fulldata->sub_cat_sno;
	if($sub_cat_id > 0){
		$image_name = $fulldata->product_img_name;
		$except = array(' ','(',')','\\', '/', ':', '*', '?', '"', '<', '>', '|');
		$sub_cat_name = str_replace($except, "_", strtolower($fulldata->sub_cat_name));
		$product_name = str_replace($except, "_", strtolower($fulldata->prod_name));
		$url0 = 'assets/images/gallery/'.$sub_cat_name.'/'.$product_name.'/'.$image_name;
		unlink($url0);
		$this->Sellar_model->product_image_delete($image_id);
		$urll = 'sellar/product_images/'.$product_id;
		redirect($urll);
	} else {
		redirect('sellar/product');
	}
}
function product_inactive(){
	$product_id = $this->uri->segment('3');
	$data['prod_status'] = '0';
	$data['prod_updated_date'] = date('Y-m-d H:i:s');
	$data['prod_updated_by'] = $this->session->userdata('sellar_id');
	$deleted = $this->Sellar_model->product_updated($data, $product_id);
	redirect('sellar/product');
}
function product_active(){
	$product_id = $this->uri->segment('3');
	$data['prod_status'] = '1';
	$data['prod_updated_date'] = date('Y-m-d H:i:s');
	$data['prod_updated_by'] = $this->session->userdata('sellar_id');
	$deleted = $this->Sellar_model->product_updated($data, $product_id);
	redirect('sellar/product');
}
function product_delete(){
	$product_id = $this->uri->segment('3');
	$product_details = $this->Sellar_model->product_details($product_id);
	if(count($product_details)){
		$product_details = $product_details[0];
		$prod_name = $product_details['prod_name'];
		$cat_name = $product_details['cat_name'];
		$sub_cat_name = $product_details['sub_cat_name'];
		$cat_id = $product_details['cat_id'];
		$sub_cat_id = $product_details['prod_sub_cat_sno'];
		$product_id = $product_details['prod_sno'];
		$except = array(' ','(',')','\\', '/', ':', '*', '?', '"', '<', '>', '|');
		$prod_name_folder = str_replace($except, "_", strtolower($prod_name));
		$sub_cat_name_folder = str_replace($except, "_", strtolower($sub_cat_name));
		$want_to_remove_path = 'assets/images/gallery/'.$sub_cat_name_folder.'/'.$prod_name_folder;
		$del_brands = $this->Sellar_model->delete_product_brands($product_id);
		$del_colors = $this->Sellar_model->delete_product_colors($product_id);
		$del_images = $this->Sellar_model->delete_product_images($product_id);
		$del_service = $this->Sellar_model->delete_product_service($product_id);
		$del_product = $this->Sellar_model->delete_product($product_id);
		rmdir($want_to_remove_path);
		redirect('sellar/product');
	}
}
/*==========    PRODUCTS SECTION END    =========*/
/*==========   SUBSCRIBERS SECTION START    =========*/
function subscribers(){
	$data['subscribers'] = $this->Sellar_model->subscribers();
	$this->load->view('sellar/subscribers', $data);
}
function subscriber_inactive(){
	$subs_id = $this->uri->segment('3');
	$data['status'] = '0';
	$data['updated_date'] = date('Y-m-d H:i:s');
	$this->Sellar_model->subscriber_update($data,$subs_id);
	redirect('sellar/subscribers');
}
function subscriber_active(){
	$subs_id = $this->uri->segment('3');
	$data['status'] = '1';
	$data['updated_date'] = date('Y-m-d H:i:s');
	$this->Sellar_model->subscriber_update($data,$subs_id);
	redirect('sellar/subscribers');
}
function subscriber_delete(){
	$subs_id = $this->uri->segment('3');
	$this->Sellar_model->subscriber_delete($subs_id);
	redirect('sellar/subscribers');
}
/*==========    SUBSCRIBERS SECTION END     =========*/
/*==========   ENQUIRIES SECTION START    =========*/
function enquiries(){
	$data['enquiries'] = $this->Sellar_model->enquiries();
	$this->load->view('sellar/enquiries', $data);
}
function enquiry_add(){
	$this->load->view('sellar/enquiry_add');
}
function enquiry_insert(){
	$data['name'] = trim($this->input->post('name'));
	$data['email'] = trim($this->input->post('email'));
	$data['phone'] = trim($this->input->post('phone'));
	$data['description'] = trim($this->input->post('description'));
	$data['status'] = trim($this->input->post('status'));
	$data['created_date'] = date('Y-m-d H:i:s');
	$inserted = $this->Sellar_model->enquiry_insert($data);
	if($inserted > 0){
		redirect('sellar/enquiries');
	} else {
		$data['errors'] = 'Data not inserted.';
		$this->load->view('sellar/enquiry_add', $data);
	}
}
function enquiry_status_change(){
	$enq_id = $this->uri->segment('3');
	$data['status'] = $this->uri->segment('4');
	$updated = $this->Sellar_model->enquiry_update($data, $enq_id);
	redirect('sellar/enquiries');
}
function enquiry_delete(){
	$enq_id = $this->uri->segment('3');
	$this->Sellar_model->enquiry_delete($enq_id);
	redirect('sellar/enquiries');
}
/*==========    ENQUIRIES SECTION END     =========*/
/*==========    REVIEWS SECTION START     =========*/
function reviews(){
	$data['reviews'] = $this->Sellar_model->reviews();
	$this->load->view('sellar/reviews', $data);
}
function review_active(){
	$rev_id = $this->uri->segment('3');
	$data['status'] = '1';
	$this->Sellar_model->review_update($data, $rev_id);
	redirect('sellar/reviews');
}
function review_reject(){
	$rev_id = $this->uri->segment('3');
	$data['status'] = '2';
	$this->Sellar_model->review_update($data, $rev_id);
	redirect('sellar/reviews');
}
function review_delete(){
	$rev_id = $this->uri->segment('3');
	$this->Sellar_model->review_delete($rev_id);
	redirect('sellar/reviews');
}
/*==========    REVIEWS SECTION END      =========*/
/*==========    BANNERS SECTION START    =========*/
function banners(){
	$data['banners_info'] = $this->Sellar_model->banners();
	$this->load->view('sellar/banners', $data);
}
function banners_add(){
	$this->load->view('sellar/banners_add');
}
function banner_insert(){
	$brandname = $this->input->post('banner_title');
	$data['banner_title'] = $brandname;
	$fileame = '';
	if(!file_exists('assets/images/banners'))
	{
		mkdir('assets/images/banners');
	}
	if($_FILES['banner_image']['name']){
		$brandimgType = $_FILES['banner_image']['type'];
		$brandimgName = $_FILES['banner_image']['name'];
		$brandimgTmp = $_FILES['banner_image']['tmp_name'];
		$brandname = str_replace(' ', '_', strtolower(trim($brandname)));
		$x = time();
		$fileame = $x."_".$brandname."_".$brandimgName;
		list($width, $height, $type, $attr) = getimagesize($brandimgTmp);
		$path = "assets/images/banners/$fileame";
		if(move_uploaded_file($brandimgTmp,$path)){
			$data['banner_image'] = $fileame;
		}
	}
	if(isset($_POST['banner_add'])){
		$data['created_date'] = date('Y-m-d H:i:s');
		$data['created_by'] = $this->session->userdata('sellar_id');
		$data['status'] = '1';
		$banner_inserted = $this->Sellar_model->banners_insert($data);
		if($banner_inserted){
			redirect('sellar/banners');
		} else {
			$error['error'] = "Data not inserted."; 
			$this->load->view('sellar/banners_add', $error);

		}
	}
	/*
	if(isset($_POST['brand_update'])){
		$data['brand_updated_date'] = date('Y-m-d H:i:s');
		$data['brand_updated_by'] = $this->session->userdata('sellar_id');
		//$data['brand_status'] = $this->input->post('brand_status');
		$old_img = $this->input->post('old_brand_image');
		if($fileame != ''){
			if($old_img){
				$file = "assets/images/brands/".$old_img;
				unlink($file);
			}
		} else {
			$data['brand_img']= $old_img;
		}
		$brand_id = $this->input->post('brand_sno');
		$brand_updated = $this->Sellar_model->brands_update($data, $brand_id);
		if($brand_updated>0){
			redirect('sellar/brands');
		} else {
			redirect("sellar/brand_edit/$brand_id");
		}
	}
	*/
}
function banner_tag_add(){
	$banner_sno = $this->uri->segment('3');
	$data['banner_info'] = $this->Sellar_model->banner_image_info($banner_sno);
	if(count($data['banner_info'])){
		$this->load->view('sellar/banner_tag_add',$data);
	} else {
		redirect('sellar/banners');
	}
}
function banner_tag_insert(){
	$banner_sno = $this->input->post('banner_sno');
	$data['alt_tag'] = $this->input->post('alt_tag');
	$data['status'] = '1';
	$data['updated_date'] = date('Y-m-d H:i:s');
	$this->Sellar_model->banner_tag_insert($data, $banner_sno);
	redirect('sellar/banners');
}
function banner_tag_edit(){
	$banner_sno = $this->uri->segment('3');
	$data['banner_info'] = $this->Sellar_model->banner_image_info($banner_sno);
	if(count($data['banner_info'])){
		$this->load->view('sellar/banner_tag_edit',$data);
	} else {
		redirect('sellar/banners');
	}
}
function banner_inactive(){
	$banner_sno = $this->uri->segment('3');
	$data['status'] = '0';
	$data['updated_date'] = date('Y-m-d H:i:s');
	$data['updated_by'] = $this->session->userdata('sellar_id');
	$updated = $this->Sellar_model->banner_tag_insert($data, $banner_sno);
	redirect('sellar/banners');
}
function banner_active(){
	$banner_sno = $this->uri->segment('3');
	$data['status'] = '1';
	$data['updated_date'] = date('Y-m-d H:i:s');
	$data['updated_by'] = $this->session->userdata('sellar_id');
	$updated = $this->Sellar_model->banner_tag_insert($data, $banner_sno);
	redirect('sellar/banners');
}
function banner_delete(){
	$banner_sno = $this->uri->segment('3');
	$banner_info = $this->Sellar_model->banner_image_info($banner_sno);
	if(count($banner_info)){
		echo '<pre>';
		print_r($banner_info);
		$image_name = $banner_info['0']['banner_image'];
		$filename = "assets/images/banners/".$image_name;
		if(file_exists($filename)) {
		    unlink($filename);
		}
		$this->Sellar_model->banner_tag_deleted($banner_sno);
		redirect('sellar/banners');
	}
}
/*==========    BANNERS SECTION END     =========*/
function product_type(){
	
	$type_make = $this->uri->segment('3');
	$pro_id = $this->uri->segment('4');
	if($type_make == 'make_dd'){
		$data['type'] = 'DD';
	}
	if($type_make == 'make_np'){
		$data['type'] = 'NP';
	}
	if($type_make == 'make_bs'){
		$data['type'] = 'BS';
	}
	if($type_make == 'delete_dd' || $type_make == 'delete_np' || $type_make == 'delete_bs'){
		$data['type'] = '';
	}
	$data['prod_updated_date'] = date('Y-m-d H:i:s');
	$data['prod_updated_by'] = $this->session->userdata('sellar_id');
	$this->Sellar_model->product_type($data,$pro_id);
	redirect('sellar/product');
}
public function orders(){
	
	 $get_sellar_id = $this->session->userdata("sellar_id");
	 
	$data['sum_order_price']=$this->Sellar_model->sum_order_price($get_sellar_id);
	$data['catgoriesData']=$this->Sellar_model->category();
	$data['orders'] = $this->Sellar_model->orders($get_sellar_id);
	$this->load->view('sellar/orders',$data);

}
public function employee_details_edit(){
	
        $data['user_data'] = $this->Sellar_model->employee_details_view();
        $this->load->view('sellar/user-edit', $data);
}
public function employee_details(){
		
		$data['user_data'] = $this->Sellar_model->employee_details();
		$this->load->view("sellar/employees_details",$data);	
		
}
public function employee_details_view(){
	
        $data['user_data'] = $this->Sellar_model->employee_details_view();
        $this->load->view('sellar/agent_emp_details_view', $data);
}
public function employee_add(){
		
		
		$this->load->view("sellar/user-add");	
		
}
public function user_insert()
	{
		
		
		$data['admin_name'] = strtolower($this->input->post('name'));
		//$data['emp_designation'] = $this->input->post('designation');
		$data['sellar_id'] = $this->input->post('email');
		$data['admin_phone'] = $this->input->post('phone_no');
		$checkStaffData = $this->Sellar_model->checkStaffData();
		
		if($checkStaffData==0){
			
		$data['admin_city'] = strtolower($this->input->post('admin_city'));
		$data['date_of_joining'] = date('Y-m-d',strtotime($this->input->post('date_of_joining'))); 
		$data['admin_type'] = strtolower($this->input->post('admin_type'));
		
		if($this->input->post('admin_type')=='Delivery Boy'){
			
			$data['admin_type_id'] = 4;
		}
		$data['father_name'] = strtolower($this->input->post('father_name'));
		$data['blood_group'] = strtolower($this->input->post('blood_group'));
		$data['gender'] = strtolower($this->input->post('gender'));
		$data['dob'] = date('Y-m-d',strtolower($this->input->post('dob')));
		$data['personal_phone'] = $this->input->post('personal_phone');
		$data['address'] = strtolower($this->input->post('address'));
		$data['admin_password'] = 'codexbox@123';
		
		$config['upload_path'] = 'assets/images/emp_pic/';
		$config['allowed_types'] = '*';
		//$config['max_size']         = '900000';
		$config['encrypt_name']     = true;
		$this->load->library('upload', $config);
		$x = time();
		//for NEW SMALL image
		$this->upload->do_upload('admin_img');
		if ( $_FILES['admin_img']['name']!='')
		{
			$file_data = $this->upload->data();	
			$fnameimage=$_FILES['admin_img']['name'];
			$tnameimage=$_FILES['admin_img']['tmp_name'];
			$typeimage=$_FILES['admin_img']['type'];
			
			if($typeimage=="image/jpeg" || $typeimage=="image/png" || $typeimage=="image/gif")
			{
				move_uploaded_file($tnameimage,"assets/images/emp_pic/".$x."_emp_".$fnameimage);
				$data['admin_img'] = $x."_emp_".$fnameimage;
			}
		}
		else
		{
			$data['admin_img'] = "emp_img.png";
		}
		
		
			$status = $this->Sellar_model->user_insert($data);
			redirect('sellar/employee_details');
		
		}
		else{
			$this->session->set_flashdata('user_name', 'Email Id or phone no already Exits');
			//$this->session->set_flashdata('user_name', 'Email Id or phone no already Exits');
			$this->load->view("sellar/user-add");
		}
		
		
	}
public function emp_agent_update()
   {
       
       $AdminNo = $this->input->post('Admin_No');
       $data['admin_name'] = strtolower($this->input->post('name'));
		//$data['emp_designation'] = $this->input->post('designation');
		$data['sellar_id'] = $this->input->post('email');
		$data['admin_phone'] = $this->input->post('phone_no');
		$data['admin_city'] = strtolower($this->input->post('admin_city'));
		
		$data['date_of_joining'] = date('Y-m-d',strtotime($this->input->post('date_of_joining'))); 
		$data['admin_type'] = strtolower($this->input->post('admin_type'));
		$data['father_name'] = strtolower($this->input->post('father_name'));
		$data['blood_group'] = strtolower($this->input->post('blood_group'));
		$data['gender'] = strtolower($this->input->post('gender'));
		$data['dob'] = date('Y-m-d',strtotime($this->input->post('dob')));
		$data['personal_phone'] = $this->input->post('personal_phone');
		$data['address'] = strtolower($this->input->post('address'));
		
	   
		
		$config['upload_path'] = 'assets/images/emp_pic/';
		$config['allowed_types'] = '*';
		$config['max_size']         = '900000';
		$config['encrypt_name']     = true;
		$this->load->library('upload', $config);

		if ( $_FILES['admin_img']['name']=='')
		{
		  $data['admin_img'] = $this->input->post('old_img');
		  //echo $_FILES['admin_img']['name']; echo '000';
		  //exit();
       }
       else
       {
           $x = time();
		   //for NEW SMALL image
		   $this->upload->do_upload('admin_img');
           $file_data = $this->upload->data();    
           $fnameimage=$_FILES['admin_img']['name'];
           $tnameimage=$_FILES['admin_img']['tmp_name'];
           $typeimage=$_FILES['admin_img']['type'];
           if($typeimage=="image/jpeg" || $typeimage=="image/png" || $typeimage=="image/gif")
           {
               move_uploaded_file($tnameimage,"assets/images/emp_pic/".$x."_emp_".$fnameimage);
               $data['admin_img'] = $x."_emp_".$fnameimage;
           }
       }
	   if(strtolower($this->input->post('admin_type'))=="agent")
		{
		  $status = $this->Sellar_model->agent_update($data, $AdminNo);
		  redirect('sellar/agents_details');
		}
		else{
			$status = $this->Sellar_model->employee_update($data, $AdminNo);
			redirect('sellar/employee_details');
		}
     
       
       
   }
   public function employee_details_delete()
   {
	   $data['admin_status']=$this->input->post('change_status');
	   $AdminNo=$this->input->post('id');
	   $data['update_date']=date("Y-m-d H:i:s");
	   $data['updated_by']=$this->session->userdata('sellar_id');
	   $status = $this->Sellar_model->employee_update($data, $AdminNo);
	    redirect('sellar/employee_details');
   }
   public function emp_agent_update_status()
   {
	   $data['admin_status']=$this->input->post('change_status');
	   $AdminNo=$this->input->post('id');
	   $data['update_date']=date("Y-m-d H:i:s");
	   $data['updated_by']=$this->session->userdata('sellar_id');
	   $status = $this->Sellar_model->agent_update($data, $AdminNo);
	    redirect('sellar/agents_details');
   }
public function accountant_orders(){
	
	
	$data['sum_order_price']=$this->Sellar_model->sum_order_price();
	$data['catgoriesData']=$this->Sellar_model->category();
	$data['orders'] = $this->Sellar_model->orders();
	$this->load->view('sellar/orders',$data);
}
    public function order_info(){
     
    $data['order_info'] = $this->Sellar_model->order_info();
    if (count($data['order_info']) > 0) {
    $this->load->view('sellar/order_view', $data);
    } else {
    redirect('sellar/orders');
    }
    }
public function order_update(){
	
	$orderid = $this->input->post('order_id');
	$data['order_status'] = $this->input->post('order_status');
	$data['shipping_status'] = $this->input->post('shipping_status');
	$data['payment_type'] = $this->input->post('payment_type');
	$data['payment_type'] = date("Y-m-d",strtotime($this->input->post('delivery_date')));
	$data['shipping_date'] = $this->input->post('delivery_id');
	$data['order_updated'] = date('Y-m-d H:i:s',time());
	$updated = $this->Sellar_model->order_update($orderid, $data);
	redirect("sellar/order_info/$orderid");
}
/*==========    BANNERS SECTION END     =========*/

public function users(){
	
	$data['users'] = $this->Sellar_model->users();
	$this->load->view('sellar/users', $data);
}
public function users_view(){
	
	$userid = $this->uri->segment('3');
	$data['user_info'] = $this->Sellar_model->users_view($userid);
	$this->load->view('sellar/users_view', $data);
}

public function account_orders(){
		
        $data['catgoriesData']=$this->Sellar_model->get_catgories();
		$data['get_orders']=$this->Sellar_model->get_orders();
		$data['sum_order_price']=$this->Sellar_model->sum_order_price();
	
		$this->load->view("sellar/orders",$data);	
		
}

}