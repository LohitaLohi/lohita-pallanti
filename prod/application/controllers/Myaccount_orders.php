<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Myaccount_orders extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
public function __Construct(){
        parent::__Construct();
		
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('html');
		
       error_reporting(0);
	   $this->load->library('form_validation');
	   $this->load->library('facebook');
		
		
   }

   
   public function prod_review(){
    
 $user_id = $this->session->userdata('userId');
   
    if($this->input->post()){
      $get_prod_id = $this->input->post('prod_id');
      $comments = $this->input->post('comments');   
      $rating = $this->input->post('rating');  
      
       $data['prod_id'] = $get_prod_id;
       
       $data['comments'] = $comments;
       
       $data['rating'] = $rating;
       
        $data['user_id'] = $user_id;
        
        
        if($this->db->insert('user_reviews',$data)){
            $this->session->set_flashdata('user_review', 'Your Review is Submitted Successfully');
            redirect('Myaccount_orders');
            }
        }
   
      }




public function index(){
		
		if(empty($this->session->userdata('userId'))){
		    
			
		    redirect('home/index');
		}
		else{
			$data['products']=$this->Detailed_model->get_ord_details();
			
			$this->load->view('myaccount-orders',$data);
		}
}

}
