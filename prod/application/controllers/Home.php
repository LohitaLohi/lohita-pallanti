<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
public function __Construct(){
      parent::__Construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('html');
   $this->load->database();
 error_reporting(0);
 $this->load->library('email');
 $this->load->library('session');
		
       
	   $this->load->library('form_validation');
	   $this->load->library('facebook');
		 
	error_reporting(0);	
}
public function index(){
		
		
		/*$data['myaccountData']=$this->Home_model->get_user_details();
		$data['products']=$this->Home_model->products();	*/	
		$this->load->view('index-home');
}
public function login(){
     
     
       
    
    
    
		if($this->session->userdata('userId')){
		redirect("home/myaccount");
		}
		else{
		
		$clientId = '1058966338695-0340gem5u3p9tt3qp5e7014jn9h38l2s.apps.googleusercontent.com';
        $clientSecret = '5Qc_6yoUk4Kb85iiNmRSAqv3';
        $redirectUrl = base_url()."home/google_login";
		$data['login_url1'] =  $this->facebook->login_url();
		$gClient = new Google_Client();
        $gClient->setApplicationName('Login in to Bostonbooks');
        $gClient->setClientId($clientId);
        $gClient->setClientSecret($clientSecret);
        $gClient->setRedirectUri($redirectUrl);
        $google_oauthV2 = new Google_Oauth2Service($gClient);

        if (isset($_REQUEST['code'])) {
            $gClient->authenticate();
            $this->session->set_userdata('token', $gClient->getAccessToken());
            redirect($redirectUrl);
        }

        $token = $this->session->userdata('token');
        if (!empty($token)) {
            $gClient->setAccessToken($token);
        }
		 $data['google_login_url'] = $gClient->createAuthUrl();
		 $this->load->view('login',$data);
		}
}
function google_login(){	
	
		$clientId = '1058966338695-0340gem5u3p9tt3qp5e7014jn9h38l2s.apps.googleusercontent.com';
        $clientSecret = '5Qc_6yoUk4Kb85iiNmRSAqv3';
        $redirectUrl = base_url()."home/google_login";
		$data['login_url1'] =  $this->facebook->login_url();
		$gClient = new Google_Client();
        $gClient->setApplicationName('Login in to Bostonbooks');
        $gClient->setClientId($clientId);
        $gClient->setClientSecret($clientSecret);
        $gClient->setRedirectUri($redirectUrl);
        $google_oauthV2 = new Google_Oauth2Service($gClient);
		$date =date('Y-m-d H:i:s');
		if (isset($_REQUEST['code'])) {
            $gClient->authenticate();
            $this->session->set_userdata('token', $gClient->getAccessToken());
            redirect($redirectUrl);
        }
        $token = $this->session->userdata('token');
        if (!empty($token)) {
            $gClient->setAccessToken($token);
        }
        if ($gClient->getAccessToken()) {
            $userProfile = $google_oauthV2->userinfo->get();
            // Preparing data for database insertion
			$userData['oauth_provider'] = 'google';
			$userData['oauth_uid'] = $userProfile['id'];
            $userData['first_name'] = $userProfile['given_name'];
            $userData['last_name'] = $userProfile['family_name'];
            $userData['email'] = $userProfile['email'];
			$userData['gender'] = $userProfile['gender'];
			$userData['locale'] = $userProfile['locale'];
            $userData['gp_profile_url'] = $userProfile['link'];	
            $userData['gp_picture_url'] = $userProfile['picture'];
			// Insert or update user data
			$this->session->set_userdata('use_email',$userProfile['email']);
			$this->session->set_userdata('user_logged_in',$userProfile['email']);
            $a = $this->Home_model->fb_checking();
			
           if($a==true)
			{
                    $userData['modified'] =$date;
					$userId=$this->Home_model->user_data_update($userData);
					$this->session->set_userdata('userId',$userData['email']);
					$this->session->set_userdata('userName',$userData['first_name']);
					$this->session->set_userdata('userSno',$userId->id);
					$this->session->set_userdata('userPhoneNo',$userId->phone_number);
					$this->session->set_userdata("gmail","gmil");	 
					redirect("myaccount");
			} 	
			else{
					$userData['created'] =$date;
					$userId=$this->Home_model->add_user($userData);
					$this->session->set_userdata('userSno',$userId);
					
					$this->session->set_userdata('userId',$userData['email']);
					$this->session->set_userdata('userName',$userData['first_name']);
					$this->session->set_userdata("gmail","gmil");
					redirect("myaccount");
			}
        } else {
            $data['authUrl'] = $gClient->createAuthUrl();
        }
		
}
public function signup(){
	
		if($this->session->userdata('userId')){
		redirect("home/myaccount");
		}
		else{
		
		$this->load->view('register');
		}
}
public function signup_add(){
	
		$this->session->unset_userdata('login_fail');
		//echo $this->input->post("user_name");
		$userName=$this->input->post('user_name');
		$userLastName=$this->input->post('last_name');
		$userEmail=$this->input->post('user_email');
		$userPhoneNo=$this->input->post('user_number');
		$userPwd=$this->input->post('user_password');
		$date =date('Y-m-d H:i:s');
		$userData['first_name']=$userName;
		$userData['last_name']=$userLastName;
		$userData['email']=$userEmail;
		$userData['phone_number']=$userPhoneNo;
		$userData['password']=$userPwd;
		$userData['created']=$date;
		$this->session->set_userdata("userPhoneNo",$this->input->post('user_number'));
		$this->session->set_userdata('email',$userEmail);
		$this->session->set_userdata('userName',$userData['first_name']);
		$this->session->set_userdata('last_name',$userLastName);
		
		$this->form_validation->set_rules('user_email', 'user_email', 'is_unique[users.email]');
		$this->form_validation->set_rules('user_number', 'user_number', 'is_unique[users.phone_number]');
		
		if($this->form_validation->run()== TRUE){
			
				  $userId=$this->Home_model->add_user($userData);
				  $this->session->set_userdata('userSno',$userId);
				  $this->session->set_userdata("normal","normal");
				  $user= $this->smsgateway['user'];
				  $pw=$this->smsgateway['pw'];
				  $api_id=$this->smsgateway['api_id']; 
				  $senderId=$this->smsgateway['sneder_id'];
                //here generate random value 
                 $headers = "MIME-Version: 1.0\r\n";
                 $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
			     $otp = rand('1000','9999');
			     $this->session->set_userdata("OtpForSignUp", $otp);
			    //$enqName=$this->session->userdata('EnqName');
                $text = urlencode("Hi $userName, $otp is your One Time Password for bostonbooks.com. Please use this password to complete your phone number verification.");
               // auth call
                $url = "http://tra.bulksmshyderabad.co.in/websms/sendsms.aspx?userid=$user&password=$pw&sender=$senderId&mobileno=$userPhoneNo&msg=$text";
                if($url){
                $ret = file($url);
              // explode our response. return string is on first line of the data returned
                $sess = explode(":",$ret[0]);
               if ($sess[0] == "OK") {
 
				$sess_id = trim($sess[1]); // remove any whitespace
				$url = "$baseurl/http/sendmsg?session_id=$sess_id&to=$to&text=$text";

               // do sendmsg call
               $ret = file($url);
             $send = explode(":",$ret[0]);
                }

               	}
				redirect("user_otp_send");
		}
		else{
				$myaccountData=$this->Home_model->get_check_user_details();
				if($myaccountData[0]['otp_status']==1)
				{
				   $this->session->set_flashdata("user_exits","Email or Phone already Exists");
				   redirect("signup");
				}
				else{
					
				  $this->session->set_userdata('userSno',$userId);
				  $this->session->set_userdata("normal","normal");
				  $this->session->set_userdata("userPhoneNo2",$userPhoneNo);
				  $this->session->set_userdata('email',$userData['email']);
				  $this->session->set_userdata('userName',$userData['first_name']);
				  $user= $this->smsgateway['user'];
				  $pw=$this->smsgateway['pw'];
				  $api_id=$this->smsgateway['api_id']; 
				  $senderId=$this->smsgateway['sneder_id'];
				//here generate random value 
				 $headers = "MIME-Version: 1.0\r\n";
				 $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
				 $otp = rand('1000','9999');
				 $this->session->set_userdata("OtpForSignUp", $otp);
				//$enqName=$this->session->userdata('EnqName');
				$text = urlencode("Hi $userName, $otp is your One Time Password for bostonbooks.com. Please use this password to complete your phone number verification.");
			   // auth call
				$url = "http://tra.bulksmshyderabad.co.in/websms/sendsms.aspx?userid=$user&password=$pw&sender=$senderId&mobileno=$userPhoneNo&msg=$text";
				if($url){
				$ret = file($url);
			  // explode our response. return string is on first line of the data returned
				$sess = explode(":",$ret[0]);
			   if ($sess[0] == "OK") {

				$sess_id = trim($sess[1]); // remove any whitespace
				$url = "$baseurl/http/sendmsg?session_id=$sess_id&to=$to&text=$text";

			   // do sendmsg call
			   $ret = file($url);
			   $send = explode(":",$ret[0]);
				}

				}
				redirect("user_otp_send");
					
				}
			
		}
		
		
}
public function login_add(){
	
	 $this->session->unset_userdata('login_fail');
	 $userId=$this->input->post('user_email');
	 $userPassword=$this->input->post('user_password');	
	 $login['Checke']=$this->Home_model->login($userId,$userPassword);
	
	 if($login['Checke']->count_email==1){
		 
		 $this->session->set_userdata('userId',$login['Checke']->email);
		 $this->session->set_userdata('userSno',$login['Checke']->id);
		 $this->session->set_userdata('userName',$login['Checke']->first_name);
		 $this->session->set_userdata('userPhoneNo',$login['Checke']->phone_number);
		 $url =$_SERVER['HTTP_REFERER'];
		 redirect("myaccount");
	 }
	 else{
		$this->session->set_userdata('login_fail','Enter Correct Login Details');
        redirect("login");
	 }
	
}
public function user_otp_send(){
	

		
		$data['myaccountData']=$this->Home_model->get_user_details();
	    $this->load->view("user_otp",$data);
}
public function user_otp_payment(){
	
	
		 
		$data['myaccountData']=$this->Home_model->get_user_details();
	    $this->load->view("user_payment_otp",$data);
}
public function user_otp_send_payment(){
	              
			  if($this->input->post("phoneNumber")){
				  
			  $userPhoneNo=$this->input->post("phoneNumber");
			  $user= $this->smsgateway['user'];
			  $pw=$this->smsgateway['pw'];
			  $api_id=$this->smsgateway['api_id']; 
			  $senderId=$this->smsgateway['sneder_id'];
                //here generate random value 
                 $headers = "MIME-Version: 1.0\r\n";
                 $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
			     $otp = rand('1000','9999');
			     $this->session->set_userdata("OtpForSignUp", $otp);
			     $this->session->set_userdata("userPhoneNo2", $userPhoneNo);
			    //$enqName=$this->session->userdata('EnqName');
                 $text = urlencode("Hi $userName, $otp is your One Time Password for bostonbooks.com. Please use this password to complete your phone number verification.");
               // auth call
                 $url = "http://tra.bulksmshyderabad.co.in/websms/sendsms.aspx?userid=$user&password=$pw&sender=$senderId&mobileno=$userPhoneNo&msg=$text";
                 if($url){
                 $ret = file($url);
              // explode our response. return string is on first line of the data returned
                 $sess = explode(":",$ret[0]);
                 if ($sess[0] == "OK") {
 
				 $sess_id = trim($sess[1]); // remove any whitespace
				 $url = "$baseurl/http/sendmsg?session_id=$sess_id&to=$to&text=$text";

               // do sendmsg call
                 $ret = file($url);
                 $send = explode(":",$ret[0]);
                 }
			
   }
}
else{
	redirect("home");
}
}
public function confirmOtpPayment(){
	
	if($this->session->userdata("OtpForSignUp")==$this->input->post('otpConfirm')){
		
		$emailTo=$this->session->userdata('userId');
		$userPhoneNo =$this->session->userdata('userPhoneNo2');
		$otpUpdateSignUp['otp_status']='1';
		$otpUpdateSignUp['phone_number']=$userPhoneNo;
		$userId=$this->Home_model->userOtpConfirm($emailTo,$userPhoneNo,$otpUpdateSignUp);
		if($userId){
			
			$this->session->set_userdata("userPhoneNo",$userPhoneNo);
			redirect("detailed/payment");
		}	
	}
	else{
		
		$this->session->set_userdata('confirmPayment','Enter Correct Otp');
		redirect("user_otp_send");
	}
}
public function confirmOtp(){
	  
if($this->session->userdata("OtpForSignUp")==$this->input->post('otpConfirm'))
   { 

        $this->session->unset_userdata('registeredButNotOtp');
        $this->session->set_userdata('userId',$this->session->userdata('email'));
	   
        $this->session->unset_userdata('otpErrorSignup');
        $emailTo=$this->session->userdata('email');
		$userName=$this->session->userdata('userName');
		$userPhoneNo =$this->session->userdata('userPhoneNo2');
		$otpUpdateSignUp['otp_status']='1';
		//$otpUpdateSignUp['email']=$emailTo;
		//$otpUpdateSignUp['phone_number']=$userPhoneNo;
		//$otpUpdateSignUp['password']=$this->session->userdata('password');
		
		echo  "<script type=\"text/javascript\">
        window.open('http://mahithawebsol.com/wateronline/myaccount', '_self')
          </script>";
		$userId=$this->Home_model->userOtpConfirm($emailTo,$userPhoneNo,$otpUpdateSignUp);
		$userPhoneNo =$this->session->set_userdata('userSno',$userId);
        $msg ='<html xmlns="https://www.w3.org/1999/xhtml">
       <head>
	   <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	   <meta name="viewport" content="width=device-width"> 
	   <title>proppick thank you mail</title>
	
	<style type="text/css" media="screen">
		.h20 a, .h40 a{color : #fff !important;}
		@media(min-width:1px) and (max-width:635px){
		.sri {text-align:center; clear:both; display: block;    width: 100%;       text-align: center !important;}
		td.img.sss a {    width: 100%;     text-align: center !important;}
		a.logo { width : 100%; text-align : center; }
		.h20 a, .h40 a{color : #fff !important;}
		}
		
	</style>
</head>
<body class="body" style="padding:0 !important; margin:0 !important; display:block !important; -webkit-text-size-adjust:none;">
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="background:#f4f4f4; background-position: 0 0; background-repeat: no-repeat repeat-y;font-family:Verdana;">
	<tr>
		<td align="center" valign="top">
			<table style="max-width:660px" border="0" cellspacing="0" cellpadding="0" bgcolor="#E0E0E0" >
				<tr>
					<td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="0" ></td>
					<td>
						<!-- Top -->
						<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#333" style="margin-top:0px; ">
							<tr>
								<td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="15"><div style="font-size:0pt; line-height:0pt; height:27px"><img src="https://www.http://devef.org/boston/.com/images/empty.gif" width="1" height="27" style="height:27px" alt="" /></div>
								</td>
								<td class="img sss" style="font-size:0pt; line-height:0pt; text-align:left">
								<a class="logo" href="https://www.http://devef.org/boston/.com/" target="_blank" style="float:left; text-align:center;  color:#fff;"><img src="https://www.proppick.com/images/logo2.png" width="210" alt="" border="0" style="margin: 5px 15px;" /></a>
								<div class="sri" style="float: right;    margin: 3% 0;    text-align: right;">
									<h2 class="h20" style="color: #fff; font-size: 20px; margin: 10px 0;  padding: 5px 0;"><a style="color:#fff !important;">24/7</a> Support : <a style="color:#fff !important;">86-88-500-500</a> </h2>
									<h4 class="h40" style="color: #fff; font-size: 15px; font-family: Verdana; margin: 10px 0; font-weight: 400; clear: both; padding: 5px 0;"><a style="color:#fff !important; text-decoration:none">Boston Books</a> 360&deg; HOME BUYING SOLUTIONS</h4>
								</div>
								</td>
								<td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="15"></td>
							</tr> 
						</table>
						
						<div style="font-size:0pt; line-height:0pt; height:20px"><img src="https://www.proppick.com/images/empty.gif" width="1" height="20" style="height:20px" alt="" /></div>
						<!-- Featured Image -->
						<table width="100%" border="0" cellspacing="0" cellpadding="0" style="background:#E0E0E0; padding:0 20px 20px ;">
        <tbody style="background:#fff;">
          <tr>
          	            	  
              <td style="padding:15px 15px 0 15px;font-family:Verdana;font-size:13px;color:#2d2d2d">Dear '.$userName.',</td>
            
			  
			  			  </tr>
			  <tr><td height="10"></td></tr>
			  <tr><td style="padding:0 15px;font-family:Verdana;font-size:13px;color:#2d2d2d">Thank you for Interest with bostonbooks.com!</td></tr>
			  <tr>
			  <td style="padding:0 15px;font-family:Verdana;font-size:13px;color:#2d2d2d;">
                            
             Thank You for register  <span style="color:#2791d8"><b><a style="color:#1155cc;text-decoration:none" href="https://www.proppick.com/" target="_blank"></a></b></span>. Our Sales representative will call you from 86-88-500-500 to understand your requirements better.
		              </td>
		              </tr>
		              <tr><td height="10"></td></tr>
		              <tr><td style="padding:0 15px;font-family:Verdana;font-size:13px;color:#2d2d2d;">
					              
             A property advisor from our dedicated team will get in touch with you soon. Our expert will help you review the properties, accompany you for free site visits, ease documentation & home loan and provide after sales customer services.
              
                            
              </td>
          </tr>
          
                    
          <tr>
            <td style="font-family:Verdana;font-size:14px;padding:15px"><b>More from Proppick.com</b></td>
          </tr>
          <tr>
            <td width="100%"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tbody>
                <tr>
                  <td style="padding:0 5px;text-align:center">
                    <table width="350" border="0" cellpadding="0" cellspacing="0" style="display:inline-block">
                      <tbody>
                        <tr>
                          <td bgcolor="#E0E0E0" style="padding:15px"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody><tr>
                              <td width="50%" align="left" valign="top" style="font-family:Verdana;text-align:left;font-size:13px;line-height:20px">Proppick: Your one stop solution for all queries related to Real Estate. Join 10,000+ others now!</td>
                              </tr>
                            <tr>
                              <td align="center" style="font-family:Verdana;font-size:13px;line-height:20px;padding:10px 0px 0px 00px">
                              <a style="color:#1155cc;text-decoration:none;    text-decoration: none;    background: red;    color: #fff;    padding: 8px 20px;    border-radius: 5px;    font-size: 17px;    display: block;    width: 100px;    margin: 0 0 5px 0;" href="https://www.proppick.com/" target="_blank">Subscribe</a>
                              </td>
                              </tr>
                            </tbody><tbody>
                              </tbody>
                            </table></td>
                          </tr>
                        </tbody>
                      </table></td>
                  </tr>
                </tbody>
              </table></td>
          </tr>
          <tr>
            <td style="padding:15px;font-family:Verdana;font-size:13px;line-height:26px">
            
                          	We thank you for your interest.<br>
							Your Trusted Partner, <br>
							<a href="https://www.proppick.com" target="_blank">www.proppick.com</a> <br>
							Feedback : <a href="mailto:customer.service@proppick.com" target="_blank">customer.service@proppick.com</a>
							</td>
							
          </tr>
        </tbody>
      </table>
      <td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="0"></td>
				</tr>
                </tbody>
			</table>	
            	<!-- END Featured Image -->
                <table style="max-width:660px" border="0" cellspacing="0" cellpadding="0" bgcolor="#E0E0E0">
                <tbody>
                <tr>
                <td>
				
						<!--<div class="img" style="font-size:0pt; line-height:0pt; text-align:left"><img src="images/footer_top.jpg" alt="" border="0" width="620" height="3" /></div>-->
						<table width="94%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" style="margin:0 auto;">
							<tr>
								<td>
									<div style="font-size:0pt; line-height:0pt; height:12px"><img src="https://www.proppick.com/images/empty.gif" width="1" height="12" style="height:12px" alt="" /></div>
				<table style="width:50%; max-width:800px; margin-left:20px; float:left;">
                        <tbody>	
                        
                            <tr>
                                <td style=" float:left;"><h5 style=" margin: 16px 0 0 0; font-family: Verdana;">ABOUT PROPPICK.COM</h5></td></tr></tbody></table>
                                
                                <table>
                                	<tbody style="width:50%;float:right;">
                                          <tr style="float: right;margin: 10px 0px 0px 130px;">
                                    <td class="img-right" style="font-size:0pt; line-height:0pt; text-align:right" width="30"><a href="https://www.facebook.com/Proppick/1631399207074329?skip_nax_wizard=trueref_type=logout_gear" target="_blank"><img src="https://www.proppick.com/images/facebook.png" alt="" border="0" width="25" height="25" /></a></td>
                                    <td class="img-right" style="font-size:0pt; line-height:0pt; text-align:right" width="30"><a href="https://twitter.com/proppick1" target="_blank"><img src="https://www.proppick.com/images/twiter.png" alt="" border="0" width="25" height="25" /></a></td>
                                    
                                    <td class="img-right" style="font-size:0pt; line-height:0pt; text-align:right" width="30"><a href="https://www.linkedin.com/company/proppick" target="_blank"><img src="https://www.proppick.com/images/linkedin.png" alt="" border="0" width="25" height="25" /></a></td>
                                    <td class="img-right" style="font-size:0pt; line-height:0pt; text-align:right" width="30"><a href="https://plus.google.com/u/1/b/102224017327951057027/102224017327951057027/about" target="_blank"><img src="https://www.proppick.com/images/google.png" alt="" border="0" width="25" height="25" /></a></td>
                                </tr>
                                
                                
                                    </tbody>
                                </table>
                        </td></tr></table>
                       
				 
                <table width="94%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" style="margin:auto;margin-bottom:20px;">
                	<tbody>
                    	<tr>
                        <td style="padding: 0 20px 20px 20px;    font-family: Verdana;    font-size: 13px;    color: #2d2d2d;"> Proppick.com is a Real-Estate Web based Property portal created, integrated and maintained by a dedicated team with 15+ years experience in Real estate. Proppick.com aspires to assist home purchasers from the day they initiated buying journey to till the time they reside in their dream home. </td>
                        </tr>
                    </tbody>
                </table>
  
									
						<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#362921">
                        
							<tr>
								<td class="footer-center" style="color:#877d76; font-family:Georgia; font-size:11px; line-height:20px; text-align:center">
									<div style="font-size:0pt; line-height:0pt; height:12px"><img src="https://www.proppick.com/images/empty.gif" width="1" height="12" style="height:12px" alt="" /></div>
									<a style="color:#fff;text-decoration:none;font-family:calibri;font-size: 14px;" href="https://www.proppick.com/" target="_blank">© Copyright 2016 Proppick.com © All Copy Rights Reserved.</a>
									<div style="font-size:0pt; line-height:0pt; height:12px"><img src="https://www.proppick.com/images/empty.gif" width="1" height="12" style="height:12px" alt="" /></div>
								</td>
							</tr>
						</table>
						<!-- END Footer -->
					</td>
					
                
		</td>
	</tr>
    </tbody>
</table>
</body>
</html>';

    $emailCustomerSubject = 'Requirement Received';
	$emailCUstomerMesg = $msg;
	$headers1 = "MIME-Version: 1.0" . "\r\n";
    $headers1 .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    $headers1 .= 'From: customer.service@proppick.com' . "\r\n";
	$sendMailToCustomer = @mail($emailTo, $emailCustomerSubject, $emailCUstomerMesg, $headers1);  
    $headers = "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
    $text2=urlencode("Dear ".$userName.", \r\nThank you for register bostonbook.com.You can explore more properties at https://www.bostonbook.com/");
    $user= $this->smsgateway['user'];
		  $pw=$this->smsgateway['pw'];
		  $api_id=$this->smsgateway['api_id']; 
		  $senderId=$this->smsgateway['sneder_id'];
               //here generate random value 
        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
			   
			   //$enqName=$this->session->userdata('EnqName');
               //$text = "Hi $userName, $otp is your One Time Password for bostonbook.com. Please use this password to complete your phone number verification.");
       // auth call
              $url = "http://tra.bulksmshyderabad.co.in/websms/sendsms.aspx?userid=$user&password=$pw&sender=$senderId&mobileno=$userPhoneNo&msg=$text2";
          if($url){
          $ret = file($url);
    // explode our response. return string is on first line of the data returned
    $sess = explode(":",$ret[0]);
    if ($sess[0] == "OK") {
 
        $sess_id = trim($sess[1]); // remove any whitespace
        $url = "$baseurl/http/sendmsg?session_id=$sess_id&to=$to&text=$text";

        // do sendmsg call
        $ret = file($url);
        $send = explode(":",$ret[0]);
	
  
	

	   
   }
		  }
		 $this->session->set_userdata('userId',$emailTo);
		 $this->session->set_userdata('userName',$userName);
		 $this->session->set_userdata("userPhoneNo",$userPhoneNo);
	     echo  "<script type=\"text/javascript\">
        window.open('http://devef.org/boston/myaccount', '_self')
          </script>";
   }
   else{
	    $this->session->unset_userdata('registeredButNotOtp');
	   $this->session->set_flashdata('otpErrorSignup','Enter correct otp');
	   $this->load->view('user_otp');
   }
}
public function loginfb(){
	
		 $date =date('Y-m-d H:i:s');
		 $PRESENTurl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; 
		
		if((strpos($PRESENTurl, 'access_denied') !== false)){
			echo "<script>window.close();</script>";
		}

       if($this->facebook->is_authenticated()){			 
        
            $insertdata = $this->facebook->request('get','/me?fields=id,picture,first_name,last_name,email,gender,locale');
            $userData['email']=$insertdata['email'];	
			
			$this->session->set_userdata('user_logged_in',$userData['email']);
			$a=$this->Home_model->fb_checking();
            if($a==true)
			{
				$userData['first_name']=$insertdata['first_name'];
				$userData['last_name']=$insertdata['last_name'];
				$userData['fb_picture_url'] =$insertdata['picture']['data']['url'];
				$userData['modified'] =$date;

				$this->session->set_userdata("facebook","facebook");
				
				$userId=$this ->Home_model->user_data_update($userData);
				$this->session->set_userdata('userSno',$userId);
				$this->session->set_userdata('userPhoneNo',$userId->phone_number);
				$this->session->set_userdata('userId',$userData['email']);
				$this->session->set_userdata('userName',$insertdata['first_name']);
				$this->session->set_userdata('userSno',$userId->id);
				 redirect("myaccount");
			} 
            else{
					$userData['first_name']=$insertdata['first_name'];
					$userData['last_name']=$insertdata['last_name'];
					$userData['email'] = $insertdata['email'];
					$userData['fb_picture_url'] =$insertdata['picture']['data']['url'];
					$userData['created'] =$date;
					$this->session->set_userdata("facebook","facebook");
					$userId=$this ->Home_model->add_user($userData);
					$this->session->set_userdata('userSno',$userId);
					//}
					
					$this->session->set_userdata('userId',$userData['email']);
					$this->session->set_userdata('userName',$userData['first_name']);
					redirect("myaccount");
			}
			
			
        }

     else{
				 $data['login_url1'] =  $this->facebook->login_url();
                // permissions here        
         }
        
}
public function logout(){
	  
	  $userLogout = $this->session->unset_userdata("userId");
	  $userLogout2 = $this->session->unset_userdata("userName");
	  $userLogout2 = $this->session->unset_userdata("gmail");
	  $userLogout2 = $this->session->unset_userdata("facebook");
	  $userLogout2 = $this->session->unset_userdata("normal");
	  $userLogout2 = $this->session->unset_userdata("userSno");
	  $userLogout2 = $this->session->unset_userdata("userPhoneNo");
	  $this->session->sess_destroy();
	  redirect("home/login");
}
public function myaccount(){
	    
		
		if($this->session->userdata('userId')){
			
			
			$data['myaccountData']=$this->Home_model->get_user_details();
		
			if($this->session->userdata('payemnt_url')){
				
				redirect("payment");
			}
			else{
			
				$this->load->view("myaccount",$data);
			}
		}
		else{
			 redirect("home/login");
		}
}
public function user_info_update(){
	
	   
		$data['first_name']=$this->input->post('first_name');
		$data['last_name']=$this->input->post('last_name');
		$data['phone_number']=$this->input->post('phone_number');
		$data['address_line1']=$this->input->post('address_line1');
		$data['address_line2']=$this->input->post('address_line2');
		$data['city']=$this->input->post('city');
		$data['state']=$this->input->post('state');
		$data['pincode']=$this->input->post('pincode');
		$data['modified']=$this->input->post('modified');
		
		$updateUserData = $this->Home_model->update_user_data($data);
		if($updateUserData==1){
			
			redirect("myaccount");
		}
		else{
			redirect("accountdetails");
		}
	
}
public function orders(){
	    
		if($this->session->userdata('userId')){

		$data['myaccountData']=$this->Home_model->get_user_details();
		$data['UserOrdersInfo']=$this->Home_model->orders();
		$this->load->view("orders",$data);
		}
		else{
			redirect("home/login");
		}
}
public function order_view(){
	if($this->session->userdata('userId')){

		$data['myaccountData']=$this->Home_model->get_user_details();
		$order_id_info = $this->Home_model->get_order_info();
		if(count($order_id_info)) {
			$data['order_info'] = $order_id_info;
			$this->load->view('order_view', $data);
		} else {
			redirect("home/login");
		}
	} else {
		redirect("home/login");
	}
}
public function addresses(){
	
		if($this->session->userdata('userId')){

		
		$data['myaccountData']=$this->Home_model->get_user_details();
		
		$this->load->view("addresses",$data);
		}
		else{
			redirect("home/login");
		}
}
public function downloads(){
	
		if($this->session->userdata('userId')){
		

		$data['myaccountData']=$this->Home_model->get_user_details();
		
		$this->load->view("downloads",$data);
		}
		else{
			redirect("home/login");
		}
}
public function accountdetails(){
	
		if($this->session->userdata('userId')){
        			
		$data['myaccountData']=$this->Home_model->get_user_details();
		
		$this->load->view("accountdetails",$data);
		}
		else{
			redirect("home/login");
		}
}
public function productview(){
	
	   $main_menu = $this->Home_model->total_menu();
		$m=-1;
		foreach ($main_menu as $main_menu0) { $m++;
			$menu_id = $main_menu0['menu_sno'];
			$categories = $this->Home_model->menu_categories($menu_id);
			$main_menu[$m]['categories'] = $categories;
			$c= -1;
			foreach ($categories as $categories0) { $c++;
				$cat_id = $categories0['cat_sno'];
				$subcategories = $this->Home_model->cat_subcategories($cat_id);
				$main_menu[$m]['categories'][$c]['subcategories'] = $subcategories;
			}
		}
		$data['main_mega_menu'] = $main_menu;
		$data['myaccountData']=$this->Home_model->get_user_details();
		
		$this->load->view("productview",$data);
		
}
public function search(){
	
	//$data=$this->data;
		 $main_menu = $this->Home_model->total_menu();
			$m=-1;
			foreach ($main_menu as $main_menu0) { $m++;
				$menu_id = $main_menu0['menu_sno'];
				$categories = $this->Home_model->menu_categories($menu_id);
				$main_menu[$m]['categories'] = $categories;
				$c= -1;
				foreach ($categories as $categories0) { $c++;
				
					$cat_id = $categories0['cat_sno'];
					$subcategories = $this->Home_model->cat_subcategories($cat_id);
					$main_menu[$m]['categories'][$c]['subcategories'] = $subcategories;
				}
			}
		$data['main_mega_menu'] = $main_menu;
		$data['myaccountData']=$this->Home_model->get_user_details();
		$catgory = str_replace("%20"," ",$this->uri->segment(1));
		
		$data['get_best_products']=$this->Home_model->get_search_products($catgory);
		$data['get_search_subcatgories']=$this->Home_model->get_search_subcatgories($catgory);
		$data['get_search_brands']=$this->Home_model->get_search_brands($catgory);
		if($this->input->post('priceLow')!=""){
		$this->load->view("ajax/price_range",$data);
		}
		else if($this->input->post('sorting')!=""){
			$this->load->view("ajax/price_range",$data);
		}
		else{
			$this->load->view("category",$data);
		}
	
}
public function search1(){
	
	echo "hi";
	
}

public function payment(){
	
	$data['myaccountData']=$this->Home_model->get_user_details();
	$this->load->view("payment");
	
}
public function payment1(){
	
	$data['myaccountData']=$this->Home_model->get_user_details();
	$this->load->view("payment");
	
}
function changepassword(){
	
	$data['myaccountData']=$this->Home_model->get_user_details();
	$clientId = '1058966338695-0340gem5u3p9tt3qp5e7014jn9h38l2s.apps.googleusercontent.com';
        $clientSecret = '5Qc_6yoUk4Kb85iiNmRSAqv3';
        $redirectUrl = base_url()."home/google_login";
		$data['login_url1'] =  $this->facebook->login_url();
		$gClient = new Google_Client();
        $gClient->setApplicationName('Login in to Bostonbooks');
        $gClient->setClientId($clientId);
        $gClient->setClientSecret($clientSecret);
        $gClient->setRedirectUri($redirectUrl);
        $google_oauthV2 = new Google_Oauth2Service($gClient);

        if (isset($_REQUEST['code'])) {
            $gClient->authenticate();
            $this->session->set_userdata('token', $gClient->getAccessToken());
            redirect($redirectUrl);
        }

        $token = $this->session->userdata('token');
        if (!empty($token)) {
            $gClient->setAccessToken($token);
        }
		 $data['google_login_url'] = $gClient->createAuthUrl();
		$this->load->view('changepassword', $data);
}

function oldview(){
	$main_menu = $this->Home_model->total_menu();
		$m=-1;
		foreach ($main_menu as $main_menu0) { $m++;
			$menu_id = $main_menu0['menu_sno'];
			$categories = $this->Home_model->menu_categories($menu_id);
			$main_menu[$m]['categories'] = $categories;
			$c= -1;
			foreach ($categories as $categories0) { $c++;
				$cat_id = $categories0['cat_sno'];
				$subcategories = $this->Home_model->cat_subcategories($cat_id);
				$main_menu[$m]['categories'][$c]['subcategories'] = $subcategories;
			}
		}
		$data['main_mega_menu'] = $main_menu;
	$this->load->view('productview1', $data);
}

function newproduct(){
	
	$main_menu = $this->Home_model->total_menu();
		$m=-1;
		foreach ($main_menu as $main_menu0) { $m++;
			$menu_id = $main_menu0['menu_sno'];
			$categories = $this->Home_model->menu_categories($menu_id);
			$main_menu[$m]['categories'] = $categories;
			$c= -1;
			foreach ($categories as $categories0) { $c++;
				$cat_id = $categories0['cat_sno'];
				$subcategories = $this->Home_model->cat_subcategories($cat_id);
				$main_menu[$m]['categories'][$c]['subcategories'] = $subcategories;
			}
		}
		$data['main_mega_menu'] = $main_menu;
	$this->load->view('newproduct', $data);
}
function javascriptenable(){
	
	$this->load->view('nonjavas');
}
}
