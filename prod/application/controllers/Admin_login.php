<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_login extends CI_Controller {
	public function __Construct(){
        parent::__Construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->library('upload');
        $this->load->model('Admin_model');
        $this->load->library('form_validation');
		 
        $this->load->library('session');
		if($this->session->userdata('admin_id'))
		{
			//$this->session->unset_userdata('admin_id');
			redirect("admin");
		}
		error_reporting(0);
    }
	public function index()	{
	    
	   
	    
	    
	    
		
		$this->load->view('admin/login');
	}
	public function login_submit(){
		
		$this->session->unset_userdata('error_message');
		$username = $this->input->post('username');
		$pwd = $this->input->post('password');
		if($username != '' && $pwd != '')
	    {
		   $result = $this->Admin_model->login($username,$pwd);
		   if($result > 0)
		   {
			 $sess_array = array();
			   $this->session->set_userdata('admin_id',$result->admin_no);
			   $this->session->set_userdata('admin_type_id',$result->admin_type_id);
			   redirect('admin','refresh');
		   }
		   else
		   {
			 $this->session->set_userdata('error_message', 'Invalid username or password');
			 redirect('admin_login');
		   }
	   }
	   else
	   {
	   	$this->session->set_userdata('error_message', 'Plase enter username and password');
		 redirect('admin_login');
	   }
	}



}











