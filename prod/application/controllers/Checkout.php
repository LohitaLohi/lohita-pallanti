<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
public function __Construct(){
        parent::__Construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('html');
		$this->load->database();
         $this->load->library('email');
	   $this->load->library('form_validation');
	   $this->load->library('facebook');
		 
		 error_reporting(0);
}
public function index(){
		
		
		
		if($this->session->userdata('userId'))
		{
		   $this->load->view('Checkout');
		}
		else{
			
			$this->session->set_userdata("check_url",$this->uri->segment(1));
			redirect("user-login",'refresh');
	 	}
}
public function checkout_add(){
	 redirect('Checkout');
 }



 public function get_password(){
     
     $email = $this->input->post('forget_password');
     
     $get_row = $this->db->query("select * from users where email = '".$email."'")->row();
     
       $full_name = $get_row->full_name;
       
       $email = $get_row->email;
     
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    
       $img_url = base_url()."assets/frontend/images/wines/logo.jpg";    
                    
    
$shuffled = str_shuffle($characters);
 
    $shuff = md5($shuffled);
     
       if(!empty($get_row)){
           
           $data['user_id'] = $get_row->id;
           $data['random'] =  $shuff;
           
           $this->db->insert('forgot_pass',$data);
           
           
           
           $url = base_url().'Checkout/change_password/'.$shuff;
           
            
            $message = '<html>
				<body>
				<table    width="800px;">
				<tr style="background-color:black;">
				<th> <img style="float:left;"height="80px;" width="80px;"  src="'.$img_url.'">  </th>
				</tr>
				<tr>
				<td>
				<br>
				<br>

				HI '.$full_name.'
				<br>
				<br>
			Click the below link to change the password 
			
				<br>
				<br>
				 
				 '.$url.'
				 
				</td>
				</tr>
				</table>
				</body>
				</html>';

     	$config['mailtype'] = 'html';
		$this->email->initialize($config);

		$this->email->to($email);
		$this->email->from('venkatp09@gmail.com','Forgot Password');
		$this->email->subject('Delivered - Change Password');
		$this->email->message($message);
		$this->email->send();                        
            
        echo "success";exit;
            
          
        }else{
          echo "invalid";exit;  
        }
      
     
   }



public function change_password($rand){
    
    
     
    
    
    		if($this->input->post()){
    		    
    		   if(!empty($rand)){
    		       
                    
    		      
    		  $get_rand = $this->db->query("select * from forgot_pass where random='$rand'")->row();
    		     
    		      
    		     
    		   if(!empty($get_rand)){
    		   
    		    
		       $user_id = $this->session->userdata('userId');
		   $password = md5($this->input->post('new_password')); 
		    $change_array = array('password'=>$password);
		    $this->db->where('id',$get_rand->user_id);
		    $this->db->update('users',$change_array);
		    
		      
		   
		    $data['success_msg'] = "Your Password is changed successfully";
		 
	 	
    		   }else{
    		       
    		        $data['err_msg'] = "Your Password is alredy Changed";
    		   }		       
    		       
    		   }
	 	
	 	
    
    		} 
    
    
    
      $this->load->view('forgot_pass',$data);
    
    
    
}
 



public function add_user_address(){
    
    
    if($this->input->post()) {
    
      
        $check_id = $this->input->post("check_address");
        $data['confirm_pincode_name']=$this->input->post("confirm_pincode_name");
        $data['confirm_name']=$this->input->post("confirm_name");
        $data['confirm_address']=$this->input->post("confirm_address");
        $data['confirm_landmark']=$this->input->post("confirm_landmark");
        $data['confirm_city']=$this->input->post("confirm_city");
        $data['confirm_state']=$this->input->post("confirm_state");
        $data['confirm_mobile_number']=$this->input->post("confirm_mobile_number");
        $data['confirm_address_type']=$this->input->post("confirm_address_type");  

	  
	$this->load->model('Home_model');
	
    $data['user_id'] = $this->session->userdata("userId");
    
    
    if(!empty($check_id)){
     $add_address_id=$this->Home_model->myaccount_update($data,$check_id);
     
         $this->db->where('con_no !=', $add_address_id);
         $this->db->where('user_id', $data['user_id']);
         
         $this->db->delete('checkout_address');
     
    }else{
 	$add_address_id=$this->Home_model->myaccount_add($data);
 	
 	
 	     $this->db->where('con_no !=', $add_address_id);
         $this->db->where('user_id', $data['user_id']);
         
         $this->db->delete('checkout_address');
         
 	
    }
    
if($add_address_id){
   echo "sucess";exit;
 }else{
     echo "fail";exit;
 }
    
    
    
}

}


public function payment(){

    
    
	$url=$this->uri->segment(1);
	$this->session->set_userdata("payemnt_url",$url);	
   
	if($this->session->userdata("userSno")){
		
		if($this->session->userdata("userPhoneNo") && $this->session->userdata("grand_total")){
			if($this->session->userdata("grand_total")){
				
		        $data['myaccountData']=$this->Home_model->get_user_details();
		        $data['cart_details']=$this->Detailed_model->get_cart_details();
				$this->session->set_userdata("order_confirm2","order_confirm");		
				$this->session->unset_userdata("payemnt_url",$data);				
				$this->load->view("payment",$data);
				
		}
	  else{
	       redirect("home");
	 }
		}
	 else{
	       redirect("home/user_otp_payment");
	 }
	}
	else{
		
		redirect("login");
	
	}
	
}


}
