<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Myaccount_address extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
public function __Construct(){
        parent::__Construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('html');
		   $this->load->database();
 error_reporting(0);
 $this->load->library('email');
 $this->load->library('session');
       
	   $this->load->library('form_validation');
	   $this->load->library('facebook');
		date_default_timezone_set("Asia/Kolkata");
		
	    
		//error_reporting(0);
	   
		
}
public function index(){
		
		
		/*$data['myaccountData']=$this->Home_model->get_user_details();
		$data['products']=$this->Home_model->products();	*/	
		$this->load->view('myaccount-address');
}
public function myaccount_add(){
	
    $data['confirm_pincode_name']=$this->input->post("confirm_pincode_name");
	$data['confirm_name']=$this->input->post("confirm_name");
	$data['confirm_address']=$this->input->post("confirm_address");
	$data['confirm_landmark']=$this->input->post("confirm_landmark");
	$data['confirm_city']=$this->input->post("confirm_city");
	$data['confirm_state']=$this->input->post("confirm_state");
	$data['confirm_mobile_number']=$this->input->post("confirm_mobile_number");
	$data['confirm_address_type']=$this->input->post("confirm_address_type");
 
 
	$a=$this->input->post("address");
	$address=implode(",",$a);
	$this->load->model('Home_model');
	
    $data['user_id'] = $this->session->userdata("userId");
 	$add_address=$this->Home_model->myaccount_add($data);
 
if($add_address){
   $this->session->set_flashdata('msg', 'Your Address is Added successfully');
 }
	
	redirect('Myaccount_address');
	
	
	
}
public function myaccount_view(){
	$this->load->model("Home_model");
	$data['myaccount_data']=$this->Home_model->myaccount_view();
	
	
	
	$this->load->view("myaccount-address",$data);
		
}

public function prod_review(){
    
 
   echo "<pre>";
   print_r($this->input->post());
   exit;
    
    
    
    
    
    
}



public function myaccount_edit(){
	$edit=$this->uri->segment(3);
	$this->load->model('Home_model');
	$data['myaccount_data']=$this->Home_model->myaccount_edit($edit);
	$this->load->view("myaccount-address",$data);
	
}
public function myaccount_editid(){
	$this->load->model("Home_model");
	$editid=$this->uri->segment(3);
	$data['confirm_pincode_name']=$this->input->post("confirm_pincode_name");
	$data['confirm_name']=$this->input->post("confirm_name");
	$data['confirm_address']=$this->input->post("confirm_address");
	$data['confirm_landmark']=$this->input->post("confirm_landmark");
	$data['confirm_city']=$this->input->post("confirm_city");
	$data['confirm_state']=$this->input->post("confirm_state");
	$data['confirm_mobile_number']=$this->input->post("confirm_mobile_number");
	$data['confirm_address_type']=$this->input->post("confirm_address_type");
	
	$data['user_id'] = $this->session->userdata("userId");
	
 
	
     $add_address = $this->Home_model->myaccount_editid($data,$editid);

if($add_address){
$this->session->set_flashdata('msg', 'Your Address is updated successfully');
}
	redirect("myaccount_view");
	
	
}
public function myaccountaddress_delete(){
	$editid=$this->uri->segment(3);
	$this->load->model("Home_model");
	$data['myaccount_data']=$this->Home_model->myaccountaddress_delete($editid);
	redirect("myaccount_view");
	
}

}
