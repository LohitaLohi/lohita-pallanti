<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
public function __Construct(){
        parent::__Construct();
		
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('html');
		
       
	   $this->load->library('form_validation');
	   $this->load->library('facebook');
		 
		 error_reporting(0);
		
}
public function index(){
		
		
		if($this->session->userdata('userSno')){
		
		$orderId['order_id']=$this->session->userdata('userSno');
		$this->Detailed_model->update_to_cart_session($orderId); 
		$this->session->unset_userdata("cart_order_no");
	}
	
    if($this->input->post('quantity')!=""){
	 if($this->session->userdata("cart_order_no") || $this->session->userdata('userSno')){
		 
		  if($this->session->userdata('userSno')){
				  $orderId=$this->session->userdata('userSno');
			  }else{
			     
				 $this->session->set_userdata("cart_order_no",strtotime(date("Y-m-d")));
				 $orderId= $this->session->userdata('cart_order_no');
			  }
		  $productSno =$this->input->post('product_sno');
		  $sizesId =$this->input->post('size_id');
		  $checkProduct = $this->Detailed_model->check_cart($productSno);
		  $cartdata['product_id']=$this->input->post('product_sno');
		  $cartdata['order_id']=$orderId;
		  $cartdata['size_id']=$sizesId;
		  $cartdata['date']=date("Y-m-d H:i:s");
		  $quanity=$this->input->post('quantity');
		  unset($_POST);
		  if($checkProduct->quanity>0){
			  
			    $cartdataupdate['quanity']=$checkProduct->quanity+$quanity;
			    $this->Detailed_model->update_to_cart($cartdataupdate,$productSno,$sizesId); 
		  
		  }
		  else{
			  
			   $cartdata['quanity']=$quanity;
			   $this->Detailed_model->add_to_cart($cartdata);
		  }
	 }
	 else{
		      if($this->session->userdata('userSno')){
				  $orderId=$this->session->userdata('userSno');
			  }else{
			     
				 $this->session->set_userdata("cart_order_no",strtotime(date("Y-m-d")));
				 $orderId= $this->session->userdata('cart_order_no');
			  }
			  $cartdata['product_id']=$this->input->post('product_sno');
			  
			  $cartdata['quanity']=$this->input->post('quantity');
			  $cartdata['order_id']=$orderId;
			  $cartdata['date']=date("Y-m-d");
		      $ddd=$this->input->post('product_sno');
			  unset($_POST);
			  $this->Detailed_model->add_to_cart($cartdata); 
	    }
	    $data['myaccountData']=$this->Home_model->get_user_details();
		$cart_count=$this->Detailed_model->cart_count();
		$data['cart_details']=$this->Detailed_model->get_cart_details();
		$totalCount = $cart_count->total_quanity;
		$this->session->set_userdata("totalCount",$totalCount);
		redirect("cart");
	}
	else if($this->input->post('cartSno1')!=""){
		 
		
		 $cartSnol=$this->input->post('cartSno1');
		 $a=$this->Detailed_model->delete_cart_item($cartSnol);
		 
   		
	}
	else if($this->input->post('Ajaxquantity')!=""){
		
		 $cartdataupdate['quanity']=$this->input->post('Ajaxquantity');
		 
		 $cartSno=$this->input->post('quanityId');
		 $a=$this->Detailed_model->update_to_cart_ajax($cartdataupdate,$cartSno);
		 if($a){
			 
			 redirect("cart");
		 }
		
	}
	
		$data['myaccountData']=$this->Home_model->get_user_details();
		$cart_count=$this->Detailed_model->cart_count();
		$data['cart_details']=$this->Detailed_model->get_cart_details();
		
		$totalCount = $cart_count->total_quanity;
		
		$this->session->set_userdata("totalCount",$totalCount);
		
			if($this->input->post('cartSno1')!=""){
				
					$data['products']=$this->Detailed_model->get_cart_details();
		            $this->load->view('Cart',$data);
			}
			else if($this->input->post('Ajaxquantity')!=""){
				
					$data['products']=$this->Detailed_model->get_cart_details();
		            $this->load->view('Cart',$data);
			}
			else{
					$data['products']=$this->Detailed_model->get_cart_details();
		            $this->load->view('Cart',$data);
			}
		

}

  public function add_to_cart(){
   
    if($this->session->userdata('userSno')){
		
		$orderId['order_id']=$this->session->userdata('userSno');
		$this->Detailed_model->update_to_cart_session($orderId); 
		$this->session->unset_userdata("cart_order_no");
	}
	
    if($this->input->post('quantity')!=""){
		
	 if($this->session->userdata("cart_order_no") || $this->session->userdata('userSno')){
		 
		  if($this->session->userdata('userSno')){
				  $orderId=$this->session->userdata('userSno');
			  }else{
			     
				 $this->session->set_userdata("cart_order_no",strtotime(date("Y-m-d")));
				 $orderId= $this->session->userdata('cart_order_no');
			  }
		  $productSno =$this->input->post('product_sno');
		  $sizesId =$this->input->post('size_id');
		  $checkProduct = $this->Detailed_model->check_cart($productSno);
		  $cartdata['product_id']=$this->input->post('product_sno');
		  $cartdata['order_id']=$orderId;
		  $cartdata['size_id']=$sizesId;
		  $cartdata['date']=date("Y-m-d H:i:s");
		  $quanity=$this->input->post('quantity');
		  unset($_POST);
		  if($checkProduct->quanity>0){
			  
			    $cartdataupdate['quanity']=$checkProduct->quanity+$quanity;
			    $this->Detailed_model->update_to_cart($cartdataupdate,$productSno,$sizesId); 
		  
		  }
		  else{
			  
			   $cartdata['quanity']=$quanity;
			   $this->Detailed_model->add_to_cart($cartdata);
		  }
	 }
	 else{
		      if($this->session->userdata('userSno')){
				  $orderId=$this->session->userdata('userSno');
			  }else{
			     
				 $this->session->set_userdata("cart_order_no",strtotime(date("Y-m-d")));
				 $orderId= $this->session->userdata('cart_order_no');
			  }
			  $cartdata['product_id']=$this->input->post('product_sno');
			  
			  $cartdata['quanity']=$this->input->post('quantity');
			  $cartdata['order_id']=$orderId;
			  $cartdata['date']=date("Y-m-d");
		      $ddd=$this->input->post('product_sno');
			  unset($_POST);
			  $this->Detailed_model->add_to_cart($cartdata); 
	    }
	    $data['myaccountData']=$this->Home_model->get_user_details();
		$cart_count=$this->Detailed_model->cart_count();
		$data['cart_details']=$this->Detailed_model->get_cart_details();
		$totalCount = $cart_count->total_quanity;
		$this->session->set_userdata("totalCount",$totalCount);
		redirect("cart");
	}
	else if($this->input->post('cartSno1')!=""){
		 
		
		 $cartSnol=$this->input->post('cartSno1');
		 $a=$this->Detailed_model->delete_cart_item($cartSnol);
		 
   		
	}
	else if($this->input->post('Ajaxquantity')!=""){
		
		 $cartdataupdate['quanity']=$this->input->post('Ajaxquantity');
		 
		 $cartSno=$this->input->post('quanityId');
		 $a=$this->Detailed_model->update_to_cart_ajax($cartdataupdate,$cartSno);
		 if($a){
			 
			 redirect("cart");
		 }
		
	}
	
		$data['myaccountData']=$this->Home_model->get_user_details();
		$cart_count=$this->Detailed_model->cart_count();
		$data['cart_details']=$this->Detailed_model->get_cart_details();
		
		$totalCount = $cart_count->total_quanity;
		
		$this->session->set_userdata("totalCount",$totalCount);
		
			if($this->input->post('cartSno1')!=""){
				
					$data['products']=$this->Detailed_model->get_cart_details();
		            $this->load->view('Cart',$data);
			}
			else if($this->input->post('Ajaxquantity')!=""){
				
					$data['products']=$this->Detailed_model->get_cart_details();
		            $this->load->view('Cart',$data);
			}
			else{
					$data['products']=$this->Detailed_model->get_cart_details();
		            $this->load->view('Cart',$data);
			}
		
		
}
     
}



 
    
        
      
      
   
     
     
 
