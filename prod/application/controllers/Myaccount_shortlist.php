<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Myaccount_shortlist extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	 
public function __Construct(){
        parent::__Construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('html');
		
       
	   $this->load->library('form_validation');
	   $this->load->library('facebook');
		date_default_timezone_set("Asia/Kolkata");
		 $this->smsgateway = array(
            'user' => 'prpick',
            'pw' => 'prp@1234',
            'api_id' =>'20150724122053000_15459853',
			'sneder_id'=>'PRPICK'
        );
	    $userDetails=$this->Home_model->get_user_details();
		$this->data=array('userDetails'=>$userDetails);
		//error_reporting(0);
	    include_once APPPATH."libraries/google-api-php-client/Google_Client.php";
		include_once APPPATH."libraries/google-api-php-client/contrib/Google_Oauth2Service.php";
		
		// Google Project API Credentials
		$clientId = '1058966338695-0340gem5u3p9tt3qp5e7014jn9h38l2s.apps.googleusercontent.com';
        $clientSecret = '5Qc_6yoUk4Kb85iiNmRSAqv3';
        $redirectUrl = base_url() . 'user_authentication/';
		
}


  public function delete_prod($prod_id){
        
        $user_id = $this->session->userdata('userId');
        
        $this->db->where('user_id',$user_id);
        $this->db->where('product_id',$prod_id);
        
        if($this->db->delete('user_fav_products')){
            
            $this->session->set_flashdata('delete_fav', 'Your Shortlisted product is deleted successfully');
      redirect('Myaccount_shortlist', 'refresh'); 
        }
        
        
        
    }   
       

public function index(){
    
        $this->load->database();
    
       $user_id = $this->session->userdata('userId');
       $get_products = $this->db->query("select * from user_fav_products where user_id='".$user_id."' group by product_id")->result();
       
        foreach($get_products as $k=>$v){
            
           $product_id[] = $v->product_id;    
        }
    
    
	    $data['get_products']=$this->Home_model->fav_products($product_id); 
	    
	   
	   
		$this->load->view('myaccount-shortlist',$data);
}

}
