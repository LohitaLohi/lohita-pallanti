<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Listing extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
public function __Construct(){
        parent::__Construct();
		
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('html');
		
       
	   $this->load->library('form_validation');
	   $this->load->library('facebook');
		 
		 error_reporting(0);
		
}
public function index(){
     
    
  
     
     $get_cat_name = $this->uri->segment(1);
     $sub_cat_name = $this->uri->segment(2);
     $get_order = $this->uri->segment(4);
     
     
    $data['sub_cat_name'] = $sub_cat_name;
    
     	$data["get_sub_cat"]=$this->Home_model->get_sub_cat();
		$data["get_products"]=$this->Home_model->products($get_cat_name,$sub_cat_name,$get_order);
		$data["get_brands"]=$this->Home_model->get_brands();
		//$data["get_quan_type"]=$this->Home_model->get_quan_type();
	    $this->load->view('products-list',$data);
		
}

public function add_fav_prod($prod_id){
    
    $get_user_id = $this->session->userdata('userId');
    
    $this->db->where('user_id',$get_user_id);
    $this->db->where('product_id',$prod_id);
      
       $this->db->delete('user_fav_products');
    
   if(empty($get_user_id)){
        
       $this->session->set_userdata('fav_product_id',$prod_id);    
         redirect('Login_new');   
    }else{
        
        $user_fav_pro['user_id'] = $get_user_id;
        $user_fav_pro['product_id'] = $prod_id;
        if($this->db->insert('user_fav_products',$user_fav_pro)){
           echo "success";exit;
        }else{
            echo "fail";exit;
        }  
        
     }
    
    
}





public function product_details(){
	
	$this->load->view("product-detail");
}

}
