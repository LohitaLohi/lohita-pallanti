
/*===========     ENTER ONLY NUMBERS     ============*/
function onlyNos(e, t) {
	try {
		if (window.event) {
			var charCode = window.event.keyCode;
		}
		else if (e) {
			var charCode = e.which;
		}
		else { return true; }
		if (charCode > 31 && ((charCode < 48 && charCode != 46 ) || charCode > 57 )) {
			return false;
		}
		return true;
	}
	catch (err) {
		alert(err.Description);
	}
}
/*===========     ENTER ONLY CHARACHERS (EXCEPT NUMBERS)     ============*/
function onlyChar(e, t) {
// alert(window.event.keyCode);
	try {
		if (window.event) {
			var charCode0 = window.event.keyCode;
		}
		else if (e) {
			var charCode0 = e.which;
		}
		else { return true; }
		if (charCode0 > 31 && charCode0 < 58) {
		return false;
		}
		return true;
	}
	catch (err) {
		alert(err.Description);
	}
}

$('.number').blur(function() {
	var fullNo = $(this).val();
	if(fullNo=='')
	{
		$(this).addClass('custom_error');
		return false;
	}
	else
	{
		$(this).removeClass('custom_error');
		return true;
	}
});

$('.custom_required').blur(function() {
	var fieldvalue = $(this).val();
	if(fieldvalue=='')
	{
		$(this).addClass('custom_error');
		return false;
	}
	else
	{
		$(this).removeClass('custom_error');
		return true;
	}
});

$('.email').blur(function() {
	var fullEmail1 = $(".email").val();
	var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	if(fullEmail1=='')
	{
		$(this).addClass('custom_error');
	}
	else if(!(filter.test(fullEmail1)))
	{
	  $(this).addClass('custom_error');
	}
	else
	{
		$(this).removeClass('custom_error');
	}
});
$('.phone').blur(function() {
	var fullNo2 = $(".phone").val();
	if(fullNo2=='' || fullNo2.length!=10)
	{
		  $(this).addClass('custom_error');
	}
	else
	{
		$(this).removeClass('custom_error');
	}
});

function validate_form(){
	$('.custom_form input.custom_required, .custom_form textarea.custom_required, .custom_form select.custom_required').each(function(){
		$value = $(this).val();
		if($value == ''){
			$(this).addClass('custom_error');
			/*
			if($(this).hasClass('this_is_image')){
				//alert('this_is_image ');
				$(this).parents('.input-append').addClass('image_required');
			}
			*/
		}
	});
	
	/*====  CHECKING FOR CUSTOM_ERROS CLASS  ====*/
	$cc=0;
	$('.custom_form .custom_error').each(function(){
		$cc++;
	});
	if($cc){
		return false;
	}
	else{
		return true;
	}
}  